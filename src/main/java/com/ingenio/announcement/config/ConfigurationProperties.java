package com.ingenio.announcement.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
//@PropertySource("file:C:/updatedwars/configuration/announcement.properties")
public class ConfigurationProperties {

	public static  String uploadImageUrl;
	public static  String studentPhotoUrl;
	public static  String staffPhotoUrl;

	public String getUploadImageUrl() {
		return uploadImageUrl;
	}

	@Value("${uploadImageUrl}")
	public void setUploadImageUrl(String uploadImageUrl) {
		ConfigurationProperties.uploadImageUrl = uploadImageUrl;
	}

	@Value("${studentPhotoUrl}")
	public void setStudentPhotoUrl(String studentPhotoUrl) {
		ConfigurationProperties.studentPhotoUrl = studentPhotoUrl;
	}

	@Value("${staffPhotoUrl}")
	public void setStaffPhotoUrl(String staffPhotoUrl) {
		ConfigurationProperties.staffPhotoUrl = staffPhotoUrl;
	}
	
	

}
