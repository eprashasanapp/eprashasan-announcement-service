package com.ingenio.announcement.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.techsync.security.jwt.BaseJwtFilter;
import com.techsync.security.jwt.BaseSecurityConfig;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.techsync.security.jwt")
public class SecurityConfig extends BaseSecurityConfig{

	public SecurityConfig(BaseJwtFilter jwtFilter) {
		super(jwtFilter);
	}

	@Override
    protected List<String> getAdditionalExcludedUrls() {
//        return Arrays.asList("/validateUserPasswordForLogin");
		List<String> urls = new ArrayList<>(super.getAdditionalExcludedUrls());
//		urls.add("/validateUserPasswordForLogin");
		urls.add("/validateOtpForResetPassword");
		urls.add("/submit_announcement_approval_status");
		urls.add("/updateTempPassword");
		return urls;
    }
	
//	@Override
//    protected List<String> getRemovedExcludedUrls() {
////        return Arrays.asList("");
//	List<String> urls = new ArrayList<>(super.getRemovedExcludedUrls());
//	urls.add("");
//	return urls;
//    }
}
