package com.ingenio.announcement.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.techsync.security.jwt.BaseJwtFilter;
import com.techsync.security.jwt.JwtUtil;

@Component
public class JwtFilter extends BaseJwtFilter{

	public JwtFilter(JwtUtil jwtUtil) {
		super(jwtUtil);
	}

	@Override
	protected List<String> getAdditionalExcludedUrls() {
//return Arrays.asList("/validateUserPasswordForLogin");
		List<String> urls = new ArrayList<>(super.getAdditionalExcludedUrls());
//		urls.add("/validateUserPasswordForLogin");
		urls.add("/validateOtpForResetPassword");
		urls.add("/submit_announcement_approval_status");
		urls.add("/updateTempPassword");
		return urls;
	}

//	@Override
//  protected List<String> getRemovedExcludedUrls() {
////return Arrays.asList("");
//	List<String> urls = new ArrayList<>(super.getRemovedExcludedUrls());
//	urls.add("");
//	return urls;
//  }
}
