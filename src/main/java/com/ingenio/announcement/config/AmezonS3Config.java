package com.ingenio.announcement.config;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

@Configuration
public class AmezonS3Config {
//	String accessKey="YTH0BjLydwo9Ewe1G3KdVkZ32wH2sdsYTU8xwgy59+8=";
//	String secretKey="cU4dhFyoPEF+DMEFfUWugBHgt5vJ2uPXEgfchvV9tqussqfzCekiSLgn4OzpegwgI4z49wA6bPk=";

//	String aKey="8C4hmaD8iCda/fonFmSj705Jc8H6QRJn1VdKo4mGvRY=";
//	String sKey="/CaCZxK4U0b0Em2EJ5W1pPXQXiNE5G+b0wo8QlSlB24QR6LYrJNnE0e4zTOZFw6tNgRzZ32q+Eo=";
	String aKey = "wHMdOURIKigFJWimH0FnzQDBd5AFD0vQGhalX5bkkrs=";
	String sKey = "wHBr4naZdPlLfHDx5FHeSXV9imFtCHc7p7dckqPWAE/vmpdz7szSUzimjrhXvzM5xJ3pkh/R+9k=";
	String endPoint="s3.ap-south-1.amazonaws.com";
    
    

    @Bean()
    public AmazonS3 generateS3Client() {
    	Regions clientRegion = Regions.AP_SOUTHEAST_1;
        AWSCredentials credentials = new BasicAWSCredentials(getKey(aKey),getKey(sKey));
    	//AWSCredentials credentials = new BasicAWSCredentials(aKey,sKey);
       
      
        AmazonS3 client = new AmazonS3Client(credentials).withRegion(Region.getRegion(clientRegion)).withEndpoint(endPoint);
        return client;
    }
    
    public String getKey(String key){
		 PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
	    SimpleStringPBEConfig config = new SimpleStringPBEConfig();
	    config.setPassword("password");
	    config.setAlgorithm("PBEWithMD5AndDES");
	    config.setKeyObtentionIterations("1000");
	    config.setPoolSize("1");
	    config.setProviderName("SunJCE");
	    config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
	    config.setStringOutputType("base64");
	    encryptor.setConfig(config);
	    String decrypt=encryptor.decrypt(key);
	   
	    return decrypt;
	}
}
