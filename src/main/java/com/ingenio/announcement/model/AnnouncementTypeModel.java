package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "announcement_type")
public class AnnouncementTypeModel {

	private Integer announcementTypeId;
	private String announcementTypeName;
	private SchoolMasterModel schoolMasterModel;
 


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "announcementTypeId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementTypeId() {
		return announcementTypeId;
	}

	public void setAnnouncementTypeId(Integer announcementTypeId) {
		this.announcementTypeId = announcementTypeId;
	}

	@Column(name="announcementTypeName",nullable = false, length = 50)
	public String getAnnouncementTypeName() {
		return announcementTypeName;
	}

	public void setAnnouncementTypeName(String announcementTypeName) {
		this.announcementTypeName = announcementTypeName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public AnnouncementTypeModel(Integer announcementTypeId, String announcementTypeName) {
		super();
		this.announcementTypeId = announcementTypeId;
		this.announcementTypeName = announcementTypeName;
	}

	public AnnouncementTypeModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
	
	