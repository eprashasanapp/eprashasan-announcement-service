package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "announcement_approvalStatus")
public class AnnouncementApprovalStatusModel {

	private Integer announcementapprovalStatusId;
	private AnnouncementModel announcementModel;
	private AppUserModel appUserModel;
	private SchoolMasterModel schoolMasterModel;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String status;
	private String remark;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "announcementapprovalStatusId", nullable = false, columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementapprovalStatusId() {
		return announcementapprovalStatusId;
	}

	public void setAnnouncementapprovalStatusId(Integer announcementapprovalStatusId) {
		this.announcementapprovalStatusId = announcementapprovalStatusId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId", nullable = false)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}

	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appUserId", nullable = false)
	public AppUserModel getAppUserModel() {
		return appUserModel;
	}

	public void setAppUserModel(AppUserModel appUserModel) {
		this.appUserModel = appUserModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name="remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
