package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="save_send_notification")
public class SaveSendNotificationModel {

	private Integer tabId;
	private Integer schoolId;
	private Integer renewStaffId;
	private String typeFlag;
	private String message;
	private String title;
	private Integer menuId;
	private String messageType;
	private String date;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private String mobileNo;
	private Integer notificationId;
	private Integer seenUnseenStatusFlag=0;
	private String successFailureFlag;
	private String url;

	private Date compareDate= new Date();
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tabId", unique = true, nullable = false)
	public Integer getTabId() {
		return tabId;
	}
	public void setTabId(Integer tabId) {
		this.tabId = tabId;
	}
	
	@Column(name="schoolid")
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	
	@Column(name="renewId_staffId")
	public Integer getRenewStaffId() {
		return renewStaffId;
	}
	public void setRenewStaffId(Integer renewStaffId) {
		this.renewStaffId = renewStaffId;
	}
	
	@Column(name="typeFlag")
	public String getTypeFlag() {
		return typeFlag;
	}
	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}
	
	@Column(name="message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name="messageType")
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	@Column(name="date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="mobileNo")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	@Column(name="notificationId")
	public Integer getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}
	
	@Column(name="seenUnseenStatusFlag")
	public Integer getSeenUnseenStatusFlag() {
		return seenUnseenStatusFlag;
	}
	public void setSeenUnseenStatusFlag(Integer seenUnseenStatusFlag) {
		this.seenUnseenStatusFlag = seenUnseenStatusFlag;
	}
	
	@Column(name="successFailureFlag")
	public String getSuccessFailureFlag() {
		return successFailureFlag;
	}
	public void setSuccessFailureFlag(String successFailureFlag) {
		this.successFailureFlag = successFailureFlag;
	}
	
	@Column(name="title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name="menuId")
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	
	@Transient
	public Date getCompareDate() {
		return compareDate;
	}
	public void setCompareDate(Date compareDate) {
		this.compareDate = compareDate;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
	
}
