package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity

@Table(name = "event_type_master")
public class EventTypeMasterModel {

	private Integer eventTypeId;
	private String eventType;
	private String eventTypeColor;
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "event_type_id", unique = true, nullable = false)
	public Integer getEventTypeId() {
		return eventTypeId;
	}
	public void setEventTypeId(Integer eventTypeId) {
		this.eventTypeId = eventTypeId;
	}
	
	@Column(name = "event_type")
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	@Column(name = "event_color")
	public String getEventTypeColor() {
		return eventTypeColor;
	}
	public void setEventTypeColor(String eventTypeColor) {
		this.eventTypeColor = eventTypeColor;
	}
	
	
	}