package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity

@Table(name="fee_bouncecharges")
public class FeeBounceChargesModel{
	
	private Integer bounceId;
	private FeeReceiptModel feeReceiptModel;
	private String bounceChargesPaidReceiptNo;
	private Double bounceCharges;
	private Double bouncePaidCharges;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private AppUserRoleModel appUserRole;
	private String isDel="0";
	private String isEdit="0";
	private String sinkingFlag="0";
	private SchoolMasterModel schoolMasterModel;
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	
	public FeeBounceChargesModel(){
		super();
	}

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "bounceId", nullable = false)
	public Integer getBounceId() {
		return bounceId;
	}
	public void setBounceId(Integer bounceId) {
		this.bounceId = bounceId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptID")
	public FeeReceiptModel getFeeReceiptModel() {
		return feeReceiptModel;
	}

	public void setFeeReceiptModel(FeeReceiptModel feeReceiptModel) {
		this.feeReceiptModel = feeReceiptModel;
	}
	
	@Column(name="bounceChargesPaidReceiptNo")
	public String getBounceChargesPaidReceiptNo() {
		return bounceChargesPaidReceiptNo;
	}

	public void setBounceChargesPaidReceiptNo(String bounceChargesPaidReceiptNo) {
		this.bounceChargesPaidReceiptNo = bounceChargesPaidReceiptNo;
	}
	
	@Column(name="bounceCharges", length=11, columnDefinition="default '0' ")
	public Double getBounceCharges() {
		return bounceCharges;
	}

	public void setBounceCharges(Double bounceCharges) {
		this.bounceCharges = bounceCharges;
	}

	@Column(name="bouncePaidCharges", length=11,  columnDefinition="default '0' ")
	public Double getBouncePaidCharges() {
		return bouncePaidCharges;
	}

	public void setBouncePaidCharges(Double bouncePaidCharges) {
		this.bouncePaidCharges = bouncePaidCharges;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate(){
		return cDate;
	}

	public void setcDate(Date cDate){
		this.cDate = cDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel(){
		return this.appUserRole;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRole){
		this.appUserRole = appUserRole;
	}

	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel(){
		return isDel;
	}

	public void setIsDel(String isDel){
		this.isDel = isDel;
	}
	
	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit(){
		return isEdit;
	}

	public void setIsEdit(String isEdit){
		this.isEdit = isEdit;
	}
	
	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag(){
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag){
		this.sinkingFlag = sinkingFlag;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "deleteBy", nullable = false)
	@JoinColumn(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "editBy", nullable = false)
	@JoinColumn(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "approvalBy", nullable = false)
	@JoinColumn(name = "approvalBy")
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

}
