package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "app_user")

public class AppUserModel {
	private static final long serialVersionUID = -9004909181952908073L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "APPUSERROLEID", nullable = false)
	private Integer appUserRoleId;
	
	@Column(name = "USERNAME", nullable = false)
	private String username;
	
	@Column(name = "PASSWORD", nullable = false)
	private String password;
	
	@Column(name = "FIRST_NAME", nullable = false)
	private String firstName;
	
	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;
	
	@Column(name = "Question1", length = 500)
	private String question1;
	
	@Column(name = "Answer1", length = 500)
	private String answer1;
	
	@Column(name = "Question2", length = 500)
	private String question2;
	
	@Column(name = "Answer2", length = 500)
	private String answer2;
	
	@Column(name = "settingString", length = 50)
	private String settingString;
	
	@Column(name = "subAdmin", length = 50)
	private String subAdmin;
	
	@Column(name = "Activatedby", length = 500)
	private String activatedby;
	
	@Column(name = "Remark", length = 500)
	private String remark;
	
	@Column(name = "staffID", length = 500)
	private Integer staffId;
	
	private Integer schoolid;
	
	@Column(name = "roleName")
	private String roleName;
	
	@Column(name = "userID")
	private AppUserRoleModel userId;
	
	
	/*@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)*/
	@Column(name = "cDate", updatable = false)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name = "isDel", columnDefinition = "default '0'")
	private String isDel = "0";
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "deleteBy")
	private AppUserRoleModel deleteBy;
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	private String isEdit = "0";
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "editBy")
	private AppUserRoleModel editBy;
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	private String deviceType = "0";
	
	@Column(name = "ipAddress", length = 100)
	private String ipAddress;
	
	@Column(name = "macAddress", length = 50)
	private String macAddress;
	
	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	private String sinkingFlag = "0";
	
	@Column(name = "activeFlag", columnDefinition = "default '0'")
	private String activeFlag = "0";
	
	@Column(name = "IMEICode", columnDefinition = "default '0'")
	private String IMEICode = "0";
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "APP_USER_ROLE", joinColumns = @JoinColumn(name = "USERNAME"), inverseJoinColumns = @JoinColumn(name = "ROLENAME"))
	private Collection<AppRoleModel> roles;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getQuestion1() {
		return question1;
	}
	
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}
	
	public String getAnswer1() {
		return answer1;
	}
	
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}
	
	public String getQuestion2() {
		return question2;
	}
	
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}
	
	public String getAnswer2() {
		return answer2;
	}
	
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}
	
	public String getSettingString() {
		return settingString;
	}
	
	public void setSettingString(String settingString) {
		this.settingString = settingString;
	}
	
	public String getSubAdmin() {
		return subAdmin;
	}
	
	public void setSubAdmin(String subAdmin) {
		this.subAdmin = subAdmin;
	}
	
	public String getActivatedby() {
		return activatedby;
	}
	
	public void setActivatedby(String activatedby) {
		this.activatedby = activatedby;
	}
	
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Integer getStaffId() {
		return staffId;
	}
	
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	
	
	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	public Collection<AppRoleModel> getRoles() {
		return roles;
	}
	
	public void setRoles(Collection<AppRoleModel> roles) {
		this.roles = roles;
	}

	public Integer getAppUserRoleId() {
		return appUserRoleId;
	}

	public void setAppUserRoleId(Integer appUserRoleId) {
		this.appUserRoleId = appUserRoleId;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getIMEICode() {
		return IMEICode;
	}

	public void setIMEICode(String iMEICode) {
		IMEICode = iMEICode;
	}

	public AppUserModel(Integer appUserRoleId, String firstName, String lastName,Integer staffId,String roleName) {
		super();
		this.appUserRoleId = appUserRoleId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.staffId=staffId;
		this.roleName=roleName;
	}

	public AppUserModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "schoolid", updatable= false)	
//
//	public SchoolMasterModel getSchoolMasterModel() {
//		return schoolMasterModel;
//	}
//
//	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
//		this.schoolMasterModel = schoolMasterModel;
//	}

	
}