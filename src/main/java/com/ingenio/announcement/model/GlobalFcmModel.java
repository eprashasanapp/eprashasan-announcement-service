package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "global_fcmKey")
public class GlobalFcmModel {

	private Integer fcmKeyId;
	private String fcm_key;
	
	@Id
	@Column(name = "fcmKeyId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getFcmKeyId() {
		return fcmKeyId;
	}
	
	public void setFcmKeyId(Integer fcmKeyId) {
		this.fcmKeyId = fcmKeyId;
	}

	@Column(name="fcm_key")
	public String getFcm_key() {
		return fcm_key;
	}

	public void setFcm_key(String fcm_key) {
		this.fcm_key = fcm_key;
	}

	

}
