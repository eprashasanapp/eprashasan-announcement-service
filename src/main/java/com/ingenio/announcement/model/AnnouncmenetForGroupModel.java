package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcement_for_group")
public class AnnouncmenetForGroupModel {

	private Integer announcementForGroupId;
	private AnnouncementModel announcementModel;
	private AnnouncementGroupModel announcementGroupModel;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "announcementForGroupId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementForGroupId() {
		return announcementForGroupId;
	}
	public void setAnnouncementForGroupId(Integer announcementForGroupId) {
		this.announcementForGroupId = announcementForGroupId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="announcementId")
	@ElementCollection(targetClass=AnnouncementModel.class)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}
	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementGroupId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="announcementGroupId")
	@ElementCollection(targetClass=AnnouncmenetForGroupModel.class)
	public AnnouncementGroupModel getAnnouncementGroupModel() {
		return announcementGroupModel;
	}
	public void setAnnouncementGroupModel(AnnouncementGroupModel announcementGroupModel) {
		this.announcementGroupModel = announcementGroupModel;
	}
	
	

}
