package com.ingenio.announcement.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "android_notification_status")

public class AndroidNotificationStatusModel {

	private Integer notificationStatusId;
	private SchoolMasterModel schoolMasterModel;
	private String joinTable = "";
	private String notificationType;
	private String joinId;
	private String title;
	private String message;
	private Integer status;
	private Date notificationDate;
	private String notificationTime;
	private Integer count;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String sinkingFlag = "0";

	@Id
	@Column(name = "notificationStatusId", nullable = false, columnDefinition = "INT(11) UNSIGNED")
	public Integer getNotificationStatusId() {
		return notificationStatusId;
	}

	public void setNotificationStatusId(Integer notificationStatusId) {
		this.notificationStatusId = notificationStatusId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL", referencedColumnName = "schoolid")
	@ElementCollection(targetClass = SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	

	@Column(name = "notificationType", nullable = false)
	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	
	@Column(name = "title", nullable = false)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "message", nullable = false)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "status", columnDefinition = "CHAR default '0'", length = 1)
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "notificationDate")
	@Type(type = "date")
	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	@Column(name = "notificationTime")
	public String getNotificationTime() {
		return notificationTime;
	}

	public void setNotificationTime(String notificationTime) {
		this.notificationTime = notificationTime;
	}

	@Column(name = "count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getJoinTable() {
		return joinTable;
	}

	public void setJoinTable(String joinTable) {
		this.joinTable = joinTable;
	}

	public String getJoinId() {
		return joinId;
	}

	public void setJoinId(String joinId) {
		this.joinId = joinId;
	}

	@Column(name = "sinkingFlag", length = 1, columnDefinition = "CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

}
