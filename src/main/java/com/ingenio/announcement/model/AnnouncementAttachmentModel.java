package com.ingenio.announcement.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "announcement_attachment")
public class AnnouncementAttachmentModel {

	private Integer announcementAttachmentId;
	private AnnouncementModel announcementModel;
	private SchoolMasterModel schoolMasterModel;
	private String fileName;
	private String serverFilePath;
	private String localFilePath;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private String isEdit="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "announcementAttachmentId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementAttachmentId() {
		return announcementAttachmentId;
	}

	public void setAnnouncementAttachmentId(Integer announcementAttachmentId) {
		this.announcementAttachmentId = announcementAttachmentId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="announcementId")
	@ElementCollection(targetClass=AnnouncementModel.class)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}

	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@Column(name="fileName",nullable = true, length = 255)
	//@Length(max = 255)
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name="serverFilePath",nullable = true, length = 255)
	//@Length(max = 255)
	public String getServerFilePath() {
		return serverFilePath;
	}

	public void setServerFilePath(String serverFilePath) {
		this.serverFilePath = serverFilePath;
	}

	@Column(name="localFilePath",nullable = true, length = 255)
	//@Length(max = 255)
	public String getLocalFilePath() {
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getCdate() {
		return cDate;
	}

	public void setCdate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

}
