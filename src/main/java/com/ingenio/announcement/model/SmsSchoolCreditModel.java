package com.ingenio.announcement.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="sms_school_credit")
public class SmsSchoolCreditModel {
	private Integer creditId;
	private Integer schoolid;
	private Integer  credit;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private List<SmsSchoolCreditModel> smsCreditInfo;
	private Integer totalCredit;
	private Integer smsCount;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getCreditId() {
		return creditId;
	}
	public void setCreditId(Integer creditId) {
		this.creditId = creditId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public Integer getCredit() {
		return credit;
	}
	public void setCredit(Integer credit) {
		this.credit = credit;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	@Transient
	public Integer getSmsCount() {
		return smsCount;
	}
	public void setSmsCount(Integer smsCount) {
		this.smsCount = smsCount;
	}
	@Transient
	public List<SmsSchoolCreditModel> getSmsCreditInfo() {
		return smsCreditInfo;
	}
	public void setSmsCreditInfo(List<SmsSchoolCreditModel> smsCreditInfo) {
		this.smsCreditInfo = smsCreditInfo;
	}
	@Transient
	public Integer getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(Integer totalCredit) {
		this.totalCredit = totalCredit;
	}
	
	
}
