package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity

@Table(name = "fee_sms_history")
public class FeeSmsHistoryModel {

	private static final long serialVersionUID = 1L;
	private Integer sendSmsId;
	private StaffBasicDetailsModel staffBasicDetailsModel;
	private AppUserRoleModel appUserRole;
	private FeeSendSmsNumbersMstrModel feeSendSmsNumbersMstr;
	private StudentStandardRenewModel studentStandardRenewModel;
	private String cdate;
	private String fromNumber;
	private String toNumber;
	private Long message;
	private String deliveryStatus;
	private String smsId; 
	private Integer msgType;
	private Integer isDel=0;
	private Integer isEdit;
	private SchoolMasterModel schoolMasterModel;
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";

	@Id
	@Column(name = "sendSmsID", unique = true, nullable = false)
	public Integer getSendSmsId() {
		return this.sendSmsId;
	}

	public void setSendSmsId(Integer sendSmsId) {
		this.sendSmsId = sendSmsId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "staffID")
	public StaffBasicDetailsModel getStaffBasicDetailsModel() {
		return this.staffBasicDetailsModel;
	}

	public void setStaffBasicDetailsModel(StaffBasicDetailsModel staffBasicDetailsModel) {
		this.staffBasicDetailsModel = staffBasicDetailsModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return this.appUserRole;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRole) {
		this.appUserRole = appUserRole;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mobNoID")
	public FeeSendSmsNumbersMstrModel getFeeSendSmsNumbersMstr() {
		return this.feeSendSmsNumbersMstr;
	}

	public void setFeeSendSmsNumbersMstr(
			FeeSendSmsNumbersMstrModel feeSendSmsNumbersMstr) {
		this.feeSendSmsNumbersMstr = feeSendSmsNumbersMstr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "studRenewID")
	public StudentStandardRenewModel getStudentStandardRenewModel() {
		return this.studentStandardRenewModel;
	}

	public void setStudentStandardRenewModel(
			StudentStandardRenewModel studentStandardRenewModel) {
		this.studentStandardRenewModel = studentStandardRenewModel;
	}

	@Column(name = "cDate", updatable= false)
	public String getCdate() {
		return this.cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}

	@Column(name = "fromNumber", length = 15)
	public String getFromNumber() {
		return this.fromNumber;
	}

	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}

	@Column(name = "toNumber", length = 15)
	public String getToNumber() {
		return this.toNumber;
	}

	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}

	@Column(name = "message", length = 1000)
	public Long getMessage() {
		return this.message;
	}

	public void setMessage(Long message) {
		this.message = message;
	}

	@Column(name = "deliveryStatus", length = 50)
	public String getDeliveryStatus() {
		return this.deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	@Column(name = "smsID", length = 100)
	public String getSmsId() {
		return this.smsId;
	}

	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}

	@Column(name = "msgType")
	public Integer getMsgType() {
		return this.msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit")
	public Integer getIsEdit() {
		return this.isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
}