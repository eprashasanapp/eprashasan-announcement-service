package com.ingenio.announcement.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "banner_images")
public class BannerImagesModel {

	private Integer bannerImagesId;	
	private SchoolMasterModel schoolMasterModel;
	private YearMasterModel yearMasterModel;	
	private Integer monthId;
	private Timestamp cDate;
	private String fileName;
	private String imagePath;
	private String yearName;
	
	@Id
	@Column(name = "bannerImagesId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getBannerImagesId() {
		return bannerImagesId;
	}
	public void setBannerImagesId(Integer bannerImagesId) {
		this.bannerImagesId = bannerImagesId;
	}
	
	@Column(name = "fileName")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name = "yearName")
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	@Column(name = "monthId")
	public Integer getMonthId() {
		return monthId;
	}
	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}
	
	@Column(name = "imagePath")
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolId")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate")
	public Timestamp getCdate() {
		return this.cDate;
	}

	public void setCdate(Timestamp cDate) {
		this.cDate = cDate;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="yearId")
	@ElementCollection(targetClass=YearMasterModel.class)
	public YearMasterModel getYearMasterModel() {
		return this.yearMasterModel;
	}
	
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	

	
	
}
