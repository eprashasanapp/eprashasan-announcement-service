package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fee_receipt_edit_log")
public class FeeReceiptEditLogModel {

	
	private Integer receiptLogId;
	private Integer schoolid;
	private Date receiptOldDate;
	private Date receiptNewDate;
	private Integer receiptId;
	private String userRemark;
	private Integer approvalStatus;
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private Integer approvrdBy;
	private String approvedDate;
	private Double oldAmount;
	private Double newAmmount;
	private String subheadRemark;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getReceiptLogId() {
		return receiptLogId;
	}
	public void setReceiptLogId(Integer receiptLogId) {
		this.receiptLogId = receiptLogId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
	public Integer getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}
	public String getUserRemark() {
		return userRemark;
	}
	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}
	public Integer getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(Integer approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public Integer getApprovrdBy() {
		return approvrdBy;
	}
	public void setApprovrdBy(Integer approvrdBy) {
		this.approvrdBy = approvrdBy;
	}
	public String getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}
	public Double getOldAmount() {
		return oldAmount;
	}
	public void setOldAmount(Double oldAmount) {
		this.oldAmount = oldAmount;
	}
	public Double getNewAmmount() {
		return newAmmount;
	}
	public void setNewAmmount(Double newAmmount) {
		this.newAmmount = newAmmount;
	}
	public String getSubheadRemark() {
		return subheadRemark;
	}
	public void setSubheadRemark(String subheadRemark) {
		this.subheadRemark = subheadRemark;
	}
	public Date getReceiptOldDate() {
		return receiptOldDate;
	}
	public void setReceiptOldDate(Date receiptOldDate) {
		this.receiptOldDate = receiptOldDate;
	}
	public Date getReceiptNewDate() {
		return receiptNewDate;
	}
	public void setReceiptNewDate(Date receiptNewDate) {
		this.receiptNewDate = receiptNewDate;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	
}
