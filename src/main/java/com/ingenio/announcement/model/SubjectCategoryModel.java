
package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "subject_category")

public class SubjectCategoryModel {
	
	private int subjectCatId;
	private String subjectCatName;
	private int subjectCatType;
	private String createdDate;
	private String updatedDate;
	private int status;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private Date editDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private String subjectCatNameGL;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "subject_cat_id")
	public int getSubjectCatId() {
		return subjectCatId;
	}
	
	public void setSubjectCatId(int subjectCatId) {
		this.subjectCatId = subjectCatId;
	}
	
	@Column(name = "subject_cat_name")
	public String getSubjectCatName() {
		return subjectCatName;
	}
	
	public void setSubjectCatName(String subjectCatName) {
		this.subjectCatName = subjectCatName;
	}
	
	@Column(name = "subject_cat_type")
	public int getSubjectCatType() {
		return subjectCatType;
	}
	
	public void setSubjectCatType(int subjectCatType) {
		this.subjectCatType = subjectCatType;
	}
	
	@Column(name = "created_date")
	public String getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "updated_date")
	public String getUpdatedDate() {
		return updatedDate;
	}
	
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	@Column(name = "subject_cat_nameGL")
	public String getSubjectCatNameGL() {
		return subjectCatNameGL;
	}
	
	public void setSubjectCatNameGL(String subjectCatNameGL) {
		this.subjectCatNameGL = subjectCatNameGL;
	}
}
