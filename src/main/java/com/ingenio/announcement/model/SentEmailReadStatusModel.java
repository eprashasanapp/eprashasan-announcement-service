package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sent_email_read_status")
public class SentEmailReadStatusModel {

	private Integer readStatusId;
	private Integer sentEmailId;
	private Integer schoolid;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private String ipAddress;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getReadStatusId() {
		return readStatusId;
	}
	public void setReadStatusId(Integer readStatusId) {
		this.readStatusId = readStatusId;
	}
	public Integer getSentEmailId() {
		return sentEmailId;
	}
	public void setSentEmailId(Integer sentEmailId) {
		this.sentEmailId = sentEmailId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
}
