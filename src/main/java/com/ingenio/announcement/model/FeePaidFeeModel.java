package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "fee_paidfeetable")
public class FeePaidFeeModel {

	private Integer paidId;
	private FeeStudentFeeModel feeStudentFeeModel;
	private AppUserRoleModel appUserRoleModel;
	private SchoolMasterModel schoolMasterModel;
	private FeeHeadMasterModel feeHeadMasterModel;
	private FeeSettingMultiModel feeSettingMultiModel;
	private FeeSubheadMasterModel feeSubheadMasterModel;
	private FeeReceiptModel feeReceiptModel;
	private YearMasterModel yearMasterModel;
	private String cdate;
	private Double payFee;
	private String paidfeeDate;
	private Integer payType;
	private String ddchqno;
	private String narration;
	private String feesClearance="0";
	private String fineClearance="0";
	private Integer isConsolation=0;
	private String isDel="0";
	private String isEdit="0";
	private String sinkingFlag="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private Double discount;
	private String orderId;
	private String transcationId;
	private Double tax;
	private Integer bankId;
	@Id
	@Column(name = "paidID", unique = true, nullable = false)
	public Integer getPaidId() {
		return this.paidId;
	}

	public void setPaidId(Integer paidId) {
		this.paidId = paidId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "studFeeID")
	public FeeStudentFeeModel getFeeStudentFeeModel() {
		return this.feeStudentFeeModel;
	}

	public void setFeeStudentFeeModel(FeeStudentFeeModel feeStudentFeeModel) {
		this.feeStudentFeeModel = feeStudentFeeModel;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "userID", updatable= false)
//	public AppUserRoleModel getAppUserRoleModel() {
//		return this.appUserRole;
//	}
//
//	public void setAppUserRoleModel(AppUserRoleModel appUserRole) {
//		this.appUserRole = appUserRole;
//	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID")
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subheadID")
	public FeeSubheadMasterModel getFeeSubheadMasterModel() {
		return this.feeSubheadMasterModel;
	}

	public void setFeeSubheadMasterModel(FeeSubheadMasterModel feeSubheadMasterModel) {
		this.feeSubheadMasterModel = feeSubheadMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptID")
	public FeeReceiptModel getFeeReceiptModel() {
		return this.feeReceiptModel;
	}

	public void setFeeReceiptModel(FeeReceiptModel feeReceiptModel) {
		this.feeReceiptModel = feeReceiptModel;
	}

	@Column(name = "cDate", updatable= false)
	public String getCdate() {
		return this.cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}

	@Column(name = "payFee", precision = 22, scale = 0)
	public Double getPayFee() {
		return this.payFee;
	}

	public void setPayFee(Double payFee) {
		this.payFee = payFee;
	}

	@Column(name = "paidfeeDate", length = 50)
	public String getPaidfeeDate() {
		return this.paidfeeDate;
	}

	public void setPaidfeeDate(String paidfeeDate) {
		this.paidfeeDate = paidfeeDate;
	}

	@Column(name = "payType")
	public Integer getPayType() {
		return this.payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	@Column(name = "DDCHQno", length = 20)
	public String getDdchqno() {
		return this.ddchqno;
	}

	public void setDdchqno(String ddchqno) {
		this.ddchqno = ddchqno;
	}

	@Column(name = "narration", length = 100)
	public String getNarration() {
		return this.narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@Column(name = "feesClearance", length = 50)
	public String getFeesClearance() {
		return this.feesClearance;
	}

	public void setFeesClearance(String feesClearance) {
		this.feesClearance = feesClearance;
	}

	@Column(name = "fineClearance", length = 50)
	public String getFineClearance() {
		return this.fineClearance;
	}

	public void setFineClearance(String fineClearance) {
		this.fineClearance = fineClearance;
	}

	@Column(name = "isConsolation", nullable = false)
	public Integer getIsConsolation() {
		return this.isConsolation;
	}

	public void setIsConsolation(Integer isConsolation) {
		this.isConsolation = isConsolation;
	}

	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="sinkingFlag")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}


	
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy")
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="discount")
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}

	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}

	@Column(name="orderId")
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name="transcationId")
	public String getTranscationId() {
		return transcationId;
	}

	public void setTranscationId(String transcationId) {
		this.transcationId = transcationId;
	}


	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "headID", nullable = false)
//	public FeeHeadMasterModel getFeeHeadMasterModel() {
//		return this.feeHeadMaster;
//	}
//
//	public void setFeeHeadMasterModel(FeeHeadMasterModel feeHeadMaster) {
//		this.feeHeadMaster = feeHeadMaster;
//	}
//
//	@ManyToOne(fetch = FetchType.LAZY)
////	@JoinColumn(name = "feeSetMultiID", nullable = false)
//	@JoinColumn(name = "feeSetMultiID")
//	public FeeSettingMultiModel getFeeSettingMultiModel() {
//		return this.feeSettingMulti;
//	}
//
//	public void setFeeSettingMultiModel(FeeSettingMultiModel feeSettingMulti) {
//		this.feeSettingMulti = feeSettingMulti;
//	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "headID", nullable = false)
	public FeeHeadMasterModel getFeeHeadMasterModel() {
		return feeHeadMasterModel;
	}

	public void setFeeHeadMasterModel(FeeHeadMasterModel feeHeadMasterModel) {
		this.feeHeadMasterModel = feeHeadMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "feeSetMultiID", nullable = true)
	public FeeSettingMultiModel getFeeSettingMultiModel() {
		return feeSettingMultiModel;
	}

	public void setFeeSettingMultiModel(FeeSettingMultiModel feeSettingMultiModel) {
		this.feeSettingMultiModel = feeSettingMultiModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return appUserRoleModel;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRoleModel) {
		this.appUserRoleModel = appUserRoleModel;
	}

	
	
	

}