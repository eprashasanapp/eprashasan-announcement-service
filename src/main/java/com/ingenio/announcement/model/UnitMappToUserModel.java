package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity

@Table(name = "unit_mapp_to_user")
public class UnitMappToUserModel {
private static final long serialVersionUID = 1L;
private Integer unitMappToUserId;
private String registerName;
private SchoolMasterModel schoolMaster;
private TypeOfUnitModel typeOfUnitModel;
private AppUserRoleModel userId;
@Generated(GenerationTime.ALWAYS)
@Temporal(javax.persistence.TemporalType.DATE)
private Date cDate = new java.sql.Date(new java.util.Date().getTime());
private String isDel="0";
private Date delDate;
private AppUserRoleModel deleteBy;
private String isEdit="0";
private Date editDate;
private AppUserRoleModel editBy;
private String isApproval="0";
private AppUserRoleModel approvalBy;
private Date approvalDate;
private String deviceType="0";
private String ipAddress;
private String macAddress;
private String sinkingFlag="0";
private Date fromDate;
private Date toDate;
private String inActive="1";

@Id
@Column(name = "unitMappToUserId", unique = true, nullable = false)
public Integer getUnitMappToUserId() {
	return unitMappToUserId;
}
public void setUnitMappToUserId(Integer unitMappToUserId) {
	this.unitMappToUserId = unitMappToUserId;
}

@Column(name="registerName", length=500)
public String getRegisterName() {
	return registerName;
}
public void setRegisterName(String registerName) {
	this.registerName = registerName;
}

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "schoolid", nullable = false)
public SchoolMasterModel getSchoolMaster() {
	return schoolMaster;
}
public void setSchoolMaster(SchoolMasterModel schoolMaster) {
	this.schoolMaster = schoolMaster;
}
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "typeUnitId")
public TypeOfUnitModel getTypeOfUnitModel() {
	return typeOfUnitModel;
}
public void setTypeOfUnitModel(TypeOfUnitModel typeOfUnitModel) {
	this.typeOfUnitModel = typeOfUnitModel;
}

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "userID", updatable= false)
public AppUserRoleModel getUserId() {
	return userId;
}
public void setUserId(AppUserRoleModel userId) {
	this.userId = userId;
}

@Column(name = "cDate", updatable= false)
public Date getcDate() {
	return cDate;
}
public void setcDate(Date cDate) {
	this.cDate = cDate;
}

@Column(name="isDel", columnDefinition="default '0'")
public String getIsDel() {
	return isDel;
}
public void setIsDel(String isDel) {
	this.isDel = isDel;
}

@Column(name="delDate")
public Date getDelDate() {
	return delDate;
}
public void setDelDate(Date delDate) {
	this.delDate = delDate;
}


@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "deleteBy", nullable = false)
public AppUserRoleModel getDeleteBy() {
	return deleteBy;
}
public void setDeleteBy(AppUserRoleModel deleteBy) {
	this.deleteBy = deleteBy;
}

@Column(name="isEdit", columnDefinition="default '0'")
public String getIsEdit() {
	return isEdit;
}
public void setIsEdit(String isEdit) {
	this.isEdit = isEdit;
}

@Column(name="editDate")
public Date getEditDate() {
	return editDate;
}
public void setEditDate(Date editDate) {
	this.editDate = editDate;
}

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "editBy", nullable = false)
public AppUserRoleModel getEditBy() {
	return editBy;
}
public void setEditBy(AppUserRoleModel editBy) {
	this.editBy = editBy;
}

@Column(name="isApproval", columnDefinition="default '0'")
public String getIsApproval() {
	return isApproval;
}
public void setIsApproval(String isApproval) {
	this.isApproval = isApproval;
}

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "approvalBy", nullable = false)
public AppUserRoleModel getApprovalBy() {
	return approvalBy;
}
public void setApprovalBy(AppUserRoleModel approvalBy) {
	this.approvalBy = approvalBy;
}
@Column(name="approvalDate")
public Date getApprovalDate() {
	return approvalDate;
}
public void setApprovalDate(Date approvalDate) {
	this.approvalDate = approvalDate;
}

@Column(name="deviceType", columnDefinition="default '0'")
public String getDeviceType() {
	return deviceType;
}
public void setDeviceType(String deviceType) {
	this.deviceType = deviceType;
}

@Column(name="ipAddress", length=100)
public String getIpAddress() {
	return ipAddress;
}
public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
}

@Column(name="macAddress", length=50)
public String getMacAddress() {
	return macAddress;
}
public void setMacAddress(String macAddress) {
	this.macAddress = macAddress;
}

@Column(name="sinkingFlag", columnDefinition="default '0'")
public String getSinkingFlag() {
	return sinkingFlag;
}
public void setSinkingFlag(String sinkingFlag) {
	this.sinkingFlag = sinkingFlag;
}
public Date getFromDate() {
	return fromDate;
}
public void setFromDate(Date fromDate) {
	this.fromDate = fromDate;
}
public Date getToDate() {
	return toDate;
}
public void setToDate(Date toDate) {
	this.toDate = toDate;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
@Column(name="inActive", columnDefinition="default '1'")
public String getInActive() {
	return inActive;
}
public void setInActive(String inActive) {
	this.inActive = inActive;
}



}
