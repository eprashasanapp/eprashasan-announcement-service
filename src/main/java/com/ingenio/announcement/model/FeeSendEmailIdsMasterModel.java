package com.ingenio.announcement.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "fee_send_emailids_mstr")

public class FeeSendEmailIdsMasterModel {

	private Integer emailID;
	private SchoolMasterModel schoolMasterModel;
	private String host;
	private Integer port;
	private String userName;
	private String password;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getEmailID() {
		return emailID;
	}
	public void setEmailID(Integer emailID) {
		this.emailID = emailID;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public FeeSendEmailIdsMasterModel(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}
	public FeeSendEmailIdsMasterModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
