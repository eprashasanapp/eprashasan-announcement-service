package com.ingenio.announcement.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="school_suspension_details")
public class SchoolSuspentionDetailsModel {

	private Integer suspensionId;
	private Integer schoolid;
	private String fromDate;
	private String toDate;
	private Integer suspendedBy;
	private String subject;
	private String body;
	private Integer suspensionStatus;
	private Integer reactivatedBy;
	private String reactivatedDate;
	private String reactivateRemark;
	private Integer sendEmail;
	private Integer sendSms;
	private Integer sendNotification;
	private String var1;
	private String var2;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getSuspensionId() {
		return suspensionId;
	}
	public void setSuspensionId(Integer suspensionId) {
		this.suspensionId = suspensionId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Integer getSuspendedBy() {
		return suspendedBy;
	}
	public void setSuspendedBy(Integer suspendedBy) {
		this.suspendedBy = suspendedBy;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Integer getSuspensionStatus() {
		return suspensionStatus;
	}
	public void setSuspensionStatus(Integer suspensionStatus) {
		this.suspensionStatus = suspensionStatus;
	}
	public Integer getReactivatedBy() {
		return reactivatedBy;
	}
	public void setReactivatedBy(Integer reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}
	public String getReactivatedDate() {
		return reactivatedDate;
	}
	public void setReactivatedDate(String reactivatedDate) {
		this.reactivatedDate = reactivatedDate;
	}
	public String getReactivateRemark() {
		return reactivateRemark;
	}
	public void setReactivateRemark(String reactivateRemark) {
		this.reactivateRemark = reactivateRemark;
	}
	public Integer getSendEmail() {
		return sendEmail;
	}
	public void setSendEmail(Integer sendEmail) {
		this.sendEmail = sendEmail;
	}
	public Integer getSendSms() {
		return sendSms;
	}
	public void setSendSms(Integer sendSms) {
		this.sendSms = sendSms;
	}
	public Integer getSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(Integer sendNotification) {
		this.sendNotification = sendNotification;
	}
	public String getVar1() {
		return var1;
	}
	public void setVar1(String var1) {
		this.var1 = var1;
	}
	public String getVar2() {
		return var2;
	}
	public void setVar2(String var2) {
		this.var2 = var2;
	}
	
	
}
