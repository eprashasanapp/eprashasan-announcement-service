package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="announcement_likeordislike")
public class AnnouncementLikeorDislikeModel {

	private Integer likeId;
	private AnnouncementModel announcementModel;
	private AppUserModel appUserModel;
	private SchoolMasterModel schoolMasterModel;
	private String likeDislike;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getLikeId() {
		return likeId;
	}
	public void setLikeId(Integer likeId) {
		this.likeId = likeId;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId",nullable = false)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}
	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appUserId",nullable = false)
	public AppUserModel getAppUserModel() {
		return appUserModel;
	}
	public void setAppUserModel(AppUserModel appUserModel) {
		this.appUserModel = appUserModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid",nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate")
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	@Column(name="like_dislike")
	public String getLikeDislike() {
		return likeDislike;
	}
	public void setLikeDislike(String likeDislike) {
		this.likeDislike = likeDislike;
	}
	
	
}
