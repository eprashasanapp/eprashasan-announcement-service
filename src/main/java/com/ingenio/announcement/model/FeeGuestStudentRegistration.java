package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity

@Table(name = "fee_guestStudent_registration")
public class FeeGuestStudentRegistration {
	
	private Integer guestStudentId;
	private String mobileNo;
	private String studFName;
	private String studMName;
	private String studLName;
	private String regNo;
	private DivisionMasterModel division;
	private YearMasterModel yearmaster;
	private StandardMasterModel standardmaster;
	private FeeTypeModel previousFeeType;
	private FeeTypeModel currentFeeType;
	private SchoolMasterModel schoolMaster;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "guestStudentId", unique = true, nullable = false)
	public Integer getGuestStudentId() {
		return guestStudentId;
	}
	public void setGuestStudentId(Integer guestStudentId) {
		this.guestStudentId = guestStudentId;
	}
	
	@Column(name = "mobileNo", length = 10)
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@Column(name = "studFName")
	public String getStudFName() {
		return studFName;
	}
	public void setStudFName(String studFName) {
		this.studFName = studFName;
	}
	@Column(name = "studMName")
	public String getStudMName() {
		return studMName;
	}
	public void setStudMName(String studMName) {
		this.studMName = studMName;
	}
	@Column(name = "studLName")
	public String getStudLName() {
		return studLName;
	}
	public void setStudLName(String studLName) {
		this.studLName = studLName;
	}
	@Column(name = "regNo")
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divisionId")
	public DivisionMasterModel getDivision() {
		return division;
	}
	public void setDivision(DivisionMasterModel division) {
		this.division = division;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public YearMasterModel getYearmaster() {
		return yearmaster;
	}
	public void setYearmaster(YearMasterModel yearmaster) {
		this.yearmaster = yearmaster;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId")
	public StandardMasterModel getStandardmaster() {
		return standardmaster;
	}
	public void setStandardmaster(StandardMasterModel standardmaster) {
		this.standardmaster = standardmaster;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "previousFeeType")
	public FeeTypeModel getPreviousFeeType() {
		return previousFeeType;
	}
	public void setPreviousFeeType(FeeTypeModel previousFeeType) {
		this.previousFeeType = previousFeeType;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currentFeeType")
	public FeeTypeModel getCurrentFeeType() {
		return currentFeeType;
	}
	public void setCurrentFeeType(FeeTypeModel currentFeeType) {
		this.currentFeeType = currentFeeType;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolId")
	public SchoolMasterModel getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	
}
