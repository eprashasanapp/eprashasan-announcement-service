package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "announcement_groupadmin")
public class AnnouncmenetGroupAdminModel {

	private Integer announcementGroupAdminId;
	private SchoolMasterModel schoolMasterModel;
	private AnnouncementGroupModel announcementGroupModel;
	private AppUserModel appUserModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "announcementGroupAdminId", nullable = false,columnDefinition = "INT(11) UNSIGNED")	
	public Integer getAnnouncementGroupAdminId() {
		return announcementGroupAdminId;
	}
	public void setAnnouncementGroupAdminId(Integer announcementGroupAdminId) {
		this.announcementGroupAdminId = announcementGroupAdminId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid",nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appUserId",nullable = false)
	public AppUserModel getAppUserModel() {
		return appUserModel;
	}
	public void setAppUserModel(AppUserModel appUserModel) {
		this.appUserModel = appUserModel;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementGroupId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="announcementGroupId")
	@ElementCollection(targetClass=AnnouncmenetGroupAdminModel.class)
	public AnnouncementGroupModel getAnnouncementGroupModel() {
		return announcementGroupModel;
	}
	public void setAnnouncementGroupModel(AnnouncementGroupModel announcementGroupModel) {
		this.announcementGroupModel = announcementGroupModel;
	}
	
	

}
