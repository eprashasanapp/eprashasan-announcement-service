package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name = "student_standard_renew")

public class StudentStandardRenewModel {
	
	private static final long serialVersionUID = 1L;
	private Integer renewStudentId;
	private DivisionMasterModel divisionMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private StudentMasterModel studentMasterModel;
	private String renewAdmissionDate;
	private String progress;
	private String conduct;
	private String reason;
	private String concession;
	private String date;
	private String remark;
	private String collegeLeavingDate;
	private Integer givenNoOfTcs;
	private Integer givenNoBonafide;
	private Short alreadyRenew;
	private Integer rollNo;
	private Short isInCatlog;
	private String studFailDate;
	private Integer autoRenew;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private Date editDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private AppUserRoleModel approvalBy;
	private String isApproval = "0";
	private Date approvalDate;
	private String progressGL;
	private String conductGL;
	private String reasonGL;
	private String remarkGL;
	private String isVerify = "0";
	private AppUserRoleModel verifiedBy;
	private Date verifiedDate;
	private String fcm_id;
	private String isOnlineAdmission;
	private String isRegistration;
	private String isRegistrationApprovedFlag;
	private String isCatlogSearch;
	private Integer isLeft=0;
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "renewstudentId", unique = true, nullable = false)
	public Integer getRenewStudentId() {
		return this.renewStudentId;
	}
	
	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divisionId")
	public DivisionMasterModel getDivisionMasterModel() {
		return this.divisionMasterModel;
	}
	
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", nullable = false)
	public YearMasterModel getYearMasterModel() {
		return this.yearMasterModel;
	}
	
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId", nullable = false)
	public StandardMasterModel getStandardMasterModel() {
		return this.standardMasterModel;
	}
	
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "studentId", nullable = false)
	public StudentMasterModel getStudentMasterModel() {
		return this.studentMasterModel;
	}
	
	public void setStudentMasterModel(StudentMasterModel studentMasterModel) {
		this.studentMasterModel = studentMasterModel;
	}
	
	@Column(name = "renewAdmissiondate", length = 50)
	public String getRenewAdmissionDate() {
		return this.renewAdmissionDate;
	}
	
	public void setRenewAdmissionDate(String renewAdmissionDate) {
		this.renewAdmissionDate = renewAdmissionDate;
	}
	
	@Column(name = "fcm_id")
	public String getFcm_id() {
		return fcm_id;
	}

	public void setFcm_id(String fcm_id) {
		this.fcm_id = fcm_id;
	}

	@Column(name = "progress", length = 100)
	public String getProgress() {
		return this.progress;
	}
	
	public void setProgress(String progress) {
		this.progress = progress;
	}
	
	@Column(name = "conduct", length = 100)
	public String getConduct() {
		return this.conduct;
	}
	
	public void setConduct(String conduct) {
		this.conduct = conduct;
	}
	
	@Column(name = "reason", length = 100)
	public String getReason() {
		return this.reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name = "Concession", length = 100)
	public String getConcession() {
		return this.concession;
	}
	
	public void setConcession(String concession) {
		this.concession = concession;
	}
	
	@Column(name = "date", length = 20)
	public String getDate() {
		return this.date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	@Column(name = "remark", length = 100)
	public String getRemark() {
		return this.remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name = "college_leavingdate", length = 20)
	public String getCollegeLeavingDate() {
		return this.collegeLeavingDate;
	}
	
	public void setCollegeLeavingDate(String collegeLeavingDate) {
		this.collegeLeavingDate = collegeLeavingDate;
	}
	
	@Column(name = "givenNoOfTcs")
	public Integer getGivenNoOfTcs() {
		return this.givenNoOfTcs;
	}
	
	public void setGivenNoOfTcs(Integer givenNoOfTcs) {
		this.givenNoOfTcs = givenNoOfTcs;
	}
	
	@Column(name = "gievnNoBonafide")
	public Integer getGivenNoBonafide() {
		return this.givenNoBonafide;
	}
	
	public void setGivenNoBonafide(Integer givenNoBonafide) {
		this.givenNoBonafide = givenNoBonafide;
	}
	
	@Column(name = "alreadyRenew")
	public Short getAlreadyRenew() {
		return this.alreadyRenew;
	}
	
	public void setAlreadyRenew(Short alreadyRenew) {
		this.alreadyRenew = alreadyRenew;
	}
	
	@Column(name = "rollNo")
	public Integer getRollNo() {
		return this.rollNo;
	}
	
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	
	@Column(name = "isInCatlog")
	public Short getIsInCatlog() {
		return this.isInCatlog;
	}
	
	public void setIsInCatlog(Short isInCatlog) {
		this.isInCatlog = isInCatlog;
	}
	
	@Column(name = "studFailDate", length = 50)
	public String getStudFailDate() {
		return this.studFailDate;
	}
	
	public void setStudFailDate(String studFailDate) {
		this.studFailDate = studFailDate;
	}
	
	@Column(name = "autoRenew")
	public Integer getAutoRenew() {
		return this.autoRenew;
	}
	
	public void setAutoRenew(Integer autoRenew) {
		this.autoRenew = autoRenew;
	}
	
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	public Date getApprovalDate() {
		return approvalDate;
	}
	
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}
	
	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	@Column(name = "isApproval", columnDefinition = "default '0'")
	public String getIsApproval() {
		return isApproval;
	}
	
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	
	@Column(name = "progressGL")
	public String getProgressGL() {
		return progressGL;
	}
	
	public void setProgressGL(String progressGL) {
		this.progressGL = progressGL;
	}
	
	@Column(name = "conductGL")
	public String getConductGL() {
		return conductGL;
	}
	
	public void setConductGL(String conductGL) {
		this.conductGL = conductGL;
	}
	
	@Column(name = "reasonGL")
	public String getReasonGL() {
		return reasonGL;
	}
	
	public void setReasonGL(String reasonGL) {
		this.reasonGL = reasonGL;
	}
	
	@Column(name = "remarkGL")
	public String getRemarkGL() {
		return remarkGL;
	}
	
	public void setRemarkGL(String remarkGL) {
		this.remarkGL = remarkGL;
	}

	@Column(name="isVerify", columnDefinition="default '0'")
	public String getIsVerify() {
		return isVerify;
	}

	public void setIsVerify(String isVerify) {
		this.isVerify = isVerify;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "verifiedBy", nullable = false)
	public AppUserRoleModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(AppUserRoleModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	@Column(name="verifiedDate")
	public Date getVerifiedDate() {
		return verifiedDate;
	}

	public void setVerifiedDate(Date verifiedDate) {
		this.verifiedDate = verifiedDate;
	}

	@Column(name="isOnlineAdmission", columnDefinition="default '0'")
	public String getIsOnlineAdmission() {
		return isOnlineAdmission;
	}

	public void setIsOnlineAdmission(String isOnlineAdmission) {
		this.isOnlineAdmission = isOnlineAdmission;
	}

	@Column(name="isRegistration", columnDefinition="default '0'")
	public String getIsRegistration() {
		return isRegistration;
	}

	public void setIsRegistration(String isRegistration) {
		this.isRegistration = isRegistration;
	}

	@Column(name="isRegistrationApprovedFlag", columnDefinition="default '0'")
	public String getIsRegistrationApprovedFlag() {
		return isRegistrationApprovedFlag;
	}

	public void setIsRegistrationApprovedFlag(String isRegistrationApprovedFlag) {
		this.isRegistrationApprovedFlag = isRegistrationApprovedFlag;
	}

	@Column(name="isCatlogSearch", columnDefinition="default '0'")
	public String getIsCatlogSearch() {
		return isCatlogSearch;
	}

	public void setIsCatlogSearch(String isCatlogSearch) {
		this.isCatlogSearch = isCatlogSearch;
	}

	public Integer getIsLeft() {
		return isLeft;
	}

	public void setIsLeft(Integer isLeft) {
		this.isLeft = isLeft;
	}
	
	
}