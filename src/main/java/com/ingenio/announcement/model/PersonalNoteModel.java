package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "personal_note")
public class PersonalNoteModel {

	private Integer personalNoteId;
	private String personal_note;
	private String reminderTime;
	private Date reminderDate;
	private SchoolMasterModel schoolMasterModel;
	private AppUserModel userId;
	private Timestamp cDate;
	private StandardMasterModel standardMasterModel;
	private DivisionMasterModel divisionMasterModel;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "personalNoteId")
	public Integer getPersonalNoteId() {
		return personalNoteId;
	}
	public void setPersonalNoteId(Integer personalNoteId) {
		this.personalNoteId = personalNoteId;
	}
	
	@Column(name = "personal_note")
	public String getPersonal_note() {
		return personal_note;
	}
	public void setPersonal_note(String personal_note) {
		this.personal_note = personal_note;
	}
	
	
	@Column(name = "reminderTime")
	public String getReminderTime() {
		return reminderTime;
	}
	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}
	
	
	@Column(name = "reminderDate")
	public Date getReminderDate() {
		return reminderDate;
	}
	public void setReminderDate(Date reminderDate) {
		this.reminderDate = reminderDate;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserModel getUserId() {
		return userId;
	}
	public void setUserId(AppUserModel userId) {
		this.userId = userId;
	}
	
	
	@Column(name = "cDate")
	public Timestamp getcDate() {
		return cDate;
	}
	public void setcDate(Timestamp cDate) {
		this.cDate = cDate;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId")
	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divisionId")
	public DivisionMasterModel getDivisionMasterModel() {
		return divisionMasterModel;
	}
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	
	
	
	
	}