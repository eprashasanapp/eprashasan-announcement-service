package com.ingenio.announcement.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
//import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "event")

public class EventModel {

	private Integer eventId;
	private Timestamp cDate;
	private Date eventStartDate;
	private String eventStartTime;
	private Date eventEndDate;
	private String eventEndtime;
	private String eventVenue;
	private String eventTitle;
	private String eventDescription;
	private String eventReminderMessage;
	private Date approvalDate;
	private Date delDate;
	private String deviceType="0";
	private Date editDate;
	private String ipAddress;
	private String isApproval="0";
	private String isDel="0";
	private String isEdit="0";
	private String macAddress;
	private String sinkingFlag="0";
	private AppUserRoleModel approvalBy;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private SchoolMasterModel schoolMasterModel;
	private Integer staffId;
	private AppUserRoleModel userId;
	private EventTypeMasterModel eventTypeMasterModel;
	private String isNotificationSend="0";
	
	
	
	@Id
	@Column(name = "eventId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "cDate")
	public Timestamp getcDate() {
		return cDate;
	}
	public void setcDate(Timestamp cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="eventStartDate")
	@Type(type="date")
	public Date getEventStartDate() {
		return eventStartDate;
	}
	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	
	@Column(name="eventStartTime",nullable = false, length = 500)
	//@Length(max = 500)
	public String getEventStartTime() {
		return eventStartTime;
	}
	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}
	
	@Column(name="eventEndDate")
	@Type(type="date")
	public Date getEventEndDate() {
		return eventEndDate;
	}
	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	
	@Column(name="eventEndtime",nullable = false, length = 500)
	//@Length(max = 500)
	public String getEventEndtime() {
		return eventEndtime;
	}
	public void setEventEndtime(String eventEndtime) {
		this.eventEndtime = eventEndtime;
	}
	
	@Column(name="eventVenue",nullable = false, length = 500)
	//@Length(max = 500)
	public String getEventVenue() {
		return eventVenue;
	}
	
	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}
	
	@Column(name="eventTitle",nullable = false, length = 500)
	//@Length(max = 500)
	public String getEventTitle() {
		return eventTitle;
	}
	
	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}
	
	@Column(name="eventDescription",nullable = false, length = 500)
	//@Length(max = 500)
	public String getEventDescription() {
		return eventDescription;
	}
	
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}
	
	@Column(name="eventReminderMessage",nullable = false, length = 500)
	//@Length(max = 500)
	public String getEventReminderMessage() {
		return eventReminderMessage;
	}
	
	public void setEventReminderMessage(String eventReminderMessage) {
		this.eventReminderMessage = eventReminderMessage;
	}
	
	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	
	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	
	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name="isNotificationSend")
	public String getIsNotificationSend() {
		return isNotificationSend;
	}
	public void setIsNotificationSend(String isNotificationSend) {
		this.isNotificationSend = isNotificationSend;
	}
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	
	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}
	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "staffId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "eventType")
	public EventTypeMasterModel getEventTypeMasterModel() {
		return eventTypeMasterModel;
	}
	public void setEventTypeMasterModel(EventTypeMasterModel eventTypeMasterModel) {
		this.eventTypeMasterModel = eventTypeMasterModel;
	}
	
	
	

}
