package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "subjects")
public class SubjectModel {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name = "subject_id")
	private int subjectId;
	
	@ManyToOne
	@JoinColumn(name = "year")
	private YearMasterModel yearMasterModel;
	
	@ManyToOne
	@JoinColumn(name = "standard")
	private StandardMasterModel standardMasterModel;
	
	@Column(name = "subject_name")
	private String subjectName;
	
	@Column(name = "subject_nameGL")
	private String subjectNameGL;
	
	@Column(name = "is_eligible_Grade")
	private Integer isEligibleGrade;
	
	@Column(name = "is_eligible_grace")
	private Integer isEligibleGrace;
	
	@Column(name = "grade_type")
	private int gradeType;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "sub_cat_id")
	private int subCatId;

	@Column(name = "priority_for_marksheet")
	private int priorityForMarksheet;
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable = false)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name = "isDel", nullable = false)
	private String isDel = "0";
	
	@Column(name = "isEdit", nullable = false)
	private String isEdit = "0";
	
	@Column(name = "sinkingFlag", nullable = false)
	private String sinkingFlag = "0";
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "deviceType", nullable = false)
	private String deviceType = "0";
	
	@Column(name = "ipAddress", nullable = false)
	private String ipAddress;
	
	@Column(name = "macAddress", nullable = false)
	private String macAddress;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	private SchoolMasterModel schoolMasterModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	private AppUserRoleModel userId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	private AppUserRoleModel deleteBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	private AppUserRoleModel editBy;

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectNameGL() {
		return subjectNameGL;
	}

	public void setSubjectNameGL(String subjectNameGL) {
		this.subjectNameGL = subjectNameGL;
	}

	public Integer getIsEligibleGrade() {
		return isEligibleGrade;
	}

	public void setIsEligibleGrade(Integer isEligibleGrade) {
		this.isEligibleGrade = isEligibleGrade;
	}

	public Integer getIsEligibleGrace() {
		return isEligibleGrace;
	}

	public void setIsEligibleGrace(Integer isEligibleGrace) {
		this.isEligibleGrace = isEligibleGrace;
	}

	public int getGradeType() {
		return gradeType;
	}

	public void setGradeType(int gradeType) {
		this.gradeType = gradeType;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}

	public int getPriorityForMarksheet() {
		return priorityForMarksheet;
	}

	public void setPriorityForMarksheet(int priorityForMarksheet) {
		this.priorityForMarksheet = priorityForMarksheet;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
}