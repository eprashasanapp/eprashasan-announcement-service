package com.ingenio.announcement.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "announcement")

public class AnnouncementModel {

	private Integer announcementId;
	private SchoolMasterModel schoolMasterModel;
	
	private Integer staffId;
	private String announcementTitle;
	
	private String announcement;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private String isEdit="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	private Date endDate;
	private Date startDate;
	private Integer announcementSendFlag=0;
	private Integer isWhatsappMsg;
	private Integer isDueFeeRemainder;
	private Integer whatAppSendingFlag;
	private Integer noticeSendingFlag;
	Set<AnnouncementAttachmentModel> studentAnnouncementAttachmentMap = new HashSet<AnnouncementAttachmentModel>(0);
	Set<AnnouncementAssignToModel> studentAnnouncementAssignedToMap = new HashSet<AnnouncementAssignToModel>(0);
	
	private AnnouncementTypeModel announcementTypeModel;
	private Date publishDate = null;
	private Integer isFeedback = 0  ;
	private Integer userCanComment = 0;
	private Integer publishFlag = 0;
	private Integer sendForApproval = 0;
	private Integer priority=0;
	private Integer otherSchoolId=0;

	private Date assignDate;
    private Date deadline;
    private Date actuallyCompletionDate;
    private String complaintBy; 
    private String completionStatus="0";  
    private Integer isSanstha=0;
    private String sansthakey;
    private Integer forPrincipal=0;
    private Integer forTeacher=0;
    private Integer forstudent=0;
    
	@Id
	@Column(name = "announcementId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}

	
	@Column(name="announcementTitle",nullable = false, length = 50)
	public String getAnnouncementTitle() {
		return announcementTitle;
	}

	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}
	
	@Column(name="announcement",nullable = false)
	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getCDate() {
		return cDate;
	}

	public void setCDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name = "staffId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "announcementModel")
	public Set<AnnouncementAttachmentModel> getStudentAnnouncementAttachmentMap() {
		return studentAnnouncementAttachmentMap;
	}

	public void setStudentAnnouncementAttachmentMap(
			Set<AnnouncementAttachmentModel> studentAnnouncementAttachmentMap) {
		this.studentAnnouncementAttachmentMap = studentAnnouncementAttachmentMap;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "announcementModel")
	public Set<AnnouncementAssignToModel> getStudentAnnouncementAssignedToMap() {
		return studentAnnouncementAssignedToMap;
	}

	public void setStudentAnnouncementAssignedToMap(
			Set<AnnouncementAssignToModel> studentAnnouncementAssignedToMap) {
		this.studentAnnouncementAssignedToMap = studentAnnouncementAssignedToMap;
	}

	@Column(name="endDate")
	@Type(type="date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name="startDate")
	@Type(type="date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	

	@Column(name="announcementSendFlag")
	public Integer getAnnouncementSendFlag() {
		return announcementSendFlag;
	}

	public void setAnnouncementSendFlag(Integer announcementSendFlag) {
		this.announcementSendFlag = announcementSendFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementTypeId",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="announcementTypeId",nullable = true)
	@ElementCollection(targetClass=AnnouncementTypeModel.class)
	public AnnouncementTypeModel getAnnouncementTypeModel() {
		return announcementTypeModel;
	}

	public void setAnnouncementTypeModel(AnnouncementTypeModel announcementTypeModel) {
		this.announcementTypeModel = announcementTypeModel;
	}

	@Column(name="publishDate")
	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	@Column(name="isFeedback")
	public Integer getIsFeedback() {
		return isFeedback;
	}

	public void setIsFeedback(Integer isFeedback) {
		this.isFeedback = isFeedback;
	}


	@Column(name="userCanComment")
	public Integer getUserCanComment() {
		return userCanComment;
	}

	public void setUserCanComment(Integer userCanComment) {
		this.userCanComment = userCanComment;
	}

	

	@Column(name="publishFlag")
	public Integer getPublishFlag() {
		return publishFlag;
	}

	public void setPublishFlag(Integer publishFlag) {
		this.publishFlag = publishFlag;
	}
	
	@Column(name="isWhatsappMsg")
	public Integer getIsWhatsappMsg() {
		return isWhatsappMsg;
	}

	public void setIsWhatsappMsg(Integer isWhatsappMsg) {
		this.isWhatsappMsg = isWhatsappMsg;
	}

	@Column(name="whatAppSendingFlag")
	public Integer getWhatAppSendingFlag() {
		return whatAppSendingFlag;
	}

	public void setWhatAppSendingFlag(Integer whatAppSendingFlag) {
		this.whatAppSendingFlag = whatAppSendingFlag;
	}

	@Column(name="noticeSendingFlag")
	public Integer getNoticeSendingFlag() {
		return noticeSendingFlag;
	}

	public void setNoticeSendingFlag(Integer noticeSendingFlag) {
		this.noticeSendingFlag = noticeSendingFlag;
	}

	@Column(name="isDueFeeRemainder")
	public Integer getIsDueFeeRemainder() {
		return isDueFeeRemainder;
	}

	public void setIsDueFeeRemainder(Integer isDueFeeRemainder) {
		this.isDueFeeRemainder = isDueFeeRemainder;

	}
	
	@Column(name="sendForApproval")
	public Integer getSendForApproval() {
		return sendForApproval;
	}

	public void setSendForApproval(Integer sendForApproval) {
		this.sendForApproval = sendForApproval;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getOtherSchoolId() {
		return otherSchoolId;
	}

	public void setOtherSchoolId(Integer otherSchoolId) {
		this.otherSchoolId = otherSchoolId;
	}

	public Date getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getActuallyCompletionDate() {
		return actuallyCompletionDate;
	}

	public void setActuallyCompletionDate(Date actuallyCompletionDate) {
		this.actuallyCompletionDate = actuallyCompletionDate;
	}

	public String getComplaintBy() {
		return complaintBy;
	}

	public void setComplaintBy(String complaintBy) {
		this.complaintBy = complaintBy;
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}

	public Integer getIsSanstha() {
		return isSanstha;
	}

	public void setIsSanstha(Integer isSanstha) {
		this.isSanstha = isSanstha;
	}

	
	public Integer getForPrincipal() {
		return forPrincipal;
	}

	public void setForPrincipal(Integer forPrincipal) {
		this.forPrincipal = forPrincipal;
	}

	public Integer getForTeacher() {
		return forTeacher;
	}

	public void setForTeacher(Integer forTeacher) {
		this.forTeacher = forTeacher;
	}

	public Integer getForstudent() {
		return forstudent;
	}

	public void setForstudent(Integer forstudent) {
		this.forstudent = forstudent;
	}

	public String getSansthakey() {
		return sansthakey;
	}

	public void setSansthakey(String sansthakey) {
		this.sansthakey = sansthakey;
	}

	


}
