package com.ingenio.announcement.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="announcement_userToGroupMapping")
public class AnnouncementUserToGroupMappingModel {

	private Integer userToGroupId;
	private AppUserModel appUserModel;// appUserId
	private AnnouncementGroupModel announcementGroupModel;// groupId

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getUserToGroupId() {
		return userToGroupId;
	}
	public void setUserToGroupId(Integer userToGroupId) {
		this.userToGroupId = userToGroupId;
	}
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "appUserId", updatable= false)	
	public AppUserModel getAppUserModel() {
		return appUserModel;
	}
	public void setAppUserModel(AppUserModel appUserModel) {
		this.appUserModel = appUserModel;
	}
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "groupId", updatable= false)	
	public AnnouncementGroupModel getAnnouncementGroupModel() {
		return announcementGroupModel;
	}
	public void setAnnouncementGroupModel(AnnouncementGroupModel announcementGroupModel) {
		this.announcementGroupModel = announcementGroupModel;
	}
	
	
}
