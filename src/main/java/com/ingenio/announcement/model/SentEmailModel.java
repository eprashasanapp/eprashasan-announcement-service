package com.ingenio.announcement.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="sent_email")
public class SentEmailModel {

	private Integer id;
	private String rcode;
	private Integer templateTypeId;
	private String subject;
	private String message;
	private String role;
	private Integer staffStudentId;
	private Integer announcementId;
	private Integer schoolId;
	private Integer appUserId;
	//private String announcement;
	private String emailId;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private Integer otherSchoolId=0;
	List<SentEmailReadStatusModel>emailReadStatus;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRcode() {
		return rcode;
	}
	public void setRcode(String rcode) {
		this.rcode = rcode;
	}
	public Integer getTemplateTypeId() {
		return templateTypeId;
	}
	public void setTemplateTypeId(Integer templateTypeId) {
		this.templateTypeId = templateTypeId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
//	public String getAnnouncement() {
//		return announcement;
//	}
//	public void setAnnouncement(String announcement) {
//		this.announcement = announcement;
//	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	public Integer getOtherSchoolId() {
		return otherSchoolId;
	}
	public void setOtherSchoolId(Integer otherSchoolId) {
		this.otherSchoolId = otherSchoolId;
	}
	@Transient
	public List<SentEmailReadStatusModel> getEmailReadStatus() {
		return emailReadStatus;
	}
	public void setEmailReadStatus(List<SentEmailReadStatusModel> emailReadStatus) {
		this.emailReadStatus = emailReadStatus;
	}
	
	
	
}
