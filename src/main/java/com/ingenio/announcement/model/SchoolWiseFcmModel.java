package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "schoolWise_fcmKey")
public class SchoolWiseFcmModel {

	private Integer fcmKeyId;
	private SchoolMasterModel schoolMasterModel;
	private String fcm_key;
	
	@Id
	@Column(name = "fcmKeyId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getFcmKeyId() {
		return fcmKeyId;
	}
	
	public void setFcmKeyId(Integer fcmKeyId) {
		this.fcmKeyId = fcmKeyId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	

	@Column(name="fcm_key")
	public String getFcm_key() {
		return fcm_key;
	}

	public void setFcm_key(String fcm_key) {
		this.fcm_key = fcm_key;
	}

	

}
