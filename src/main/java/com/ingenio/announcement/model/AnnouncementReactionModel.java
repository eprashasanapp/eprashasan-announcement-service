package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="announcement_reaction")
public class AnnouncementReactionModel {

	private Integer reactionId;
	private AnnouncementModel announcementModel;// announcementId
	private SchoolMasterModel schoolMasterModel;// schoolid
	private AppUserModel appUserModel;//appUserId
	private AnnouncmenetQuestionsModel announcmenetQuestionsModel;// questionId;
	private AnnouncmentOptionsOfQuestionsModel announcmentOptionsOfQuestionsModel;// optionId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getReactionId() {
		return reactionId;
	}
	public void setReactionId(Integer reactionId) {
		this.reactionId = reactionId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId",nullable = false)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}
	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid",nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appUserId",nullable = false)
	public AppUserModel getAppUserModel() {
		return appUserModel;
	}
	public void setAppUserModel(AppUserModel appUserModel) {
		this.appUserModel = appUserModel;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "questionId",nullable = false)
	public AnnouncmenetQuestionsModel getAnnouncmenetQuestionsModel() {
		return announcmenetQuestionsModel;
	}
	public void setAnnouncmenetQuestionsModel(AnnouncmenetQuestionsModel announcmenetQuestionsModel) {
		this.announcmenetQuestionsModel = announcmenetQuestionsModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "optionId",nullable = false)
	public AnnouncmentOptionsOfQuestionsModel getAnnouncmentOptionsOfQuestionsModel() {
		return announcmentOptionsOfQuestionsModel;
	}
	public void setAnnouncmentOptionsOfQuestionsModel(
			AnnouncmentOptionsOfQuestionsModel announcmentOptionsOfQuestionsModel) {
		this.announcmentOptionsOfQuestionsModel = announcmentOptionsOfQuestionsModel;
	}
	
	
}

