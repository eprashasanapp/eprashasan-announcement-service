package com.ingenio.announcement.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "android_fcm_token_master")

public class AndroidFCMTokenMaster {
	private static final long serialVersionUID = -9004909181952908073L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "fcmMasterTokenId", unique = true, nullable = false,columnDefinition = "INT(11)")
	private Integer fcmMasterTokenId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "notificationUserId", columnDefinition = "INT(10) UNSIGNED ")
	private AppUserRoleModel notificationUserId;

	@Column(name = "mobileNo")
	private String mobileNo;
	
	@Column(name = "token")
	private String token;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	private SchoolMasterModel schoolMasterModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	private AppUserRoleModel userId;
	
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate",nullable = true, updatable= false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	private String isDel = "0";
	
	@Column(name="delDate")
	@Type(type="date")
	private Date delDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	private AppUserRoleModel deleteBy;
	
	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	private String isEdit = "0";
	
	@Column(name="editDate")
	@Type(type="date")
	private Date editDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	private AppUserRoleModel editBy;
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	private String deviceType = "0";
	
	@Column(name = "ipAddress", length = 100)
	private String ipAddress;
	
	@Column(name = "macAddress", length = 50)
	private String macAddress;
	
	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	private String sinkingFlag = "0";
	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "APP_USER_ROLE", joinColumns = @JoinColumn(name = "USERNAME"), inverseJoinColumns = @JoinColumn(name = "ROLENAME"))
	private Collection<AppRoleModel> roles;


	public Integer getFcmMasterTokenId() {
		return fcmMasterTokenId;
	}

	public void setFcmMasterTokenId(Integer fcmMasterTokenId) {
		this.fcmMasterTokenId = fcmMasterTokenId;
	}
	
	public AppUserRoleModel getNotificationUserId() {
		return notificationUserId;
	}

	public void setNotificationUserId(AppUserRoleModel notificationUserId) {
		this.notificationUserId = notificationUserId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public Collection<AppRoleModel> getRoles() {
		return roles;
	}

	public void setRoles(Collection<AppRoleModel> roles) {
		this.roles = roles;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}