package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
@Entity
@Table(name = "student_master", uniqueConstraints = @UniqueConstraint(columnNames = "Student_RegNo"))

public class StudentMasterModel {
	
	private static final long serialVersionUID = 1L;
	
	private Integer studentId;
	private ReligionMasterModel religion;
	private CategoryMasterModel category;
	private CasteMasterModel castemaster;
	private String studentRegNo;
	private String studentFormNo;
	private String admissionDate;
	private String medium;
	private String birthDate;
	private String birthPlace;
	private String birthCity;
	private String studGender;
	private String studBloodGroup;
	private String studHeight;
	private String studWeight;
	private String mothertonge;
	private String nationlity;
	private String laddress;
	private String lcity;
	private String lstate;
	private String lpincode;
	private String lcountry;
	private String paddress;
	private String pcity;
	private String pstate;
	private String ppincode;
	private String pcountry;
	private String fatherName;
	private String feducation;
	private String foccupation;
	private String fage;
	private String fincome;
	private String fmobile;
	private String ftelephone;
	private String ffax;
	private String femail;
	private String motherName;
	private String meducation;
	private String moccupation;
	private String mage;
	private String mincome;
	private String mmobile;
	private String mtelephone;
	private String mfax;
	private String memail;
	private String previousSchool;
	private String previousStandard;
	private String previousYear;
	private String previousAddress;
	private String previousCity;
	private String previousState;
	private String previousReason;
	private String previousRemark;
	private String filePath;
	private String remark;
	private Integer status;
	private String userName;
	private Integer isQuickAdmission;
	private String eduOfficerNo;
	private String submitDocLisst;
	private String provisionalRegNo;
	private String isProvisionalAdmission;
	private String isAdmissionCancelled;
	private String studInitialName;
	private String studFName;
	private String studMName;
	private String studLName;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private String isEdit = "0";
	private Date editDate;
	private String sinkingFlag = "0";
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private GRBookNameModel grBookName;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private AppUserRoleModel approvalBy;
	private String isApproval = "0";
	private Date approvalDate;
	private String StudentRegNoGL;
	private String mediumGL;
	private String birthPlaceGL;
	private String birthCityGL;
	private String studGenderGL;
	private String mothertongeGL;
	private String nationlityGL;
	private String lAddressGL;
	private String lCityGL;
	private String lStateGL;
	private String lCountryGL;
	private String pAddressGL;
	private String pCityGL;
	private String pStateGL;
	private String pCountryGL;
	private String fatherNameGL;
	private String fEducationGL;
	private String fOccupationGL;
	private String fAgeGL;
	private String fIncomeGL;
	private String motherNameGL;
	private String mEducationGL;
	private String mOccupationGL;
	private String mAgeGL;
	private String mIncomeGL;
	private String previousSchoolGL;
	private String previousStandardGL;
	private String previousAddressGL;
	private String previousCityGL;
	private String previousStateGL;
	private String previousReasonGL;
	private String previousRemarkGL;
	private String remarkGL;
	private String userNameGL;
	private String submitDocLisstGL;
	private String provisionalRegNoGL;
	private String studInitialNameGL;
	private String studFNameGL;
	private String studMNameGL;
	private String studLNameGL;
	private String cancelRemarkGL;
	private String isOnlineAdmission = "0";
	
	private Set<StudentStandardRenewModel> studentStandardRenews = new HashSet<StudentStandardRenewModel>(0);
	private Set<AppUserModel> user = new HashSet<AppUserModel>(0);
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "stud_id", unique = true, nullable = false)
	public Integer getStudentId() {
		return this.studentId;
	}
	
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "religionId")
	public ReligionMasterModel getReligion() {
		return this.religion;
	}
	
	public void setReligion(ReligionMasterModel religion) {
		this.religion = religion;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoryId")
	public CategoryMasterModel getCategory() {
		return this.category;
	}
	
	public void setCategory(CategoryMasterModel category) {
		this.category = category;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caste_id")
	public CasteMasterModel getCastemaster() {
		return this.castemaster;
	}
	
	public void setCastemaster(CasteMasterModel castemaster) {
		this.castemaster = castemaster;
	}
	
	
	@Column(name = "Student_RegNo", unique = true, length = 100)
	public String getStudentRegNo() {
		return this.studentRegNo;
	}
	
	public void setStudentRegNo(String studentRegNo) {
		this.studentRegNo = studentRegNo;
	}
	
	@Column(name = "Student_formNo", length = 50)
	public String getStudentFormNo() {
		return this.studentFormNo;
	}
	
	public void setStudentFormNo(String studentFormNo) {
		this.studentFormNo = studentFormNo;
	}
	
	@Column(name = "admission_date", length = 50)
	public String getAdmissionDate() {
		return this.admissionDate;
	}
	
	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}
	
	@Column(name = "medium", length = 50)
	public String getMedium() {
		return this.medium;
	}
	
	public void setMedium(String medium) {
		this.medium = medium;
	}
	
	@Column(name = "birth_date", length = 200)
	public String getBirthDate() {
		return this.birthDate;
	}
	
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	
	@Column(name = "birth_place", length = 200)
	public String getBirthPlace() {
		return this.birthPlace;
	}
	
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	
	@Column(name = "birth_city", length = 200)
	public String getBirthCity() {
		return this.birthCity;
	}
	
	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}
	
	@Column(name = "stud_gender", length = 200)
	public String getStudGender() {
		return this.studGender;
	}
	
	public void setStudGender(String studGender) {
		this.studGender = studGender;
	}
	
	@Column(name = "stud_blood_group", length = 200)
	public String getStudBloodGroup() {
		return this.studBloodGroup;
	}
	
	public void setStudBloodGroup(String studBloodGroup) {
		this.studBloodGroup = studBloodGroup;
	}
	
	@Column(name = "stud_height", length = 200)
	public String getStudHeight() {
		return this.studHeight;
	}
	
	public void setStudHeight(String studHeight) {
		this.studHeight = studHeight;
	}
	
	@Column(name = "stud_weight", length = 200)
	public String getStudWeight() {
		return this.studWeight;
	}
	
	public void setStudWeight(String studWeight) {
		this.studWeight = studWeight;
	}
	
	@Column(name = "mothertonge", length = 200)
	public String getMothertonge() {
		return this.mothertonge;
	}
	
	public void setMothertonge(String mothertonge) {
		this.mothertonge = mothertonge;
	}
	
	@Column(name = "nationlity", length = 200)
	public String getNationlity() {
		return this.nationlity;
	}
	
	public void setNationlity(String nationlity) {
		this.nationlity = nationlity;
	}
	
	@Column(name = "lAddress", length = 200)
	public String getLaddress() {
		return this.laddress;
	}
	
	public void setLaddress(String laddress) {
		this.laddress = laddress;
	}
	
	@Column(name = "lCity", length = 50)
	public String getLcity() {
		return this.lcity;
	}
	
	public void setLcity(String lcity) {
		this.lcity = lcity;
	}
	
	@Column(name = "lState", length = 50)
	public String getLstate() {
		return this.lstate;
	}
	
	public void setLstate(String lstate) {
		this.lstate = lstate;
	}
	
	@Column(name = "lPincode", length = 50)
	public String getLpincode() {
		return this.lpincode;
	}
	
	public void setLpincode(String lpincode) {
		this.lpincode = lpincode;
	}
	
	@Column(name = "lCountry", length = 50)
	public String getLcountry() {
		return this.lcountry;
	}
	
	public void setLcountry(String lcountry) {
		this.lcountry = lcountry;
	}
	
	@Column(name = "pAddress", length = 600)
	public String getPaddress() {
		return this.paddress;
	}
	
	public void setPaddress(String paddress) {
		this.paddress = paddress;
	}
	
	@Column(name = "pCity", length = 50)
	public String getPcity() {
		return this.pcity;
	}
	
	public void setPcity(String pcity) {
		this.pcity = pcity;
	}
	
	@Column(name = "pState", length = 50)
	public String getPstate() {
		return this.pstate;
	}
	
	public void setPstate(String pstate) {
		this.pstate = pstate;
	}
	
	@Column(name = "pPincode", length = 50)
	public String getPpincode() {
		return this.ppincode;
	}
	
	public void setPpincode(String ppincode) {
		this.ppincode = ppincode;
	}
	
	@Column(name = "pCountry", length = 20)
	public String getPcountry() {
		return this.pcountry;
	}
	
	public void setPcountry(String pcountry) {
		this.pcountry = pcountry;
	}
	
	@Column(name = "fatherName", length = 100)
	public String getFatherName() {
		return this.fatherName;
	}
	
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	
	@Column(name = "fEducation", length = 50)
	public String getFeducation() {
		return this.feducation;
	}
	
	public void setFeducation(String feducation) {
		this.feducation = feducation;
	}
	
	@Column(name = "fOccupation", length = 50)
	public String getFoccupation() {
		return this.foccupation;
	}
	
	public void setFoccupation(String foccupation) {
		this.foccupation = foccupation;
	}
	
	@Column(name = "fAge", length = 50)
	public String getFage() {
		return this.fage;
	}
	
	public void setFage(String fage) {
		this.fage = fage;
	}
	
	@Column(name = "fIncome", length = 50)
	public String getFincome() {
		return this.fincome;
	}
	
	public void setFincome(String fincome) {
		this.fincome = fincome;
	}
	
	@Column(name = "fMobile", length = 50)
	public String getFmobile() {
		return this.fmobile;
	}
	
	public void setFmobile(String fmobile) {
		this.fmobile = fmobile;
	}
	
	@Column(name = "fTelephone", length = 200)
	public String getFtelephone() {
		return this.ftelephone;
	}
	
	public void setFtelephone(String ftelephone) {
		this.ftelephone = ftelephone;
	}
	
	@Column(name = "fFax", length = 50)
	public String getFfax() {
		return this.ffax;
	}
	
	public void setFfax(String ffax) {
		this.ffax = ffax;
	}
	
	@Column(name = "fEmail", length = 50)
	public String getFemail() {
		return this.femail;
	}
	
	public void setFemail(String femail) {
		this.femail = femail;
	}
	
	@Column(name = "motherName", length = 50)
	public String getMotherName() {
		return this.motherName;
	}
	
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	
	@Column(name = "mEducation", length = 300)
	public String getMeducation() {
		return this.meducation;
	}
	
	public void setMeducation(String meducation) {
		this.meducation = meducation;
	}
	
	@Column(name = "mOccupation", length = 300)
	public String getMoccupation() {
		return this.moccupation;
	}
	
	public void setMoccupation(String moccupation) {
		this.moccupation = moccupation;
	}
	
	@Column(name = "mAge", length = 50)
	public String getMage() {
		return this.mage;
	}
	
	public void setMage(String mage) {
		this.mage = mage;
	}
	
	@Column(name = "mIncome", length = 50)
	public String getMincome() {
		return this.mincome;
	}
	
	public void setMincome(String mincome) {
		this.mincome = mincome;
	}
	
	@Column(name = "mMobile", length = 12)
	public String getMmobile() {
		return this.mmobile;
	}
	
	public void setMmobile(String mmobile) {
		this.mmobile = mmobile;
	}
	
	@Column(name = "mTelephone", length = 15)
	public String getMtelephone() {
		return this.mtelephone;
	}
	
	public void setMtelephone(String mtelephone) {
		this.mtelephone = mtelephone;
	}
	
	@Column(name = "mFax", length = 15)
	public String getMfax() {
		return this.mfax;
	}
	
	public void setMfax(String mfax) {
		this.mfax = mfax;
	}
	
	@Column(name = "mEmail", length = 50)
	public String getMemail() {
		return this.memail;
	}
	
	public void setMemail(String memail) {
		this.memail = memail;
	}
	
	@Column(name = "previousSchool", length = 200)
	public String getPreviousSchool() {
		return this.previousSchool;
	}
	
	public void setPreviousSchool(String previousSchool) {
		this.previousSchool = previousSchool;
	}
	
	@Column(name = "previousStandard", length = 200)
	public String getPreviousStandard() {
		return this.previousStandard;
	}
	
	public void setPreviousStandard(String previousStandard) {
		this.previousStandard = previousStandard;
	}
	
	@Column(name = "previousYear", length = 50)
	public String getPreviousYear() {
		return this.previousYear;
	}
	
	public void setPreviousYear(String previousYear) {
		this.previousYear = previousYear;
	}
	
	@Column(name = "previousAddress", length = 150)
	public String getPreviousAddress() {
		return this.previousAddress;
	}
	
	public void setPreviousAddress(String previousAddress) {
		this.previousAddress = previousAddress;
	}
	
	@Column(name = "previousCity", length = 50)
	public String getPreviousCity() {
		return this.previousCity;
	}
	
	public void setPreviousCity(String previousCity) {
		this.previousCity = previousCity;
	}
	
	@Column(name = "previousState", length = 50)
	public String getPreviousState() {
		return this.previousState;
	}
	
	public void setPreviousState(String previousState) {
		this.previousState = previousState;
	}
	
	@Column(name = "previousReason", length = 200)
	public String getPreviousReason() {
		return this.previousReason;
	}
	
	public void setPreviousReason(String previousReason) {
		this.previousReason = previousReason;
	}
	
	@Column(name = "previousRemark", length = 200)
	public String getPreviousRemark() {
		return this.previousRemark;
	}
	
	public void setPreviousRemark(String previousRemark) {
		this.previousRemark = previousRemark;
	}
	
	@Column(name = "filePath")
	public String getFilePath() {
		return this.filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	@Column(name = "remark", length = 100)
	public String getRemark() {
		return this.remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name = "status")
	public Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Column(name = "user_name", length = 100)
	public String getUserName() {
		return this.userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "isQuickAdmission")
	public Integer getIsQuickAdmission() {
		return this.isQuickAdmission;
	}
	
	public void setIsQuickAdmission(Integer isQuickAdmission) {
		this.isQuickAdmission = isQuickAdmission;
	}
	
	@Column(name = "EduOfficerNo", length = 50)
	public String getEduOfficerNo() {
		return this.eduOfficerNo;
	}
	
	public void setEduOfficerNo(String eduOfficerNo) {
		this.eduOfficerNo = eduOfficerNo;
	}
	
	@Column(name = "submitDocLisst", length = 500)
	public String getSubmitDocLisst() {
		return this.submitDocLisst;
	}
	
	public void setSubmitDocLisst(String submitDocLisst) {
		this.submitDocLisst = submitDocLisst;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "studentMasterModel")
	public Set<StudentStandardRenewModel> getStudentStandardRenews() {
		return this.studentStandardRenews;
	}
	
	public void setStudentStandardRenews(Set<StudentStandardRenewModel> studentStandardRenews) {
		this.studentStandardRenews = studentStandardRenews;
	}
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffId")
	public Set<AppUserModel> getUser() {
		return user;
	}

	public void setUser(Set<AppUserModel> user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "provisionalRegNo", length = 10)
	public String getProvisionalRegNo() {
		return provisionalRegNo;
	}
	
	public void setProvisionalRegNo(String provisionalRegNo) {
		this.provisionalRegNo = provisionalRegNo;
	}
	
	@Column(name = "isProvisionalAdmission", length = 1, columnDefinition = "default '0'")
	public String getIsProvisionalAdmission() {
		return isProvisionalAdmission;
	}
	
	public void setIsProvisionalAdmission(String isProvisionalAdmission) {
		this.isProvisionalAdmission = isProvisionalAdmission;
	}
	
	@Column(name = "isAdmissionCancelled", length = 1, columnDefinition = "default '1'")
	
	public String getIsAdmissionCancelled() {
		return isAdmissionCancelled;
	}
	
	public void setIsAdmissionCancelled(String isAdmissionCancelled) {
		this.isAdmissionCancelled = isAdmissionCancelled;
	}
	
	@Column(name = "studInitialName", length = 15)
	public String getStudInitialName() {
		return studInitialName;
	}
	
	public void setStudInitialName(String studInitialName) {
		this.studInitialName = studInitialName;
	}
	
	@Column(name = "studFName", length = 30)
	public String getStudFName() {
		return studFName;
	}
	
	public void setStudFName(String studFName) {
		this.studFName = studFName;
	}
	
	@Column(name = "studMName", length = 30)
	public String getStudMName() {
		return studMName;
	}
	
	public void setStudMName(String studMName) {
		this.studMName = studMName;
	}
	
	@Column(name = "studLName", length = 30)
	public String getStudLName() {
		return studLName;
	}
	
	public void setStudLName(String studLName) {
		this.studLName = studLName;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "grBookId")
	public GRBookNameModel getGrBookName() {
		return grBookName;
	}
	
	public void setGrBookName(GRBookNameModel grBookName) {
		this.grBookName = grBookName;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}
	
	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	@Column(name = "isApproval", columnDefinition = "default '0'")
	public String getIsApproval() {
		return isApproval;
	}
	
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	
	@Column(name = "approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}
	
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name = "Student_RegNoGL")
	public String getStudentRegNoGL() {
		return StudentRegNoGL;
	}
	
	public void setStudentRegNoGL(String studentRegNoGL) {
		StudentRegNoGL = studentRegNoGL;
	}
	
	@Column(name = "birth_placeGL")
	public String getBirthPlaceGL() {
		return birthPlaceGL;
	}
	
	public void setBirthPlaceGL(String birthPlaceGL) {
		this.birthPlaceGL = birthPlaceGL;
	}
	
	@Column(name = "birth_cityGL")
	public String getBirthCityGL() {
		return birthCityGL;
	}
	
	public void setBirthCityGL(String birthCityGL) {
		this.birthCityGL = birthCityGL;
	}
	
	@Column(name = "stud_genderGL")
	public String getStudGenderGL() {
		return studGenderGL;
	}
	
	public void setStudGenderGL(String studGenderGL) {
		this.studGenderGL = studGenderGL;
	}
	
	@Column(name = "user_nameGL")
	public String getUserNameGL() {
		return userNameGL;
	}
	
	public void setUserNameGL(String userNameGL) {
		this.userNameGL = userNameGL;
	}
	
	@Column(name = "mediumGL")
	public String getMediumGL() {
		return mediumGL;
	}
	
	public void setMediumGL(String mediumGL) {
		this.mediumGL = mediumGL;
	}
	
	@Column(name = "mothertongeGL")
	public String getMothertongeGL() {
		return mothertongeGL;
	}
	
	public void setMothertongeGL(String mothertongeGL) {
		this.mothertongeGL = mothertongeGL;
	}
	
	@Column(name = "nationlityGL")
	public String getNationlityGL() {
		return nationlityGL;
	}
	
	public void setNationlityGL(String nationlityGL) {
		this.nationlityGL = nationlityGL;
	}
	
	@Column(name = "lAddressGL")
	public String getlAddressGL() {
		return lAddressGL;
	}
	
	public void setlAddressGL(String lAddressGL) {
		this.lAddressGL = lAddressGL;
	}
	
	@Column(name = "lCityGL")
	public String getlCityGL() {
		return lCityGL;
	}
	
	public void setlCityGL(String lCityGL) {
		this.lCityGL = lCityGL;
	}
	
	@Column(name = "lStateGL")
	public String getlStateGL() {
		return lStateGL;
	}
	
	public void setlStateGL(String lStateGL) {
		this.lStateGL = lStateGL;
	}
	
	@Column(name = "lCountryGL")
	public String getlCountryGL() {
		return lCountryGL;
	}
	
	public void setlCountryGL(String lCountryGL) {
		this.lCountryGL = lCountryGL;
	}
	
	@Column(name = "pAddressGL")
	public String getpAddressGL() {
		return pAddressGL;
	}
	
	public void setpAddressGL(String pAddressGL) {
		this.pAddressGL = pAddressGL;
	}
	
	@Column(name = "pCityGL")
	public String getpCityGL() {
		return pCityGL;
	}
	
	public void setpCityGL(String pCityGL) {
		this.pCityGL = pCityGL;
	}
	
	@Column(name = "pStateGL")
	public String getpStateGL() {
		return pStateGL;
	}
	
	public void setpStateGL(String pStateGL) {
		this.pStateGL = pStateGL;
	}
	
	@Column(name = "pCountryGL")
	public String getpCountryGL() {
		return pCountryGL;
	}
	
	public void setpCountryGL(String pCountryGL) {
		this.pCountryGL = pCountryGL;
	}
	
	@Column(name = "fatherNameGL")
	public String getFatherNameGL() {
		return fatherNameGL;
	}
	
	public void setFatherNameGL(String fatherNameGL) {
		this.fatherNameGL = fatherNameGL;
	}
	
	@Column(name = "fEducationGL")
	public String getfEducationGL() {
		return fEducationGL;
	}
	
	public void setfEducationGL(String fEducationGL) {
		this.fEducationGL = fEducationGL;
	}
	
	@Column(name = "fOccupationGL")
	public String getfOccupationGL() {
		return fOccupationGL;
	}
	
	public void setfOccupationGL(String fOccupationGL) {
		this.fOccupationGL = fOccupationGL;
	}
	
	@Column(name = "fAgeGL")
	public String getfAgeGL() {
		return fAgeGL;
	}
	
	public void setfAgeGL(String fAgeGL) {
		this.fAgeGL = fAgeGL;
	}
	
	@Column(name = "fIncomeGL")
	public String getfIncomeGL() {
		return fIncomeGL;
	}
	
	public void setfIncomeGL(String fIncomeGL) {
		this.fIncomeGL = fIncomeGL;
	}
	
	@Column(name = "motherNameGL")
	public String getMotherNameGL() {
		return motherNameGL;
	}
	
	public void setMotherNameGL(String motherNameGL) {
		this.motherNameGL = motherNameGL;
	}
	
	@Column(name = "mEducationGL")
	public String getmEducationGL() {
		return mEducationGL;
	}
	
	public void setmEducationGL(String mEducationGL) {
		this.mEducationGL = mEducationGL;
	}
	
	@Column(name = "mOccupationGL")
	public String getmOccupationGL() {
		return mOccupationGL;
	}
	
	public void setmOccupationGL(String mOccupationGL) {
		this.mOccupationGL = mOccupationGL;
	}
	
	@Column(name = "mAgeGL")
	public String getmAgeGL() {
		return mAgeGL;
	}
	
	public void setmAgeGL(String mAgeGL) {
		this.mAgeGL = mAgeGL;
	}
	
	@Column(name = "mIncomeGL")
	public String getmIncomeGL() {
		return mIncomeGL;
	}
	
	public void setmIncomeGL(String mIncomeGL) {
		this.mIncomeGL = mIncomeGL;
	}
	
	@Column(name = "previousSchoolGL")
	public String getPreviousSchoolGL() {
		return previousSchoolGL;
	}
	
	public void setPreviousSchoolGL(String previousSchoolGL) {
		this.previousSchoolGL = previousSchoolGL;
	}
	
	@Column(name = "previousStandardGL")
	public String getPreviousStandardGL() {
		return previousStandardGL;
	}
	
	public void setPreviousStandardGL(String previousStandardGL) {
		this.previousStandardGL = previousStandardGL;
	}
	
	@Column(name = "previousAddressGL")
	public String getPreviousAddressGL() {
		return previousAddressGL;
	}
	
	public void setPreviousAddressGL(String previousAddressGL) {
		this.previousAddressGL = previousAddressGL;
	}
	
	@Column(name = "previousCityGL")
	public String getPreviousCityGL() {
		return previousCityGL;
	}
	
	public void setPreviousCityGL(String previousCityGL) {
		this.previousCityGL = previousCityGL;
	}
	
	@Column(name = "previousStateGL")
	public String getPreviousStateGL() {
		return previousStateGL;
	}
	
	public void setPreviousStateGL(String previousStateGL) {
		this.previousStateGL = previousStateGL;
	}
	
	@Column(name = "previousReasonGL")
	public String getPreviousReasonGL() {
		return previousReasonGL;
	}
	
	public void setPreviousReasonGL(String previousReasonGL) {
		this.previousReasonGL = previousReasonGL;
	}
	
	@Column(name = "previousRemarkGL")
	public String getPreviousRemarkGL() {
		return previousRemarkGL;
	}
	
	public void setPreviousRemarkGL(String previousRemarkGL) {
		this.previousRemarkGL = previousRemarkGL;
	}
	
	@Column(name = "remarkGL")
	public String getRemarkGL() {
		return remarkGL;
	}
	
	public void setRemarkGL(String remarkGL) {
		this.remarkGL = remarkGL;
	}
	
	@Column(name = "submitDocLisstGL")
	public String getSubmitDocLisstGL() {
		return submitDocLisstGL;
	}
	
	public void setSubmitDocLisstGL(String submitDocLisstGL) {
		this.submitDocLisstGL = submitDocLisstGL;
	}
	
	@Column(name = "provisionalRegNoGL")
	public String getProvisionalRegNoGL() {
		return provisionalRegNoGL;
	}
	
	public void setProvisionalRegNoGL(String provisionalRegNoGL) {
		this.provisionalRegNoGL = provisionalRegNoGL;
	}
	
	@Column(name = "studInitialNameGL")
	public String getStudInitialNameGL() {
		return studInitialNameGL;
	}
	
	public void setStudInitialNameGL(String studInitialNameGL) {
		this.studInitialNameGL = studInitialNameGL;
	}
	
	@Column(name = "studFNameGL")
	public String getStudFNameGL() {
		return studFNameGL;
	}
	
	public void setStudFNameGL(String studFNameGL) {
		this.studFNameGL = studFNameGL;
	}
	
	@Column(name = "studMNameGL")
	public String getStudMNameGL() {
		return studMNameGL;
	}
	
	public void setStudMNameGL(String studMNameGL) {
		this.studMNameGL = studMNameGL;
	}
	
	@Column(name = "studLNameGL")
	public String getStudLNameGL() {
		return studLNameGL;
	}
	
	public void setStudLNameGL(String studLNameGL) {
		this.studLNameGL = studLNameGL;
	}
	
	@Column(name = "cancelRemarkGL")
	public String getCancelRemarkGL() {
		return cancelRemarkGL;
	}
	
	public void setCancelRemarkGL(String cancelRemarkGL) {
		this.cancelRemarkGL = cancelRemarkGL;
	}

	@Column(name = "isOnlineAdmission", columnDefinition = "default '0'")
	public String getIsOnlineAdm() {
		return isOnlineAdmission;
	}

	public void setIsOnlineAdm(String isOnlineAdm) {
		this.isOnlineAdmission = isOnlineAdm;
	}

}