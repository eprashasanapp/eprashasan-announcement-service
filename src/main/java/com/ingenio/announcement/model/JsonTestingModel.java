package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.ingenio.announcement.bean.AnnoucementLabelBean;
import com.vladmihalcea.hibernate.type.json.JsonType;

@Entity
@Table(name = "jsontesting")
@TypeDef(name = "json", typeClass = JsonType.class)
public class JsonTestingModel {

	private Integer jsontestingId;
	private Integer schoolId;
	


	@Column(name = "additionalProperties")
    private String additionalProperties;

	



	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "jsontestingId", unique = true, nullable = false)
	public Integer getJsontestingId() {
		return jsontestingId;
	}

	public void setJsontestingId(Integer jsontestingId) {
		this.jsontestingId = jsontestingId;
	}

	

	@Column(name = "schoolId")
	public Integer getSchoolId() {
		return schoolId;
	}

	@Override
	public String toString() {
		return "JsonTestingModel [jsontestingId=" + jsontestingId + ", schoolId=" + schoolId + ", additionalProperties="
				+ additionalProperties + "]";
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(String additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	

	
	

}
