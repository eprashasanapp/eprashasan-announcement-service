package com.ingenio.announcement.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="sent_messages")
public class SentMessagesModel {

	private Integer id;
	private Integer templateTypeId;
	private String message;
	private String role;
	private Integer staffStudentId;
	private Integer announcementId;
	private Integer schoolId;
	private Integer appUserId;
	private String mobileNumber;
	private String rcode;
	private String announcement;
	private String announcementTitle;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private Integer otherSchoolId=0;
	private List<SentMessagesReadStatusModel> readStatus;
	private String studentName;
	private String staffName;
	List<SentEmailModel> sentEmail;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	public Integer getTemplateTypeId() {
		return templateTypeId;
	}
	public void setTemplateTypeId(Integer templateTypeId) {
		this.templateTypeId = templateTypeId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getRcode() {
		return rcode;
	}
	public void setRcode(String rcode) {
		this.rcode = rcode;
	}
	@Transient
	public List<SentMessagesReadStatusModel> getReadStatus() {
		return readStatus;
	}
	public void setReadStatus(List<SentMessagesReadStatusModel> readStatus) {
		this.readStatus = readStatus;
	}
	@Transient
	public String getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}
	@Transient
	public String getAnnouncementTitle() {
		return announcementTitle;
	}
	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}
	
	
	//int, int, java.lang.String, java.lang.String, int, int, int, int, java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.lang.String
	
	public Integer getOtherSchoolId() {
		return otherSchoolId;
	}
	public void setOtherSchoolId(Integer otherSchoolId) {
		this.otherSchoolId = otherSchoolId;
	}
	public SentMessagesModel(Integer id, Integer templateTypeId, String message, String role, Integer staffStudentId,
			Integer announcementId, Integer schoolId, Integer appUserId, String mobileNumber, String rcode,
			 Date cDate,String announcement, String announcementTitle,String studentName,String staffName) {
		super();
		this.id = id;
		this.templateTypeId = templateTypeId;
		this.message = message;
		this.role = role;
		this.staffStudentId = staffStudentId;
		this.announcementId = announcementId;
		this.schoolId = schoolId;
		this.appUserId = appUserId;
		this.mobileNumber = mobileNumber;
		this.rcode = rcode;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.cDate = cDate;
		this.studentName=studentName;
		this.staffName=staffName;
	}
	public SentMessagesModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Transient
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	@Transient
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	@Transient
	public List<SentEmailModel> getSentEmail() {
		return sentEmail;
	}
	public void setSentEmail(List<SentEmailModel> sentEmail) {
		this.sentEmail = sentEmail;
	}
	
	
	
}
