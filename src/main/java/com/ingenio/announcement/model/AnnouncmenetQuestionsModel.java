package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcement_questions")
public class AnnouncmenetQuestionsModel {

	private Integer announcementQuestionsId;
	private AnnouncementModel announcementModel;
	private SchoolMasterModel schoolMasterModel;
	private String questions;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "announcementQuestionsId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementQuestionsId() {
		return announcementQuestionsId;
	}
	
	public void setAnnouncementQuestionsId(Integer announcementQuestionsId) {
		this.announcementQuestionsId = announcementQuestionsId;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="announcementId")
	@ElementCollection(targetClass=AnnouncementModel.class)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}
	
	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@Column(name = "questions")
	public String getQuestions() {
		return questions;
	}

	public void setQuestions(String questions) {
		this.questions = questions;
	}
	
	
	
}
