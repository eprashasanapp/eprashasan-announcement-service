package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 * IcardMaster entity. @author MyEclipse Persistence Tools
 */
@Entity
@DynamicUpdate
@Table(name = "icard_master")
public class IcardMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer icardId;
	private Integer studId;
	private Integer renewId;
	private String regno;
	private String imagePath;
	private Integer isDel=0;
	private Integer isEdit;
	private SchoolMasterModel schoolId;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	private String originalName;

	/** default constructor */
	public IcardMaster() {
	}

	/** minimal constructor */
	public IcardMaster(Integer studId, Integer renewId, String regno) {
		this.studId = studId;
		this.renewId = renewId;
		this.regno = regno;
	}

	/** full constructor */
	public IcardMaster(Integer icardId, Integer studId, Integer renewId, String regno, String image, Integer isDel, Integer isEdit, SchoolMasterModel schoolId, AppUserRoleModel userId, Date cDate, Date delDate, AppUserRoleModel deleteBy, Date editDate, AppUserRoleModel editBy, String deviceType, String ipAddress, String macAddress, String sinkingFlag) {
		super();
		this.icardId = icardId;
		this.studId = studId;
		this.renewId = renewId;
		this.regno = regno;
		this.imagePath = image;
		this.isDel = isDel;
		this.isEdit = isEdit;
		this.schoolId = schoolId;
		this.userId = userId;
		this.cDate = cDate;
		this.delDate = delDate;
		this.deleteBy = deleteBy;
		this.editDate = editDate;
		this.editBy = editBy;
		this.deviceType = deviceType;
		this.ipAddress = ipAddress;
		this.macAddress = macAddress;
		this.sinkingFlag = sinkingFlag;
	}

	@Id
	@Column(name = "icardID", unique = true, nullable = false)
	public Integer getIcardId() {
		return this.icardId;
	}

	public void setIcardId(Integer icardId) {
		this.icardId = icardId;
	}

	@Column(name = "studId", nullable = false)
	public Integer getStudId() {
		return this.studId;
	}

	public void setStudId(Integer studId) {
		this.studId = studId;
	}

	@Column(name = "renewId", nullable = false)
	public Integer getRenewId() {
		return this.renewId;
	}

	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}

	@Column(name = "regno", nullable = false, length = 50)
	public String getRegno() {
		return this.regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}
	
	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit")
	public Integer getIsEdit() {
		return this.isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolId() {
		return schoolId;
	}
	
	public void setSchoolId(SchoolMasterModel schoolId) {
		this.schoolId = schoolId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Column(name = "originalName")
	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
}