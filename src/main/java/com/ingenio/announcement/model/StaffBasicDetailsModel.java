package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name = "staff_basicdetails", uniqueConstraints = @UniqueConstraint(columnNames = "sreg_no"))

public class StaffBasicDetailsModel {
	
	private static final long serialVersionUID = 1L;
	private Integer staffId;
	private ReligionMasterModel religionMasterModel;
	private CategoryMasterModel categoryMasterModel;
	private AppUserRoleModel appUserRoleModel;
	private DepartmentModel departmentModel;
	private StaffHandicapTypeModel staffHandicapTypeModel;
	private YearMasterModel yearMasterModel;
	private StaffTypeModel staffTypeModel;
	private StaffPaymentTypeModel staffPaymentTypeModel;
	private DesignationModel designationModel;
	private CasteMasterModel casteMasterModel;
	private StaffSchoolTypeModel staffSchoolTypeModel;
	private String sregNo;
	private String initialname;
	private String firstname;
	private String secondname;
	private String lastname;
	private String birthdate;
	private String birthplace;
	private String gender;
	private String mothertongue;
	private String nationality;
	private String contactnosms;
	private String contactno;
	private String femailId;
	private String laddress;
	private String lcity;
	private String lstate;
	private String lpincode;
	private String lcountry;
	private String paddress;
	private String pcity;
	private String pstate;
	private String ppincode;
	private String pcountry;
	private String joiningDate;
	private String hqualification;
	private byte[] staffImg;
	private String bloodgroup;
	private String height;
	private String weight;
	private String identiMark1;
	private String identiMark2;
	private String flgQuickAdmission;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private Date editDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "staaffId", unique = true, nullable = false)
	public Integer getstaffId() {
		return this.staffId;
	}
	
	public void setstaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "religionId", nullable = false)
	public ReligionMasterModel getReligionMasterModel() {
		return this.religionMasterModel;
	}
	
	public void setReligionMasterModel(ReligionMasterModel religionMasterModel) {
		this.religionMasterModel = religionMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoryId", nullable = false)
	public CategoryMasterModel getCategoryMasterModel() {
		return this.categoryMasterModel;
	}
	
	public void setCategoryMasterModel(CategoryMasterModel categoryMasterModel) {
		this.categoryMasterModel = categoryMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getAppUserRoleModel() {
		return this.appUserRoleModel;
	}
	
	public void setAppUserRoleModel(AppUserRoleModel appUserRoleModel) {
		this.appUserRoleModel = appUserRoleModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "departmentId", nullable = false)
	public DepartmentModel getDepartmentModel() {
		return this.departmentModel;
	}
	
	public void setDepartmentModel(DepartmentModel departmentModel) {
		this.departmentModel = departmentModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "handicapId", nullable = false)
	public StaffHandicapTypeModel getStaffHandicapTypeModel() {
		return this.staffHandicapTypeModel;
	}
	
	public void setStaffHandicapTypeModel(StaffHandicapTypeModel staffHandicapTypeModel) {
		this.staffHandicapTypeModel = staffHandicapTypeModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", nullable = false)
	public YearMasterModel getYearMasterModel() {
		return this.yearMasterModel;
	}
	
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stafftypeId", nullable = false)
	public StaffTypeModel getStaffTypeModel() {
		return this.staffTypeModel;
	}
	
	public void setStaffTypeModel(StaffTypeModel staffTypeModel) {
		this.staffTypeModel = staffTypeModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "paymenttypeId", nullable = false)
	public StaffPaymentTypeModel getStaffPaymenttypeModel() {
		return this.staffPaymentTypeModel;
	}
	
	public void setStaffPaymenttypeModel(StaffPaymentTypeModel staffPaymentTypeModel) {
		this.staffPaymentTypeModel = staffPaymentTypeModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "designationId", nullable = false)
	public DesignationModel getDesignationModel() {
		return this.designationModel;
	}
	
	public void setDesignationModel(DesignationModel designationModel) {
		this.designationModel = designationModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "casteId", nullable = false)
	public CasteMasterModel getCasteMasterModel() {
		return this.casteMasterModel;
	}
	
	public void setCasteMasterModel(CasteMasterModel casteMasterModel) {
		this.casteMasterModel = casteMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stafSchooltypeId", nullable = false)
	public StaffSchoolTypeModel getStaffSchoolTypeModel() {
		return this.staffSchoolTypeModel;
	}
	
	public void setStaffSchoolTypeModel(StaffSchoolTypeModel staffSchoolTypeModel) {
		this.staffSchoolTypeModel = staffSchoolTypeModel;
	}
	
	@Column(name = "sreg_no", unique = true, nullable = false, length = 50)
	public String getSregNo() {
		return this.sregNo;
	}
	
	public void setSregNo(String sregNo) {
		this.sregNo = sregNo;
	}
	
	@Column(name = "initialname", length = 50)
	public String getInitialname() {
		return this.initialname;
	}
	
	public void setInitialname(String initialname) {
		this.initialname = initialname;
	}
	
	@Column(name = "firstname", length = 200)
	public String getFirstname() {
		return this.firstname;
	}
	
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	@Column(name = "secondname", length = 200)
	public String getSecondname() {
		return this.secondname;
	}
	
	public void setSecondname(String secondname) {
		this.secondname = secondname;
	}
	
	@Column(name = "lastname", length = 200)
	public String getLastname() {
		return this.lastname;
	}
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	@Column(name = "birthdate", length = 200)
	public String getBirthdate() {
		return this.birthdate;
	}
	
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	@Column(name = "birthplace", length = 200)
	public String getBirthplace() {
		return this.birthplace;
	}
	
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	
	@Column(name = "gender", length = 50)
	public String getGender() {
		return this.gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(name = "mothertongue", length = 200)
	public String getMothertongue() {
		return this.mothertongue;
	}
	
	public void setMothertongue(String mothertongue) {
		this.mothertongue = mothertongue;
	}
	
	@Column(name = "nationality", length = 200)
	public String getNationality() {
		return this.nationality;
	}
	
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	@Column(name = "contactnosms", length = 200)
	public String getContactnosms() {
		return this.contactnosms;
	}
	
	public void setContactnosms(String contactnosms) {
		this.contactnosms = contactnosms;
	}
	
	@Column(name = "contactno", length = 200)
	public String getContactno() {
		return this.contactno;
	}
	
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	
	@Column(name = "femailId", length = 200)
	public String getFemailId() {
		return this.femailId;
	}
	
	public void setFemailId(String femailId) {
		this.femailId = femailId;
	}
	
	@Column(name = "laddress", length = 600)
	public String getLaddress() {
		return this.laddress;
	}
	
	public void setLaddress(String laddress) {
		this.laddress = laddress;
	}
	
	@Column(name = "lcity", length = 50)
	public String getLcity() {
		return this.lcity;
	}
	
	public void setLcity(String lcity) {
		this.lcity = lcity;
	}
	
	@Column(name = "lstate", length = 50)
	public String getLstate() {
		return this.lstate;
	}
	
	public void setLstate(String lstate) {
		this.lstate = lstate;
	}
	
	@Column(name = "lpincode", length = 50)
	public String getLpincode() {
		return this.lpincode;
	}
	
	public void setLpincode(String lpincode) {
		this.lpincode = lpincode;
	}
	
	@Column(name = "lcountry", length = 50)
	public String getLcountry() {
		return this.lcountry;
	}
	
	public void setLcountry(String lcountry) {
		this.lcountry = lcountry;
	}
	
	@Column(name = "paddress", length = 600)
	public String getPaddress() {
		return this.paddress;
	}
	
	public void setPaddress(String paddress) {
		this.paddress = paddress;
	}
	
	@Column(name = "pcity", length = 50)
	public String getPcity() {
		return this.pcity;
	}
	
	public void setPcity(String pcity) {
		this.pcity = pcity;
	}
	
	@Column(name = "pstate", length = 50)
	public String getPstate() {
		return this.pstate;
	}
	
	public void setPstate(String pstate) {
		this.pstate = pstate;
	}
	
	@Column(name = "ppincode", length = 50)
	public String getPpincode() {
		return this.ppincode;
	}
	
	public void setPpincode(String ppincode) {
		this.ppincode = ppincode;
	}
	
	@Column(name = "pcountry", length = 50)
	public String getPcountry() {
		return this.pcountry;
	}
	
	public void setPcountry(String pcountry) {
		this.pcountry = pcountry;
	}
	
	@Column(name = "joiningDate", length = 200)
	public String getJoiningDate() {
		return this.joiningDate;
	}
	
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	
	@Column(name = "hqualification", length = 50)
	public String getHqualification() {
		return this.hqualification;
	}
	
	public void setHqualification(String hqualification) {
		this.hqualification = hqualification;
	}
	
	@Column(name = "staffImg")
	public byte[] getStaffImg() {
		return this.staffImg;
	}
	
	public void setStaffImg(byte[] staffImg) {
		this.staffImg = staffImg;
	}
	
	@Column(name = "bloodgroup", length = 10)
	public String getBloodgroup() {
		return this.bloodgroup;
	}
	
	public void setBloodgroup(String bloodgroup) {
		this.bloodgroup = bloodgroup;
	}
	
	@Column(name = "height", length = 10)
	public String getHeight() {
		return this.height;
	}
	
	public void setHeight(String height) {
		this.height = height;
	}
	
	@Column(name = "weight", length = 10)
	public String getWeight() {
		return this.weight;
	}
	
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	@Column(name = "identiMark1", length = 200)
	public String getIdentiMark1() {
		return this.identiMark1;
	}
	
	public void setIdentiMark1(String identiMark1) {
		this.identiMark1 = identiMark1;
	}
	
	@Column(name = "identiMark2", length = 200)
	public String getIdentiMark2() {
		return this.identiMark2;
	}
	
	public void setIdentiMark2(String identiMark2) {
		this.identiMark2 = identiMark2;
	}
	
	@Column(name = "FLG_QUICK_ADMISSION", columnDefinition = "char(1) default 1")
	public String getFlgQuickAdmission() {
		return flgQuickAdmission;
	}
	
	public void setFlgQuickAdmission(String flgQuickAdmission) {
		this.flgQuickAdmission = flgQuickAdmission;
	}
	
	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffSubjectdetails> getStaffSubjectdetailses() {
		return this.staffSubjectdetailses;
	}
	
	public void setStaffSubjectdetailses(Set<StaffSubjectdetails> staffSubjectdetailses) {
		this.staffSubjectdetailses = staffSubjectdetailses;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffUidDetails> getStaffUidDetailses() {
		return this.staffUidDetailses;
	}
	
	public void setStaffUidDetailses(Set<StaffUidDetails> staffUidDetailses) {
		this.staffUidDetailses = staffUidDetailses;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffExperienceDetails> getStaffExperienceDetailses() {
		return this.staffExperienceDetailses;
	}
	
	public void setStaffExperienceDetailses(Set<StaffExperienceDetails> staffExperienceDetailses) {
		this.staffExperienceDetailses = staffExperienceDetailses;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffDependantDetails> getStaffDependantDetailses() {
		return this.staffDependantDetailses;
	}
	
	public void setStaffDependantDetailses(Set<StaffDependantDetails> staffDependantDetailses) {
		this.staffDependantDetailses = staffDependantDetailses;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffPaymentDetails> getStaffPaymentDetailses() {
		return this.staffPaymentDetailses;
	}
	
	public void setStaffPaymentDetailses(Set<StaffPaymentDetails> staffPaymentDetailses) {
		this.staffPaymentDetailses = staffPaymentDetailses;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffBankDetails> getStaffBankDetailses() {
		return this.staffBankDetailses;
	}
	
	public void setStaffBankDetailses(Set<StaffBankDetails> staffBankDetailses) {
		this.staffBankDetailses = staffBankDetailses;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "staffBasicdetails")
	public Set<StaffAttachmentModel> getStaffAttachmentMap() {
		return staffAttachmentMap;
	}
	
	public void setStaffAttachmentMap(Set<StaffAttachmentModel> staffAttachmentMap) {
		this.staffAttachmentMap = staffAttachmentMap;
	}
	*/
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
}