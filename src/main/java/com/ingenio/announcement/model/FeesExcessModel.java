package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="fee_excess_master")
public class FeesExcessModel {
	private Integer feeExcessId;
	private String feeExcessName;
	private AppUserModel appUserRole;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private Date delDate;
	private String isEdit="0";
	private Date editDate;
	private String sinkingFlag="0";
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private SchoolMasterModel schoolMaster;
	private AppUserModel deleteBy;
	private AppUserModel editBy;
    private AppUserModel approvalBy;
	private String isApproval="0";
	private Date approvalDate;
	
	
	@Id
	@Column(name = "feeExcessId", nullable = false)
	public Integer getFeeExcessId() {
		return feeExcessId;
	}

	public void setFeeExcessId(Integer feeExcessId) {
		this.feeExcessId = feeExcessId;
	}

	
	@Column(name="feeExcessName", length=50)
	public String getFeeExcessName() {
		return feeExcessName;
	}

	public void setFeeExcessName(String feeExcessName) {
		this.feeExcessName = feeExcessName;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserModel getAppUserRole() {
		return appUserRole;
	}
	
	public void setAppUserRole(AppUserModel appUserRole) {
		this.appUserRole = appUserRole;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate(){
		return cDate;
	}

	public void setcDate(Date cDate){
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel(){
		return isDel;
	}

	public void setIsDel(String isDel){
		this.isDel = isDel;
	}
	
	@Column(name="delDate")
	public Date getDelDate(){
		return delDate;
	}

	public void setDelDate(Date delDate){
		this.delDate = delDate;
	}
	
	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit(){
		return isEdit;
	}

	public void setIsEdit(String isEdit){
		this.isEdit = isEdit;
	}
	
	@Column(name="editDate")
	public Date getEditDate(){
		return editDate;
	}

	public void setEditDate(Date editDate){
		this.editDate = editDate;
	}
	
	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag(){
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag){
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserModel editBy) {
		this.editBy = editBy;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}


	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name = "deviceType", nullable=false)
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable=false)
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable=false)
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}
