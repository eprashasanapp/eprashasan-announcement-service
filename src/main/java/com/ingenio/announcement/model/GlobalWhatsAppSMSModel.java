package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="global_whatsAppSMS_Setting")
public class GlobalWhatsAppSMSModel {
	private Integer tabId;
	private String instanceId;
	private String accessToken;
	private Integer language;
	private String messageFormat;
	private String imagePath;
	private String assignMsgCount;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tabId", unique = true, nullable = false)
	public Integer getTabId() {
		return tabId;
	}
	public void setTabId(Integer tabId) {
		this.tabId = tabId;
	}
	@Column(name="instanceId")
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
	@Column(name="accessToken")
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	@Column(name="messageFormat")
	public String getMessageFormat() {
		return messageFormat;
	}
	public void setMessageFormat(String messageFormat) {
		this.messageFormat = messageFormat;
	}
	
	@Column(name="imagePath")
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	@Column(name="language")
	public Integer getLanguage() {
		return language;
	}
	public void setLanguage(Integer language) {
		this.language = language;
	}
	
	@Column(name="assignMsgCount")
	public String getAssignMsgCount() {
		return assignMsgCount;
	}
	public void setAssignMsgCount(String assignMsgCount) {
		this.assignMsgCount = assignMsgCount;
	}
	
	
}
