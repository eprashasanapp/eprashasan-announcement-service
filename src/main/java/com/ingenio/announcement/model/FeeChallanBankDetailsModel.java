package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "challanbankdetails")
public class FeeChallanBankDetailsModel {

	private static final long serialVersionUID = 1L;
	private Integer bankId;
	private String bankName;
	private String branchName;
	private String ifscNo;
	private String bankAccNo;
	private String accHolderName;
	private String bankAddress;
	private String challanName;
	private String accType;
	private AppUserRoleModel appUserRole;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cdate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private Date delDate;
	private String isEdit="0";
	private Date editDate;
	private String sinkingFlag="0";
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;


	public FeeChallanBankDetailsModel() {
	}

	public FeeChallanBankDetailsModel(String bankName, String branchName,
			String ifscNo, String bankAccNo, String accHolderName,
			String bankAddress, String challanName, String accType) {
		this.bankName = bankName;
		this.branchName = branchName;
		this.ifscNo = ifscNo;
		this.bankAccNo = bankAccNo;
		this.accHolderName = accHolderName;
		this.bankAddress = bankAddress;
		this.challanName = challanName;
		this.accType = accType;
	}

	@Id
	@Column(name = "bankId", unique = true, nullable = false)
	public Integer getBankId() {
		return this.bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	@Column(name = "bankName", nullable = false, length = 100)
	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name = "branchName", nullable = false, length = 100)
	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	@Column(name = "ifscNo", nullable = false, length = 50)
	public String getIfscNo() {
		return this.ifscNo;
	}

	public void setIfscNo(String ifscNo) {
		this.ifscNo = ifscNo;
	}

	@Column(name = "bankAccNo", nullable = false, length = 400)
	public String getBankAccNo() {
		return this.bankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}

	@Column(name = "accHolderName", nullable = false, length = 100)
	public String getAccHolderName() {
		return this.accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	@Column(name = "bankAddress", nullable = false, length = 400)
	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	
	@Column(name = "challanName", nullable = false, length = 50)
	public String getChallanName() {
		return this.challanName;
	}

	public void setChallanName(String challanName) {
		this.challanName = challanName;
	}

	@Column(name = "accType", nullable = false, length = 1)
	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return appUserRole;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRole) {
		this.appUserRole = appUserRole;
	}

	@Column(name = "cDate", updatable= false)
	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = true)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = true)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

}