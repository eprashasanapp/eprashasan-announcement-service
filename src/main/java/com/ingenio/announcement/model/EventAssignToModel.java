package com.ingenio.announcement.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "event_assign_to")

public class EventAssignToModel {

	private Integer eventAssignToId;
	private String eventStatus;
	private Date approvalDate;
	private EventModel eventModel;
	private SchoolMasterModel schoolMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private DivisionMasterModel divisionMasterModel;
	private Timestamp cDate;
	private String isDel="0";
	private String isEdit="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;   
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	private AppUserRoleModel userId;
	private Integer staffStudentId;
	private String role="0";
	
	
	
	@Id
	@Column(name = "eventAssignToId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getEventAssignToId() {
		return eventAssignToId;
	}
	public void setEventAssignToId(Integer eventAssignToId) {
		this.eventAssignToId = eventAssignToId;
	}
	
	@Column(name="eventStatus", columnDefinition="CHAR default '0'", length=1)
	public String getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	@Column(name="role",nullable = false, length = 20)
	//@Length(max = 20)
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	@Column(name="staffStudentId")
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "eventId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="eventId")
	@ElementCollection(targetClass=EventModel.class)
	public EventModel getEventModel() {
		return eventModel;
	}
	public void setEventModel(EventModel eventModel) {
		this.eventModel = eventModel;
	}	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate",nullable = true, updatable= false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public Timestamp getCdate() {
		return this.cDate;
	}

	public void setCdate(Timestamp cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="yearId")
	@ElementCollection(targetClass=YearMasterModel.class)
	public YearMasterModel getYearMasterModel() {
		return this.yearMasterModel;
	}
	
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="standardId")
	@ElementCollection(targetClass=StandardMasterModel.class)
	public StandardMasterModel getStandardMasterModel() {
		return this.standardMasterModel;
	}
	
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divisionId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="divisionId")
	@ElementCollection(targetClass=DivisionMasterModel.class)
	public DivisionMasterModel getDivisionMasterModel() {
		return this.divisionMasterModel;
	}
	
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	
	
}
