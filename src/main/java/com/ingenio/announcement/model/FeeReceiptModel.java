package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity

@Table(name = "fee_receipt")
public class FeeReceiptModel {

	private static final long serialVersionUID = 1L;
	private Integer receiptId;
	private String receiptNo;
	private Integer isconsolidation;
	private Integer isDel=0;
	private Integer isEdit;
	private Integer headId;
	private String payTypeOrPrintFlag;
	private String returnFlag;
	private String reconciliationFlag;
	private String bounceClearFlag;
	private String discardFlag;
	private Date payTypeOrPrintDate;
	private Date bankPaidDate;
	private Date returnByUserDate;
	private Date reconciliationDate;
	private Date bounceClearDate;
	private Date discardDate;
	private Integer bankId;
	private YearMasterModel yearMaster;
	private String remark;
	private String sinkingFlag="0";
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	private TypeOfUnitModel typeOfUnitModel;
	private UnitMappToUserModel unitMappToUserModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private Integer studentId;
	private String typeOfReceipt;
	private double totalAmount;
	private double totalPayFine;
	private double totalPayDiscount;
	private double totalPayExcessFee;
	private String isSocietyReceipt;
	private String existingStudent;
	private String rCode;
	@Id
	@Column(name = "receiptID", unique = true, nullable = false)
	public Integer getReceiptId() {
		return this.receiptId;
	}

	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}

	@Column(name = "receiptNo", nullable = false, length = 50)
	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	@Column(name = "isconsolidation", nullable = false)
	public Integer getIsconsolidation() {
		return this.isconsolidation;
	}

	public void setIsconsolidation(Integer isconsolidation) {
		this.isconsolidation = isconsolidation;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit")
	public Integer getIsEdit() {
		return this.isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "headId", nullable = false)
	public Integer getHeadId() {
		return this.headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	@Column(name="payTypeOrPrintFlag")
	public String getPayTypeOrPrintFlag() {
		return payTypeOrPrintFlag;
	}

	public void setPayTypeOrPrintFlag(String payTypeOrPrintFlag) {
		this.payTypeOrPrintFlag = payTypeOrPrintFlag;
	}

	@Column(name="returnFlag")
	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	@Column(name="reconciliationFlag")
	public String getReconciliationFlag() {
		return reconciliationFlag;
	}

	public void setReconciliationFlag(String reconciliationFlag) {
		this.reconciliationFlag = reconciliationFlag;
	}
	
	@Column(name="reconciliationDate")
	public Date getReconciliationDate() {
		return reconciliationDate;
	}

	public void setReconciliationDate(Date reconciliationDate) {
		this.reconciliationDate = reconciliationDate;
	}
	
	@Column(name="bounceClearFlag")
	public String getBounceClearFlag() {
		return bounceClearFlag;
	}

	public void setBounceClearFlag(String bounceClearFlag) {
		this.bounceClearFlag = bounceClearFlag;
	}

	@Column(name="discardFlag")
	public String getDiscardFlag() {
		return discardFlag;
	}

	public void setDiscardFlag(String discardFlag) {
		this.discardFlag = discardFlag;
	}

	@Column(name="payTypeOrPrintDate")
	public Date getPayTypeOrPrintDate() {
		return payTypeOrPrintDate;
	}

	public void setPayTypeOrPrintDate(Date payTypeOrPrintDate) {
		this.payTypeOrPrintDate = payTypeOrPrintDate;
	}
	
	@Column(name="bankPaidDate")
	public Date getBankPaidDate() {
		return bankPaidDate;
	}

	public void setBankPaidDate(Date bankPaidDate) {
		this.bankPaidDate = bankPaidDate;
	}

	@Column(name="returnByUserDate")
	public Date getReturnByUserDate() {
		return returnByUserDate;
	}

	public void setReturnByUserDate(Date returnByUserDate) {
		this.returnByUserDate = returnByUserDate;
	}

	@Column(name="bounceClearDate")
	public Date getBounceClearDate() {
		return bounceClearDate;
	}

	public void setBounceClearDate(Date bounceClearDate) {
		this.bounceClearDate = bounceClearDate;
	}

	@Column(name="discardDate")
	public Date getDiscardDate() {
		return discardDate;
	}

	public void setDiscardDate(Date discardDate) {
		this.discardDate = discardDate;
	}

	@Column(name = "bankId",nullable=true)
	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public YearMasterModel getYearMaster() {
		return yearMaster;
	}


	public void setYearMaster(YearMasterModel yearMaster) {
		this.yearMaster = yearMaster;
	}

	public String getRemark() {
		return remark;
	}

	@Column(name="remark")
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name="sinkingFlag")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy")
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getTypeOfReceipt() {
		return typeOfReceipt;
	}

	public void setTypeOfReceipt(String typeOfReceipt) {
		this.typeOfReceipt = typeOfReceipt;
	}

	@Column(name="totalAmount")
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "typeOfUnitId", columnDefinition="default '0'")
	public TypeOfUnitModel getTypeOfUnitModel() {
		return typeOfUnitModel;
	}

	public void setTypeOfUnitModel(TypeOfUnitModel typeOfUnitModel) {
		this.typeOfUnitModel = typeOfUnitModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "registerId", columnDefinition="default '0'")
	public UnitMappToUserModel getUnitMappToUserModel() {
		return unitMappToUserModel;
	}
	
	public void setUnitMappToUserModel(UnitMappToUserModel unitMappToUserModel) {
		this.unitMappToUserModel = unitMappToUserModel;
	}

	@Column(name="totalPayFine")
	public double getTotalPayFine() {
		return totalPayFine;
	}

	public void setTotalPayFine(double totalPayFine) {
		this.totalPayFine = totalPayFine;
	}

	@Column(name="totalPayDiscount")
	public double getTotalPayDiscount() {
		return totalPayDiscount;
	}

	public void setTotalPayDiscount(double totalPayDiscount) {
		this.totalPayDiscount = totalPayDiscount;
	}

	@Column(name="totalPayExcessFee")
	public double getTotalPayExcessFee() {
		return totalPayExcessFee;
	}

	public void setTotalPayExcessFee(double totalPayExcessFee) {
		this.totalPayExcessFee = totalPayExcessFee;
	}

	@Column(name="isSocietyReceipt")
	public String getIsSocietyReceipt() {
		return isSocietyReceipt;
	}

	public void setIsSocietyReceipt(String isSocietyReceipt) {
		this.isSocietyReceipt = isSocietyReceipt;
	}
	@Column(name="exstingStudentOrOtherStudent")
	public String getExistingStudent() {
		return existingStudent;
	}

	public void setExistingStudent(String existingStudent) {
		this.existingStudent = existingStudent;
	}

	@Column(name="rCode",unique = true,nullable = true)
	public String getrCode() {
		return rCode;
	}

	public void setrCode(String rCode) {
		this.rCode = rCode;
	}

	

	
}