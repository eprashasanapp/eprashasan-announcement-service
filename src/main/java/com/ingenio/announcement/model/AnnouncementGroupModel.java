package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcement_group")
public class AnnouncementGroupModel {

	private Integer announcementGroupId;
	private String announcementGroupName;
	private SchoolMasterModel schoolMasterModel;
	
	@Id
	@Column(name = "announcementGroupId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementGroupId() {
		return announcementGroupId;
	}
	public void setAnnouncementGroupId(Integer announcementGroupId) {
		this.announcementGroupId = announcementGroupId;
	}
	
	@Column(name="announcementGroupName",nullable = false, length = 50)
	public String getAnnouncementGroupName() {
		return announcementGroupName;
	}
	public void setAnnouncementGroupName(String announcementGroupName) {
		this.announcementGroupName = announcementGroupName;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}





}
