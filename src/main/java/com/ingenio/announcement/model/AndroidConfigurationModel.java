package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="android_configuration")

public class AndroidConfigurationModel {

	@Id
    @Column(name="androidId", nullable=false)
	private Integer androidId;
	private Integer offset;
	private String currentVersion;
	
	@Column(name = "offset")
	public Integer getAndroidId() {
		return androidId;
	}
	public void setAndroidId(Integer androidId) {
		this.androidId = androidId;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	@Column(name = "currentVersion",length=50)
	public String getCurrentVersion() {
		return currentVersion;
	}
	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}
	
	
}
