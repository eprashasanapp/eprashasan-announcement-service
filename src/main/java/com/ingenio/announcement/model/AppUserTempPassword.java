package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="app_user_temp_password")
public class AppUserTempPassword {

	
	private Integer tempPassId;
	private String mobileNumber;
	private String password;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private String ipAddress;
	private String longitude;
	private String latitude;
	private Integer schoolid=0;
	private Integer isPassUpdate=0;
	private Date updateTimeStamp;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getTempPassId() {
		return tempPassId;
	}
	public void setTempPassId(Integer tempPassId) {
		this.tempPassId = tempPassId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
	
	public Integer getIsPassUpdate() {
		return isPassUpdate;
	}
	public void setIsPassUpdate(Integer isPassUpdate) {
		this.isPassUpdate = isPassUpdate;
	}
	public Date getUpdateTimeStamp() {
		return updateTimeStamp;
	}
	public void setUpdateTimeStamp(Date updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}
	
	
}
