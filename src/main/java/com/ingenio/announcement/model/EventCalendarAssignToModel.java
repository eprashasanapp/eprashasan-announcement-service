package com.ingenio.announcement.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "event_calendar__assign_to")
public class EventCalendarAssignToModel {

	private Integer eventCalendarAssignToId;
	private EventModel eventModel;
	private StandardMasterModel standardMasterModel;
	private DivisionMasterModel divisionMasterModel;
	private Timestamp cDate;
	private YearMasterModel yearMasterModel;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "eventCalendarAssignToId", nullable = false, columnDefinition = "INT(11) UNSIGNED")
	public Integer getEventCalendarAssignToId() {
		return eventCalendarAssignToId;
	}

	public void setEventCalendarAssignToId(Integer eventCalendarAssignToId) {
		this.eventCalendarAssignToId = eventCalendarAssignToId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "eventId")
	public EventModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(EventModel eventModel) {
		this.eventModel = eventModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId")
	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}

	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}

	@Column(name = "cDate")
	public Timestamp getcDate() {
		return cDate;
	}

	public void setcDate(Timestamp cDate) {
		this.cDate = cDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}

	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divisionId")
	public DivisionMasterModel getDivisionMasterModel() {
		return divisionMasterModel;
	}

	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}

}
