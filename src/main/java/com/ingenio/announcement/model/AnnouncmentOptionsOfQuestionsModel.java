package com.ingenio.announcement.model;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcement_optionsofQuestions")
public class AnnouncmentOptionsOfQuestionsModel {

	private Integer announcementOptionsofQuestionsId;
	private AnnouncementModel announcementModel;
	private AnnouncmenetQuestionsModel announcmenetQuestionsModel;
	private SchoolMasterModel schoolMasterModel;
	private String options;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "announcementOptionsofQuestionsId", nullable = false, columnDefinition = "INT(11) UNSIGNED")
	public Integer getAnnouncementOptionsofQuestionsId() {
		return announcementOptionsofQuestionsId;
	}

	public void setAnnouncementOptionsofQuestionsId(Integer announcementOptionsofQuestionsId) {
		this.announcementOptionsofQuestionsId = announcementOptionsofQuestionsId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementQuestionsId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL", referencedColumnName = "announcementQuestionsId")
	@ElementCollection(targetClass = AnnouncmenetQuestionsModel.class)
	public AnnouncmenetQuestionsModel getAnnouncmenetQuestionsModel() {
		return announcmenetQuestionsModel;
	}

	public void setAnnouncmenetQuestionsModel(AnnouncmenetQuestionsModel announcmenetQuestionsModel) {
		this.announcmenetQuestionsModel = announcmenetQuestionsModel;
	}

	@Column(name = "optionName")
	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "announcementId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL", referencedColumnName = "announcementId")
	@ElementCollection(targetClass = AnnouncementModel.class)
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}

	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL", referencedColumnName = "schoolid")
	@ElementCollection(targetClass = SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	

}
