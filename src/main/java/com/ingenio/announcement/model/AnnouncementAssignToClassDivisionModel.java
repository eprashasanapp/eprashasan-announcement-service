package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="announcement_assign_to_class_division")
public class AnnouncementAssignToClassDivisionModel {

	private Integer announcementAssigntoClassDivisionId;
	private AnnouncementModel announcementModel;
	private SchoolMasterModel schoolMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private DivisionMasterModel divisionMasterModel;
	private Integer selectionFlag;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "announcementAssigntoClassDivisionId", unique = true, nullable = false)
	public Integer getAnnouncementAssigntoClassDivisionId() {
		return announcementAssigntoClassDivisionId;
	}
	public void setAnnouncementAssigntoClassDivisionId(Integer announcementAssigntoClassDivisionId) {
		this.announcementAssigntoClassDivisionId = announcementAssigntoClassDivisionId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="announcementId")
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}
	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="schoolId")
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="yearId")
	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="standardId")
	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="divisionId")
	public DivisionMasterModel getDivisionMasterModel() {
		return divisionMasterModel;
	}
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	
	@Column(name="selectionFlag")
	public Integer getSelectionFlag() {
		return selectionFlag;
	}
	public void setSelectionFlag(Integer selectionFlag) {
		this.selectionFlag = selectionFlag;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
}
