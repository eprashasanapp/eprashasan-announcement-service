package com.ingenio.announcement.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity

@Table(name="fee_setting_multi")
public class FeeSettingMultiModel  {

	 private static final long serialVersionUID = 1L;
     private BigInteger feeSetMultiId;
     private AppUserRoleModel appUserRoleModel;
     private SchoolMasterModel schoolMasterModel;
     private FeeSettingMultiNarrationModel feeSettingMultiNarrationModel;
     private FeeHeadMasterModel feeHeadMasterModel;
     private FeeSettingModel feeSettingModel;
     private FeeSubheadMasterModel feeSubheadMasterModel;
     private Double assignFee;
     private Double fine;
     private String cdate;
     private String dueDate;
     private Integer fineIncreament=0;
     private Integer isConsolation;
     private String isDel="0";
 	 private String isEdit="0";
  	 private Date delDate;
 	 private AppUserRoleModel deleteBy;
 	 private Date editDate;
 	 private AppUserRoleModel editBy;
 	 private String isApproval="0";
     private AppUserRoleModel approvalBy;
     private Date approvalDate;
 	 private String deviceType="0";
 	 private String ipAddress;
 	 private String macAddress;
 	 private String sinkingFlag="0";
 	 private Double preDiscount;
 	 
    @Id 
    @Column(name="feeSetMultiID", unique=true, nullable=false)
    public BigInteger getFeeSetMultiId() {
        return this.feeSetMultiId;
    }
    
    public void setFeeSetMultiId(BigInteger feeSetMultiId) {
        this.feeSetMultiId = feeSetMultiId;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="userID", nullable=false)
    public AppUserRoleModel getAppUserRoleModel() {
        return this.appUserRoleModel;
    }
    
    public void setAppUserRoleModel(AppUserRoleModel appUserRoleModel) {
        this.appUserRoleModel = appUserRoleModel;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="schoolID")
    public SchoolMasterModel getSchoolMasterModel() {
        return this.schoolMasterModel;
    }
    
    public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
        this.schoolMasterModel = schoolMasterModel;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="narrationID")
    public FeeSettingMultiNarrationModel getFeeSettingMultiNarrationModel() {
        return this.feeSettingMultiNarrationModel;
    }
    
    public void setFeeSettingMultiNarrationModel(FeeSettingMultiNarrationModel feeSettingMultiNarrationModel) {
        this.feeSettingMultiNarrationModel = feeSettingMultiNarrationModel;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="headID", nullable=false)

    public FeeHeadMasterModel getFeeHeadMasterModel() {
        return this.feeHeadMasterModel;
    }
    
    public void setFeeHeadMasterModel(FeeHeadMasterModel feeHeadMasterModel) {
        this.feeHeadMasterModel = feeHeadMasterModel;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="feeSettingID", nullable=false)
    public FeeSettingModel getFeeSettingModel() {
        return this.feeSettingModel;
    }
    
    public void setFeeSettingModel(FeeSettingModel feeSettingModel) {
        this.feeSettingModel = feeSettingModel;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="subheadID", nullable=false)
    public FeeSubheadMasterModel getFeeSubheadMasterModel() {
        return this.feeSubheadMasterModel;
    }
    
    public void setFeeSubheadMasterModel(FeeSubheadMasterModel feeSubheadMasterModel) {
        this.feeSubheadMasterModel = feeSubheadMasterModel;
    }
    
    @Column(name="assignFee", precision=22, scale=0)
    public Double getAssignFee() {
        return this.assignFee;
    }
    
    public void setAssignFee(Double assignFee) {
        this.assignFee = assignFee;
    }
    
    @Column(name="fine", precision=22, scale=0)
    public Double getFine() {
        return this.fine;
    }
    
    public void setFine(Double fine) {
        this.fine = fine;
    }
    
    @Column(name="cDate", length=50)
    public String getCdate() {
        return this.cdate;
    }
    
    public void setCdate(String cdate) {
        this.cdate = cdate;
    }
    
    @Column(name="dueDate", length=50)
    public String getDueDate() {
        return this.dueDate;
    }
    
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    
    @Column(name="fineIncreament", nullable=false, columnDefinition="default 0")
    public Integer getFineIncreament() {
        return this.fineIncreament;
    }
    
    public void setFineIncreament(Integer fineIncreament) {
        this.fineIncreament = fineIncreament;
    }
    
    @Column(name="isConsolation", nullable=false)
    public Integer getIsConsolation() {
        return this.isConsolation;
    }
    
    public void setIsConsolation(Integer isConsolation) {
        this.isConsolation = isConsolation;
    }
    
    @Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "deleteBy", nullable = false)
	@JoinColumn(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "editBy", nullable = false)
	@JoinColumn(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "approvalBy", nullable = false)
	@JoinColumn(name = "approvalBy")
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public Double getPreDiscount() {
		return preDiscount;
	}

	public void setPreDiscount(Double preDiscount) {
		this.preDiscount = preDiscount;
	}
	

}