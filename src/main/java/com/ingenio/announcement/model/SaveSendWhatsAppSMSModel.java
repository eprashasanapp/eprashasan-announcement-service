package com.ingenio.announcement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="save_send_whatsAppSMS")
public class SaveSendWhatsAppSMSModel {

	private Integer tabId;
	private Integer schoolId;
	private Integer renewStaffId;
	private String typeFlag;
	private String message;
	private String messageType;
	private String date;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private String mobileNo;
	private AnnouncementModel announcementModel;
	private Integer compaignStatusFlag=0;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tabId", unique = true, nullable = false)
	public Integer getTabId() {
		return tabId;
	}
	public void setTabId(Integer tabId) {
		this.tabId = tabId;
	}
	
	@Column(name="schoolid")
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	
	@Column(name="renewId_staffId")
	public Integer getRenewStaffId() {
		return renewStaffId;
	}
	public void setRenewStaffId(Integer renewStaffId) {
		this.renewStaffId = renewStaffId;
	}
	
	@Column(name="typeFlag")
	public String getTypeFlag() {
		return typeFlag;
	}
	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}
	
	@Column(name="message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name="messageType")
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	@Column(name="date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="mobileNo")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="announcementId")
	public AnnouncementModel getAnnouncementModel() {
		return announcementModel;
	}
	
	public void setAnnouncementModel(AnnouncementModel announcementModel) {
		this.announcementModel = announcementModel;
	}
	
	@Column(name="compaignStatusFlag")
	public Integer getCompaignStatusFlag() {
		return compaignStatusFlag;
	}
	public void setCompaignStatusFlag(Integer compaignStatusFlag) {
		this.compaignStatusFlag = compaignStatusFlag;
	}
	
	
}
