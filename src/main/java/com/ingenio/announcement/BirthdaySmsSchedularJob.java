package com.ingenio.announcement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.GlobalWhatsAppSMSModel;
import com.ingenio.announcement.model.SaveSendWhatsAppSMSModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.SchoolWiseWhatsAppSMSModel;
import com.ingenio.announcement.repository.GlobalWhatsAppSMSRepository;
import com.ingenio.announcement.repository.SaveSendWhatsAppSMSRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.StudentStandardRenewRepository;
import com.ingenio.announcement.util.CommonUtlitity;
import com.ingenio.announcement.util.CryptographyUtil;
import com.ingenio.announcement.util.Utility;


@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")

  @PropertySource("classpath:messages_en.properties")
  
  @PropertySource("classpath:messages_mr.properties")
  
  @PropertySource("classpath:messages_hn.properties")
 
public class BirthdaySmsSchedularJob {

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	CommonUtlitity commonUtility;
	
	@Autowired
	StudentStandardRenewRepository studentStandardRenewRepository;
	
	CryptographyUtil cry = new CryptographyUtil();
	
	@Autowired
	GlobalWhatsAppSMSRepository globalWhatsAppSMSRepository;
	
	@Autowired
	SaveSendWhatsAppSMSRepository saveSendWhatsAppSMSRepository;
	
	Utility utility = new Utility();
	
	@SuppressWarnings("static-access")
	@Scheduled(cron = "0 0 8 * * ?" , zone = "GMT+5:30")
	public void executeNotificationJob() {
		// TODO Auto-generated method stub
		try {
			System.out.println("Birthday scheduler started...");
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			Date dateobj = new Date();
			String date = df.format(dateobj);
//			String date = "05-02-2022";
			String todaysSplit[]=date.split("-");
			List<SchoolMasterModel> schoolList = schoolMasterRepository.findAllSchool();
			schoolList.parallelStream().forEach( school -> {
				List<GlobalWhatsAppSMSModel> settingList=globalWhatsAppSMSRepository.getMessageSettingByLanguage(school.getLanguage().getLanguageId());
				List<SchoolWiseWhatsAppSMSModel> schoolWiseList=globalWhatsAppSMSRepository.getSchoolWiseSetting(school.getSchoolid());
				String instanceId,accessToken,messageFormat,imagePath,assignMsgCount;
				Integer msgCount = 0;
				if(schoolWiseList.size()>0) {
					if(schoolWiseList.get(0).getInstanceId().equalsIgnoreCase("") && schoolWiseList.get(0).getInstanceId().equalsIgnoreCase(null)) {
						instanceId=settingList.get(0).getInstanceId();
					}else {
						instanceId=schoolWiseList.get(0).getInstanceId();
					}
					if(schoolWiseList.get(0).getAccessToken().equalsIgnoreCase("") && schoolWiseList.get(0).getAccessToken().equalsIgnoreCase(null)) {
						accessToken=settingList.get(0).getAccessToken();
					}else {
						accessToken=schoolWiseList.get(0).getAccessToken();
					}
					if(schoolWiseList.get(0).getMessageFormat().equalsIgnoreCase("") && schoolWiseList.get(0).getMessageFormat().equalsIgnoreCase(null)) {
						messageFormat=settingList.get(0).getMessageFormat();
					}else {
						messageFormat=schoolWiseList.get(0).getMessageFormat();	
					}
					if(schoolWiseList.get(0).getImagePath().equalsIgnoreCase("") && schoolWiseList.get(0).getImagePath().equalsIgnoreCase(null)) {
						imagePath=settingList.get(0).getImagePath();
					}else {
						imagePath=schoolWiseList.get(0).getImagePath();	
					}
					if(schoolWiseList.get(0).getAssignMsgCount().toString().equalsIgnoreCase("") && schoolWiseList.get(0).getAssignMsgCount().toString().equalsIgnoreCase(null)) {
						assignMsgCount=settingList.get(0).getAssignMsgCount();
					}else {
						assignMsgCount=schoolWiseList.get(0).getAssignMsgCount();	
					}
				}else {
					instanceId=settingList.get(0).getInstanceId();
					accessToken=settingList.get(0).getAccessToken();
					messageFormat=settingList.get(0).getMessageFormat();
					imagePath=settingList.get(0).getImagePath();
					assignMsgCount=settingList.get(0).getAssignMsgCount();
				}
				
				List<StudentDetailsBean> studentList = studentStandardRenewRepository.getstudentBirthdayList(school.getSchoolid(),date);
				for (StudentDetailsBean studentBean : studentList) {
					boolean flag=false;
					String dateForSplit=studentBean.getBirthDate();
					if(!dateForSplit.equalsIgnoreCase("NA") && !dateForSplit.equalsIgnoreCase("--")) {
						String birthdaySplit[]=dateForSplit.split("-");
						if(birthdaySplit[0].equalsIgnoreCase(todaysSplit[0]) && birthdaySplit[1].equalsIgnoreCase(todaysSplit[1])) {
							if(!studentBean.getMobileNo().equalsIgnoreCase("0") && !studentBean.getMobileNo().equalsIgnoreCase("") && !studentBean.getMobileNo().equalsIgnoreCase(null) && !studentBean.getMobileNo().equalsIgnoreCase("0000000000")) {
									String newSchoolName="+";
									try {
										String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils
												.escapeJava(school.getSchoolName().replaceAll("[\\r\\n]+", "")));
										String newMsg[]=schoolName.split(" ");
										for (String str : newMsg) {
											newSchoolName=newSchoolName.concat("+"+str);
										}
									} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException
											| NoSuchPaddingException | InvalidAlgorithmParameterException
											| IllegalBlockSizeException | BadPaddingException | IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
//									String newSname="+";
//									String StudName[]=studentBean.getStudentName().split(" ");
//									for (String str : StudName) {
//										newSname=newSname.concat("+"+str);
//									}
									LocalDate curDate = LocalDate.now();
									LocalDate localDate = LocalDate.parse(birthdaySplit[2]+"-"+birthdaySplit[1]+"-"+birthdaySplit[0]);
									int currentYear=Period.between(localDate, curDate).getYears();
//									System.out.println("studentBean.getStudentName() = "+studentBean.getStudentName());
									String message = StringUtils.replace(messageFormat, "$studentName", studentBean.getStudentName());
									String newmessage=StringUtils.replace(message, "$age", utility.ordinal(currentYear));
									String newMsg="+";
									String newMssg[]=newmessage.split(" ");
									for (String str : newMssg) {
										newMsg=newMsg.concat("+"+str);
									}
									newMsg=newMsg+"+-"+newSchoolName;
									String mobileNo="91"+utility.convertOtherToEnglish(studentBean.getMobileNo());
//									System.out.println("mobileNo : "+mobileNo);
//									String mobileNo="918149869838";
//									String mediaUrl="https://eprashasan.s3.ap-south-1.amazonaws.com/maindb/Birthday/bday-165.gif";
									if(assignMsgCount == null) {
										flag=true;
									}else {
										if(Integer.parseInt(assignMsgCount)>msgCount) {
											flag=true;
										}
									}
									if(flag) {
										try {
											URL obj = new URL("https://app.wappapi.in/api/send.php?number="+mobileNo+"&type=media&message="+newMsg+"&media_url="+imagePath+"&instance_id="+instanceId+"&access_token="+accessToken+"");
											HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
											postConnection.addRequestProperty("User-Agent", "Chrome");
											postConnection.setRequestMethod("POST");
											postConnection.setDoOutput(true);
											postConnection.connect();
											System.out.println("URL: "+obj+" status code: "+postConnection.getResponseCode());
											if(postConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
												BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
											    String inputLine;
											    StringBuilder response = new StringBuilder();
											    while ((inputLine = in .readLine()) != null) {
											        response.append(inputLine);
											    } in .close();
											    System.out.println("RESPONSE : "+response.toString());
											    if(!response.toString().equalsIgnoreCase(null) && response.toString() != null) {
											    	JSONObject jsonObj = new JSONObject(response.toString());
												    if(!jsonObj.getString("status").equalsIgnoreCase("error")) {
												    	msgCount++;
														SaveSendWhatsAppSMSModel saveSendWhatsAppSMSModel = new SaveSendWhatsAppSMSModel();
														saveSendWhatsAppSMSModel.setDate(date);
														saveSendWhatsAppSMSModel.setMessage(newmessage);
														saveSendWhatsAppSMSModel.setMessageType("1");
														saveSendWhatsAppSMSModel.setTypeFlag("1");
														saveSendWhatsAppSMSModel.setRenewStaffId(studentBean.getRenewStudentId());
														saveSendWhatsAppSMSModel.setSchoolId(school.getSchoolid());
														saveSendWhatsAppSMSModel.setMobileNo(studentBean.getMobileNo());
														saveSendWhatsAppSMSRepository.saveAndFlush(saveSendWhatsAppSMSModel);
												    }
											    }
											}
										} catch (MalformedURLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (ProtocolException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								
							}
							
							
						}
					}
				}
				if(assignMsgCount!=null) {
					Integer totalCount=Integer.parseInt(assignMsgCount)-msgCount;
					globalWhatsAppSMSRepository.updateSchoolAssignMessage(school.getSchoolid(),totalCount.toString());
				}
			});
	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	
	}
}
