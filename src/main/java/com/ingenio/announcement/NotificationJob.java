package com.ingenio.announcement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ingenio.announcement.bean.EventResponseBean;
import com.ingenio.announcement.bean.FCMTokenBean;
import com.ingenio.announcement.model.AndroidNotificationStatusModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.repository.AndroidNotificationStatusRepository;
import com.ingenio.announcement.repository.AssignSubjectToStudentRepository;
import com.ingenio.announcement.repository.EventRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.util.CommonUtlitity;
import com.ingenio.announcement.util.Utility;

@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")

  @PropertySource("classpath:messages_en.properties")
  
  @PropertySource("classpath:messages_mr.properties")
  
  @PropertySource("classpath:messages_hn.properties")
 
public class NotificationJob {

	@Autowired
	EventRepository eventRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	CommonUtlitity commonUtility;

	@Autowired
	private Environment env;

	@Autowired
	AssignSubjectToStudentRepository assignSubjectToStudentRepository;

	@Scheduled(cron = "0 0 8-10 * * *", zone = "GMT+5:30")
	public void executeNotificationJob() {
		System.out.println("In notification");
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date dateobj = new Date();
		System.out.println("Job started");
		String date = df.format(dateobj);
		System.out.println(date);
		List<EventResponseBean> eventList = eventRepository.findEvents(date);
		System.out.println(eventList.size());
		eventList.stream().forEach(event -> {
			List<String> androidTokenList = new ArrayList<>();
			List<String> androidTokenList1 = new ArrayList<>();
			androidTokenList = eventRepository.getAndroidTokenList(event.getStandardId(), event.getDivisionId());
			System.out.println(androidTokenList.size());
			for(int i=12;i<androidTokenList.size();i++) {
				if(androidTokenList.get(i)!=null || !androidTokenList.get(i).equalsIgnoreCase("")) {
					androidTokenList1.add(androidTokenList.get(i));
				}
			}
			System.out.println(androidTokenList1.size());
			Utility.pushNotification(androidTokenList1, event.getEventType(), event.getEventTitle(), "2");

		});
	}
}
