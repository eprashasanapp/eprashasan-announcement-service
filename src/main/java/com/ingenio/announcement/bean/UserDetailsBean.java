package com.ingenio.announcement.bean;

import java.util.List;

import com.ingenio.announcement.model.AnnouncementCommentModel;

public class UserDetailsBean {
	
	private Integer schoolId;
	private String schoolName;
	private String schoolSansthaKey;
	private Integer userId;
	private String roleName;
	private String userName;
	private String activeFlag;
	private String IMEICode;
	private String deviceType;
	private String appRegDate;
	private String appStartDate;
	private String appEndDate;
	private Integer currentYear;
	private String currentYearText;
	
	private String registartionNo;
	private Integer studentId;
	private String initialName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private String caste;
	private String category;
	private String religion;
	private String medium;
	private String mobileNumber;
	private String birthDate;
	private String birthPlace;
	private String nationality;
	private String joiningDate;
	private Integer renewId;
	private Integer staffId;
	private String designation;
	private String department;
	private Integer isClassTeacher;
	private String studentPhotoUrl;
	private Integer grBookId;
	private String fatherName;
	private String motherName;
	private String standardName;
	private String divisionName;
	private Integer rollNo;
	private String schoolLogoUrl;
	private String roleNameForDisplay;
	private String rollColor;
	private Integer standardId;
	private String admissionDate;
	private String studentFormNo;
	private String mothertonge;
	private String localAddress;
	private Integer yearId;
	private String emailId;
	private Integer view;
	private Integer appUserRoleId;
	List<AnnouncementCommentModel> comments;
	public UserDetailsBean() {
		super();
	}

	public UserDetailsBean(String registartionNo,Integer staffId, String initialName, String firstName, String middleName,
			String lastName, String gender, String caste, String category, String religion, String mobileNumber,
			String birthDate, String birthPlace,String nationality, String joiningDate, String designation,
			String department,String emailId,Integer view,Integer appUserRoleId) {
		super();
		this.registartionNo = registartionNo;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.staffId = staffId;
		this.designation = designation;
		this.department = department;
		this.emailId=emailId;
		this.view=view;
		this.appUserRoleId=appUserRoleId;
	}
	
	public String getAdmissionDate() {
		return admissionDate;
	}
	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}
	public String getStudentFormNo() {
		return studentFormNo;
	}
	public void setStudentFormNo(String studentFormNo) {
		this.studentFormNo = studentFormNo;
	}
	public String getMothertonge() {
		return mothertonge;
	}
	public void setMothertonge(String mothertonge) {
		this.mothertonge = mothertonge;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getRegistartionNo() {
		return registartionNo;
	}
	public void setRegistartionNo(String registartionNo) {
		this.registartionNo = registartionNo;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getIMEICode() {
		return IMEICode;
	}
	public void setIMEICode(String iMEICode) {
		IMEICode = iMEICode;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getAppRegDate() {
		return appRegDate;
	}
	public void setAppRegDate(String appRegDate) {
		this.appRegDate = appRegDate;
	}
	public String getAppStartDate() {
		return appStartDate;
	}
	public void setAppStartDate(String appStartDate) {
		this.appStartDate = appStartDate;
	}
	public String getAppEndDate() {
		return appEndDate;
	}
	public void setAppEndDate(String appEndDate) {
		this.appEndDate = appEndDate;
	}
	public Integer getCurrentYear() {
		return currentYear;
	}
	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}
	public String getCurrentYearText() {
		return currentYearText;
	}
	public void setCurrentYearText(String currentYearText) {
		this.currentYearText = currentYearText;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
//	public List<AndroidMenuBean> getAndroidMenuList() {
//		return androidMenuList;
//	}
//	public void setAndroidMenuList(List<AndroidMenuBean> androidMenuList) {
//		this.androidMenuList = androidMenuList;
//	}
	public Integer getIsClassTeacher() {
		return isClassTeacher;
	}
	public void setIsClassTeacher(Integer isClassTeacher) {
		this.isClassTeacher = isClassTeacher;
	}
//	public List<DynamicLabelValueBean> getDynamicLabelValueBeanList() {
//		return dynamicLabelValueBeanList;
//	}
//	public void setDynamicLabelValueBeanList(List<DynamicLabelValueBean> dynamicLabelValueBeanList) {
//		this.dynamicLabelValueBeanList = dynamicLabelValueBeanList;
//	}
	public String getStudentPhotoUrl() {
		return studentPhotoUrl;
	}
	public void setStudentPhotoUrl(String studentPhotoUrl) {
		this.studentPhotoUrl = studentPhotoUrl;
	}
	public Integer getGrBookId() {
		return grBookId;
	}
	public void setGrBookId(Integer grBookId) {
		this.grBookId = grBookId;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public String getSchoolSansthaKey() {
		return schoolSansthaKey;
	}
	public void setSchoolSansthaKey(String schoolSansthaKey) {
		this.schoolSansthaKey = schoolSansthaKey;
	}
	public String getSchoolLogoUrl() {
		return schoolLogoUrl;
	}
	public void setSchoolLogoUrl(String schoolLogoUrl) {
		this.schoolLogoUrl = schoolLogoUrl;
	}
	public String getRoleNameForDisplay() {
		return roleNameForDisplay;
	}
	public void setRoleNameForDisplay(String roleNameForDisplay) {
		this.roleNameForDisplay = roleNameForDisplay;
	}
	public String getRollColor() {
		return rollColor;
	}
	public void setRollColor(String rollColor) {
		this.rollColor = rollColor;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getView() {
		return view;
	}

	public void setView(Integer view) {
		this.view = view;
	}

	public Integer getAppUserRoleId() {
		return appUserRoleId;
	}

	public void setAppUserRoleId(Integer appUserRoleId) {
		this.appUserRoleId = appUserRoleId;
	}

	public List<AnnouncementCommentModel> getComments() {
		return comments;
	}

	public void setComments(List<AnnouncementCommentModel> comments) {
		this.comments = comments;
	}

	 
	
}
