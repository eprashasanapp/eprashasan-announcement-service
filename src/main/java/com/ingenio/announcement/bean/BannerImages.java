package com.ingenio.announcement.bean;

import java.sql.Timestamp;

public class BannerImages {

	private Integer  bannerImagesId;
	private String fileName;
	private String imagePath;
	private String yearName;
	private Integer monthId;
	private Timestamp cDate;
	
	public BannerImages(Integer bannerImagesId, String fileName, String imagePath,String yearName,
			Integer monthId,Timestamp cDate) {
		super();
		this.bannerImagesId = bannerImagesId;
		this.fileName = fileName;
		this.imagePath = imagePath;
		this.yearName=yearName;
		this.monthId = monthId;
		this.cDate = cDate;
	}
	
	public BannerImages(Integer bannerImagesId, String fileName, String imagePath,String yearName,
			Integer monthId) {
		super();
		this.bannerImagesId = bannerImagesId;
		this.fileName = fileName;
		this.imagePath = imagePath;
		this.yearName=yearName;
		this.monthId = monthId;
	}
	
	public Timestamp getcDate() {
		return cDate;
	}

	public void setcDate(Timestamp cDate) {
		this.cDate = cDate;
	}

	public Integer getBannerImagesId() {
		return bannerImagesId;
	}
	public void setBannerImagesId(Integer bannerImagesId) {
		this.bannerImagesId = bannerImagesId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


	public String getYearName() {
		return yearName;
	}


	public void setYearName(String yearName) {
		this.yearName = yearName;
	}


	public Integer getMonthId() {
		return monthId;
	}


	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}
	
	
	
}
