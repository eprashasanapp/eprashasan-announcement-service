package com.ingenio.announcement.bean;


public class AnnnouncementLikeOrDislikeBean {

	private Integer likeId;
	private Integer announcementId;
	private Integer appUserId;
	private Integer schoolid;
	private String likeDislike;
	
	public Integer getLikeId() {
		return likeId;
	}
	public void setLikeId(Integer likeId) {
		this.likeId = likeId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public String getLikeDislike() {
		return likeDislike;
	}
	public void setLikeDislike(String likeDislike) {
		this.likeDislike = likeDislike;
	}
	
	

}
