package com.ingenio.announcement.bean;

import java.util.List;

public class AnnoucementLabelBean {
	
	private String id;
	private String name;
	private List<StudentDetailsBean> studentDetailsList;
	
	public AnnoucementLabelBean() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<StudentDetailsBean> getStudentDetailsList() {
		return studentDetailsList;
	}

	public void setStudentDetailsList(List<StudentDetailsBean> studentDetailsList) {
		this.studentDetailsList = studentDetailsList;
	}
		
	
}
