package com.ingenio.announcement.bean;

import java.util.Map;

public class JsonRequestBean {
	
	@Override
	public String toString() {
		return " additionalproperties=" + additionalproperties ;
	}
	private Integer schoolId;
	private Map<String ,String> additionalproperties;
	
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Map<String, String> getAdditionalproperties() {
		return additionalproperties;
	}
	public void setAdditionalproperties(Map<String, String> additionalproperties) {
		this.additionalproperties = additionalproperties;
	}
	
	

}
