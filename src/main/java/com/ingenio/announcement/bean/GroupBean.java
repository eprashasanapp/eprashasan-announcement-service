package com.ingenio.announcement.bean;

public class GroupBean {
	
	private Integer userToGroupId;
	private Integer groupId;
	private String groupName;
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public GroupBean(Integer userToGroupId,Integer groupId, String groupName) {
		super();
		this.userToGroupId=userToGroupId;
		this.groupId = groupId;
		this.groupName = groupName;
	}
	public GroupBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getUserToGroupId() {
		return userToGroupId;
	}
	public void setUserToGroupId(Integer userToGroupId) {
		this.userToGroupId = userToGroupId;
	}
	
	
}
