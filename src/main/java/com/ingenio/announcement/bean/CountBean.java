package com.ingenio.announcement.bean;

import java.math.BigInteger;
import java.util.List;

public class CountBean {

	
	private BigInteger count;
	private String categoryId;
	private String categoryName;
	private Integer standardId;
	private String standardName;
	private String divisionId;
	private String divisionName;
	private String studGender ;
	
	private BigInteger maleCount;
	private BigInteger feMaleCount;
	private BigInteger transGenderCount;
	private BigInteger noGenderCount;
	private String age_in_years;
	private BigInteger studentCount;

	private List<CountBean> counts;
	private CountBean ageWisecounts;

	
	private List<CountBean> divisionBean;
	
	public BigInteger getCount() {
		return count;
	}
	public void setCount(BigInteger count) {
		this.count = count;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getStudGender() {
		return studGender;
	}
	public void setStudGender(String studGender) {
		this.studGender = studGender;
	}
	public List<CountBean> getCounts() {
		return counts;
	}
	public void setCounts(List<CountBean> counts) {
		this.counts = counts;
	}
	public BigInteger getMaleCount() {
		return maleCount;
	}
	public void setMaleCount(BigInteger maleCount) {
		this.maleCount = maleCount;
	}
	public BigInteger getFeMaleCount() {
		return feMaleCount;
	}
	public void setFeMaleCount(BigInteger feMaleCount) {
		this.feMaleCount = feMaleCount;
	}
	public BigInteger getNoGenderCount() {
		return noGenderCount;
	}
	public void setNoGenderCount(BigInteger noGenderCount) {
		this.noGenderCount = noGenderCount;
	}
	public CountBean(Integer standardId, String standardName) {
		super();
		this.standardId = standardId;
		this.standardName = standardName;
	}
	
	public CountBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public CountBean(String divisionId, String divisionName,Integer standardId) {
		super();
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.standardId=standardId;
	}
	public List<CountBean> getDivisionBean() {
		return divisionBean;
	}
	public void setDivisionBean(List<CountBean> divisionBean) {
		this.divisionBean = divisionBean;
	}
	public String getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}
	public BigInteger getStudentCount() {
		return studentCount;
	}
	public void setStudentCount(BigInteger studentCount) {
		this.studentCount = studentCount;
	}
	public String getAge_in_years() {
		return age_in_years;
	}
	public void setAge_in_years(String age_in_years) {
		this.age_in_years = age_in_years;
	}
	public CountBean getAgeWisecounts() {
		return ageWisecounts;
	}
	public void setAgeWisecounts(CountBean ageWisecounts) {
		this.ageWisecounts = ageWisecounts;
	}
	public BigInteger getTransGenderCount() {
		return transGenderCount;
	}
	public void setTransGenderCount(BigInteger transGenderCount) {
		this.transGenderCount = transGenderCount;
	}
	


	
	
	

}
