package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

public class AnnouncementResponseBean {

	private String approvedStatus;
	private String profilePic;
	private String roleName;
	private List<AnnoucementLabelBean> displayList;
	private Integer announcementId;
	private String announcement;
	private String announcementTitle;
	private Date startDate;
	private Long totalStudentCount;
	private Long deliveredCount;
	private Long unseenCount;
	private Long seenCount;
	private Long completeCount;
	private Long noOfAttachments;
	private String announcedBy;
	private String announcedByImgUrl;
	private String announcedByRole;
	private String assignmentStatus;
	private Integer announcementAssignedtoId;
	private List<AnnouncementResponseBean> attachmentList;
	private Integer schoolId;
	private String registrationNumber;
	private Integer staffId;
	private boolean showcount;
	private String filePath;
	private Integer attachmentId;
	private String sendingList;
	private Integer announcementSendFlag;
	private Integer isWhatsappMsg;
	private Integer whatAppSendingFlag;
	private Integer noticeSendingFlag;
	private String StandardDivision;
	private String StandardDivisionId;
	private List<StaffDetailsBean> staffList;
	private List<AnnouncementResponseBean> studentCountList;
	private List<FeedbackData> feedbackList;
	private Boolean userlikestatus = false;
	private Boolean userViewStatus = false;
	private Integer groupId;
	private String groupName;
	private Integer announcementapprovalAuthorityId;
	private Integer staffUserId;
	private AnnouncementCommentResponseBean lastComment;
	private String postedTime;
	private String fileName;
	private Integer announcementTypeId;
	private String announcementTypeName;
	private Integer sendForApproval = 0;
	private Integer isfeedback = 0;
	private Integer userCanComment = 0;
	private Integer publishFlag = 0;
	private long totalViewsCount;
	private long totalCommentCount;
	private long totalLikes;
	private String userComment;
	
	private Date assignDate;
    private Date deadline;
    private Date actuallyCompletionDate;
    private String complaintBy; 
    private String completionStatus="0"; 
    private Integer isSanstha;
    private String sansthakey;
    private Integer forPrincipal;
    private Integer forTeacher;
    private Integer forstudent;
    private String userFName;
    private String userLastName;
    private String userDesignation;
    private List<UserDetailsBean> userDetails;

	public Integer getPublishFlag() {
		return publishFlag;
	}

	public void setPublishFlag(Integer publishFlag) {
		this.publishFlag = publishFlag;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public Boolean getUserViewStatus() {
		return userViewStatus;
	}

	public void setUserViewStatus(Boolean userViewStatus) {
		this.userViewStatus = userViewStatus;
	}

	public Integer getAnnouncementTypeId() {
		return announcementTypeId;
	}

	public void setAnnouncementTypeId(Integer announcementTypeId) {
		this.announcementTypeId = announcementTypeId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPostedTime() {
		return postedTime;
	}

	public void setPostedTime(String postedTime) {
		this.postedTime = postedTime;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getAnnouncementapprovalAuthorityId() {
		return announcementapprovalAuthorityId;
	}

	public void setAnnouncementapprovalAuthorityId(Integer announcementapprovalAuthorityId) {
		this.announcementapprovalAuthorityId = announcementapprovalAuthorityId;
	}

	public Integer getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(Integer staffUserId) {
		this.staffUserId = staffUserId;
	}

	public AnnouncementCommentResponseBean getLastComment() {
		return lastComment;
	}

	public void setLastComment(AnnouncementCommentResponseBean lastComment) {
		this.lastComment = lastComment;
	}

	public List<FeedbackData> getFeedbackList() {
		return feedbackList;
	}

	public void setFeedbackList(List<FeedbackData> feedbackList) {
		this.feedbackList = feedbackList;
	}

	public Boolean getUserlikestatus() {
		return userlikestatus;
	}

	public void setUserlikestatus(Boolean userlikestatus) {
		this.userlikestatus = userlikestatus;
	}

	public String getSendingList() {
		return sendingList;
	}

	public void setSendingList(String sendingList) {
		this.sendingList = sendingList;
	}

	public AnnouncementResponseBean() {

	}

	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long totalStudentCount, Long deliveredCount, Long unseenCount, Long seenCount,
			Long completeCount, Long noOfAttachments,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.totalStudentCount = totalStudentCount;
		this.deliveredCount = deliveredCount;
		this.unseenCount = unseenCount;
		this.seenCount = seenCount;
		this.completeCount = completeCount;
		this.noOfAttachments = noOfAttachments;
		
		this.isSanstha=isSanstha;
		this.sansthakey=sansthakey;
		this.forPrincipal=forPrincipal;
		this.forTeacher=forTeacher;
		this.forstudent=forstudent;
	}

	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Integer announcementSendFlag, String announcedBy,
			String announcedByRole, String assignmentStatus, Integer announcementAssignedtoId, Integer schoolId,
			String registrationNumber, Integer staffId,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.announcementSendFlag = announcementSendFlag;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.assignmentStatus = assignmentStatus;
		this.announcementAssignedtoId = announcementAssignedtoId;
		this.schoolId = schoolId;
		this.registrationNumber = registrationNumber;
		this.isSanstha=isSanstha;
		this.sansthakey=sansthakey;
		this.forPrincipal=forPrincipal;
		this.forTeacher=forTeacher;
		this.forstudent=forstudent;
		this.setStaffId(staffId);
	}

		//for announcementSchoowise

		public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
				Date startDate, Integer announcementSendFlag,  String announcedBy,
				String announcedByRole, String assignmentStatus, Integer announcementAssignedtoId, Integer schoolId,
				String registrationNumber, Integer staffId, Integer isWhatsAppMsg, Integer whatAppSendingFlag,
				Integer noticeSendingFlag, Integer isfeedback, Integer userCanComment,
				Integer publishFlag,String profilePic,Integer announcementTypeId,String announcementTypeName,Date assignDate,Date deadline
				,Date actuallyCompletionDate,String complaintBy,String completionStatus,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent) {
			super();
			this.announcementId = announcementId;
			this.announcement = announcement;
			this.announcementTitle = announcementTitle;
			this.startDate = startDate;
			this.announcementSendFlag = announcementSendFlag;
			this.announcedBy = announcedBy;
			this.announcedByRole = announcedByRole;
			this.assignmentStatus = assignmentStatus;
			this.announcementAssignedtoId = announcementAssignedtoId;
			this.schoolId = schoolId;
			this.registrationNumber = registrationNumber;
			this.setStaffId(staffId);
			this.isWhatsappMsg = isWhatsAppMsg;
			this.whatAppSendingFlag = whatAppSendingFlag;
			this.noticeSendingFlag = noticeSendingFlag;
			this.isfeedback =isfeedback;
			this.userCanComment =userCanComment;
			this.publishFlag = publishFlag;
			this.profilePic=profilePic;
			this.announcementTypeId=announcementTypeId;
			this.announcementTypeName=announcementTypeName;
			this.assignDate=assignDate;
			this.deadline=deadline;
			this.actuallyCompletionDate=actuallyCompletionDate;
			this.complaintBy=complaintBy;
			this.completionStatus=completionStatus;
			this.isSanstha=isSanstha;
			this.sansthakey=sansthakey;
			this.forPrincipal=forPrincipal;
			this.forTeacher=forTeacher;
			this.forstudent=forstudent;

		}

//	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
//			Date startDate, Integer announcementSendFlag,  String announcedBy,
//			String announcedByRole, Integer schoolId,
//			String registrationNumber, Integer staffId, Integer isWhatsAppMsg, Integer whatAppSendingFlag,
//			Integer noticeSendingFlag, Integer isfeedback, Integer userCanComment,
//			Integer publishFlag,String profilePic,Integer announcementTypeId,String announcementTypeName) {
//		super();
//		this.announcementId = announcementId;
//		this.announcement = announcement;
//		this.announcementTitle = announcementTitle;
//		this.startDate = startDate;
//		this.announcementSendFlag = announcementSendFlag;
//		this.announcedBy = announcedBy;
//		this.announcedByRole = announcedByRole;
//		this.schoolId = schoolId;
//		this.registrationNumber = registrationNumber;
//		this.setStaffId(staffId);
//		this.isWhatsappMsg = isWhatsAppMsg;
//		this.whatAppSendingFlag = whatAppSendingFlag;
//		this.noticeSendingFlag = noticeSendingFlag;
//		this.isfeedback =isfeedback;
//		this.userCanComment =userCanComment;
//		this.publishFlag = publishFlag;
//		this.profilePic=profilePic;
//		this.announcementTypeId=announcementTypeId;
//		this.announcementTypeName=announcementTypeName;
//
//	}

	//new constructor for sanstha level
	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Integer announcementSendFlag,  String announcedBy,
			String announcedByRole, Integer schoolId,
			String registrationNumber, Integer staffId, Integer isWhatsAppMsg, Integer whatAppSendingFlag,
			Integer noticeSendingFlag, Integer isfeedback, Integer userCanComment,
			Integer publishFlag,String profilePic,Integer announcementTypeId,String announcementTypeName
			,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent,String userFName,String userLastName,String userDesignation
) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.announcementSendFlag = announcementSendFlag;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.schoolId = schoolId;
		this.registrationNumber = registrationNumber;
		this.setStaffId(staffId);
		this.isWhatsappMsg = isWhatsAppMsg;
		this.whatAppSendingFlag = whatAppSendingFlag;
		this.noticeSendingFlag = noticeSendingFlag;
		this.isfeedback =isfeedback;
		this.userCanComment =userCanComment;
		this.publishFlag = publishFlag;
		this.profilePic=profilePic;
		this.announcementTypeId=announcementTypeId;
		this.announcementTypeName=announcementTypeName;
		this.isSanstha=isSanstha;
		this.sansthakey=sansthakey;
		this.forPrincipal=forPrincipal;
		this.forTeacher=forTeacher;
		this.forstudent=forstudent;
		this.userFName=userFName;
		this.userLastName=userLastName;
		this.userDesignation=userDesignation;

	}

	//26
	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Integer announcementSendFlag,  String announcedBy,
			String announcedByRole, String assignmentStatus, Integer announcementAssignedtoId, Integer schoolId,
			String registrationNumber, Integer staffId, Integer isWhatsAppMsg, Integer whatAppSendingFlag,
			Integer noticeSendingFlag, Integer isfeedback, Integer userCanComment,
			Integer publishFlag,String profilePic,Integer announcementTypeId,String announcementTypeName
			,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.announcementSendFlag = announcementSendFlag;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.assignmentStatus = assignmentStatus;
		this.announcementAssignedtoId = announcementAssignedtoId;
		this.schoolId = schoolId;
		this.registrationNumber = registrationNumber;
		this.setStaffId(staffId);
		this.isWhatsappMsg = isWhatsAppMsg;
		this.whatAppSendingFlag = whatAppSendingFlag;
		this.noticeSendingFlag = noticeSendingFlag;
		this.isfeedback =isfeedback;
		this.userCanComment =userCanComment;
		this.publishFlag = publishFlag;
		this.profilePic=profilePic;
		this.announcementTypeId=announcementTypeId;
		this.announcementTypeName=announcementTypeName;
		this.isSanstha=isSanstha;
		this.sansthakey=sansthakey;
		this.forPrincipal=forPrincipal;
		this.forTeacher=forTeacher;
		this.forstudent=forstudent;


	}

	

	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Integer announcementSendFlag,  String announcedBy,
			String announcedByRole, Integer schoolId, String registrationNumber, Integer staffId, Integer isWhatsAppMsg,
			Integer whatAppSendingFlag, Integer noticeSendingFlag, Integer sendForApproval,
			Integer announcementTypeId ,Integer isfeedback, Integer userCanComment,
			Integer publishFlag,String profilePic,Integer announcementTypeIId,String announcementTypeName
			,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.announcementSendFlag = announcementSendFlag;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.schoolId = schoolId;
		this.registrationNumber = registrationNumber;
		this.setStaffId(staffId);
		this.isWhatsappMsg = isWhatsAppMsg;
		this.whatAppSendingFlag = whatAppSendingFlag;
		this.noticeSendingFlag = noticeSendingFlag;
		this.sendForApproval = sendForApproval;
		this.announcementTypeId = announcementTypeId;
		this.isfeedback =isfeedback;
		this.userCanComment =userCanComment;
		this.publishFlag = publishFlag;
		this.profilePic=profilePic;
		this.announcementTypeId=announcementTypeIId;
		this.announcementTypeName=announcementTypeName;
		this.isSanstha=isSanstha;
		this.sansthakey=sansthakey;
		this.forPrincipal=forPrincipal;
		this.forTeacher=forTeacher;
		this.forstudent=forstudent;


	}


	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Integer announcementSendFlag,  String announcedBy,
			String announcedByRole, Integer schoolId, String registrationNumber, Integer staffId, Integer isWhatsAppMsg,
			Integer whatAppSendingFlag, Integer noticeSendingFlag,  Integer sendForApproval, Integer isfeedback, Integer userCanComment,
			Integer publishFlag,Integer isSanstha,String sansthakey,Integer forPrincipal,Integer forTeacher,Integer forstudent) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.announcementSendFlag = announcementSendFlag;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.schoolId = schoolId;
		this.registrationNumber = registrationNumber;
		this.setStaffId(staffId);
		this.isWhatsappMsg = isWhatsAppMsg;
		this.whatAppSendingFlag = whatAppSendingFlag;
		this.noticeSendingFlag = noticeSendingFlag;
		this.sendForApproval = sendForApproval;
		this.isfeedback =isfeedback;
		this.userCanComment =userCanComment;
		this.publishFlag = publishFlag;
		this.isSanstha=isSanstha;
		this.sansthakey=sansthakey;
		this.forPrincipal=forPrincipal;
		this.forTeacher=forTeacher;
		this.forstudent=forstudent;

	}

	


	public Integer getIsfeedback() {
		return isfeedback;
	}

	public void setIsfeedback(Integer isfeedback) {
		this.isfeedback = isfeedback;
	}

	public Integer getUserCanComment() {
		return userCanComment;
	}

	public void setUserCanComment(Integer userCanComment) {
		this.userCanComment = userCanComment;
	}

	public AnnouncementResponseBean(String filePath, Integer attachmentId, String fileName) {
		super();
		this.filePath = filePath;
		this.attachmentId = attachmentId;
		this.fileName = fileName;
	}

	public AnnouncementResponseBean(Integer groupId, String groupName) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
	}

	public Integer getAnnouncementSendFlag() {
		return announcementSendFlag;
	}

	public void setAnnouncementSendFlag(Integer announcementSendFlag) {
		this.announcementSendFlag = announcementSendFlag;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<AnnoucementLabelBean> getDisplayList() {
		return displayList;
	}

	public void setDisplayList(List<AnnoucementLabelBean> displayList) {
		this.displayList = displayList;
	}

	public Integer getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	public String getAnnouncementTitle() {
		return announcementTitle;
	}

	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Long getTotalStudentCount() {
		return totalStudentCount;
	}

	public void setTotalStudentCount(Long totalStudentCount) {
		this.totalStudentCount = totalStudentCount;
	}

	public Long getDeliveredCount() {
		return deliveredCount;
	}

	public void setDeliveredCount(Long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}

	public Long getUnseenCount() {
		return unseenCount;
	}

	public void setUnseenCount(Long unseenCount) {
		this.unseenCount = unseenCount;
	}

	public Long getSeenCount() {
		return seenCount;
	}

	public void setSeenCount(Long seenCount) {
		this.seenCount = seenCount;
	}

	public Long getCompleteCount() {
		return completeCount;
	}

	public void setCompleteCount(Long completeCount) {
		this.completeCount = completeCount;
	}

	public Long getNoOfAttachments() {
		return noOfAttachments;
	}

	public void setNoOfAttachments(Long noOfAttachments) {
		this.noOfAttachments = noOfAttachments;
	}

	public String getAnnouncedBy() {
		return announcedBy;
	}

	public void setAnnouncedBy(String announcedBy) {
		this.announcedBy = announcedBy;
	}

	public String getAssignmentStatus() {
		return assignmentStatus;
	}

	public void setAssignmentStatus(String assignmentStatus) {
		this.assignmentStatus = assignmentStatus;
	}

	public Integer getAnnouncementAssignedtoId() {
		return announcementAssignedtoId;
	}

	public void setAnnouncementAssignedtoId(Integer announcementAssignedtoId) {
		this.announcementAssignedtoId = announcementAssignedtoId;
	}

	public String getAnnouncedByRole() {
		return announcedByRole;
	}

	public void setAnnouncedByRole(String announcedByRole) {
		this.announcedByRole = announcedByRole;
	}

	public List<AnnouncementResponseBean> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<AnnouncementResponseBean> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public String getAnnouncedByImgUrl() {
		return announcedByImgUrl;
	}

	public void setAnnouncedByImgUrl(String announcedByImgUrl) {
		this.announcedByImgUrl = announcedByImgUrl;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public boolean isShowcount() {
		return showcount;
	}

	public void setShowcount(boolean showcount) {
		this.showcount = showcount;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}

	public List<StaffDetailsBean> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<StaffDetailsBean> staffList) {
		this.staffList = staffList;
	}

	public Integer getIsWhatsappMsg() {
		return isWhatsappMsg;
	}

	public void setIsWhatsappMsg(Integer isWhatsappMsg) {
		this.isWhatsappMsg = isWhatsappMsg;
	}

	public Integer getWhatAppSendingFlag() {
		return whatAppSendingFlag;
	}

	public void setWhatAppSendingFlag(Integer whatAppSendingFlag) {
		this.whatAppSendingFlag = whatAppSendingFlag;
	}

	public Integer getNoticeSendingFlag() {
		return noticeSendingFlag;
	}

	public void setNoticeSendingFlag(Integer noticeSendingFlag) {
		this.noticeSendingFlag = noticeSendingFlag;
	}

	public String getStandardDivision() {
		return StandardDivision;
	}

	public void setStandardDivision(String standardDivision) {
		StandardDivision = standardDivision;
	}

	public List<AnnouncementResponseBean> getStudentCountList() {
		return studentCountList;
	}

	public void setStudentCountList(List<AnnouncementResponseBean> studentCountList) {
		this.studentCountList = studentCountList;
	}

	public String getStandardDivisionId() {
		return StandardDivisionId;
	}

	public void setStandardDivisionId(String standardDivisionId) {
		StandardDivisionId = standardDivisionId;
	}

	public Integer getSendForApproval() {
		return sendForApproval;
	}

	public void setSendForApproval(Integer sendForApproval) {
		this.sendForApproval = sendForApproval;
	}

	public long getTotalViewsCount() {
		return totalViewsCount;
	}

	public void setTotalViewsCount(long totalViewsCount) {
		this.totalViewsCount = totalViewsCount;
	}

	public long getTotalCommentCount() {
		return totalCommentCount;
	}

	public void setTotalCommentCount(long totalCommentCount) {
		this.totalCommentCount = totalCommentCount;
	}

	public long getTotalLikes() {
		return totalLikes;
	}

	public void setTotalLikes(long totalLikes) {
		this.totalLikes = totalLikes;
	}

	public String getAnnouncementTypeName() {
		return announcementTypeName;
	}

	public void setAnnouncementTypeName(String announcementTypeName) {
		this.announcementTypeName = announcementTypeName;
	}

	public String getApprovedStatus() {
		return approvedStatus;
	}

	public void setApprovedStatus(String approvedStatus) {
		this.approvedStatus = approvedStatus;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public Date getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getActuallyCompletionDate() {
		return actuallyCompletionDate;
	}

	public void setActuallyCompletionDate(Date actuallyCompletionDate) {
		this.actuallyCompletionDate = actuallyCompletionDate;
	}

	public String getComplaintBy() {
		return complaintBy;
	}

	public void setComplaintBy(String complaintBy) {
		this.complaintBy = complaintBy;
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}

	public List<UserDetailsBean> getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(List<UserDetailsBean> userDetails) {
		this.userDetails = userDetails;
	}

	public Integer getIsSanstha() {
		return isSanstha;
	}

	public void setIsSanstha(Integer isSanstha) {
		this.isSanstha = isSanstha;
	}

	

	public Integer getForPrincipal() {
		return forPrincipal;
	}

	public void setForPrincipal(Integer forPrincipal) {
		this.forPrincipal = forPrincipal;
	}

	public Integer getForTeacher() {
		return forTeacher;
	}

	public void setForTeacher(Integer forTeacher) {
		this.forTeacher = forTeacher;
	}

	public Integer getForstudent() {
		return forstudent;
	}

	public void setForstudent(Integer forstudent) {
		this.forstudent = forstudent;
	}

	public String getSansthakey() {
		return sansthakey;
	}

	public void setSansthakey(String sansthakey) {
		this.sansthakey = sansthakey;
	}

	public String getUserFName() {
		return userFName;
	}

	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserDesignation() {
		return userDesignation;
	}

	public void setUserDesignation(String userDesignation) {
		this.userDesignation = userDesignation;
	}

	
}
