package com.ingenio.announcement.bean;

import java.util.ArrayList;
import java.util.List;

public class ReportBean {

	
	private List<PhoneNumberBean> staffReportBean;
	private List<PhoneNumberBean> studentReportBean;
	private String group;
	private List<PhoneNumberBean> groupReport;
	public List<PhoneNumberBean> getStaffReportBean() {
		return staffReportBean;
	}
	public void setStaffReportBean(List<PhoneNumberBean> staffReportBean) {
		this.staffReportBean = staffReportBean;
	}
	public List<PhoneNumberBean> getStudentReportBean() {
		return studentReportBean;
	}
	public void setStudentReportBean(List<PhoneNumberBean> studentReportBean) {
		this.studentReportBean = studentReportBean;
	}
	
	
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public List<PhoneNumberBean> getGroupReport() {
		return groupReport;
	}
	public void setGroupReport(List<PhoneNumberBean> groupReport) {
		this.groupReport = groupReport;
	}
	
	
}
