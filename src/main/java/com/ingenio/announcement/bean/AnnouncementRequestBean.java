package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

import com.ingenio.announcement.model.AnnouncementTypeModel;

public class AnnouncementRequestBean {

	private Integer yearId;
	private Integer staffId;
	private Integer userId;
	private String IPAddress;
	private String deviceType;
	private String announcementTitle;
	private String announcement;
	private String smsTitle;
	private String smsMessage;
	private Integer schoolId;
	private Integer announcementId;
	private Date startDate;
	private Integer apiFlag;
	private List<AnnouncementResponseBean> annoucementResponseList;
	private String teacherName;
	private String time;
	private String divisionList;
	private List<AnnouncementResponseBean> sendingList;
	private Integer announcement_type_id;
	private Date publishDate;
	private Integer isFeedback = 0;
	private Integer userCanComment =0;
	private Integer publishFlag =0;
	private List<FeedbackData> feedbackDataList;
    private Integer staffUserId;
    private String status;
    private String remark;
    public Integer feedback_status_flag;
    public Integer priority;
    public Integer otherSchoolId;

    private Date assignDate;
    private Date deadline;
    private Date actuallyCompletionDate;
    private String complaintBy; 
    private String completionStatus="0"; 
    private Integer whatAppSendingFlag;  
    private Integer isSanstha;
    private String sansthakey;
    private Integer forPrincipal;
    private Integer forTeacher;
    private Integer forstudent;


    
    

	public Integer getFeedback_status_flag() {
		return feedback_status_flag;
	}

	public void setFeedback_status_flag(Integer feedback_status_flag) {
		this.feedback_status_flag = feedback_status_flag;
	}

	public Integer getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(Integer staffUserId) {
		this.staffUserId = staffUserId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getAnnouncement_type_id() {
		return announcement_type_id;
	}

	public void setAnnouncement_type_id(Integer announcement_type_id) {
		this.announcement_type_id = announcement_type_id;
	}

	public List<FeedbackData> getFeedbackDataList() {
		return feedbackDataList;
	}

	public void setFeedbackDataList(List<FeedbackData> feedbackDataList) {
		this.feedbackDataList = feedbackDataList;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	
	public Integer getIsFeedback() {
		return isFeedback;
	}

	public void setIsFeedback(Integer isFeedback) {
		this.isFeedback = isFeedback;
	}

	public Integer getUserCanComment() {
		return userCanComment;
	}

	public void setUserCanComment(Integer userCanComment) {
		this.userCanComment = userCanComment;
	}

	public Integer getPublishFlag() {
		return publishFlag;
	}

	public void setPublishFlag(Integer publishFlag) {
		this.publishFlag = publishFlag;
	}

	public List<AnnouncementResponseBean> getSendingList() {
		return sendingList;
	}

	public void setSendingList(List<AnnouncementResponseBean> sendingList) {
		this.sendingList = sendingList;
	}

	public String getDivisionList() {
		return divisionList;
	}

	public void setDivisionList(String divisionList) {
		this.divisionList = divisionList;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getAnnouncementTitle() {
		return announcementTitle;
	}

	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getApiFlag() {
		return apiFlag;
	}

	public void setApiFlag(Integer apiFlag) {
		this.apiFlag = apiFlag;
	}

	public List<AnnouncementResponseBean> getAnnoucementResponseList() {
		return annoucementResponseList;
	}

	public void setAnnoucementResponseList(List<AnnouncementResponseBean> annoucementResponseList) {
		this.annoucementResponseList = annoucementResponseList;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSmsTitle() {
		return smsTitle;
	}

	public void setSmsTitle(String smsTitle) {
		this.smsTitle = smsTitle;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getOtherSchoolId() {
		return otherSchoolId;
	}

	public void setOtherSchoolId(Integer otherSchoolId) {
		this.otherSchoolId = otherSchoolId;
	}

	public Date getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getActuallyCompletionDate() {
		return actuallyCompletionDate;
	}

	public void setActuallyCompletionDate(Date actuallyCompletionDate) {
		this.actuallyCompletionDate = actuallyCompletionDate;
	}

	public String getComplaintBy() {
		return complaintBy;
	}

	public void setComplaintBy(String complaintBy) {
		this.complaintBy = complaintBy;
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}

	public Integer getWhatAppSendingFlag() {
		return whatAppSendingFlag;
	}

	public void setWhatAppSendingFlag(Integer whatAppSendingFlag) {
		this.whatAppSendingFlag = whatAppSendingFlag;
	}

	public Integer getIsSanstha() {
		return isSanstha;
	}

	public void setIsSanstha(Integer isSanstha) {
		this.isSanstha = isSanstha;
	}

	
	public Integer getForPrincipal() {
		return forPrincipal;
	}

	public void setForPrincipal(Integer forPrincipal) {
		this.forPrincipal = forPrincipal;
	}

	public Integer getForTeacher() {
		return forTeacher;
	}

	public void setForTeacher(Integer forTeacher) {
		this.forTeacher = forTeacher;
	}

	public Integer getForstudent() {
		return forstudent;
	}

	public void setForstudent(Integer forstudent) {
		this.forstudent = forstudent;
	}

	public String getSansthakey() {
		return sansthakey;
	}

	public void setSansthakey(String sansthakey) {
		this.sansthakey = sansthakey;
	}
	

	

}
