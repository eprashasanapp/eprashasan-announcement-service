package com.ingenio.announcement.bean;

public class EventTypeBean {

	
	private Integer eventTypeId;
	private String eventType;
	private String eventTypeColor;
	
	public Integer getEventTypeId() {
		return eventTypeId;
	}
	public void setEventTypeId(Integer eventTypeId) {
		this.eventTypeId = eventTypeId;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventTypeColor() {
		return eventTypeColor;
	}
	public void setEventTypeColor(String eventTypeColor) {
		this.eventTypeColor = eventTypeColor;
	}
	
	
	public EventTypeBean(Integer eventTypeId, String eventType, String eventTypeColor) {
		super();
		this.eventTypeId = eventTypeId;
		this.eventType = eventType;
		this.eventTypeColor = eventTypeColor;
	}
	
	public EventTypeBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
