package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

public class GalleryResponseBean {
	
	private String roleName;
	private List<GalleryLabelBean> displayList;
	private Integer galleryId;
	private String galleryMessage;
	private String galleryTitle;
	private Date startDate;
	private Long totalStudentCount;
	private Long deliveredCount;
	private Long unseenCount;
	private Long seenCount;
	private Long completeCount;
	private Long noOfAttachments;
	private String postedBy;
	private String postedByImgUrl;
	private String postedByRole;
	private String postedGalleryStatus;
	private Integer galleryAssignedtoId;
	private List<GalleryResponseBean> attachmentList;
	private Integer schoolId;
	private String registrationNumber;
	private Integer staffId;
	private boolean showcount;
	private String filePath;
	private Integer attachmentId;
	private String galleryTime;
	
	public GalleryResponseBean() {
		
	}
	public GalleryResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long totalStudentCount, Long deliveredCount, Long unseenCount, Long seenCount,
			Long completeCount, Long noOfAttachments,String galleryTime) {
		super();
		this.galleryId = announcementId;
		this.galleryMessage = announcement;
		this.galleryTitle = announcementTitle;
		this.startDate = startDate;
		this.totalStudentCount = totalStudentCount;
		this.deliveredCount = deliveredCount;
		this.unseenCount = unseenCount;
		this.seenCount = seenCount;
		this.completeCount = completeCount;
		this.noOfAttachments = noOfAttachments;
		this.galleryTime=galleryTime;
	}
	
	public GalleryResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long noOfAttachments, String announcedBy, String announcedByRole, String assignmentStatus,
			Integer announcementAssignedtoId,Integer schoolId,String registrationNumber,Integer staffId,String galleryTime) {
		super();
		this.galleryId = announcementId;
		this.galleryMessage = announcement;
		this.galleryTitle = announcementTitle;
		this.startDate = startDate;
		this.noOfAttachments = noOfAttachments;
		this.postedBy = announcedBy;
		this.postedByRole = announcedByRole;
		this.postedGalleryStatus = assignmentStatus;
		this.galleryAssignedtoId = announcementAssignedtoId;
		this.schoolId=schoolId;
		this.registrationNumber=registrationNumber;
		this.setStaffId(staffId);
		this.galleryTime=galleryTime;
	}
	
	public GalleryResponseBean(String filePath, Integer attachmentId) {
		super();
		this.filePath = filePath;
		this.attachmentId = attachmentId;
	}
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<GalleryLabelBean> getDisplayList() {
		return displayList;
	}
	public void setDisplayList(List<GalleryLabelBean> displayList) {
		this.displayList = displayList;
	}
	
	public Integer getGalleryId() {
		return galleryId;
	}
	public void setGalleryId(Integer galleryId) {
		this.galleryId = galleryId;
	}
	public String getGalleryMessage() {
		return galleryMessage;
	}
	public void setGalleryMessage(String galleryMessage) {
		this.galleryMessage = galleryMessage;
	}
	public String getGalleryTitle() {
		return galleryTitle;
	}
	public void setGalleryTitle(String galleryTitle) {
		this.galleryTitle = galleryTitle;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Long getTotalStudentCount() {
		return totalStudentCount;
	}
	public void setTotalStudentCount(Long totalStudentCount) {
		this.totalStudentCount = totalStudentCount;
	}
	public Long getDeliveredCount() {
		return deliveredCount;
	}
	public void setDeliveredCount(Long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}
	public Long getUnseenCount() {
		return unseenCount;
	}
	public void setUnseenCount(Long unseenCount) {
		this.unseenCount = unseenCount;
	}
	public Long getSeenCount() {
		return seenCount;
	}
	public void setSeenCount(Long seenCount) {
		this.seenCount = seenCount;
	}
	public Long getCompleteCount() {
		return completeCount;
	}
	public void setCompleteCount(Long completeCount) {
		this.completeCount = completeCount;
	}
	public Long getNoOfAttachments() {
		return noOfAttachments;
	}
	public void setNoOfAttachments(Long noOfAttachments) {
		this.noOfAttachments = noOfAttachments;
	}
	
	public Integer getGalleryAssignedtoId() {
		return galleryAssignedtoId;
	}
	public void setGalleryAssignedtoId(Integer galleryAssignedtoId) {
		this.galleryAssignedtoId = galleryAssignedtoId;
	}
	
	public List<GalleryResponseBean> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<GalleryResponseBean> attachmentList) {
		this.attachmentList = attachmentList;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public boolean isShowcount() {
		return showcount;
	}
	public void setShowcount(boolean showcount) {
		this.showcount = showcount;
	}
	public String getPostedBy() {
		return postedBy;
	}
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	public String getPostedByImgUrl() {
		return postedByImgUrl;
	}
	public void setPostedByImgUrl(String postedByImgUrl) {
		this.postedByImgUrl = postedByImgUrl;
	}
	public String getPostedByRole() {
		return postedByRole;
	}
	public void setPostedByRole(String postedByRole) {
		this.postedByRole = postedByRole;
	}
	public String getPostedGalleryStatus() {
		return postedGalleryStatus;
	}
	public void setPostedGalleryStatus(String postedGalleryStatus) {
		this.postedGalleryStatus = postedGalleryStatus;
	}

	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Integer getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getGalleryTime() {
		return galleryTime;
	}
	public void setGalleryTime(String galleryTime) {
		this.galleryTime = galleryTime;
	}
		
	
}
