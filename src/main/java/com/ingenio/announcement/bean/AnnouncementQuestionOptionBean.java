package com.ingenio.announcement.bean;

import java.util.List;

public class AnnouncementQuestionOptionBean {

	private Integer announcementId;
	private Integer schoolid;
	private List<FeedbackData> feedbackDataList;
	
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public List<FeedbackData> getFeedbackDataList() {
		return feedbackDataList;
	}
	public void setFeedbackDataList(List<FeedbackData> feedbackDataList) {
		this.feedbackDataList = feedbackDataList;
	}

	

}
