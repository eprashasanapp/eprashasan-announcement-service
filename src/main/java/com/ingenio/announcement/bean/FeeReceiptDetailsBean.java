package com.ingenio.announcement.bean;
import java.util.Date;
import java.util.List;



public class FeeReceiptDetailsBean {
	private Integer schoolId;
	private String schoolName;
	private String schoolSansthaKey;
	private Integer userId;
	private String roleName;
	private String userName;
	private String activeFlag;
	private String IMEICode;
	private String deviceType;
	private String appRegDate;
	private String appStartDate;
	private String appEndDate;
	private Integer currentYear;
	private String currentYearText;
	private String registartionNo;
	//private Integer studentId;
	private String initialName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private String caste;
	private String category;
	private String religion;
	private String medium;
	private String mobileNumber;
	private String birthDate;
	private String birthPlace;
	private String nationality;
	private String joiningDate;
	private Integer renewId;
	private Integer staffId;
	private String designation;
	private String department;
	private Integer isClassTeacher;
	private String studentPhotoUrl;
	private Integer grBookId;
	private String fatherName;
	private String motherName;
	private String standardName;
	private String divisionName;
	private Integer rollNo;
	private String schoolLogoUrl;
	private String roleNameForDisplay;
	private String rollColor;
	private Integer standardId;
	private String admissionDate;
	private String studentFormNo;
	private String mothertonge;
	private String localAddress;
	private String isOnlineAdmission;
	private String onlineAdmissionApprovalFlag;
	private Integer divisionId;
	private String userActiveOrInActiveStatus;
	private String userActiveInActiceStatusName;
	private String isOrganization;
	private Integer unseenNotificationCount;
	private Integer feeSettingId;
	private Integer feeTypeId;
	private String feeType;
	private Integer yearId;
	private String year;
	private Double totalAssignFee;
	private Integer isConsolation;
	private FeeReceiptDetailsBean feeReceiptModel;
	private Integer receiptId;
	private String receiptNo;
	private Integer isconsolidation;
	private Integer headId;
	private String payTypeOrPrintFlag;
	private String returnFlag;
	private String reconciliationFlag;
	private String bounceClearFlag;
	private String discardFlag;
	private Date payTypeOrPrintDate;
	private Date bankPaidDate;
	private Date returnByUserDate;
	private Date reconciliationDate;
	private Date bounceClearDate;
	private Date discardDate;
	private Integer bankId;
	private String remark;
	private Integer typeOfUnitId;
	private String type;
    private Date approvalDate;
    private Integer studentId;
	private String typeOfReceipt;
	private double totalAmount;
	private double totalPayFine;
	private double totalPayDiscount;
	private double totalPayExcessFee;
	private String isSocietyReceipt;
	private String existingStudent;
	private Double payFee;
	private Double pendingFee;
	
	private String mmobileNo;
	private String mEmail;
	private String fcmToken;
	private String shortSchoolName;
	private String name;
	
	private Integer guestStudentId;
	private String mobileNo;
	private String studFName;
	private String studMName;
	private String studLName;
	private String regNo;
	private String yearName;
	private Integer previousFeeTypeId;
	private String previousFeeType;
	private Integer currentFeeTypeId;
	private String currentFeeType;

	private Integer excessFeeId;
	private String excessFee;
	List<FeeReceiptDetailsBean>excessFeeBean;

	//private Double totalAmount;
	//private Date payTypeOrPrintDate;
	
	
	//a.studentId,b.appUserRoleId,c.mmobile,c.memail,d.token

	//private Integer appUserId;
	
	
	public FeeReceiptDetailsBean(Integer receiptId,Double payFee,Integer yearId,Double discount) {
		super();
		
		this.receiptId=receiptId;
		this.payFee=payFee;
		this.yearId=yearId;
		this.totalPayDiscount=discount;
	}
	//a.totalAmount,a.receiptNo,a.payTypeOrPrintDate,f.feeTypeModel.feeType

	public FeeReceiptDetailsBean( Integer studentId,Integer userId, String mmobileNo, String mEmail, String fcmToken,
			String schoolName,String shortSchoolName,String name,Double totalAmount,String receiptNo,Date payTypeOrPrintDate) {
		super();
		this.userId = userId;
		this.studentId = studentId;
		this.mmobileNo = mmobileNo;
		this.mEmail = mEmail;
		this.fcmToken = fcmToken;
		this.schoolName=schoolName;
		this.shortSchoolName=shortSchoolName;
		this.name=name;
		this.totalAmount=totalAmount;
		this.receiptNo=receiptNo;
		this.payTypeOrPrintDate=payTypeOrPrintDate;
		
		
	}
	public String getMmobileNo() {
		return mmobileNo;
	}
	public void setMmobileNo(String mmobileNo) {
		this.mmobileNo = mmobileNo;
	}
	public String getmEmail() {
		return mEmail;
	}
	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}
	public String getFcmToken() {
		return fcmToken;
	}
	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}
	public FeeReceiptDetailsBean(String registartionNo, Integer studentId, String initialName, String firstName,
			String middleName, String lastName, String gender, String caste, String category, String religion,
			String medium, String mobileNumber, String birthDate, String birthPlace, String nationality,
			String joiningDate,Integer renewId,Integer grBookId,String fatherName,String motherName,String standardName,String divisionName,
			Integer rollNo ,Integer standardId,String isonlineAdmission,String onlineAdmissionApprovalFlag,Integer renewStudentId,Integer divisionId
			,Integer feeSettingId,Integer feeTypeId,String feeType,Integer yearId,String year,Double totalAssignFee,Integer isConsolation) {
		super();
		this.registartionNo = registartionNo;
		this.studentId = studentId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.medium = medium;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.renewId=renewId;
		this.grBookId=grBookId;
		this.fatherName=fatherName;
		this.motherName=motherName;
		this.standardName=standardName;
		this.divisionName=divisionName;
		this.rollNo=rollNo;
		this.standardId =standardId;
		this.isOnlineAdmission = isonlineAdmission;
		this.onlineAdmissionApprovalFlag=onlineAdmissionApprovalFlag;
		this.renewId=renewStudentId;
		this.divisionId = divisionId;
		this.feeSettingId=feeSettingId;
		this.feeTypeId=feeTypeId;
		this.feeType=feeType;
		this.yearId=yearId;
		this.year=year;
		this.totalAssignFee=totalAssignFee;
		this.isConsolation=isConsolation;
	}
	

	public FeeReceiptDetailsBean() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getIsOnlineAdmission() {
		return isOnlineAdmission;
	}
	public void setIsOnlineAdmission(String isOnlineAdmission) {
		this.isOnlineAdmission = isOnlineAdmission;
	}
	public String getAdmissionDate() {
		return admissionDate;
	}
	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}
	public String getStudentFormNo() {
		return studentFormNo;
	}
	public void setStudentFormNo(String studentFormNo) {
		this.studentFormNo = studentFormNo;
	}
	public String getMothertonge() {
		return mothertonge;
	}
	public void setMothertonge(String mothertonge) {
		this.mothertonge = mothertonge;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getRegistartionNo() {
		return registartionNo;
	}
	public void setRegistartionNo(String registartionNo) {
		this.registartionNo = registartionNo;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getIMEICode() {
		return IMEICode;
	}
	public void setIMEICode(String iMEICode) {
		IMEICode = iMEICode;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getAppRegDate() {
		return appRegDate;
	}
	public void setAppRegDate(String appRegDate) {
		this.appRegDate = appRegDate;
	}
	public String getAppStartDate() {
		return appStartDate;
	}
	public void setAppStartDate(String appStartDate) {
		this.appStartDate = appStartDate;
	}
	public String getAppEndDate() {
		return appEndDate;
	}
	public void setAppEndDate(String appEndDate) {
		this.appEndDate = appEndDate;
	}
	public Integer getCurrentYear() {
		return currentYear;
	}
	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}
	public String getCurrentYearText() {
		return currentYearText;
	}
	public void setCurrentYearText(String currentYearText) {
		this.currentYearText = currentYearText;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	public String getStudentPhotoUrl() {
		return studentPhotoUrl;
	}
	public void setStudentPhotoUrl(String studentPhotoUrl) {
		this.studentPhotoUrl = studentPhotoUrl;
	}
	public Integer getGrBookId() {
		return grBookId;
	}
	public void setGrBookId(Integer grBookId) {
		this.grBookId = grBookId;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public String getSchoolSansthaKey() {
		return schoolSansthaKey;
	}
	public void setSchoolSansthaKey(String schoolSansthaKey) {
		this.schoolSansthaKey = schoolSansthaKey;
	}
	public String getSchoolLogoUrl() {
		return schoolLogoUrl;
	}
	public void setSchoolLogoUrl(String schoolLogoUrl) {
		this.schoolLogoUrl = schoolLogoUrl;
	}
	public String getRoleNameForDisplay() {
		return roleNameForDisplay;
	}
	public void setRoleNameForDisplay(String roleNameForDisplay) {
		this.roleNameForDisplay = roleNameForDisplay;
	}
	public String getRollColor() {
		return rollColor;
	}
	public void setRollColor(String rollColor) {
		this.rollColor = rollColor;
	}
	
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public String getUserActiveOrInActiveStatus() {
		return userActiveOrInActiveStatus;
	}
	public void setUserActiveOrInActiveStatus(String userActiveOrInActiveStatus) {
		this.userActiveOrInActiveStatus = userActiveOrInActiveStatus;
	}
	public String getUserActiveInActiceStatusName() {
		return userActiveInActiceStatusName;
	}
	public void setUserActiveInActiceStatusName(String userActiveInActiceStatusName) {
		this.userActiveInActiceStatusName = userActiveInActiceStatusName;
	}
	public String getIsOrganization() {
		return isOrganization;
	}
	public void setIsOrganization(String isOrganization) {
		this.isOrganization = isOrganization;
	}
	public Integer getUnseenNotificationCount() {
		return unseenNotificationCount;
	}
	public void setUnseenNotificationCount(Integer unseenNotificationCount) {
		this.unseenNotificationCount = unseenNotificationCount;
	}


	public Integer getIsClassTeacher() {
		return isClassTeacher;
	}


	public void setIsClassTeacher(Integer isClassTeacher) {
		this.isClassTeacher = isClassTeacher;
	}


	public String getOnlineAdmissionApprovalFlag() {
		return onlineAdmissionApprovalFlag;
	}


	public void setOnlineAdmissionApprovalFlag(String onlineAdmissionApprovalFlag) {
		this.onlineAdmissionApprovalFlag = onlineAdmissionApprovalFlag;
	}


	public Integer getFeeSettingId() {
		return feeSettingId;
	}


	public void setFeeSettingId(Integer feeSettingId) {
		this.feeSettingId = feeSettingId;
	}


	public Integer getFeeTypeId() {
		return feeTypeId;
	}


	public void setFeeTypeId(Integer feeTypeId) {
		this.feeTypeId = feeTypeId;
	}


	public String getFeeType() {
		return feeType;
	}


	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}


	public Integer getYearId() {
		return yearId;
	}


	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}


	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}


	public Double getTotalAssignFee() {
		return totalAssignFee;
	}


	public void setTotalAssignFee(Double totalAssignFee) {
		this.totalAssignFee = totalAssignFee;
	}


	public Integer getIsConsolation() {
		return isConsolation;
	}


	public void setIsConsolation(Integer isConsolation) {
		this.isConsolation = isConsolation;
	}


	
	public Integer getReceiptId() {
		return receiptId;
	}


	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}


	public String getReceiptNo() {
		return receiptNo;
	}


	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}


	public String getReturnFlag() {
		return returnFlag;
	}


	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}


	public String getReconciliationFlag() {
		return reconciliationFlag;
	}


	public void setReconciliationFlag(String reconciliationFlag) {
		this.reconciliationFlag = reconciliationFlag;
	}


	public Date getReturnByUserDate() {
		return returnByUserDate;
	}


	public void setReturnByUserDate(Date returnByUserDate) {
		this.returnByUserDate = returnByUserDate;
	}


	public Date getReconciliationDate() {
		return reconciliationDate;
	}


	public void setReconciliationDate(Date reconciliationDate) {
		this.reconciliationDate = reconciliationDate;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Integer getTypeOfUnitId() {
		return typeOfUnitId;
	}


	public void setTypeOfUnitId(Integer typeOfUnitId) {
		this.typeOfUnitId = typeOfUnitId;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTypeOfReceipt() {
		return typeOfReceipt;
	}


	public void setTypeOfReceipt(String typeOfReceipt) {
		this.typeOfReceipt = typeOfReceipt;
	}


	public double getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}


	public double getTotalPayFine() {
		return totalPayFine;
	}


	public void setTotalPayFine(double totalPayFine) {
		this.totalPayFine = totalPayFine;
	}


	public double getTotalPayDiscount() {
		return totalPayDiscount;
	}


	public void setTotalPayDiscount(double totalPayDiscount) {
		this.totalPayDiscount = totalPayDiscount;
	}


	public double getTotalPayExcessFee() {
		return totalPayExcessFee;
	}


	public void setTotalPayExcessFee(double totalPayExcessFee) {
		this.totalPayExcessFee = totalPayExcessFee;
	}


	public Integer getIsconsolidation() {
		return isconsolidation;
	}


	public void setIsconsolidation(Integer isconsolidation) {
		this.isconsolidation = isconsolidation;
	}


	public Integer getHeadId() {
		return headId;
	}


	public void setHeadId(Integer headId) {
		this.headId = headId;
	}


	public String getPayTypeOrPrintFlag() {
		return payTypeOrPrintFlag;
	}


	public void setPayTypeOrPrintFlag(String payTypeOrPrintFlag) {
		this.payTypeOrPrintFlag = payTypeOrPrintFlag;
	}


	public String getBounceClearFlag() {
		return bounceClearFlag;
	}


	public void setBounceClearFlag(String bounceClearFlag) {
		this.bounceClearFlag = bounceClearFlag;
	}


	public String getDiscardFlag() {
		return discardFlag;
	}


	public void setDiscardFlag(String discardFlag) {
		this.discardFlag = discardFlag;
	}


	public Date getPayTypeOrPrintDate() {
		return payTypeOrPrintDate;
	}


	public void setPayTypeOrPrintDate(Date payTypeOrPrintDate) {
		this.payTypeOrPrintDate = payTypeOrPrintDate;
	}


	public Date getBankPaidDate() {
		return bankPaidDate;
	}


	public void setBankPaidDate(Date bankPaidDate) {
		this.bankPaidDate = bankPaidDate;
	}


	public Date getBounceClearDate() {
		return bounceClearDate;
	}


	public void setBounceClearDate(Date bounceClearDate) {
		this.bounceClearDate = bounceClearDate;
	}


	public Date getDiscardDate() {
		return discardDate;
	}


	public void setDiscardDate(Date discardDate) {
		this.discardDate = discardDate;
	}


	public Integer getBankId() {
		return bankId;
	}


	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}


	public Date getApprovalDate() {
		return approvalDate;
	}


	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	public String getIsSocietyReceipt() {
		return isSocietyReceipt;
	}


	public void setIsSocietyReceipt(String isSocietyReceipt) {
		this.isSocietyReceipt = isSocietyReceipt;
	}


	public String getExistingStudent() {
		return existingStudent;
	}


	public void setExistingStudent(String existingStudent) {
		this.existingStudent = existingStudent;
	}


	public FeeReceiptDetailsBean(Integer schoolid,Integer receiptId, String receiptNo, Integer isconsolidation, Integer headId,
			String payTypeOrPrintFlag, String returnFlag, String reconciliationFlag, String bounceClearFlag,
			String discardFlag, Date payTypeOrPrintDate, Date bankPaidDate, Date returnByUserDate,
			Date reconciliationDate, Date bounceClearDate, Date discardDate, Integer bankId, String remark,
			Integer typeOfUnitId, String type, Date approvalDate, Integer studentId, String typeOfReceipt,
			double totalAmount, double totalPayFine, double totalPayDiscount, double totalPayExcessFee,
			String isSocietyReceipt, String existingStudent,String schoolName,Integer yearId) {
		super();
		this.receiptId = receiptId;
		this.receiptNo = receiptNo;
		this.isconsolidation = isconsolidation;
		this.headId = headId;
		this.payTypeOrPrintFlag = payTypeOrPrintFlag;
		this.returnFlag = returnFlag;
		this.reconciliationFlag = reconciliationFlag;
		this.bounceClearFlag = bounceClearFlag;
		this.discardFlag = discardFlag;
		this.payTypeOrPrintDate = payTypeOrPrintDate;
		this.bankPaidDate = bankPaidDate;
		this.returnByUserDate = returnByUserDate;
		this.reconciliationDate = reconciliationDate;
		this.bounceClearDate = bounceClearDate;
		this.discardDate = discardDate;
		this.bankId = bankId;
		this.remark = remark;
		this.typeOfUnitId = typeOfUnitId;
		this.type = type;
		this.approvalDate = approvalDate;
		this.studentId = studentId;
		this.typeOfReceipt = typeOfReceipt;
		this.totalAmount = totalAmount;
		this.totalPayFine = totalPayFine;
		this.totalPayDiscount = totalPayDiscount;
		this.totalPayExcessFee = totalPayExcessFee;
		this.isSocietyReceipt = isSocietyReceipt;
		this.existingStudent = existingStudent;
		this.schoolName=schoolName;
		this.yearId=yearId;
		this.schoolId=schoolid;
	}

	public FeeReceiptDetailsBean(Integer receiptId, String receiptNo, Integer isconsolidation, Integer headId,
			String payTypeOrPrintFlag, String returnFlag, String reconciliationFlag, String bounceClearFlag,
			String discardFlag, Date payTypeOrPrintDate, Date bankPaidDate, Date returnByUserDate,
			Date reconciliationDate, Date bounceClearDate, Date discardDate, Integer bankId, String remark,
			Integer typeOfUnitId, String type, Date approvalDate, Integer studentId, String typeOfReceipt,
			double totalAmount, double totalPayFine, double totalPayDiscount, double totalPayExcessFee,
			String isSocietyReceipt, String existingStudent,String schoolName) {
		super();
		this.receiptId = receiptId;
		this.receiptNo = receiptNo;
		this.isconsolidation = isconsolidation;
		this.headId = headId;
		this.payTypeOrPrintFlag = payTypeOrPrintFlag;
		this.returnFlag = returnFlag;
		this.reconciliationFlag = reconciliationFlag;
		this.bounceClearFlag = bounceClearFlag;
		this.discardFlag = discardFlag;
		this.payTypeOrPrintDate = payTypeOrPrintDate;
		this.bankPaidDate = bankPaidDate;
		this.returnByUserDate = returnByUserDate;
		this.reconciliationDate = reconciliationDate;
		this.bounceClearDate = bounceClearDate;
		this.discardDate = discardDate;
		this.bankId = bankId;
		this.remark = remark;
		this.typeOfUnitId = typeOfUnitId;
		this.type = type;
		this.approvalDate = approvalDate;
		this.studentId = studentId;
		this.typeOfReceipt = typeOfReceipt;
		this.totalAmount = totalAmount;
		this.totalPayFine = totalPayFine;
		this.totalPayDiscount = totalPayDiscount;
		this.totalPayExcessFee = totalPayExcessFee;
		this.isSocietyReceipt = isSocietyReceipt;
		this.existingStudent = existingStudent;
		this.schoolName=schoolName;
	}


	public FeeReceiptDetailsBean getFeeReceiptModel() {
		return feeReceiptModel;
	}


	public void setFeeReceiptModel(FeeReceiptDetailsBean feeReceiptModel) {
		this.feeReceiptModel = feeReceiptModel;
	}
	public Double getPayFee() {
		return payFee;
	}
	public void setPayFee(Double payFee) {
		this.payFee = payFee;
	}
	public Double getPendingFee() {
		return pendingFee;
	}
	public void setPendingFee(Double pendingFee) {
		this.pendingFee = pendingFee;
	}
	public String getShortSchoolName() {
		return shortSchoolName;
	}
	public void setShortSchoolName(String shortSchoolName) {
		this.shortSchoolName = shortSchoolName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	public FeeReceiptDetailsBean(Integer guestStudentId, String mobileNo, String studFName, String studMName,
			String studLName, String regNo, Integer divisionId, String divisionName, Integer yearId, String yearName,
			Integer standardId, String standardName, Integer previousFeeTypeId, String previousFeeType,
			Integer currentFeeTypeId, String currentFeeType) {
		super();
		this.studentId = guestStudentId;
		this.mobileNo = mobileNo;
		this.studFName = studFName;
		this.studMName = studMName;
		this.studLName = studLName;
		this.regNo = regNo;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.yearId = yearId;
		this.yearName = yearName;
		this.standardId = standardId;
		this.standardName = standardName;
		this.previousFeeTypeId = previousFeeTypeId;
		this.previousFeeType = previousFeeType;
		this.currentFeeTypeId = currentFeeTypeId;
		this.currentFeeType = currentFeeType;
	}

	
	public Integer getGuestStudentId() {
		return guestStudentId;
	}

	public void setGuestStudentId(Integer guestStudentId) {
		this.guestStudentId = guestStudentId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getStudFName() {
		return studFName;
	}

	public void setStudFName(String studFName) {
		this.studFName = studFName;
	}

	public String getStudMName() {
		return studMName;
	}

	public void setStudMName(String studMName) {
		this.studMName = studMName;
	}

	public String getStudLName() {
		return studLName;
	}

	public void setStudLName(String studLName) {
		this.studLName = studLName;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public Integer getPreviousFeeTypeId() {
		return previousFeeTypeId;
	}

	public void setPreviousFeeTypeId(Integer previousFeeTypeId) {
		this.previousFeeTypeId = previousFeeTypeId;
	}

	public String getPreviousFeeType() {
		return previousFeeType;
	}

	public void setPreviousFeeType(String previousFeeType) {
		this.previousFeeType = previousFeeType;
	}

	public Integer getCurrentFeeTypeId() {
		return currentFeeTypeId;
	}

	public void setCurrentFeeTypeId(Integer currentFeeTypeId) {
		this.currentFeeTypeId = currentFeeTypeId;
	}

	public String getCurrentFeeType() {
		return currentFeeType;
	}

	public void setCurrentFeeType(String currentFeeType) {
		this.currentFeeType = currentFeeType;
	}

	public Integer getExcessFeeId() {
		return excessFeeId;
	}

	public void setExcessFeeId(Integer excessFeeId) {
		this.excessFeeId = excessFeeId;
	}

	
	public List<FeeReceiptDetailsBean> getExcessFeeBean() {
		return excessFeeBean;
	}

	public void setExcessFeeBean(List<FeeReceiptDetailsBean> excessFeeBean) {
		this.excessFeeBean = excessFeeBean;
	}
	public FeeReceiptDetailsBean(String excessFee,Integer excessFeeId) {
		super();
		this.excessFeeId = excessFeeId;
		this.excessFee = excessFee;
	}

	public String getExcessFee() {
		return excessFee;
	}

	public void setExcessFee(String excessFee) {
		this.excessFee = excessFee;
	}

	

	
	
}
