package com.ingenio.announcement.bean;

import java.util.List;

public class CashBankAmountBean {

	private String date;
	private Integer schoolid;
	private Integer typeOfUnitId;	
	private Double cash;
	private Double paidFee;
	private Integer payType;
	private String payTypeName;
	private String bankId;
	private String bankName;
	private String bankAccNo;
	private String ifscNo;
	private String accBankLegderId;
	private String akey;
	private String asalt;

	private String subheadID;
	private String subheadName;
	private String receiptID;
	private String isSocietyFlag;
	private String isOtherFee;
	private String headID;
	private String headName;
	private String sinkingFlag;
	private String isScholarshipFlag;
	private String paidfeeDate;
	
	
	
	
	
	//List<List<CashBankAmountBean>> payTypeWisebifurcation;
	List<CashBankAmountBean>payTypeWisebifurcation;
	
	List<CashBankAmountBean> bankDetailsBean;
	
	List<CashBankAmountBean> totalAmountBankAndCash;
	List<CashBankAmountBean> subheadDetails;

	
	public Double getPaidFee() {
		return paidFee;
	}
	public void setPaidFee(Double paidFee) {
		this.paidFee = paidFee;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public String getPayTypeName() {
		return payTypeName;
	}
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccNo() {
		return bankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}
	public String getIfscNo() {
		return ifscNo;
	}
	public void setIfscNo(String ifscNo) {
		this.ifscNo = ifscNo;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public Integer getTypeOfUnitId() {
		return typeOfUnitId;
	}
	public void setTypeOfUnitId(Integer typeOfUnitId) {
		this.typeOfUnitId = typeOfUnitId;
	}
	public Double getCash() {
		return cash;
	}
	public void setCash(Double cash) {
		this.cash = cash;
	}
//	public List<CashBankAmountBean> getPayTypeWisebifurcation() {
//		return payTypeWisebifurcation;
//	}
//	public void setPayTypeWisebifurcation(List<CashBankAmountBean> payTypeWisebifurcation) {
//		this.payTypeWisebifurcation = payTypeWisebifurcation;
//	}
	public List<CashBankAmountBean> getPayTypeWisebifurcation() {
		return payTypeWisebifurcation;
	}
	public void setPayTypeWisebifurcation(List<CashBankAmountBean> payTypeWisebifurcation) {
		this.payTypeWisebifurcation = payTypeWisebifurcation;
	}
	public List<CashBankAmountBean> getBankDetailsBean() {
		return bankDetailsBean;
	}
	public void setBankDetailsBean(List<CashBankAmountBean> bankDetailsBean) {
		this.bankDetailsBean = bankDetailsBean;
	}
	
	public String getSubheadName() {
		return subheadName;
	}
	public void setSubheadName(String subheadName) {
		this.subheadName = subheadName;
	}
	
	public String getIsSocietyFlag() {
		return isSocietyFlag;
	}
	public void setIsSocietyFlag(String isSocietyFlag) {
		this.isSocietyFlag = isSocietyFlag;
	}
	public String getIsOtherFee() {
		return isOtherFee;
	}
	public void setIsOtherFee(String isOtherFee) {
		this.isOtherFee = isOtherFee;
	}
	
	public String getHeadName() {
		return headName;
	}
	public void setHeadName(String headName) {
		this.headName = headName;
	}
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	public String getIsScholarshipFlag() {
		return isScholarshipFlag;
	}
	public void setIsScholarshipFlag(String isScholarshipFlag) {
		this.isScholarshipFlag = isScholarshipFlag;
	}
	public String getSubheadID() {
		return subheadID;
	}
	public void setSubheadID(String subheadID) {
		this.subheadID = subheadID;
	}
	public String getReceiptID() {
		return receiptID;
	}
	public void setReceiptID(String receiptID) {
		this.receiptID = receiptID;
	}
	public String getHeadID() {
		return headID;
	}
	public void setHeadID(String headID) {
		this.headID = headID;
	}
	public List<CashBankAmountBean> getTotalAmountBankAndCash() {
		return totalAmountBankAndCash;
	}
	public void setTotalAmountBankAndCash(List<CashBankAmountBean> totalAmountBankAndCash) {
		this.totalAmountBankAndCash = totalAmountBankAndCash;
	}
	public List<CashBankAmountBean> getSubheadDetails() {
		return subheadDetails;
	}
	public void setSubheadDetails(List<CashBankAmountBean> subheadDetails) {
		this.subheadDetails = subheadDetails;
	}
	public String getAccBankLegderId() {
		return accBankLegderId;
	}
	public void setAccBankLegderId(String accBankLegderId) {
		this.accBankLegderId = accBankLegderId;
	}
	public String getAkey() {
		return akey;
	}
	public void setAkey(String akey) {
		this.akey = akey;
	}
	public String getAsalt() {
		return asalt;
	}
	public void setAsalt(String asalt) {
		this.asalt = asalt;
	}
	public String getPaidfeeDate() {
		return paidfeeDate;
	}
	public void setPaidfeeDate(String paidfeeDate) {
		this.paidfeeDate = paidfeeDate;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
	
	
	
	
}
