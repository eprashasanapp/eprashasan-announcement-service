package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;


public class EventResponseBean {
	
	private String roleName;
	private List<EventLabelBean> displayList;
	private Integer eventId;
	private Date eventStartDate;
	private Date eventEndDate;
	private String eventStartTime;
	private String eventEndtime;
	private String eventVenue;
	private String eventTitle;
	private String eventDescription;
	private String eventReminderMessage;
	private Long totalStudentCount;
	private Long deliveredCount;
	private Long unseenCount;
	private Long seenCount;
	private Long completeCount;	
	private Long  goingCount;
	private Long notGoingCount;
	private Long maybeCount;
	private String eventStatus;
	private String eventStatusFlag;
	private Integer eventAssignedtoId;
	private Integer schoolId;
	private String registrationNumber;
	private Integer staffId;
	private boolean showcount;
	private List<StudentDetailsBean> studentDetails;
	private List<StaffDetailsBean> staffDetails;
	private Integer standardId;
	private String standardName;
	private Integer divisionId;
	private String divisionName;
	private Integer staffStudentId;
	private String postedBy;	
	private String postedByRole;
	private String postedByImgUrl;
	private String sregNo;
	private String eventTypeColor;
	private String eventType;
	private Integer eventTypeId;
	private List<PersonalNoteRequestBean> personalNoteRequestList;
	private List<AnnouncementResponseBean> attachmentList;

	public EventResponseBean() {
		
	}
	
	public EventResponseBean(Integer announcementId,Long totalStudentCount, Long deliveredCount, Long unseenCount, Long seenCount,
			Long completeCount,Long goingCount ,Long notGoingCount ,Long maybeCount ) {
		super();
		this.eventId = announcementId;		
		this.totalStudentCount = totalStudentCount;
		this.deliveredCount = deliveredCount;
		this.unseenCount = unseenCount;
		this.seenCount = seenCount;
		this.completeCount = completeCount;
		this.goingCount=goingCount;
		this.notGoingCount=notGoingCount;
		this.maybeCount=maybeCount;

	}
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventStatus  ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;		
		this.eventStatus = eventStatus;
	
	}
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventVenue,String eventDescription, String eventReminderMessage,Integer staffId ,
			String eventStatus,Integer eventAssignedtoId,Integer eventTypeId , String eventType ,String eventTypeColor ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;
		this.eventVenue = eventVenue;
		this.eventDescription = eventDescription;
		this.eventReminderMessage = eventReminderMessage;
		this.staffId=staffId;
		this.eventStatus = eventStatus;
		this.eventAssignedtoId=eventAssignedtoId;
		this.eventTypeId =eventTypeId;
		this.eventType=eventType;
		this.eventTypeColor=eventTypeColor;
	}
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventVenue,String eventDescription, String eventReminderMessage,Integer staffId ,
			Integer eventTypeId , String eventType ,String eventTypeColor ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;
		this.eventVenue = eventVenue;
		this.eventDescription = eventDescription;
		this.eventReminderMessage = eventReminderMessage;
		this.staffId=staffId;
		this.eventTypeId =eventTypeId;
		this.eventType=eventType;
		this.eventTypeColor=eventTypeColor;
	}
	
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventVenue,String eventDescription, String eventReminderMessage,
			Integer standardId, Integer divisionId ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;
		this.eventVenue = eventVenue;
		this.eventDescription = eventDescription;
		this.eventReminderMessage = eventReminderMessage;
		this.standardId=standardId;
		this.divisionId =divisionId;
	}
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventVenue,String eventDescription, String eventReminderMessage,Integer staffId ,String eventStatus,Integer eventAssignedtoId
			,String postedBy,String postedByRole,String sregNo ,Integer schoolId ,Integer eventTypeId , String eventType ,String eventTypeColor ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;
		this.eventVenue = eventVenue;
		this.eventDescription = eventDescription;
		this.eventReminderMessage = eventReminderMessage;
		this.staffId=staffId;
		this.eventStatus = eventStatus;
		this.eventAssignedtoId=eventAssignedtoId;
		this.postedBy=postedBy;	
		this.postedByRole=postedByRole;
		this.sregNo=sregNo;
		this.schoolId=schoolId;
		this.eventTypeId =eventTypeId;
		this.eventType=eventType;
		this.eventTypeColor=eventTypeColor;
	}
	
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventVenue,String eventDescription, String eventReminderMessage,Integer staffId 
			,String postedBy,String postedByRole,String sregNo ,Integer schoolId ,Integer eventTypeId , String eventType ,String eventTypeColor ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;
		this.eventVenue = eventVenue;
		this.eventDescription = eventDescription;
		this.eventReminderMessage = eventReminderMessage;
		this.staffId=staffId;
		this.postedBy=postedBy;	
		this.postedByRole=postedByRole;
		this.sregNo=sregNo;
		this.schoolId=schoolId;
		this.eventTypeId =eventTypeId;
		this.eventType=eventType;
		this.eventTypeColor=eventTypeColor;
	}
	
	
	public EventResponseBean(Integer eventId, String eventTitle, Date eventStartDate, Date eventEndDate, String eventStartTime,
			String eventEndtime, String eventVenue,String eventDescription, String eventReminderMessage,Integer staffId 
			,String postedBy,String postedByRole,String sregNo ,Integer schoolId ,Integer eventTypeId ,
			String eventType ,String eventTypeColor,Integer standardId, Integer divisionId ) {
		super();
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndtime = eventEndtime;
		this.eventVenue = eventVenue;
		this.eventDescription = eventDescription;
		this.eventReminderMessage = eventReminderMessage;
		this.staffId=staffId;
		this.postedBy=postedBy;	
		this.postedByRole=postedByRole;
		this.sregNo=sregNo;
		this.schoolId=schoolId;
		this.eventTypeId =eventTypeId;
		this.eventType=eventType;
		this.eventTypeColor=eventTypeColor;
		
	}


	public EventResponseBean(Integer standardId, String standardName, Integer divisionId, String divisionName,Integer eventId, String eventTitle, 
			String eventVenue,Date eventStartDate, String eventStartTime, Integer staffStudentId,String roleName) {
		super();
		this.standardId = standardId;
		this.standardName = standardName;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.eventTitle = eventTitle;
		this.eventVenue = eventVenue;
		this.eventStartDate = eventStartDate;
		this.eventStartTime = eventStartTime;
		this.staffStudentId = staffStudentId;
		this.roleName = roleName;
	}
	
	public List<PersonalNoteRequestBean> getPersonalNoteRequestList() {
		return personalNoteRequestList;
	}

	public void setPersonalNoteRequestList(List<PersonalNoteRequestBean> personalNoteRequestList) {
		this.personalNoteRequestList = personalNoteRequestList;
	}

	public String getEventTypeColor() {
		return eventTypeColor;
	}

	public void setEventTypeColor(String eventTypeColor) {
		this.eventTypeColor = eventTypeColor;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Integer getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(Integer eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public List<EventLabelBean> getDisplayList() {
		return displayList;
	}

	public void setDisplayList(List<EventLabelBean> displayList) {
		this.displayList = displayList;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public List<AnnouncementResponseBean> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<AnnouncementResponseBean> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public String getEventEndtime() {
		return eventEndtime;
	}

	public void setEventEndtime(String eventEndtime) {
		this.eventEndtime = eventEndtime;
	}

	public String getEventVenue() {
		return eventVenue;
	}

	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventReminderMessage() {
		return eventReminderMessage;
	}

	public void setEventReminderMessage(String eventReminderMessage) {
		this.eventReminderMessage = eventReminderMessage;
	}

	public Long getTotalStudentCount() {
		return totalStudentCount;
	}

	public void setTotalStudentCount(Long totalStudentCount) {
		this.totalStudentCount = totalStudentCount;
	}

	public Long getDeliveredCount() {
		return deliveredCount;
	}

	public void setDeliveredCount(Long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}

	public Long getUnseenCount() {
		return unseenCount;
	}

	public void setUnseenCount(Long unseenCount) {
		this.unseenCount = unseenCount;
	}

	public Long getSeenCount() {
		return seenCount;
	}

	public void setSeenCount(Long seenCount) {
		this.seenCount = seenCount;
	}

	public Long getCompleteCount() {
		return completeCount;
	}

	public void setCompleteCount(Long completeCount) {
		this.completeCount = completeCount;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public Integer getEventAssignedtoId() {
		return eventAssignedtoId;
	}

	public void setEventAssignedtoId(Integer eventAssignedtoId) {
		this.eventAssignedtoId = eventAssignedtoId;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public boolean isShowcount() {
		return showcount;
	}

	public void setShowcount(boolean showcount) {
		this.showcount = showcount;
	}

	public String getEventStatusFlag() {
		return eventStatusFlag;
	}

	public void setEventStatusFlag(String eventStatusFlag) {
		this.eventStatusFlag = eventStatusFlag;
	}

	public Long getGoingCount() {
		return goingCount;
	}

	public void setGoingCount(Long goingCount) {
		this.goingCount = goingCount;
	}

	public Long getNotGoingCount() {
		return notGoingCount;
	}

	public void setNotGoingCount(Long notGoingCount) {
		this.notGoingCount = notGoingCount;
	}

	public Long getMaybeCount() {
		return maybeCount;
	}

	public void setMaybeCount(Long maybeCount) {
		this.maybeCount = maybeCount;
	}
	
	public List<StudentDetailsBean> getStudentDetails() {
		return studentDetails;
	}

	public void setStudentDetails(List<StudentDetailsBean> studentDetails) {
		this.studentDetails = studentDetails;
	}

	public List<StaffDetailsBean> getStaffDetails() {
		return staffDetails;
	}

	public void setStaffDetails(List<StaffDetailsBean> staffDetails) {
		this.staffDetails = staffDetails;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getStaffStudentId() {
		return staffStudentId;
	}

	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}

	public String getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}

	public String getPostedByImgUrl() {
		return postedByImgUrl;
	}

	public void setPostedByImgUrl(String postedByImgUrl) {
		this.postedByImgUrl = postedByImgUrl;
	}

	public String getPostedByRole() {
		return postedByRole;
	}

	public void setPostedByRole(String postedByRole) {
		this.postedByRole = postedByRole;
	}

	public String getSregNo() {
		return sregNo;
	}

	public void setSregNo(String sregNo) {
		this.sregNo = sregNo;
	}
	
	
	
	
}
