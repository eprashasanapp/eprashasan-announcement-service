package com.ingenio.announcement.bean;

import java.util.List;

public class AnnouncementUserToGroupBean {

	private Integer appUserId;
	private List<Integer>groupId;
	
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public List<Integer> getGroupId() {
		return groupId;
	}
	public void setGroupId(List<Integer> groupId) {
		this.groupId = groupId;
	}
	@Override
	public String toString() {
		return "AnnouncementUserToGroupBean [appUserId=" + appUserId + ", groupId=" + groupId + "]";
	}
	
	
	
	
}
