package com.ingenio.announcement.bean;

import java.util.List;

public class FeedbackData {

	private String questionName;
	private Integer questionId;
	private List<FeedBack> questionOption;
	
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public List<FeedBack> getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(List<FeedBack> questionOption) {
		this.questionOption = questionOption;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	
}
