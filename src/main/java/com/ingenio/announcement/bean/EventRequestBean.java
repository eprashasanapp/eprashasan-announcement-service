package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

public class EventRequestBean {

	private Integer yearId;
	private Integer staffId;
	private Integer userId;
	private String IPAddress;
	private String deviceType;
	private Date eventStartDate;
	private String eventStartTime;
	private Date eventEndDate;
	private String eventEndtime;
	private String eventVenue;
	private String eventTitle;
	private String eventDescription;
	private String eventReminderMessage;	
	private Integer schoolId;
	private Integer eventId;
	private Integer apiFlag;
	private Integer allSchoolFlag;
	private List<EventResponseBean> eventResponseList;
	private String teacherName;
	private Integer eventTypeId;
	private String isNotificationSend;
	private Integer standardId;
	private Integer divisionId;
	


	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getIsNotificationSend() {
		return isNotificationSend;
	}

	public void setIsNotificationSend(String isNotificationSend) {
		this.isNotificationSend = isNotificationSend;
	}

	public Integer getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(Integer eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}


	public Integer getApiFlag() {
		return apiFlag;
	}

	public void setApiFlag(Integer apiFlag) {
		this.apiFlag = apiFlag;
	}
	

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public String getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventEndtime() {
		return eventEndtime;
	}

	public void setEventEndtime(String eventEndtime) {
		this.eventEndtime = eventEndtime;
	}

	public String getEventVenue() {
		return eventVenue;
	}

	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventReminderMessage() {
		return eventReminderMessage;
	}

	public void setEventReminderMessage(String eventReminderMessage) {
		this.eventReminderMessage = eventReminderMessage;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public List<EventResponseBean> getEventResponseList() {
		return eventResponseList;
	}

	public void setEventResponseList(List<EventResponseBean> eventResponseList) {
		this.eventResponseList = eventResponseList;
	}

	public Integer getAllSchoolFlag() {
		return allSchoolFlag;
	}

	public void setAllSchoolFlag(Integer allSchoolFlag) {
		this.allSchoolFlag = allSchoolFlag;
	}
	
}
