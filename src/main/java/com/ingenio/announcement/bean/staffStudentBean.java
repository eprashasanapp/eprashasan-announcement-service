package com.ingenio.announcement.bean;

public class staffStudentBean {

	private Integer staffStudentId;
	private String contatctNumber;
	private String name;
	private Integer yearId;
	private String year;
	private Integer standardId;
	private Integer announcementId;
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	public String getContatctNumber() {
		return contatctNumber;
	}
	public void setContatctNumber(String contatctNumber) {
		this.contatctNumber = contatctNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	
	
}
