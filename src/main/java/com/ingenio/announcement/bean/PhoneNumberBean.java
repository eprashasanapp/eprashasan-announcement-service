package com.ingenio.announcement.bean;

import java.util.List;

import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SentEmailModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;

public class PhoneNumberBean {

	private String staffContact;
	private Integer staffId;
	private Integer studentId;
	private Integer staffStudentId;
	private String role;
	private String studentContackNumber;
	private String studentName;
	private String staffName;
	private Integer staffUserId;
	private Integer studentUserId;
	private String staffEmailId;
	private String studentEmailId;
	private String staffFcmToken;
	private String studentFcmToken;
	private String studRegNo;
	private Integer standardId;
	private String standardName;
	private Integer divisionId;
	private String divisionName;
	private List<SentMessagesReadStatusModel> readStatus;
	private SentMessagesModel sentMessage;
	private Integer userId;
	private String fcmToken;
	private Integer groupId;
	private String groupName;
	private String emailId;
	private Integer isView;
	private String isLike;
	private Integer isComment;
	private Integer isReaction;
	private Integer yearId;
	private String year;
	private Integer schoolid;
	List<SaveSendNotificationModel> allNotifications;
	List<SentEmailModel> sentEmail;
	

	public String getStaffContact() {
		return staffContact;
	}
	public void setStaffContact(String staffContact) {
		this.staffContact = staffContact;
	}
	public String getStudentContackNumber() {
		return studentContackNumber;
	}
	public void setStudentContackNumber(String studentContackNumber) {
		this.studentContackNumber = studentContackNumber;
	}
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public PhoneNumberBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	
	//for fee pending
	public PhoneNumberBean(Integer staffStudentId, String role, String studentContackNumber, String studentName,Integer studentUserId,String emailId,Integer yearId,String year,Integer standardId) {
		super();
		this.staffStudentId = staffStudentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName=studentName;
		this.studentUserId=studentUserId;
		this.emailId=emailId;
		this.yearId=yearId;
		this.year=year;
		this.standardId=standardId;
	}
	
	public PhoneNumberBean(Integer staffStudentId, String role, String studentContackNumber, String studentName,Integer studentUserId,String emailId) {
		super();
		this.staffStudentId = staffStudentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName=studentName;
		this.studentUserId=studentUserId;
		this.emailId=emailId;
	}
	
	public PhoneNumberBean(Integer staffStudentId, String role, String studentContackNumber, String studentName) {
		super();
		this.staffStudentId = staffStudentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName=studentName;
	}
	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
			String studentContackNumber) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.studentId = studentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	
	//assign to report controller
//for fee pending
	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
			String studentContackNumber, String studentName, String staffName,Integer staffUserId,Integer studentUserId,String emailId,Integer yearId,String year,Integer standardId) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.studentId = studentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName = studentName;
		this.staffName = staffName;
		this.staffUserId=staffUserId;
		this.studentUserId=studentUserId;
		this.emailId=emailId;
		this.yearId=yearId;
		this.year=year;
		this.standardId=standardId;
		
	}
	
	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
			String studentContackNumber, String studentName, String staffName,Integer staffUserId,Integer studentUserId,String emailId) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.studentId = studentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName = studentName;
		this.staffName = staffName;
		this.staffUserId=staffUserId;
		this.studentUserId=studentUserId;
		this.emailId=emailId;
		
	}
//	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
//			String studentContackNumber, String studentName, String staffName,Integer staffUserId,Integer studentUserId) {
//		super();
//		this.staffContact = staffContact;
//		this.staffId = staffId;
//		this.studentId = studentId;
//		this.role = role;
//		this.studentContackNumber = studentContackNumber;
//		this.studentName = studentName;
//		this.staffName = staffName;
//		this.staffUserId=staffUserId;
//		this.studentUserId=studentUserId;
//		
//		
//	}
	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
			String studentContackNumber, String studentName, String staffName) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.studentId = studentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName = studentName;
		this.staffName = staffName;
		
		
	}
	
	//
	public PhoneNumberBean(String staffContact, Integer staffId ,String role,String staffName,Integer schoolid,Integer staffUserId ) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.role = role;
		this.staffName = staffName;
		this.schoolid=schoolid;
		this.staffUserId=staffUserId;
		
		
	}
	
	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
			String studentContackNumber, String studentName, String staffName,Integer staffUserId,Integer studentUserId,String staffEmailId,String studentEmailId,
			String staffFcmToken,String  studentFcmToken,String studRegNo,Integer standardId,String standardName,Integer divisionId,String divisionName,Integer isView,String isLike,Integer isComment,Integer isReaction) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.studentId = studentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName = studentName;
		this.staffName = staffName;
		this.staffUserId=staffUserId;
		this.studentUserId=studentUserId;
		this.staffEmailId=staffEmailId;
		this.studentEmailId=studentEmailId;
		this.staffFcmToken=staffFcmToken;
		this.studentFcmToken=studentFcmToken;
		this.studRegNo=studRegNo;
		this.standardId=standardId;
		this.standardName=standardName;
		this.divisionId=divisionId;
		this.divisionName=divisionName;
		this.isView=isView;
		this.isLike=isLike;
		this.isComment=isComment;
		this.isReaction=isReaction;
	}
	
	public PhoneNumberBean( Integer studentId,Integer studentUserId,
			String studentContackNumber, String studentName,String studentEmailId,
			String  studentFcmToken,String studRegNo,Integer standardId,String standardName,Integer divisionId,String divisionName,Integer isView,String isLike,Integer isComment,Integer isReaction) {
		super();
		this.studentId=studentId;
		this.studentContackNumber = studentContackNumber;
		this.studentName = studentName;
		this.studentUserId=studentUserId;
		this.studentEmailId=studentEmailId;
		this.studentFcmToken=studentFcmToken;
		this.studRegNo=studRegNo;
		this.standardId=standardId;
		this.standardName=standardName;
		this.divisionId=divisionId;
		this.divisionName=divisionName;
		this.isView=isView;
		this.isLike=isLike;
		this.isComment=isComment;
		this.isReaction=isReaction;
	}
	
	public Integer getStaffUserId() {
		return staffUserId;
	}
	public void setStaffUserId(Integer staffUserId) {
		this.staffUserId = staffUserId;
	}
	public Integer getStudentUserId() {
		return studentUserId;
	}
	public void setStudentUserId(Integer studentUserId) {
		this.studentUserId = studentUserId;
	}
	
	public String getStaffEmailId() {
		return staffEmailId;
	}
	public void setStaffEmailId(String staffEmailId) {
		this.staffEmailId = staffEmailId;
	}
	public String getStudentEmailId() {
		return studentEmailId;
	}
	public void setStudentEmailId(String studentEmailId) {
		this.studentEmailId = studentEmailId;
	}
	public String getStaffFcmToken() {
		return staffFcmToken;
	}
	public void setStaffFcmToken(String staffFcmToken) {
		this.staffFcmToken = staffFcmToken;
	}
	public String getStudentFcmToken() {
		return studentFcmToken;
	}
	public void setStudentFcmToken(String studentFcmToken) {
		this.studentFcmToken = studentFcmToken;
	}
	public String getStudRegNo() {
		return studRegNo;
	}
	public void setStudRegNo(String studRegNo) {
		this.studRegNo = studRegNo;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public List<SaveSendNotificationModel> getAllNotifications() {
		return allNotifications;
	}
	public void setAllNotifications(List<SaveSendNotificationModel> allNotifications) {
		this.allNotifications = allNotifications;
	}
	public List<SentMessagesReadStatusModel> getReadStatus() {
		return readStatus;
	}
	public void setReadStatus(List<SentMessagesReadStatusModel> readStatus) {
		this.readStatus = readStatus;
	}
	public SentMessagesModel getSentMessage() {
		return sentMessage;
	}
	public void setSentMessage(SentMessagesModel sentMessage) {
		this.sentMessage = sentMessage;
	}
	

	//group report constructor
	public PhoneNumberBean(String staffContact, Integer staffId, Integer studentId, String role,
			String studentContackNumber, String studentName, String staffName,Integer userId,String staffEmailId,String studentEmailId,
			String fcmToken,String studRegNo,Integer standardId,String standardName,Integer divisionId,String divisionName,Integer groupId,String groupName,Integer isView,String isLike,Integer isComment,Integer isReaction) {
		super();
		this.staffContact = staffContact;
		this.staffId = staffId;
		this.studentId = studentId;
		this.role = role;
		this.studentContackNumber = studentContackNumber;
		this.studentName = studentName;
		this.staffName = staffName;
		this.userId=userId;
		this.staffEmailId=staffEmailId;
		this.studentEmailId=studentEmailId;
		this.fcmToken=fcmToken;
		this.studRegNo=studRegNo;
		this.standardId=standardId;
		this.standardName=standardName;
		this.divisionId=divisionId;
		this.divisionName=divisionName;
		this.groupId=groupId;
		this.groupName=groupName;
		this.isView=isView;
		this.isLike=isLike;
		this.isComment=isComment;
		this.isReaction=isReaction;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFcmToken() {
		return fcmToken;
	}
	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Integer getIsView() {
		return isView;
	}
	public void setIsView(Integer isView) {
		this.isView = isView;
	}
	public String getIsLike() {
		return isLike;
	}
	public void setIsLike(String isLike) {
		this.isLike = isLike;
	}
	public Integer getIsComment() {
		return isComment;
	}
	public void setIsComment(Integer isComment) {
		this.isComment = isComment;
	}
	public Integer getIsReaction() {
		return isReaction;
	}
	public void setIsReaction(Integer isReaction) {
		this.isReaction = isReaction;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public List<SentEmailModel> getSentEmail() {
		return sentEmail;
	}
	public void setSentEmail(List<SentEmailModel> sentEmail) {
		this.sentEmail = sentEmail;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	public PhoneNumberBean(Integer staffStudentId, Integer studentUserId) {
		super();
		this.staffStudentId = staffStudentId;
		this.studentUserId = studentUserId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
}
