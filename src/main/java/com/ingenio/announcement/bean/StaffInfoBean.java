package com.ingenio.announcement.bean;

public class StaffInfoBean {

	private String mobileNumber;
	private String emailId;
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public StaffInfoBean(String mobileNumber, String emailId) {
		super();
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
	}
	public StaffInfoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
}
