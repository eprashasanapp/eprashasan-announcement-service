package com.ingenio.announcement.bean;

import java.util.Date;

public class StandardDivisionBean {
	
	private Integer standardId;
	private String standardName;
	private Integer divisionId;
	private String divisionName;
    private Integer yearId;
    private Date publishDate;
	public StandardDivisionBean(Integer standardId, String standardName,Integer divisionId, String divisionName) {
		super();
		this.standardId = standardId;
		this.divisionId = divisionId;
		this.standardName = standardName;
		this.divisionName = divisionName;
	}
	
	public StandardDivisionBean(Integer standardId, String standardName) {
		super();
		this.standardId = standardId;
		this.standardName = standardName;
	}
	
	public StandardDivisionBean(String divisionName,Integer divisionId) {
		super();
		this.divisionId = divisionId;
		this.divisionName = divisionName;
	}
	
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public StandardDivisionBean(Integer standardId, String standardName, Integer divisionId, String divisionName,
			Integer yearId, Date publishDate) {
		super();
		this.standardId = standardId;
		this.standardName = standardName;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.yearId = yearId;
		this.publishDate = publishDate;
	}
	
	

}
