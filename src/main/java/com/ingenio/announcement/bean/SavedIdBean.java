package com.ingenio.announcement.bean;

public class SavedIdBean {

	private String mobileNumber;
	private String savedId;
	private Integer staffStudentId;
	private String name;
	private Integer yearId;
	private String year;
	private Integer standardId;
	private Integer announcementId;
	private String rCode;
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSavedId() {
		return savedId;
	}
	public void setSavedId(String savedId) {
		this.savedId = savedId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public String getrCode() {
		return rCode;
	}
	public void setrCode(String rCode) {
		this.rCode = rCode;
	}
	
	
}
