package com.ingenio.announcement.bean;

import java.util.List;

public class WhatsAppMessageBean {

	private String instanceId;
	private String accessToken;
	private String mobileNo;
	private String Encryptedmessage;
	private Integer renewId;
	private Integer schoolId;
	private String typeFlag;
	private String message;
	private String title;
	private String messageType;
	private Integer isMediaType;
	private String imagePath;
	private String assignMsgCount;
	private String serverKey;
	private List<FCMTokenBean> tokenList;
	List<WhatsAppMessageBean> sendingList;
	private Integer customizeMessageFlag;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEncryptedmessage() {
		return Encryptedmessage;
	}
	public void setEncryptedmessage(String encryptedmessage) {
		Encryptedmessage = encryptedmessage;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getTypeFlag() {
		return typeFlag;
	}
	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public Integer getIsMediaType() {
		return isMediaType;
	}
	public void setIsMediaType(Integer isMediaType) {
		this.isMediaType = isMediaType;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getAssignMsgCount() {
		return assignMsgCount;
	}
	public void setAssignMsgCount(String assignMsgCount) {
		this.assignMsgCount = assignMsgCount;
	}
	public List<FCMTokenBean> getTokenList() {
		return tokenList;
	}
	public void setTokenList(List<FCMTokenBean> tokenList) {
		this.tokenList = tokenList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<WhatsAppMessageBean> getSendingList() {
		return sendingList;
	}
	public void setSendingList(List<WhatsAppMessageBean> sendingList) {
		this.sendingList = sendingList;
	}
	public Integer getCustomizeMessageFlag() {
		return customizeMessageFlag;
	}
	public void setCustomizeMessageFlag(Integer customizeMessageFlag) {
		this.customizeMessageFlag = customizeMessageFlag;
	}
	public String getServerKey() {
		return serverKey;
	}
	public void setServerKey(String serverkey2) {
		this.serverKey = serverkey2;
	}
	
	
}
