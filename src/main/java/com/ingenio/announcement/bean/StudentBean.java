package com.ingenio.announcement.bean;

public class StudentBean {

	private Integer studentId;
	private Integer appUserId;
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public StudentBean(Integer studentId, Integer appUserId) {
		super();
		this.studentId = studentId;
		this.appUserId = appUserId;
	}
	
	
}
