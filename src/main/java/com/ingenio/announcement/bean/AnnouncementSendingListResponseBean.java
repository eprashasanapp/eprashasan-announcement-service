package com.ingenio.announcement.bean;

import java.util.List;

import com.ingenio.announcement.model.AnnouncementAssignToClassDivisionModel;

public class AnnouncementSendingListResponseBean {
	
	
	private List<Integer> groupIdList;
	private List<ClassDivisionBean> classDivisionList;
	private List<AssignToBean> assignToList;
	
	public List<Integer> getGroupIdList() {
		return groupIdList;
	}
	public void setGroupIdList(List<Integer> groupIdList) {
		this.groupIdList = groupIdList;
	}
	public List<ClassDivisionBean> getClassDivisionList() {
		return classDivisionList;
	}
	public void setClassDivisionList(List<ClassDivisionBean> classDivisionList) {
		this.classDivisionList = classDivisionList;
	}
	public List<AssignToBean> getAssignToList() {
		return assignToList;
	}
	public void setAssignToList(List<AssignToBean> assignToList) {
		this.assignToList = assignToList;
	}
	
	
	

}
