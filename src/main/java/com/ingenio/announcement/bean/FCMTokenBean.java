package com.ingenio.announcement.bean;

public class FCMTokenBean {
	
	private String token;
	private String deviceType;
	private String mobileNo;
	private Integer userId;
	private String menuName;
	private Integer menuId;
	private Integer renewStaffId;
	private String typeFlag;
	private String messageType;
	private Integer schoolId;
	private Integer notificationId;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	public FCMTokenBean() {
		super();
	}
	public FCMTokenBean(String token, String deviceType) {
		super();
		this.token = token;
		this.deviceType = deviceType;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public FCMTokenBean(String token, String deviceType, Integer userId) {
		super();
		this.token = token;
		this.deviceType = deviceType;
		this.userId = userId;
	}
	public FCMTokenBean(String token, String deviceType, String mobileNo, Integer userId) {
		super();
		this.token = token;
		this.deviceType = deviceType;
		this.mobileNo = mobileNo;
		this.userId = userId;
	}
	public Integer getRenewStaffId() {
		return renewStaffId;
	}
	public void setRenewStaffId(Integer renewStaffId) {
		this.renewStaffId = renewStaffId;
	}
	public String getTypeFlag() {
		return typeFlag;
	}
	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}
	
	

}
