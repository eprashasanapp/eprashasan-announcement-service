package com.ingenio.announcement.bean;

import java.util.Date;

import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.StudentMasterModel;
import com.ingenio.announcement.model.YearMasterModel;

public class StudentStandardRenewBean {

	
	private static final long serialVersionUID = 1L;
	private Integer renewStudentId;
	private Integer divisionId;
	private Integer standardId;
	private String mobileNo;
	
	public StudentStandardRenewBean(Integer renewStudentId, Integer standardId,Integer divisionId) {
		super();
		this.renewStudentId = renewStudentId;
		this.divisionId = divisionId;
		this.standardId = standardId;
	}
	
	public StudentStandardRenewBean(Integer renewStudentId, Integer standardId,Integer divisionId,String mobileNo) {
		super();
		this.renewStudentId = renewStudentId;
		this.divisionId = divisionId;
		this.standardId = standardId;
		this.mobileNo=mobileNo;
	}
	public Integer getRenewStudentId() {
		return renewStudentId;
	}
	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	
	
}