package com.ingenio.announcement.bean;

import org.springframework.web.multipart.MultipartFile;

public class FeedbackEmailRequestBean {
	private String title;
	private String description;
	private String userName;
	private String role;
	private String regNo;
	private String contactNo;
	private String schoolName;
	private String renewStudentId;
	private String orderId;
	private String schoolId;
	private String studFeeID;
	private String schoolSansthaKey;
	private String processId;
	private String statusCode;
	private String statusMessage;
	private String status;
	private MultipartFile File; 

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getRenewStudentId() {
		return renewStudentId;
	}
	public void setRenewStudentId(String renewStudentId) {
		this.renewStudentId = renewStudentId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getStudFeeID() {
		return studFeeID;
	}
	public void setStudFeeID(String studFeeID) {
		this.studFeeID = studFeeID;
	}
	public String getSchoolSansthaKey() {
		return schoolSansthaKey;
	}
	public void setSchoolSansthaKey(String schoolSansthaKey) {
		this.schoolSansthaKey = schoolSansthaKey;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public MultipartFile getFile() {
		return File;
	}
	public void setFile(MultipartFile file) {
		File = file;
	}
	
	
	
}
