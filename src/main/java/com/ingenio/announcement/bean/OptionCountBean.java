package com.ingenio.announcement.bean;

public class OptionCountBean {

	private Integer optionId;
	private String optionName;
	private Integer userCount;
	
	
	
	public OptionCountBean(Integer optionId, String optionName, Integer userCount) {
		super();
		this.optionId = optionId;
		this.optionName = optionName;
		this.userCount = userCount;
	}
	
	
	public OptionCountBean(Integer optionId, String optionName) {
		super();
		this.optionId = optionId;
		this.optionName = optionName;
	}


	public OptionCountBean() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getOptionId() {
		return optionId;
	}
	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public Integer getUserCount() {
		return userCount;
	}
	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}
	
}
