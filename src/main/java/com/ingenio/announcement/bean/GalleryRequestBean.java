package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

public class GalleryRequestBean {

	private Integer yearId;
	private Integer staffId;
	private Integer userId;
	private String IPAddress;
	private String deviceType;
	private String galleryTitle;
	private String galleryMessage;
	private String smsTitle;
	private String smsMessage;
	private Integer schoolId;
	private Integer galleryId;
	private Date startDate;
	private Integer apiFlag;
	private List<GalleryResponseBean> galleryResponseList;
	private String teacherName;
	private String time;

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	

	public String getGalleryTitle() {
		return galleryTitle;
	}

	public void setGalleryTitle(String galleryTitle) {
		this.galleryTitle = galleryTitle;
	}

	public String getGalleryMessage() {
		return galleryMessage;
	}

	public void setGalleryMessage(String galleryMessage) {
		this.galleryMessage = galleryMessage;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getApiFlag() {
		return apiFlag;
	}

	public void setApiFlag(Integer apiFlag) {
		this.apiFlag = apiFlag;
	}

	

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSmsTitle() {
		return smsTitle;
	}

	public void setSmsTitle(String smsTitle) {
		this.smsTitle = smsTitle;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public Integer getGalleryId() {
		return galleryId;
	}

	public void setGalleryId(Integer galleryId) {
		this.galleryId = galleryId;
	}

	public List<GalleryResponseBean> getGalleryResponseList() {
		return galleryResponseList;
	}

	public void setGalleryResponseList(List<GalleryResponseBean> galleryResponseList) {
		this.galleryResponseList = galleryResponseList;
	}

	
	
	

}
