package com.ingenio.announcement.bean;

import java.util.Date;

public class AnnouncementBean {

	private Integer priority;
	private Integer otherSchoolId;
	private Integer announcementTypeId;
	private Date deadline;
	private String complaintBy;
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Integer getOtherSchoolId() {
		return otherSchoolId;
	}
	public void setOtherSchoolId(Integer otherSchoolId) {
		this.otherSchoolId = otherSchoolId;
	}
	public AnnouncementBean(Integer priority, Integer otherSchoolId,Integer announcementTypeId,Date deadLine,String complaintBy) {
		super();
		this.priority = priority;
		this.otherSchoolId = otherSchoolId;
		this.announcementTypeId=announcementTypeId;
		this.deadline=deadLine;
		this.complaintBy=complaintBy;
	}
	public AnnouncementBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getAnnouncementTypeId() {
		return announcementTypeId;
	}
	public void setAnnouncementTypeId(Integer announcementTypeId) {
		this.announcementTypeId = announcementTypeId;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public String getComplaintBy() {
		return complaintBy;
	}
	public void setComplaintBy(String complaintBy) {
		this.complaintBy = complaintBy;
	}
	
}
