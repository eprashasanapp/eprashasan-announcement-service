package com.ingenio.announcement.bean;

public class AssignToBean {
	
	private String role;
	private Integer staffStudentId;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getStaffStudentId() {
		return staffStudentId;
	}
	public void setStaffStudentId(Integer staffStudentId) {
		this.staffStudentId = staffStudentId;
	}
	
	
	

}
