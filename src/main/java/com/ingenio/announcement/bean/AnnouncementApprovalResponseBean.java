package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;

import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AnnouncementTypeModel;
import com.ingenio.announcement.model.AppUserModel;
import com.ingenio.announcement.model.SchoolMasterModel;

public class AnnouncementApprovalResponseBean {

	private Integer postId;
	private Integer approvalStatusId;
	private Integer appuserId;
	private Integer schoolId;
	private String status;
	private String remark;

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public Integer getApprovalStatusId() {
		return approvalStatusId;
	}

	public void setApprovalStatusId(Integer approvalStatusId) {
		this.approvalStatusId = approvalStatusId;
	}

	public Integer getAppuserId() {
		return appuserId;
	}

	public void setAppuserId(Integer appuserId) {
		this.appuserId = appuserId;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
