package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.SchoolMasterModel;

public class PersonalNoteRequestBean {
	
	private Integer personalNoteId;
	private String personalNote;
	private String reminderTime;
	private String reminderDate;
	private Date reminderDate1;
	private Integer schoolId;
	private Integer userId;
	private Integer standardId;
	private Integer divisionId;
	
	
	
	public PersonalNoteRequestBean(Integer personalNoteId, String personalNote, String reminderTime,
			String reminderDate, Integer schoolId, Integer userId) {
		super();
		this.personalNoteId = personalNoteId;
		this.personalNote = personalNote;
		this.reminderTime = reminderTime;
		this.reminderDate = reminderDate;
		this.schoolId = schoolId;
		this.userId = userId;
	}
	
	public PersonalNoteRequestBean(Integer personalNoteId, String personalNote, String reminderTime,
			Date reminderDate1, Integer schoolId, Integer userId) {
		super();
		this.personalNoteId = personalNoteId;
		this.personalNote = personalNote;
		this.reminderTime = reminderTime;
		this.reminderDate1 = reminderDate1;
		this.schoolId = schoolId;
		this.userId = userId;
	}
	
	
	public Date getReminderDate1() {
		return reminderDate1;
	}

	public PersonalNoteRequestBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setReminderDate1(Date reminderDate1) {
		this.reminderDate1 = reminderDate1;
	}

	public Integer getStandardId() {
		return standardId;
	}




	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}




	public Integer getDivisionId() {
		return divisionId;
	}




	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}




	public Integer getPersonalNoteId() {
		return personalNoteId;
	}
	public void setPersonalNoteId(Integer personalNoteId) {
		this.personalNoteId = personalNoteId;
	}
	
	public String getPersonalNote() {
		return personalNote;
	}
	public void setPersonalNote(String personalNote) {
		this.personalNote = personalNote;
	}
	public String getReminderTime() {
		return reminderTime;
	}
	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}
	public String getReminderDate() {
		return reminderDate;
	}
	public void setReminderDate(String reminderDate) {
		this.reminderDate = reminderDate;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
}
