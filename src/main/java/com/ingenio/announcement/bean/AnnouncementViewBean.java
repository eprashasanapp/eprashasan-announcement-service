package com.ingenio.announcement.bean;

public class AnnouncementViewBean {

	private Integer viewsId;
	private Integer announcementId ;
	private Integer appUserId;
	private Integer schoolid;
	public Integer getViewsId() {
		return viewsId;
	}
	public void setViewsId(Integer viewsId) {
		this.viewsId = viewsId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
	

}
