package com.ingenio.announcement.bean;

public class AnnouncementReactionBean {

	private Integer reactionId;
	private Integer announcementId;
	private Integer schoolid;
	private Integer appUserId;
	private Integer questionId;
	private Integer optionId;
	public Integer getReactionId() {
		return reactionId;
	}
	public void setReactionId(Integer reactionId) {
		this.reactionId = reactionId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public Integer getOptionId() {
		return optionId;
	}
	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}
	
	

}
