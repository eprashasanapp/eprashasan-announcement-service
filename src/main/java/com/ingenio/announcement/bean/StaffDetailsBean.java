
package com.ingenio.announcement.bean;

public class StaffDetailsBean {
	
	private String staffName;
	private Integer staffId;
	private String regNo;
	private String staffPhotoUrl;

	public StaffDetailsBean() {
		super();
	}

	public StaffDetailsBean(Integer staffId,String staffName,String regNo) {
		super();
		this.staffName = staffName;
		this.staffId=staffId;
		this.regNo=regNo;
	}
	
	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getStaffPhotoUrl() {
		return staffPhotoUrl;
	}

	public void setStaffPhotoUrl(String staffPhotoUrl) {
		this.staffPhotoUrl = staffPhotoUrl;
	}

}
