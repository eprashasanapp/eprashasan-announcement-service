package com.ingenio.announcement.bean;

public class AnnouncementOptionOfQuestionBean {

	private Integer announcementId;
	private Integer schoolid;
	private Integer announcementQuestionId;
	private String options;
	private Integer announcementOptionOfQuestionId;
	
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public Integer getAnnouncementQuestionId() {
		return announcementQuestionId;
	}
	public void setAnnouncementQuestionId(Integer announcementQuestionId) {
		this.announcementQuestionId = announcementQuestionId;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public Integer getAnnouncementOptionOfQuestionId() {
		return announcementOptionOfQuestionId;
	}
	public void setAnnouncementOptionOfQuestionId(Integer announcementOptionOfQuestionId) {
		this.announcementOptionOfQuestionId = announcementOptionOfQuestionId;
	}
	
	
	

}
