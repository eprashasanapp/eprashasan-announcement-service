package com.ingenio.announcement.bean;

public class FeedBack {

	private String optionName;
	private Integer optionId;
	
	

	public Integer getOptionId() {
		return optionId;
	}

	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	
	
	
}
