package com.ingenio.announcement.bean;

import java.util.List;

public class ResponceBean {

	private Integer Schoolid;
	private List<CashBankAmountBean> cashBankAmountList;
	public Integer getSchoolid() {
		return Schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		Schoolid = schoolid;
	}
	public List<CashBankAmountBean> getCashBankAmountList() {
		return cashBankAmountList;
	}
	public void setCashBankAmountList(List<CashBankAmountBean> cashBankAmountList) {
		this.cashBankAmountList = cashBankAmountList;
	}
	
	
}
