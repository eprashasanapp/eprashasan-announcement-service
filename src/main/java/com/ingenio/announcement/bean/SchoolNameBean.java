package com.ingenio.announcement.bean;

public class SchoolNameBean {

	private String schoolShortCode;
	private String schoolName;
	public String getSchoolShortCode() {
		return schoolShortCode;
	}
	public void setSchoolShortCode(String schoolShortCode) {
		this.schoolShortCode = schoolShortCode;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public SchoolNameBean(String schoolShortCode, String schoolName) {
		super();
		this.schoolShortCode = schoolShortCode;
		this.schoolName = schoolName;
	}
	
	
	
}
