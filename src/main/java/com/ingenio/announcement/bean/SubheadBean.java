package com.ingenio.announcement.bean;

import java.math.BigInteger;

public class SubheadBean {

	private Integer paidId;
	private BigInteger feeSettingMultiId;
	private Integer headId;
	private Integer subheadId;
	private Integer studFeeId;
	private Double paidFee;
	private String paidFeeDate;
	private Integer payType;
	private String ddchqno;
	private String narration;
	private Double discount;
	private Integer yearId;
	private String orderId;
	private String transcationId;
	private Double tax;
	private Integer bankId;
	

	public BigInteger getFeeSettingMultiId() {
		return feeSettingMultiId;
	}
	public void setFeeSettingMultiId(BigInteger feeSettingMultiId) {
		this.feeSettingMultiId = feeSettingMultiId;
	}
	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	public Integer getSubheadId() {
		return subheadId;
	}
	public void setSubheadId(Integer subheadId) {
		this.subheadId = subheadId;
	}
	public Integer getStudFeeId() {
		return studFeeId;
	}
	public void setStudFeeId(Integer studFeeId) {
		this.studFeeId = studFeeId;
	}
	public Double getPaidFee() {
		return paidFee;
	}
	public void setPaidFee(Double paidFee) {
		this.paidFee = paidFee;
	}
	public String getPaidFeeDate() {
		return paidFeeDate;
	}
	public void setPaidFeeDate(String paidFeeDate) {
		this.paidFeeDate = paidFeeDate;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public String getDdchqno() {
		return ddchqno;
	}
	public void setDdchqno(String ddchqno) {
		this.ddchqno = ddchqno;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTranscationId() {
		return transcationId;
	}
	public void setTranscationId(String transcationId) {
		this.transcationId = transcationId;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Integer getBankId() {
		return bankId;
	}
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public Integer getPaidId() {
		return paidId;
	}
	public void setPaidId(Integer paidId) {
		this.paidId = paidId;
	}
	
	
	
	

}
