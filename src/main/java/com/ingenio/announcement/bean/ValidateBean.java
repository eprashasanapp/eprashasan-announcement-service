package com.ingenio.announcement.bean;

public class ValidateBean {

	private Integer tempPassId;
	private String mobileNumber;
	private Integer schoolId=0;
	private String otp;
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Integer getTempPassId() {
		return tempPassId;
	}
	public void setTempPassId(Integer tempPassId) {
		this.tempPassId = tempPassId;
	}
	
	
}
