package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

import com.ingenio.announcement.model.FeeReceiptEditLogModel;


public class EditFeeReceiptBean {


	private Integer schoolid;
	private Integer receiptId;
//	private Date newDate; //yyyy-mm-dd 
//	
	private FeeReceiptEditLogModel feeReceiptEditLogModel;
	
	List<SubheadBean> subheadBean;	//private List<Fee>

	//private List<FeePaidFeeModel> feePaidFeeModel;
	
	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	

	public Integer getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}

	public List<SubheadBean> getSubheadBean() {
		return subheadBean;
	}

	public void setSubheadBean(List<SubheadBean> subheadBean) {
		this.subheadBean = subheadBean;
	}

	public FeeReceiptEditLogModel getFeeReceiptEditLogModel() {
		return feeReceiptEditLogModel;
	}

	public void setFeeReceiptEditLogModel(FeeReceiptEditLogModel feeReceiptEditLogModel) {
		this.feeReceiptEditLogModel = feeReceiptEditLogModel;
	}

	
		
	
	
}
