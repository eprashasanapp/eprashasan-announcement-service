package com.ingenio.announcement.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.http.HttpStatus;

@XmlRootElement
public class ResponseBodyBean<T> {
	
	private String statusCode;
	private HttpStatus stausMessage;
	private List<T> responseList;
	private T saveId;
	private String message;
	
	public ResponseBodyBean() {
		super();
	}

	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, List<T> responseList) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseList = responseList;
	}
	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, List<T> responseList,String message) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseList = responseList;
		this.message=message;
	}
	
	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, T saveId) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.saveId = saveId;
	}

	public List<T> getResponseList() {
		return responseList;
	}
	
	public void setResponseList(List<T> responseList) {
		this.responseList = responseList;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public HttpStatus getStausMessage() {
		return stausMessage;
	}

	public void setStausMessage(HttpStatus stausMessage) {
		this.stausMessage = stausMessage;
	}

	public T getSaveId() {
		return saveId;
	}

	public void setSaveId(T saveId) {
		this.saveId = saveId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
