package com.ingenio.announcement.bean;

public class Tokenbean {

	private String token;
	private String mobileNumber;
	private Integer appUserId;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Tokenbean(String token, String mobileNumber) {
		super();
		this.token = token;
		this.mobileNumber = mobileNumber;
	}
	public Tokenbean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public Tokenbean(String token, String mobileNumber, Integer appUserId) {
		super();
		this.token = token;
		this.mobileNumber = mobileNumber;
		this.appUserId = appUserId;
	}
	
	
}
