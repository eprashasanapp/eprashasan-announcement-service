package com.ingenio.announcement.bean;

public class StudentDifferenceBean {
	private Integer studentId;
	private Integer renewstudentId;
	private String Student_RegNo;
	private String studFName;
	private String studLName;
	private Integer monthId;
	private Integer rollNo;
	private Integer stdId;
    private String standardName;
    private Integer divId;
    private String divisionName;
    private Integer yearId;
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public Integer getRenewstudentId() {
		return renewstudentId;
	}
	public void setRenewstudentId(Integer renewstudentId) {
		this.renewstudentId = renewstudentId;
	}
	public String getStudent_RegNo() {
		return Student_RegNo;
	}
	public void setStudent_RegNo(String student_RegNo) {
		Student_RegNo = student_RegNo;
	}
	public String getStudFName() {
		return studFName;
	}
	public void setStudFName(String studFName) {
		this.studFName = studFName;
	}
	public String getStudLName() {
		return studLName;
	}
	public void setStudLName(String studLName) {
		this.studLName = studLName;
	}
	public Integer getMonthId() {
		return monthId;
	}
	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public Integer getStdId() {
		return stdId;
	}
	public void setStdId(Integer stdId) {
		this.stdId = stdId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Integer getDivId() {
		return divId;
	}
	public void setDivId(Integer divId) {
		this.divId = divId;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
    
    
}
