package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;

import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AnnouncementTypeModel;
import com.ingenio.announcement.model.AppUserModel;
import com.ingenio.announcement.model.SchoolMasterModel;

public class SubmitApprovalBean {

	private Integer postId;
	private Integer approvalStatusId;
	private Integer appuserId;
	private Integer schoolId;
	private Integer announcementId;
	private String status;
	private String remark;
	private Integer isSanstha;
    private String sansthakey;
    private Integer forPrincipal;
    private Integer forTeacher;
    private Integer forstudent;

	
	
	public Integer getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public Integer getApprovalStatusId() {
		return approvalStatusId;
	}

	public void setApprovalStatusId(Integer approvalStatusId) {
		this.approvalStatusId = approvalStatusId;
	}

	public Integer getAppuserId() {
		return appuserId;
	}

	public void setAppuserId(Integer appuserId) {
		this.appuserId = appuserId;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getIsSanstha() {
		return isSanstha;
	}

	public void setIsSanstha(Integer isSanstha) {
		this.isSanstha = isSanstha;
	}

	
	public Integer getForPrincipal() {
		return forPrincipal;
	}

	public void setForPrincipal(Integer forPrincipal) {
		this.forPrincipal = forPrincipal;
	}

	public Integer getForTeacher() {
		return forTeacher;
	}

	public void setForTeacher(Integer forTeacher) {
		this.forTeacher = forTeacher;
	}

	public Integer getForstudent() {
		return forstudent;
	}

	public void setForstudent(Integer forstudent) {
		this.forstudent = forstudent;
	}

	public String getSansthakey() {
		return sansthakey;
	}

	public void setSansthakey(String sansthakey) {
		this.sansthakey = sansthakey;
	}
	

}
