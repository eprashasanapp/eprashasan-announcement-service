package com.ingenio.announcement.bean;

import java.util.Date;
import java.util.List;

import com.ingenio.announcement.model.AnnouncmentOptionsOfQuestionsModel;

public class AnnouncementQuestionAnswersBean {
	
	private Integer questionId;
	private String question;
	private List <AnnouncementOptionOfQuestionBean> optionList;
	private Integer answerId = 0;
	private String answer= "";
	private Integer answerOptionId = 0;
	private Integer announcementId ;
	
	public Integer getAnswerOptionId() {
		return answerOptionId;
	}
	public void setAnswerOptionId(Integer answerOptionId) {
		this.answerOptionId = answerOptionId;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	public List<AnnouncementOptionOfQuestionBean> getOptionList() {
		return optionList;
	}
	public void setOptionList(List<AnnouncementOptionOfQuestionBean> optionList) {
		this.optionList = optionList;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
}
