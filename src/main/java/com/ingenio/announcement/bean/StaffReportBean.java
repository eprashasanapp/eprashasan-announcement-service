package com.ingenio.announcement.bean;

import java.util.List;

public class StaffReportBean {

	List<PhoneNumberBean> staffReport;

	public List<PhoneNumberBean> getStaffReport() {
		return staffReport;
	}

	public void setStaffReport(List<PhoneNumberBean> staffReport) {
		this.staffReport = staffReport;
	}
	
}
