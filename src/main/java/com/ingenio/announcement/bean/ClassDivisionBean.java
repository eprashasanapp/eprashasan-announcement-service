package com.ingenio.announcement.bean;

public class ClassDivisionBean {
	
	private Integer classId;
	private Integer divisionId;
	
	public Integer getClassId() {
		return classId;
	}
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	
	
	
	
	

}
