package com.ingenio.announcement.bean;

import java.util.List;

public class RequestBean {

	private Integer schoolid;
	private List<Integer> standardId;
	private List<Integer> typeOfReceipt;
	private Integer isSocityReceipt;
	private String fromDate;
	private String toDate;
	private List<Integer> subheadId;
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
	public List<Integer> getTypeOfReceipt() {
		return typeOfReceipt;
	}
	public void setTypeOfReceipt(List<Integer> typeOfReceipt) {
		this.typeOfReceipt = typeOfReceipt;
	}
	public Integer getIsSocityReceipt() {
		return isSocityReceipt;
	}
	public void setIsSocityReceipt(Integer isSocityReceipt) {
		this.isSocityReceipt = isSocityReceipt;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public List<Integer> getSubheadId() {
		return subheadId;
	}
	public void setSubheadId(List<Integer> subheadId) {
		this.subheadId = subheadId;
	}
	public List<Integer> getStandardId() {
		return standardId;
	}
	public void setStandardId(List<Integer> standardId) {
		this.standardId = standardId;
	}
	
	
	
}
