package com.ingenio.announcement.bean;

import java.sql.Timestamp;
import java.util.Date;

public class FeeReceiptBean {

	private String userRemark;
	private Integer receiptLogId;
	private String receiptNo;
	private Date receiptOldDate;
	private Date receiptNewDate;
	private String approvalStatus;
	private Timestamp cDate;
	private Double oldAmount;
	private Double newAmmount;
	private String subheadRemark;
	private Integer studentId;
	private Integer yearId;
	private String studInitialName;
	private String studFName;
	private String studMName;
	private String studLName;
	private Integer standardId;
	private String standardName;
	private String year;
	private Integer receiptId;
	public String getUserRemark() {
		return userRemark;
	}
	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	
	public Timestamp getcDate() {
		return cDate;
	}
	public void setcDate(Timestamp cDate) {
		this.cDate = cDate;
	}
	public String getSubheadRemark() {
		return subheadRemark;
	}
	public void setSubheadRemark(String subheadRemark) {
		this.subheadRemark = subheadRemark;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getStudInitialName() {
		return studInitialName;
	}
	public void setStudInitialName(String studInitialName) {
		this.studInitialName = studInitialName;
	}
	public String getStudFName() {
		return studFName;
	}
	public void setStudFName(String studFName) {
		this.studFName = studFName;
	}
	public String getStudMName() {
		return studMName;
	}
	public void setStudMName(String studMName) {
		this.studMName = studMName;
	}
	public String getStudLName() {
		return studLName;
	}
	public void setStudLName(String studLName) {
		this.studLName = studLName;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Double getOldAmount() {
		return oldAmount;
	}
	public void setOldAmount(Double oldAmount) {
		this.oldAmount = oldAmount;
	}
	public Double getNewAmmount() {
		return newAmmount;
	}
	public void setNewAmmount(Double newAmmount) {
		this.newAmmount = newAmmount;
	}
	public Date getReceiptOldDate() {
		return receiptOldDate;
	}
	public void setReceiptOldDate(Date receiptOldDate) {
		this.receiptOldDate = receiptOldDate;
	}
	public Date getReceiptNewDate() {
		return receiptNewDate;
	}
	public void setReceiptNewDate(Date receiptNewDate) {
		this.receiptNewDate = receiptNewDate;
	}
	public Integer getReceiptLogId() {
		return receiptLogId;
	}
	public void setReceiptLogId(Integer receiptLogId) {
		this.receiptLogId = receiptLogId;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public Integer getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}
		

}
