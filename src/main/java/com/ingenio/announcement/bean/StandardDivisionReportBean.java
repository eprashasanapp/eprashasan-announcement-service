package com.ingenio.announcement.bean;



public class StandardDivisionReportBean {

	private Integer standardId;
	private String standardName;
	private Integer divisionId;
	private String divisionName;
	private long totalSmsSent;
	private long totalSmsRead;
	private long noMobileNumber;
	private long mobileAppAvailable;
	private long notificationRead;
	private long noticeView;
	private long like;
	private long dislike;
	private long comment;
	private long reaction;
	private Integer totalStudents;
	private long emailSent;
	private long emailRead;
	private long noEmailFound;
	
	
	
	public StandardDivisionReportBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StandardDivisionReportBean(long totalSmsSent, long totalSmsRead) {
		super();
		this.totalSmsSent = totalSmsSent;
		this.totalSmsRead = totalSmsRead;
	}

	public long getTotalSmsSent() {
		return totalSmsSent;
	}

	public void setTotalSmsSent(long totalSmsSent) {
		this.totalSmsSent = totalSmsSent;
	}

	public long getTotalSmsRead() {
		return totalSmsRead;
	}

	public void setTotalSmsRead(long totalSmsRead) {
		this.totalSmsRead = totalSmsRead;
	}

	public long getNoMobileNumber() {
		return noMobileNumber;
	}

	public void setNoMobileNumber(long noMobileNumber) {
		this.noMobileNumber = noMobileNumber;
	}

	public long getMobileAppAvailable() {
		return mobileAppAvailable;
	}

	public void setMobileAppAvailable(long mobileAppAvailable) {
		this.mobileAppAvailable = mobileAppAvailable;
	}

	public long getNotificationRead() {
		return notificationRead;
	}

	public void setNotificationRead(long notificationRead) {
		this.notificationRead = notificationRead;
	}

	public long getNoticeView() {
		return noticeView;
	}

	public void setNoticeView(long noticeView) {
		this.noticeView = noticeView;
	}

	public long getLike() {
		return like;
	}

	public void setLike(long like) {
		this.like = like;
	}

	public long getDislike() {
		return dislike;
	}

	public void setDislike(long dislike) {
		this.dislike = dislike;
	}

//	public StandardDivisionReportBean(long totalSmsSent, long totalSmsRead, long mobileAppAvailable) {
//		super();
//		this.totalSmsSent = totalSmsSent;
//		this.totalSmsRead = totalSmsRead;
//		this.mobileAppAvailable = mobileAppAvailable;
//	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public StandardDivisionReportBean(long totalSmsSent, long totalSmsRead, long mobileAppAvailable,
			long notificationRead, long noticeView, long like,long dislike,long comment,long reaction,long emailSent,long noEmailFound,long emailRead) {
		super();
		this.totalSmsSent = totalSmsSent;
		this.totalSmsRead = totalSmsRead;
		this.mobileAppAvailable = mobileAppAvailable;
		this.notificationRead = notificationRead;
		this.noticeView = noticeView;
		this.like = like;
		this.dislike=dislike;
		this.comment=comment;
		this.reaction=reaction;
		this.emailSent=emailSent;
		this.noEmailFound=noEmailFound;
		this.emailRead=emailRead;
	}

	public Integer getTotalStudents() {
		return totalStudents;
	}

	public void setTotalStudents(Integer totalStudents) {
		this.totalStudents = totalStudents;
	}

	public long getComment() {
		return comment;
	}

	public void setComment(long comment) {
		this.comment = comment;
	}

	public long getReaction() {
		return reaction;
	}

	public void setReaction(long reaction) {
		this.reaction = reaction;
	}

	public long getEmailSent() {
		return emailSent;
	}

	public void setEmailSent(long emailSent) {
		this.emailSent = emailSent;
	}

	public long getEmailRead() {
		return emailRead;
	}

	public void setEmailRead(long emailRead) {
		this.emailRead = emailRead;
	}

	public long getNoEmailFound() {
		return noEmailFound;
	}

	public void setNoEmailFound(long noEmailFound) {
		this.noEmailFound = noEmailFound;
	}

//	totalSmsSent
//	totalSmsRead
//	mobileAppAvailable
//	notificationRead
//	noticeView
//	like
	
	
	
}
