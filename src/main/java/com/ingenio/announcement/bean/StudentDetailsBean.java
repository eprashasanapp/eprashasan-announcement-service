package com.ingenio.announcement.bean;

import java.util.List;

public class StudentDetailsBean {
	
	private Integer renewStudentId;
	private String studentName;
	private Integer rollNo;
	private String regNo;
	private Integer studentId;
	private String announcementStatus;
	private String announcementStatusText;
	private String eventStatus;
	private String eventStatusText;
	private String galleryStatus;
	private Integer schoolId;
	private Integer grBookId;
	private String photoUrl;
	private String galleryStatusText;
	private Integer staffId;
	private String staffName;
	private List<StudentDetailsBean> studentDetailsBean;
	private String role;
	private String fcm_id;
	private String birthDate;
	private String mobileNo;
	private String studentFName;
	private String studentLName;
	List<StudentDifferenceBean> studentDiffList;

	public StudentDetailsBean(String fcm_id) {
		super();
		this.fcm_id = fcm_id;
	}
	
	public String getFcm_id() {
		return fcm_id;
	}
	public void setFcm_id(String fcm_id) {
		this.fcm_id = fcm_id;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	public StudentDetailsBean() {
		super();
	}
	
	public StudentDetailsBean(Integer renewStudentId,Integer studentId, String studentName, Integer rollNo, String status,String regNo,Integer schoolId,Integer grBookId) {
		super();
		this.renewStudentId = renewStudentId;
		this.studentName = studentName;
		this.rollNo = rollNo;
		this.galleryStatus = status;
		this.eventStatus = status;
		this.announcementStatus=status;
		this.regNo=regNo;
		this.schoolId = schoolId;
		this.grBookId = grBookId;
		this.studentId=studentId;
		this.photoUrl="/";
		this.role="Student";
	}
	
	public StudentDetailsBean(Integer renewStudentId, String studentName, Integer rollNo,String regNo,Integer schoolId,Integer grBookId) {
		super();
		this.renewStudentId = renewStudentId;
		this.studentName = studentName;
		this.rollNo = rollNo;
		this.regNo=regNo;
		this.schoolId = schoolId;
		this.grBookId = grBookId;
		this.photoUrl="/";
		this.role="Student";
	}
	
	public StudentDetailsBean(Integer renewStudentId, String studentName,String status,String regNo,Integer schoolId) {
		super();
		this.renewStudentId = renewStudentId;
		this.studentName = studentName;
		this.regNo=regNo;
		this.schoolId = schoolId;
		this.galleryStatus = status;
		this.eventStatus = status;
		this.announcementStatus=status;
		this.photoUrl="/";
		this.rollNo =0;
		this.role="Teacher";
	}
	
	public StudentDetailsBean(Integer renewStudentId, String studentName, Integer rollNo,String regNo,Integer studentId,String birthDate,String mobileNo) {
		super();
		this.renewStudentId = renewStudentId;
		this.studentName = studentName;
		this.rollNo = rollNo;
		this.regNo=regNo;
		this.studentId = studentId;
		this.birthDate=birthDate;
		this.mobileNo=mobileNo;
	}
	
	public StudentDetailsBean(Integer renewStudentId, String studentName, Integer rollNo, String announcementStatus) {
		super();
		this.renewStudentId = renewStudentId;
		this.studentName = studentName;
		this.rollNo = rollNo;
		this.announcementStatus = announcementStatus;
	}
	public Integer getRenewStudentId() {
		return renewStudentId;
	}
	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public String getAnnouncementStatus() {
		return announcementStatus;
	}
	public void setAnnouncementStatus(String announcementStatus) {
		this.announcementStatus = announcementStatus;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getGrBookId() {
		return grBookId;
	}
	public void setGrBookId(Integer grBookId) {
		this.grBookId = grBookId;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getGalleryStatus() {
		return galleryStatus;
	}
	public void setGalleryStatus(String galleryStatus) {
		this.galleryStatus = galleryStatus;
	}
	public String getGalleryStatusText() {
		return galleryStatusText;
	}
	public void setGalleryStatusText(String galleryStatusText) {
		this.galleryStatusText = galleryStatusText;
	}
	public List<StudentDetailsBean> getStudentDetailsBean() {
		return studentDetailsBean;
	}
	public void setStudentDetailsBean(List<StudentDetailsBean> studentDetailsBean) {
		this.studentDetailsBean = studentDetailsBean;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEventStatusText() {
		return eventStatusText;
	}
	public void setEventStatusText(String eventStatusText) {
		this.eventStatusText = eventStatusText;
	}
	public String getAnnouncementStatusText() {
		return announcementStatusText;
	}
	public void setAnnouncementStatusText(String announcementStatusText) {
		this.announcementStatusText = announcementStatusText;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getStudentFName() {
		return studentFName;
	}

	public void setStudentFName(String studentFName) {
		this.studentFName = studentFName;
	}

	public String getStudentLName() {
		return studentLName;
	}

	public void setStudentLName(String studentLName) {
		this.studentLName = studentLName;
	}

	public List<StudentDifferenceBean> getStudentDiffList() {
		return studentDiffList;
	}

	public void setStudentDiffList(List<StudentDifferenceBean> studentDiffList) {
		this.studentDiffList = studentDiffList;
	}
	
	
	
	
}
