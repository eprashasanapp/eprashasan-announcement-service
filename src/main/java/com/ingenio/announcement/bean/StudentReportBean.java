package com.ingenio.announcement.bean;

import java.util.List;

public class StudentReportBean {
	List<PhoneNumberBean> studentReport;

	public List<PhoneNumberBean> getStudentReport() {
		return studentReport;
	}

	public void setStudentReport(List<PhoneNumberBean> studentReport) {
		this.studentReport = studentReport;
	}
 
}
