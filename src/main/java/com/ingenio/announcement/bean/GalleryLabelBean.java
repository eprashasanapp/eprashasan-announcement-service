package com.ingenio.announcement.bean;

import java.util.List;

public class GalleryLabelBean {
	
	private String id;
	private String name;
	private String regNo;
	private String photoUrl;
	private List<StudentDetailsBean> studentDetailsList;
	
	public GalleryLabelBean() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<StudentDetailsBean> getStudentDetailsList() {
		return studentDetailsList;
	}

	public void setStudentDetailsList(List<StudentDetailsBean> studentDetailsList) {
		this.studentDetailsList = studentDetailsList;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	
	
		
	
}
