package com.ingenio.announcement.bean;

public class AnnouncementViewResponseBean {
	

	public Integer announcementId;
	public String firstName;
	public String lastName;
	public String photoUrl;
	public Integer appUserId;
	public Integer schoolId ;
	public Integer viewsId ;
	
	public Integer getViewsId() {
		return viewsId;
	}
	public void setViewsId(Integer viewsId) {
		this.viewsId = viewsId;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public Integer getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(Integer appUserId) {
		this.appUserId = appUserId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	
	

	
	
}
