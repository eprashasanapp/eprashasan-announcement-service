package com.ingenio.announcement.bean;

public class OptionBean {
	
	private Integer optionId;
	private String OptionName;
	
	public Integer getOptionId() {
		return optionId;
	}
	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}
	public String getOptionName() {
		return OptionName;
	}
	public void setOptionName(String optionName) {
		OptionName = optionName;
	}
	
	

}
