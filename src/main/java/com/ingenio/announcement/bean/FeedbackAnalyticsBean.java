package com.ingenio.announcement.bean;

import java.util.List;

public class FeedbackAnalyticsBean {

	private Integer questionId;
	private String question;
	List<OptionCountBean> optionCountBean;
	List<FeedbackAnalyticsBean>feedbackAnalyticsBean;
	private Integer totalUsers;
	
	
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public List<OptionCountBean> getOptionCountBean() {
		return optionCountBean;
	}
	public void setOptionCountBean(List<OptionCountBean> optionCountBean) {
		this.optionCountBean = optionCountBean;
	}
	public FeedbackAnalyticsBean(Integer questionId, String question) {
		super();
		this.questionId = questionId;
		this.question = question;
	}
	public FeedbackAnalyticsBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public List<FeedbackAnalyticsBean> getFeedbackAnalyticsBean() {
		return feedbackAnalyticsBean;
	}
	public void setFeedbackAnalyticsBean(List<FeedbackAnalyticsBean> feedbackAnalyticsBean) {
		this.feedbackAnalyticsBean = feedbackAnalyticsBean;
	}
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	} 
	
	
	
}
