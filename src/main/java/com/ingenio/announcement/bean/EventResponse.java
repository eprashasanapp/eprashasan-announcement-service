package com.ingenio.announcement.bean;

import java.util.ArrayList;
import java.util.List;

public class EventResponse {
	
	List<EventResponseBean> eventList ;
	List<PersonalNoteRequestBean> personalRequestList;
	
	
	public List<EventResponseBean> getEventList() {
		return eventList;
	}
	public void setEventList(List<EventResponseBean> eventList) {
		this.eventList = eventList;
	}
	public List<PersonalNoteRequestBean> getPersonalRequestList() {
		return personalRequestList;
	}
	public void setPersonalRequestList(List<PersonalNoteRequestBean> personalRequestList) {
		this.personalRequestList = personalRequestList;
	}
	
	


}
