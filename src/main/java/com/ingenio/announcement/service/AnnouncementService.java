package com.ingenio.announcement.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.bean.AnnnouncementLikeOrDislikeBean;
import com.ingenio.announcement.bean.AnnouncementApprovalResponseBean;
import com.ingenio.announcement.bean.AnnouncementComentBean;
import com.ingenio.announcement.bean.AnnouncementCommentResponseBean;
import com.ingenio.announcement.bean.AnnouncementLikeResponseBean;
import com.ingenio.announcement.bean.AnnouncementQuestionAnswersBean;
import com.ingenio.announcement.bean.AnnouncementQuestionOptionBean;
import com.ingenio.announcement.bean.AnnouncementReactionBean;
import com.ingenio.announcement.bean.AnnouncementRequestBean;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementUserToGroupBean;
import com.ingenio.announcement.bean.AnnouncementViewBean;
import com.ingenio.announcement.bean.AnnouncementViewResponseBean;
import com.ingenio.announcement.bean.FeedbackEmailRequestBean;
import com.ingenio.announcement.bean.GroupBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.SubmitApprovalBean;
import com.ingenio.announcement.model.AnnouncementTypeModel;

public interface AnnouncementService {


	public Integer saveAnnouncement(AnnouncementRequestBean studentAnnouncementBean, Integer isWhatsApp, Integer isDueFeeRemainder);

	public List<AnnouncementResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole);

//	public List<AnnouncementResponseBean> getMyAnnouncement(Integer staffId, Integer yearId, String profileRole,String announcementDate,Integer offset,
//			Integer limit, Integer appUserId, Integer typeId, Integer schoolId, Integer announcementId);

	public Integer saveAttachments(MultipartFile file, String fileName, Integer assignHomeWorkId, String yearName,
			Integer schoolId);

	public Integer updateAnnouncementStatus(Integer announcementAssignId, Integer studentId, String statusFlag);

	public Integer sendFeedbackEmail(FeedbackEmailRequestBean feedbackEmailRequestBean);

	public Integer deleteAnnouncement(Integer announcementId);

	public Integer deleteAttachment(Integer announcementAttachmentId);

	public List<AnnouncementResponseBean> getAttachmentList(Integer announcementId);

	public List<StudentDetailsBean> getStudentDetailsForAnnouncement(Integer announcementId, Integer offset);

	public Integer newSendFeedbackEmail(MultipartFile file, FeedbackEmailRequestBean feedbackEmailRequestBean);

	public Integer saveAttachmentsWithId(MultipartFile file, String fileName, Integer announcmentId, Integer yearId,
			Integer schoolId);

	public void postAnnouncementForMembers(AnnouncementRequestBean studentAnnouncementBean, Integer saveId);

	public void sendNotification(AnnouncementRequestBean studentAnnouncementBean, Integer saveId);


	public List<AnnouncementTypeModel> getAnnouncementType(Integer schoolId);

	public List<AnnouncementResponseBean> getAnnouncementGroupList(Integer schoolId);


	public Integer saveQuestionsAndOptions(AnnouncementQuestionOptionBean announcementQuestionOptionBean);
	
	public void sendWhatsappSms(AnnouncementRequestBean studentAnnouncementBean, Integer saveId, Integer isDueFeeRemainder);

	public Integer getWhatsAppApiDetails(Integer schoolId);

	public Integer updatedueFeeRemainderStatus(Integer announcementId, Integer renewId, String statusFlag);

	public Integer SendDueFeeRemainderMsg(Integer schoolId, Integer renewId);

	public List<AnnouncementResponseBean> getStandardDivisionList(Integer announcementId, Integer schoolId);

	public List<StudentDetailsBean> studentListByStandardDivision(Integer announcementId, Integer standardId,
			Integer divisionId, Integer yearId, Integer schoolId);

	
	Integer saveAnnounceMentView(AnnouncementViewBean announcementViewBean);

	Integer saveAnnnouncementLikeOrDislike(AnnnouncementLikeOrDislikeBean annnouncementLikeOrDislikeBean);

	Integer saveComment(AnnouncementComentBean announcementComentBean);

	Integer saveReaction(List<AnnouncementReactionBean> announcementReactionBean);

	public List<AnnouncementResponseBean> getAnnouncementAuthorityusingGroupId(Integer schoolId,
			Integer groupId);

	public Integer submitAnnouncementApprovalStatus(SubmitApprovalBean studentAnnouncementBean);

	public List<AnnouncementCommentResponseBean> getAnnouncementAllComment(Integer schoolId, Integer announcementId);

	public List<AnnouncementLikeResponseBean> getAnnouncementAllLikes(Integer schoolId, Integer announcementId);

	public List<AnnouncementViewResponseBean> getAnnouncementAllViews(Integer schoolId, Integer announcementId);

	public Integer saveAnnouncementUserToGroup(AnnouncementUserToGroupBean announcementUserToGroupBean);

	public List<AnnouncementResponseBean> getAdminAnnouncement(Integer appuserId, Integer offset, Integer limit, Integer typeId, Integer schoolId);

	public List<AnnouncementResponseBean> getannoucementforapproval(Integer appuserId, Integer offset, Integer limit,
			Integer typeId, Integer schoolId);

	public List<AnnouncementApprovalResponseBean> getApprovalStatusOfAnnouncement(Integer schoolId,Integer postId);

	public Integer sendPostForApproval(Integer schoolId, Integer postId);

	public List<AnnouncementQuestionAnswersBean> getQuestionsOptionsAnswers(Integer schoolId, Integer announcementId, Integer appUserId);

	public List<GroupBean> getListofGroupForUser(Integer appUserId);

	public Integer saveAnnouncementView(List<AnnouncementViewBean> announcementReactionBean);

	public Runnable sendNotificationNew(SubmitApprovalBean studentAnnouncementBean);





}
