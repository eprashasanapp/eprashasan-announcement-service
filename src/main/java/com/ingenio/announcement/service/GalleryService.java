package com.ingenio.announcement.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.bean.GalleryRequestBean;
import com.ingenio.announcement.bean.GalleryResponseBean;
import com.ingenio.announcement.bean.StudentDetailsBean;

public interface GalleryService {

	public List<GalleryResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole, Integer yearId);

	public Integer saveGalleryPost(GalleryRequestBean studentGalleryBean);

	public Integer updateGalleryStatus(Integer galleryAssignedtoId, Integer studentId, String statusFlag);

	public Integer saveGalleryAttachments(MultipartFile file, String fileName, Integer galleryId, String yearName,
			Integer schoolId);

	public Integer deleteGalleryPost(Integer galleryId);

	public Integer deleteGalleryAttachment(Integer galleryAttachmentId);

	public List<GalleryResponseBean>getGalleryAttachmentList(Integer galleryId);

	public List<GalleryResponseBean> getMyGalleryPosts(Integer staffId, Integer yearId, String profileRole,
			Integer offset, String standardDivision);

	public List<GalleryResponseBean> getdivisionWiseFilterList(Integer schoolId, Integer yearId, Integer staffId);

	public List<GalleryResponseBean> getMyGalleryPostDetails(Integer galleryId, Integer yearId, Integer studentStaffId, String profileRole);

	public List<StudentDetailsBean> getStudentDetailsForGallery(Integer galleryId, Integer offset);
	public List<GalleryResponseBean> getDivisionWiseStudentList(Integer schoolId, String assignedByRole, Integer yearId,
			String standardDivision, String standardDivisionName);

	public Integer saveGalleryAttachmentsWithId(MultipartFile file, String fileName, Integer galleryId, Integer yeraId,
			Integer schoolId);

	public void postGalleryForMembers(GalleryRequestBean studentGalleryBean, Integer saveId);

	public void sendNotification(GalleryRequestBean studentGalleryBean, Integer saveId);


}
