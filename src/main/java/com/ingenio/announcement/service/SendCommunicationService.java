package com.ingenio.announcement.service;

import java.util.List;
import java.util.Optional;

import com.ingenio.announcement.bean.ReportBean;
import com.ingenio.announcement.bean.SubmitApprovalBean;
import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SentEmailModel;
import com.ingenio.announcement.model.SentEmailReadStatusModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;

public interface SendCommunicationService {

	String sendCommunication(SubmitApprovalBean studentAnnouncementBean);

	Optional<SentMessagesModel> getMessageDetails(String rcode);

	Integer recordMessageReadStatus(SentMessagesReadStatusModel sentMessagesReadStatus);

	//List<SaveSendNotificationModel> getAllNotification(Integer appUserId, Integer flag, Integer offset, Integer limit);

	List<SaveSendNotificationModel> getAllNotification(Integer appUserId, String flag, Integer offset, Integer limit);

	List<SentMessagesModel> getAllMessages(Integer staffStudentId, Integer flag);

	ReportBean getannouncementdeliveryreportassignto(Integer schoolid, Integer announcementId);


	List<SentMessagesModel> getAllMessagesReport(Integer schoolid, String fromDate, String toDate, Integer otherSchoolId, Integer offset,
			Integer limit);

	Integer submitAnnouncementApprovalStatus(SubmitApprovalBean studentAnnouncementBean);

	Optional<SentEmailModel> getEmailDetails(String rcode);

	Integer recordEmailReadStatus(SentEmailReadStatusModel sentEmailReadStatus);

	List<SentMessagesModel> getAllEmails(Integer staffStudentId, Integer flag);

	ReportBean getStudentListDeliveryReportForAnnouncement(Integer schoolid, Integer yearId, Integer standardId,
			Integer divisionId, Integer announcementId, String announcementDate);


//	List<SentMessagesModel> getMessageReadStatus(String messageSavedId);

}
