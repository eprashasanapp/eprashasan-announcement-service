package com.ingenio.announcement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementSendingListResponseBean;
import com.ingenio.announcement.bean.FeedbackAnalyticsBean;


public interface MyAnnouncementService {

	List<AnnouncementResponseBean> getMyAnnouncement(Integer staffId, Integer yearId, String profileRole,
			String announcementDate, Integer offset, Integer limit, Integer appUserId, Integer typeId, Integer schoolId,
			Integer announcementId);

	List<AnnouncementResponseBean> getMyAnnouncementGroupWise(String groupId, Integer typeId, Integer schoolId,
			Integer offset, Integer limit, Integer appUserId);

	List<AnnouncementResponseBean> getMyAnnouncementClassWise(Integer standardId, Integer offset, Integer yearId,
			Integer divisionId, Integer appUserId, Integer typeId, Integer limit, Integer schoolId);

	FeedbackAnalyticsBean getFeedbackAnalytics(Integer schoolId, Integer announcementId);

	AnnouncementSendingListResponseBean getAnnouncementUsersList(Integer announcementId, Integer schoolId);

	List<AnnouncementResponseBean> getAnnouncement(Integer announcementId, Integer schoolId, Integer appUserId);

	List<AnnouncementResponseBean> getAnnouncementSchoolWise(Integer schoolId, Integer appUserId, Integer announcementCategory);

	

}
