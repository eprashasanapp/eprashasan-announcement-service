package com.ingenio.announcement.service;

import java.util.List;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.CashBankAmountBean;
import com.ingenio.announcement.bean.CountBean;
import com.ingenio.announcement.bean.EditFeeReceiptBean;
import com.ingenio.announcement.bean.FeeReceiptBean;
import com.ingenio.announcement.bean.RequestBean;
import com.ingenio.announcement.bean.ResponceBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.StudentDifferenceBean;
import com.ingenio.announcement.bean.ValidateBean;
import com.ingenio.announcement.model.AppUserTempPassword;
import com.ingenio.announcement.model.FeeReceiptEditLogModel;

public interface SchoolSuspentionService {

	void sendSuspensionMessage();

	List<CountBean> getAllClassAllCategoryStudentCountClasswise(Integer schoolId, Integer yearId,
			String date, Integer catOrRelignOrMinotyOrConc);

	List<CashBankAmountBean> getCashBankAmountFromFeeForTrailb(List<RequestBean> requestBean);

	Integer updateTempPassword(AppUserTempPassword appUserTempPassword);

	Integer validateOtpForResetPassword(ValidateBean validateBean);

	Integer editFeeReceipt(EditFeeReceiptBean editFeeReceiptBean);

	List<FeeReceiptBean> getEditedReceipt(Integer schoolId);

	List<CountBean> getDivisionList(Integer schoolId, Integer yearId, String date, Integer catOrRelignOrMinotyOrConc);

	List<CountBean> getAgeStandardWise(Integer schoolId, Integer yearId, String date);

	List<CountBean> getAgeDivisionWise(Integer schoolId, Integer yearId, String date);

	List<StudentDetailsBean> studDifference(Integer schoolId, Integer yearId, Integer standardId, Integer divisionId,
			String date);

	List<StudentDifferenceBean> getStudentsPresentInAttendanceCatalogButNotInStudentRenew(Integer schoolId, Integer yearId, String date);

	List<ResponceBean> getCashBankAmountFromFeeForTrailbDateWise(List<RequestBean> requestBean);

}
