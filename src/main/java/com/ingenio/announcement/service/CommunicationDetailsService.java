package com.ingenio.announcement.service;

import java.util.List;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.ReportBean;
import com.ingenio.announcement.bean.StandardDivisionReportBean;
import com.ingenio.announcement.model.SmsSchoolCreditModel;

public interface CommunicationDetailsService {

	SmsSchoolCreditModel getSmsCreditInfo(Integer schoolId);

	List<StandardDivisionReportBean> getClassDevisionReport(Integer schoolId, Integer announcementId);

	List<ReportBean> getannouncementdeliveryReportGroup(Integer schoolid, Integer announcementId);


}
