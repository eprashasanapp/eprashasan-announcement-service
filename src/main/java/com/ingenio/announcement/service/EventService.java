package com.ingenio.announcement.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.bean.BannerImages;
import com.ingenio.announcement.bean.EventRequestBean;
import com.ingenio.announcement.bean.EventResponse;
import com.ingenio.announcement.bean.EventResponseBean;
import com.ingenio.announcement.bean.EventTypeBean;
import com.ingenio.announcement.bean.PersonalNoteRequestBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;

public interface EventService {

	List<EventResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole);

	Integer saveEvent(EventRequestBean eventRequestBean);

	EventResponse getMyEvent(Integer day ,Integer month, Integer yearId, String year, String profileRole,
			Integer studentStaffId, Integer schoolId, Integer standardId, Integer divisionId,Integer userId);

	List<EventResponseBean> getMyEventDetails(Integer eventId, Integer yearId, Integer studentStaffId, Integer schoolId);

	Integer updateEventStatus(Integer eventAssignId, Integer studentStaffId, String statusFlag);

	Integer deleteEvent(Integer eventId);

	List<EventResponseBean> getStudentDetailsForEventStatus(Integer schoolId, Integer eventId, String eventStatus);

	List<StudentDetailsBean> getStudentDetailsForEvent(Integer eventId, Integer offset);

	List<EventTypeBean> getAllEventTypes();

	Integer savePersonalNote(PersonalNoteRequestBean personalNoteRequestBean);

	Integer saveEventAttachments(MultipartFile file, String fileName, Integer eventId, String yearName,
			Integer schoolId);

	List<StandardDivisionBean> getAllStandardsList(Integer schoolId);

	List<StandardDivisionBean> getAllDivisionList(Integer standardId);

	Integer deleteAttachment(Integer eventDeleteAttachmentId);

	List<BannerImages> getAllBannerList(Integer schoolId, Integer yearId, Integer monthId, String yearName);

	Integer deletePersonalNote(Integer personalNoteId);



	

	


}
