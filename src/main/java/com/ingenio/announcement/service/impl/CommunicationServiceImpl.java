package com.ingenio.announcement.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.ReportBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StandardDivisionReportBean;
import com.ingenio.announcement.bean.StudentBean;
import com.ingenio.announcement.model.AnnouncementAssignToClassDivisionModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AnnouncementUserToGroupMappingModel;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;
import com.ingenio.announcement.model.SmsSchoolCreditModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.StudentStandardRenewModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AnnouncementAssignToClassDivisionRepository;
import com.ingenio.announcement.repository.AnnouncementUserToGroupMappingRepository;
import com.ingenio.announcement.repository.SaveSendNotificationRepository;
import com.ingenio.announcement.repository.SentMessagesReadStatusRepository;
import com.ingenio.announcement.repository.SentMessagesRepository;
import com.ingenio.announcement.repository.SmsSchoolCreditRepository;
import com.ingenio.announcement.repository.StudentStandardRenewRepository;
import com.ingenio.announcement.service.CommunicationDetailsService;

@Component
public class CommunicationServiceImpl implements CommunicationDetailsService{

	@Autowired
	SmsSchoolCreditRepository creditRepo;
	@Autowired
	SentMessagesRepository sentMessageRepo;
	@Autowired
	AnnouncementAssignToClassDivisionRepository classDivisionRepo;
	@Autowired
	StudentStandardRenewRepository studentStandardRenewRepo;
	@Autowired
	AnnouncementUserToGroupMappingRepository announcementUserToGroupMappingRepo;
	@Autowired
	SentMessagesReadStatusRepository sentMessageReadStatusRepo;
	@Autowired
	SaveSendNotificationRepository notificationRepo;
	
	@Override
	public SmsSchoolCreditModel getSmsCreditInfo(Integer schoolId) {

		SmsSchoolCreditModel mainBean=new SmsSchoolCreditModel();
		List<SmsSchoolCreditModel> smsCreditInfo=creditRepo.findBySchoolid(schoolId);
		
		Integer totalCredit=creditRepo.totalCredit(schoolId);

		mainBean.setTotalCredit(totalCredit);
		mainBean.setSmsCreditInfo(smsCreditInfo);
		Integer smsCount=sentMessageRepo.getSentMessagesToSchool(schoolId);
		mainBean.setSmsCount(smsCount);
		return mainBean;
	}
	@Override
	public List<StandardDivisionReportBean> getClassDevisionReport(Integer schoolId, Integer announcementId) {
			
			
			SchoolMasterModel schoolModel=new SchoolMasterModel();
			schoolModel.setSchoolid(schoolId);
			AnnouncementModel announcement=new AnnouncementModel();
			announcement.setAnnouncementId(announcementId);
		
		   // List<AnnouncementAssignToClassDivisionModel> announcementAssignToClassDivisionModel=classDivisionRepo.findBySchoolMasterModelAndAnnouncementModel(schoolModel,announcement);
		    List<StandardDivisionBean> announcementAssignToClassDivisionModel=classDivisionRepo.getStandardDivisinYearAndPublishDate(schoolId,announcementId);

		    List<StandardDivisionReportBean> reportClassDivision=new ArrayList<>();
		    
		    for (StandardDivisionBean classDivisionModel : announcementAssignToClassDivisionModel) {
				
		    	
//		    	Integer divisionId=classDivisionModel.getDivisionMasterModel().getDivisionId();
//			    String divisoinName=classDivisionModel.getDivisionMasterModel().getDivisionName();
//			    Integer yearId=classDivisionModel.getYearMasterModel().getYearId();
//			    Integer standardId=classDivisionModel.getStandardMasterModel().getStandardId();
//			    String standardName=classDivisionModel.getStandardMasterModel().getStandardName();
//			    Date publishDate = classDivisionModel.getAnnouncementModel().getPublishDate();
		    	
		    	Integer divisionId=classDivisionModel.getDivisionId();
			    String divisoinName=classDivisionModel.getDivisionName();
			    Integer yearId=classDivisionModel.getYearId();
			    Integer standardId=classDivisionModel.getStandardId();
			    String standardName=classDivisionModel.getStandardName();
			    Date publishDate = classDivisionModel.getPublishDate();
			    
			    DivisionMasterModel division=new DivisionMasterModel();
			    division.setDivisionId(divisionId);
			    
			    StandardMasterModel standard=new StandardMasterModel();
			    standard.setStandardId(standardId);
			    
			    YearMasterModel year=new YearMasterModel();
			    year.setYearId(yearId);
			   // List<StudentStandardRenewModel> studentStandardRenew=studentStandardRenewRepo.findByStandardMasterModelAndDivisionMasterModelAndYearMasterModel(standard,division,year);
			    
			    List<StudentBean> studentStandardRenew=studentStandardRenewRepo.getAllStudents(standard,division,year,publishDate);
			    
			    List<Integer> studentId=new ArrayList<>();
			    List<Integer> appUserId=new ArrayList<>();

			    for (StudentBean studentUserId : studentStandardRenew) {
			    	studentId.add(studentUserId.getStudentId());
			    	appUserId.add(studentUserId.getAppUserId());
				}
			    //List<Integer> appUserId=studentStandardRenewRepo.getAllStudents(standard,division,year,publishDate);

			    
			    StandardDivisionReportBean standardDivisinReport=new StandardDivisionReportBean();
			    
			    if(!CollectionUtils.isEmpty(studentId)) {
			 //   Integer totalSmsSent;//////
			    standardDivisinReport= sentMessageRepo.getTotalMessageSent(announcementId,schoolId,appUserId,studentId);
			    //standardDivisinReport.setTotalSmsSent(totalSmsSent);
			    standardDivisinReport.setNoMobileNumber(appUserId.size()-standardDivisinReport.getTotalSmsSent());
			    standardDivisinReport.setStandardId(standardId);
			    standardDivisinReport.setStandardName(standardName);
			    standardDivisinReport.setDivisionId(divisionId);
			    standardDivisinReport.setDivisionName(divisoinName);
			    standardDivisinReport.setTotalStudents(studentStandardRenew.size());
			    reportClassDivision.add(standardDivisinReport);
			    }
			    
			}
		    
		    
		    System.out.println("");
		    
		return reportClassDivision;
	}
	@Override
	public List<ReportBean> getannouncementdeliveryReportGroup(Integer schoolid, Integer announcementId) {
List<PhoneNumberBean> phNoFromAssignToTable=announcementUserToGroupMappingRepo.getUsersAssignToGroup(announcementId);
		
		List<ReportBean> reportBean=new ArrayList<>();
		Map<String, List<PhoneNumberBean>> groupedData = new HashMap<>();
		for (PhoneNumberBean reports : phNoFromAssignToTable) {
		
			 String groupName = reports.getGroupName(); // Get the group name from PhoneNumberBean
			    if (!groupedData.containsKey(groupName)) {
			        groupedData.put(groupName, new ArrayList<>());
			    }
			if(reports.getRole().equals("Staff")) {
				SentMessagesModel sentMessage= sentMessageRepo.getMessagesForRepot(reports.getStaffId(),"Staff",announcementId);
				if(sentMessage!=null) {
				List<SentMessagesReadStatusModel>sentMessagesReadStatusModel=sentMessageReadStatusRepo.findBySentMessageId(sentMessage.getId());
				//SentMessagesReadStatusModel
				reports.setSentMessage(sentMessage);
				reports.setReadStatus(sentMessagesReadStatusModel);
				}
				List<SaveSendNotificationModel> allNotifications = notificationRepo.getAllNotificationsForReport(reports.getUserId(), "2",announcementId);
				reports.setAllNotifications(allNotifications);
				//staffReportBean.add(reports);
				
			}else if(reports.getRole().equals("Student")){
				SentMessagesModel sentMessage= sentMessageRepo.getMessagesForRepot(reports.getStudentId(),"Student",announcementId);
				if(sentMessage!=null) {
				List<SentMessagesReadStatusModel>sentMessagesReadStatusModel=sentMessageReadStatusRepo.findBySentMessageId(sentMessage.getId());
				reports.setSentMessage(sentMessage);
				reports.setReadStatus(sentMessagesReadStatusModel);
				}
				List<SaveSendNotificationModel> allNotifications = notificationRepo.getAllNotificationsForReport(reports.getUserId(), "1",announcementId);
				reports.setAllNotifications(allNotifications);
				//studentReportBean.add(reports);
					
			}
		    groupedData.get(groupName).add(reports);

		}

		for (Map.Entry<String, List<PhoneNumberBean>> entry : groupedData.entrySet()) {
			
			ReportBean ob=new ReportBean();
			ob.setGroup(entry.getKey());
		    ob.setGroupReport(entry.getValue());
		    reportBean.add(ob);

		   
		}
		
//		report.setStaffReportBean(staffReportBean);
//		report.setStudentReportBean(studentReportBean);
		System.out.println("");
		return reportBean;
	}

	
	
	
}
