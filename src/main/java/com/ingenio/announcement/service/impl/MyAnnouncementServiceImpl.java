package com.ingenio.announcement.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.ingenio.announcement.bean.AnnouncementCommentResponseBean;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementSendingListResponseBean;
import com.ingenio.announcement.bean.AssignToBean;
import com.ingenio.announcement.bean.ClassDivisionBean;
import com.ingenio.announcement.bean.FeedbackAnalyticsBean;
import com.ingenio.announcement.bean.OptionCountBean;
import com.ingenio.announcement.bean.UserDetailsBean;
import com.ingenio.announcement.model.AnnouncementAssignToClassDivisionModel;
import com.ingenio.announcement.model.AnnouncementAssignToModel;
import com.ingenio.announcement.model.AnnouncementCommentModel;
import com.ingenio.announcement.model.AnnouncementLikeorDislikeModel;
import com.ingenio.announcement.model.AnnouncementViewsModel;
import com.ingenio.announcement.model.AppUserModel;
import com.ingenio.announcement.model.IcardMaster;
import com.ingenio.announcement.repository.AnnouncementAssignToClassDivisionRepository;
import com.ingenio.announcement.repository.AnnouncementCommentRepository;
import com.ingenio.announcement.repository.AnnouncementForGroupRepository;
import com.ingenio.announcement.repository.AnnouncementLikeOrDislikeRepository;
import com.ingenio.announcement.repository.AnnouncementOptionsForQuestionsRepository;
import com.ingenio.announcement.repository.AnnouncementQuestionRepository;
import com.ingenio.announcement.repository.AnnouncementReactionRepository;
import com.ingenio.announcement.repository.AnnouncementRepository;
import com.ingenio.announcement.repository.AnnouncementViewRepository;
import com.ingenio.announcement.repository.AppUserRepository;
import com.ingenio.announcement.repository.IcardMasterRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.StudentAnnouncementAssignedToRepository;
import com.ingenio.announcement.service.MyAnnouncementService;
import com.ingenio.announcement.util.CommonUtlitity;

@Component
public class MyAnnouncementServiceImpl implements MyAnnouncementService {

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	AnnouncementRepository announcementRepository;
	
	@Autowired
	IcardMasterRepository icardMasterRepository;
	
	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	AnnouncementCommentRepository commentRepo;

	@Autowired
	AnnouncementLikeOrDislikeRepository likeDislikeRepo;

	@Autowired
	CommonUtlitity commonUtlitity;

	@Autowired
	AnnouncementForGroupRepository announcementForGroupRepository;

	@Autowired
	AnnouncementViewRepository announcementViewRepository;

	@Autowired
	AnnouncementAssignToClassDivisionRepository announcementAssignToClassDivisionRepository;

	@Autowired
	StudentAnnouncementAssignedToRepository studentAnnouncementAssignedToRepository;

	@Autowired
	AnnouncementQuestionRepository announcementQuestionRepository;
	
	@Autowired
	AnnouncementOptionsForQuestionsRepository optionRepo;
	
	@Autowired
	AnnouncementReactionRepository reactionRepo;
	
	@Override
	public List<AnnouncementResponseBean> getMyAnnouncement(Integer staffStudentId, Integer yearId, String profileRole,
			String announcementDate, Integer offset, Integer limit, Integer appUserId, Integer typeId, Integer schoolId,
			Integer announcementId) {
		System.out.println("limit" + limit);
		if (limit == null || limit == 0) {
			limit = 10;
		}
		if (offset == null || offset == 0) {
			offset = 0;
		}
		List<AnnouncementResponseBean> announcementList1 = new ArrayList<>();

		try {
			if (StringUtils.isNotEmpty(announcementDate)) {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(announcementDate);
				if (profileRole.equalsIgnoreCase("Parent")) {
					if (announcementId != 0) {
						announcementList1 = announcementRepository.getStudentAnnouncementParentWithAnnouncementId(
								staffStudentId, profileRole, date, typeId, announcementId, schoolId,
								PageRequest.of(offset, limit));
					} else {
						
						if(typeId!=0)
						announcementList1 = announcementRepository.getStudentAnnouncementParent(staffStudentId,
								profileRole, date, typeId, schoolId);
						else
							announcementList1 = announcementRepository.getStudentAnnouncementParent(staffStudentId, 
									profileRole, date , schoolId);
					}
				} else {
					if (announcementId != 0) {

						announcementList1 = announcementRepository.getStudentAnnouncementWithAnnouncementId(
								staffStudentId, profileRole, date, typeId, announcementId, schoolId);

					} else {
						if(typeId!=0)
						announcementList1 = announcementRepository.getStudentAnnouncement(staffStudentId,
								profileRole, date, typeId, schoolId);
						else
							announcementList1 = announcementRepository.getStudentAnnouncement(staffStudentId,
									profileRole, date, schoolId);		
					}
				}

			} else {
				if (profileRole.equalsIgnoreCase("Parent")) {

					if (announcementId != 0) {
						announcementList1 = announcementRepository.getStudentAnnouncementParentWithAnnouncementId(
								staffStudentId, profileRole, typeId, announcementId, schoolId);
					} else {
						if(typeId!=0)
						announcementList1 = announcementRepository.getStudentAnnouncementParent(staffStudentId,
								profileRole, typeId, schoolId);
						else
							
							announcementList1 = announcementRepository.getStudentAnnouncementParent(staffStudentId,
									profileRole,schoolId);
					}
				} else {
					if (announcementId != 0) {
						announcementList1 = announcementRepository.getStudentAnnouncementWithAnnouncementId(
								staffStudentId, profileRole, typeId, announcementId, schoolId);

					} else {
						
						if(typeId!=0)
						announcementList1 = announcementRepository.getStudentAnnouncement(staffStudentId,
								profileRole, typeId, schoolId);
						else
							announcementList1 = announcementRepository.getStudentAnnouncement(staffStudentId,
									profileRole, schoolId);

					}
				}

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return sendAnnouncementList(announcementList1,offset,limit,schoolId,appUserId);
	}

	@Override
	public List<AnnouncementResponseBean> getMyAnnouncementGroupWise(String groupId, Integer typeId, Integer schoolId,
			Integer offset, Integer limit, Integer appUserId) {
		if (limit == null || limit == 0) {
			limit = 10;
		}
		if (offset == null || offset == 0) {
			offset = 0;
		}
		List<AnnouncementResponseBean> announcementList = new ArrayList<>();
		List<AnnouncementResponseBean> announcementList1 = new ArrayList<>();
		List<String> groupIdList = Arrays.asList(groupId.split("-"));
		groupIdList.stream().forEach(group -> {
			List<Integer> PostIdsList = announcementForGroupRepository.getPostIds(Integer.parseInt(group));
			PostIdsList.parallelStream().forEach(postId -> {
				AnnouncementResponseBean announcementResponseBean =null;
				if (typeId != 0) {
					 announcementResponseBean = announcementRepository.getAnnouncements(postId,
							typeId);
				} else {
					 announcementResponseBean = announcementRepository.getAnnouncements(postId);
				}
				announcementList1.add(announcementResponseBean);
			});
		});
		return sendAnnouncementList(announcementList1,offset,limit,schoolId,appUserId);
	}

	@Override
	public List<AnnouncementResponseBean> getMyAnnouncementClassWise(Integer standardId, Integer offset, Integer yearId,
			Integer divisionId, Integer appUserId, Integer typeId, Integer limit, Integer schoolId) {
		if (limit == null || limit == 0) {
			limit = 10;
		}
		if (offset == null || offset == 0) {
			offset = 0;
		}
		List<AnnouncementResponseBean> announcementList = new ArrayList<>();
		List<AnnouncementResponseBean> announcementList1 = new ArrayList<>();

		List<Integer> AnnouncementIds = announcementAssignToClassDivisionRepository.findAnnouncementIds(yearId,
				standardId, divisionId, schoolId);
		AnnouncementIds.parallelStream().forEach(postId -> {
			AnnouncementResponseBean announcementResponseBean = announcementRepository.getAnnouncements(postId, typeId);
			//AnnouncementResponseBean announcementResponseBean = announcementRepository.getAnnouncementsClassWise(postId, typeId,standardId,divisionId,yearId);

			if(announcementResponseBean!=null)
			announcementList1.add(announcementResponseBean);
		});
		return sendAnnouncementList(announcementList1,offset,limit,schoolId,appUserId);
	}
	
	private List<AnnouncementResponseBean> sendAnnouncementList(List<AnnouncementResponseBean> announcementList1,
			Integer offset, Integer limit, Integer schoolId, Integer appUserId) {
		List<AnnouncementResponseBean> announcementList = new ArrayList<>();
		announcementList = announcementList1.parallelStream().skip(offset).limit(limit).collect(Collectors.toList());
		announcementList.stream().forEach(announcement -> {
			if(announcement!=null){
			
			AnnouncementCommentResponseBean announcementCommentResponseBean = new AnnouncementCommentResponseBean();
			
			try {
				List<AnnouncementCommentModel> comments = commentRepo.findComments(schoolId,
						announcement.getAnnouncementId());
				
				announcementCommentResponseBean.setAppUserId(comments.get(0).getAppUserModel().getAppUserRoleId());
				AppUserModel appUser = appUserRepository.getUserDetailsWithUserId(comments.get(0).getAppUserModel().getAppUserRoleId(), schoolId);
				announcementCommentResponseBean.setFirstName(appUser.getFirstName());
				announcementCommentResponseBean.setLastName(appUser.getLastName());
				
				String  image=null;
				if(!appUser.getRoleName().equals("ROLE_PARENT1")) {
					image=icardMasterRepository.getImageForStaff(appUser.getStaffId());
				}else {
					image=icardMasterRepository.getImageForStudent(appUser.getStaffId());

				}				announcementCommentResponseBean.setPhotoUrl(image);
				announcementCommentResponseBean.setSchoolId(schoolId);
				
				announcementCommentResponseBean.setCommentId(comments.get(0).getCommentId());
				announcementCommentResponseBean.setComment(comments.get(0).getComment());
				announcement.setLastComment(announcementCommentResponseBean);
				announcement.setTotalCommentCount(comments.size());

				comments.stream().forEach(commentmodule -> {
					if (commentmodule.getAppUserModel().getAppUserRoleId().equals(appUserId)) {
						announcement.setUserComment(commentmodule.getComment());
					}
				});

			} catch (Exception e) {
				System.out.println(e);
			}

			List<AnnouncementLikeorDislikeModel> likes = likeDislikeRepo.findLikes(schoolId,
					announcement.getAnnouncementId());
			announcement.setTotalLikes(likes.size());
			likes.stream().forEach(likemodule -> {
				if (likemodule.getAppUserModel().getAppUserRoleId().equals(appUserId)) {
					announcement.setUserlikestatus(true);
				}
			});
			List<AnnouncementViewsModel> viewList = announcementViewRepository.findViews(schoolId,
					announcement.getAnnouncementId());
			announcement.setTotalViewsCount(viewList.size());
			viewList.stream().forEach(view -> {
				if (view.getAppUserModel().getAppUserRoleId().equals(appUserId)) {
					announcement.setUserViewStatus(true);
				}
			});

			announcement.setPostedTime(announcement.getPostedTime());
			announcement.setIsfeedback(announcement.getIsfeedback());
			announcement.setUserCanComment(announcement.getUserCanComment());
			announcement.setPublishFlag(announcement.getPublishFlag());
			announcement.setSendForApproval(announcement.getSendForApproval());

			List<AnnouncementResponseBean> attachmentList = announcementRepository
					.getAttachments(announcement.getAnnouncementId(), schoolId);
//			attachmentList.stream().forEach(attachment -> {
//				attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
//				attachment.setFileName(attachment.getFileName());
//			});
			announcement.setNoOfAttachments(Long.parseLong(""+attachmentList.size()));
			announcement.setAttachmentList(attachmentList);
		}});
		return announcementList;
	}

	@Override
	public FeedbackAnalyticsBean getFeedbackAnalytics(Integer schoolId, Integer announcementId) {
		List<FeedbackAnalyticsBean> feedbackAnalytics= announcementQuestionRepository.FindByAnnouncementIdAndSchoolId(announcementId,schoolId);
		
		FeedbackAnalyticsBean questionsAndaoptionCount=new FeedbackAnalyticsBean();
		for (FeedbackAnalyticsBean feedbackAnalyticsBean : feedbackAnalytics) {
			List<OptionCountBean> optionCountBean=optionRepo.getOptins(feedbackAnalyticsBean.getQuestionId(),announcementId,schoolId);
			feedbackAnalyticsBean.setOptionCountBean(optionCountBean);
		}
		
		for (FeedbackAnalyticsBean feedbackAnalyticsBean : feedbackAnalytics) {
			
			for (OptionCountBean optionCountBean : feedbackAnalyticsBean.getOptionCountBean()) {
			
				Integer count=reactionRepo.getCount(feedbackAnalyticsBean.getQuestionId(),optionCountBean.getOptionId());
				optionCountBean.setUserCount(count);
			}
		}
		
		Integer totalUser=reactionRepo.getTotalUser(announcementId,schoolId);
		questionsAndaoptionCount.setTotalUsers(totalUser);
		questionsAndaoptionCount.setFeedbackAnalyticsBean(feedbackAnalytics);
		System.out.println(feedbackAnalytics);
		return questionsAndaoptionCount;
	}

	@Override
	public AnnouncementSendingListResponseBean getAnnouncementUsersList(Integer announcementId, Integer schoolId) {
		List<Integer> groupIdList = announcementForGroupRepository.getGroupId(announcementId);
		List<AnnouncementAssignToClassDivisionModel> classDivisionList = announcementAssignToClassDivisionRepository.findByAnnouncementAndSchoolId(announcementId,schoolId);
		List<AnnouncementAssignToModel> assignToList = studentAnnouncementAssignedToRepository.getAssignToList(announcementId,schoolId);
		AnnouncementSendingListResponseBean announcementSendingListResponseBean =new AnnouncementSendingListResponseBean();
		announcementSendingListResponseBean.setGroupIdList(groupIdList);
		List<ClassDivisionBean> classDivisionList1 =new ArrayList<ClassDivisionBean>();
	        classDivisionList.parallelStream().forEach( a2 -> {
	        	ClassDivisionBean classDivisionBean =new ClassDivisionBean();	
	        	classDivisionBean.setClassId(a2.getStandardMasterModel().getStandardId());
	        	classDivisionBean.setDivisionId(a2.getDivisionMasterModel().getDivisionId());
	        	classDivisionList1.add(classDivisionBean);
	        });
		announcementSendingListResponseBean.setClassDivisionList(classDivisionList1);
		List<AssignToBean> assignToList1 =new ArrayList<AssignToBean>();
		 assignToList.parallelStream().forEach(a1->{
			AssignToBean assignToBean =new AssignToBean();
			assignToBean.setRole(a1.getRole());
			assignToBean.setStaffStudentId(a1.getStaffStudentId());
			assignToList1.add(assignToBean);
		});
		announcementSendingListResponseBean.setAssignToList(assignToList1);
		return announcementSendingListResponseBean ;

	}

	@Override
	public List<AnnouncementResponseBean> getAnnouncement(Integer announcementId,Integer schoolId,Integer appuserId) {

		List<AnnouncementResponseBean> announcementList = new ArrayList<>();

		announcementList=announcementRepository.getAnnouncement(announcementId,schoolId);
		
//		for (AnnouncementResponseBean announcementResponseBean : announcementList) {
//			List<AnnouncementResponseBean> attachmentList = announcementRepository
//					.getAttachments(announcementResponseBean.getAnnouncementId(), schoolId);
////			attachmentList.stream().forEach(attachment -> {
////				attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
////				attachment.setFileName(attachment.getFileName());
////			});
//			announcementResponseBean.setNoOfAttachments(Long.parseLong(""+attachmentList.size()));
//			announcementResponseBean.setAttachmentList(attachmentList);
//		}
		
		
		return sendAnnouncementList(announcementList,0,100,schoolId,appuserId);
	}

	@Override
	public List<AnnouncementResponseBean> getAnnouncementSchoolWise(Integer schoolId, Integer appUserId,Integer announcementCategory) {
		List<AnnouncementResponseBean> announcementList = new ArrayList<>();
		if(announcementCategory!=null) {
			announcementList=announcementRepository.getAnnouncementSchoolWise(schoolId,announcementCategory);
		}else {
		announcementList=announcementRepository.getAnnouncementSchoolWise(schoolId);
		}
		
		for (AnnouncementResponseBean announcementResponseBean : announcementList) {
			
			List<UserDetailsBean> userDetails=studentAnnouncementAssignedToRepository.findStaffDetails(announcementResponseBean.getAnnouncementId());
			
//			for (UserDetailsBean user : userDetails) {
//				List<AnnouncementCommentModel> comments = commentRepo.getComments(schoolId,
//						announcementResponseBean.getAnnouncementId(),user.getAppUserRoleId());
//				user.setComments(comments);
//			}
			announcementResponseBean.setUserDetails(userDetails);

			System.out.println("");
			
		}
		
		
		if(appUserId!=null) {
		List<AnnouncementResponseBean> sendAnnouncementList = sendAnnouncementList(announcementList,0,100,schoolId,appUserId);
		return sendAnnouncementList;
		}
		else { 
		return	announcementList;
		}
	}

}
