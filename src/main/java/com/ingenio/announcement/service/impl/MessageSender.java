package com.ingenio.announcement.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ingenio.announcement.bean.EmailBean;


public class MessageSender  implements Runnable {
	
	
	SchoolServiceSuspentionServiceImpl schoolServiceSuspentionServiceImpl;
	private Integer schoolId;
	private String senderEmail;
	private String senderPassword;
	private String subject;
	private String body;
	private List<String> batch;

	    public MessageSender(List<String> batch,SchoolServiceSuspentionServiceImpl schoolServiceSuspentionServiceImpl2,Integer schoolId,String subject,String body,String senderEmail1,String senderPassword1) {
	        this.batch = batch;
	        this.schoolServiceSuspentionServiceImpl=schoolServiceSuspentionServiceImpl2;
	        this.schoolId=schoolId;
	        this.subject=subject;
	        this.body=body;
	        this.senderEmail=senderEmail1;
	        this.senderPassword=senderPassword1;
	    }
	public void run() {
		
		for (String emailId : batch) {
			List<String> emailIds=new ArrayList<>();
			emailIds.add(emailId);
			EmailBean ob=new EmailBean();
			ob.setSchoolid(schoolId);
			ob.setToEmailIds(emailIds);
			ob.setSubject(subject);
			ob.setBody(body);
			schoolServiceSuspentionServiceImpl.sendEmail(ob,senderEmail,senderPassword);

		} 
	}
}


