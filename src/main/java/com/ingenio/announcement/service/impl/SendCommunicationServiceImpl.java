package com.ingenio.announcement.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException; 
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ingenio.announcement.Constants.ApplicationConstants;
import com.ingenio.announcement.bean.AnnouncementBean;
import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.ReportBean;
import com.ingenio.announcement.bean.SavedIdBean;
import com.ingenio.announcement.bean.SchoolNameBean;
import com.ingenio.announcement.bean.StaffReportBean;
import com.ingenio.announcement.bean.StudentBean;
import com.ingenio.announcement.bean.StudentReportBean;
import com.ingenio.announcement.bean.SubmitApprovalBean;
import com.ingenio.announcement.bean.TokenMobileNumberBean;
import com.ingenio.announcement.bean.staffStudentBean;
import com.ingenio.announcement.model.AnnouncementApprovalStatusModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AppUserModel;
import com.ingenio.announcement.model.CronLogModel;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.SentEmailModel;
import com.ingenio.announcement.model.SentEmailReadStatusModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;
import com.ingenio.announcement.model.SmsSchoolCreditModel;
import com.ingenio.announcement.model.SmsSchoolDebitModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AndroidFcmTokenRepository;
import com.ingenio.announcement.repository.AnnouncementApprovalStatusRepository;
import com.ingenio.announcement.repository.AnnouncementAssignToClassDivisionRepository;
import com.ingenio.announcement.repository.AnnouncementForGroupRepository;
import com.ingenio.announcement.repository.AnnouncementGroupRepository;
import com.ingenio.announcement.repository.AnnouncementOptionsForQuestionsRepository;
import com.ingenio.announcement.repository.AnnouncementQuestionRepository;
import com.ingenio.announcement.repository.AnnouncementRepository;
import com.ingenio.announcement.repository.CronLogModelRepository;
import com.ingenio.announcement.repository.SaveSendNotificationRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.SentEmailReadStatusRepository;
import com.ingenio.announcement.repository.SentEmailRepository;
import com.ingenio.announcement.repository.SentMessagesReadStatusRepository;
import com.ingenio.announcement.repository.SentMessagesRepository;
import com.ingenio.announcement.repository.SmsSchoolDebitRepository;
import com.ingenio.announcement.repository.StudentAnnouncementAssignedToRepository;
import com.ingenio.announcement.repository.StudentStandardRenewRepository;
import com.ingenio.announcement.repository.impl.CommonServiceDaoImpl;
import com.ingenio.announcement.repository.impl.SendCommunicationRepositoryImpl;
import com.ingenio.announcement.service.SendCommunicationService;
import com.ingenio.announcement.util.UniqueCodeGenerator;
//import com.netflix.discovery.converters.Auto;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SendCommunicationServiceImpl implements SendCommunicationService {

	@Autowired
	StudentAnnouncementAssignedToRepository studentAnnouncementAssignedToRepository;

	@Autowired
	AnnouncementForGroupRepository announcementForGroupRepository;

	@Autowired
	AnnouncementGroupRepository announcementGroupRepository;

	@Autowired
	AnnouncementOptionsForQuestionsRepository announcementOptionsForQuestionsRepository;

	@Autowired
	AnnouncementQuestionRepository announcementQuestionRepository;

	@Autowired
	AnnouncementAssignToClassDivisionRepository announcementAssignToClassDivisionRepository;

	@Autowired
	SentMessagesRepository sentMessagesRepository;

	@Autowired
	SchoolMasterRepository schoolRepo;

	@Autowired
	AnnouncementRepository announcement;

	@Autowired
	AnnouncementApprovalStatusRepository announcementApprovalStatusRepository;

	@Autowired
	SentMessagesReadStatusRepository sentMessagesReadStatusRepo;
	@Autowired
	SaveSendNotificationRepository notification;

	@Autowired
	AndroidFcmTokenRepository fcmRepo;
	@Autowired
	SaveSendNotificationRepository notificationRepo;

	@Autowired
	SmsSchoolDebitRepository smsSchoolDebitRepo;
	@Autowired
	SendCommunicationRepositoryImpl sendCommunicationRepoImpl;
	@Autowired
	SentEmailRepository sentEmailRepository;
	@Autowired
	SentEmailReadStatusRepository sentEmailReadStatusRepository;
	@Autowired
	SentEmailRepository sentEmailRepo;
	RestTemplate restTemplate=new RestTemplate();
	UniqueCodeGenerator uniqueCode=new UniqueCodeGenerator();
	@Autowired
	CommunicationServiceImpl communicationServiceImpl;

	@Autowired
	CronLogModelRepository cronRepo; 
	@Autowired
	StudentStandardRenewRepository studentStandardRenewRepository;
	@Autowired
	CommonServiceDaoImpl commonServiceDaoImpl;

//	private EntityManager entityManager;
//
//	public SendCommunicationServiceImpl(EntityManager entityManager) {
//		this.entityManager = entityManager;
//	}


	@Override
	public String sendCommunication(SubmitApprovalBean studentAnnouncementBean) {
		String responseBody=null;
		try {
			/*all phone numbers from different tables will be store here */
			
			if(studentAnnouncementBean.getIsSanstha()==null || studentAnnouncementBean.getIsSanstha()==0) {
			List<String> phoneNumbers=new ArrayList<>();//we will send text msg on this phone number
			Integer announcementId=studentAnnouncementBean.getAnnouncementId();
			Integer schoolid=studentAnnouncementBean.getSchoolId();
			AnnouncementBean priority=announcement.getPriority(announcementId,schoolid);
			SchoolNameBean schoolName=schoolRepo.getShortSchoolName(studentAnnouncementBean.getSchoolId());
			Map<Integer,String> role=new HashMap<>();//staffIdStudentids and role mapping
			Map<Integer,Integer> appUserIds=new HashMap<>();
			Map<Integer,String> contactNumbers=new HashMap<>();

			List<Integer> staffStudentId=new ArrayList<>();//usinig these ids we will find roles
			List<staffStudentBean> staffStud=new ArrayList<>();//staffStudentId,name,contact number

			List<TokenMobileNumberBean>fcmTokenBean=new ArrayList<>();
			List<TokenMobileNumberBean>emailBean=new ArrayList<>();

			Integer announcementType=priority.getAnnouncementTypeId();
			
			if(announcementType==null) {
				announcementType=0;
			}
			
			/*getting phone numbers for sending sms*/
			List<PhoneNumberBean> phNoFromAssignToTable=studentAnnouncementAssignedToRepository.getPhoneNumbers(schoolid,announcementId);

			if(!CollectionUtils.isEmpty(phNoFromAssignToTable)) {

				assignToTableCode(phoneNumbers, phNoFromAssignToTable, priority.getPriority(), role, staffStudentId, staffStud,
						fcmTokenBean,announcementId,appUserIds,contactNumbers,emailBean);
			}
			/*getting phone numbers for sending sms*/
			List<PhoneNumberBean> phoneClassDivision=announcementAssignToClassDivisionRepository.getPhoneNumbers(schoolid,announcementId);					

			if(!CollectionUtils.isEmpty(phoneClassDivision)) {

				classDivisionTableCode(phoneNumbers, priority.getPriority(), role, staffStudentId, staffStud, fcmTokenBean,
						phoneClassDivision,announcementId,appUserIds,contactNumbers,emailBean);

			}
			List<PhoneNumberBean> phoneNoGroups=announcementForGroupRepository.getContactNumbersFromGroups(announcementId);
			if(!CollectionUtils.isEmpty(phoneNoGroups)) {
				assignToTableCode(phoneNumbers, phoneNoGroups, priority.getPriority(), role, staffStudentId, staffStud,
						fcmTokenBean,announcementId,appUserIds,contactNumbers,emailBean);
			}

			
			

			StringBuilder message=new StringBuilder();
			if( announcementType==null || announcementType==7) {

				
			message.append("Dear Employee, ");
			message.append("The Following task is assigned to you.");
			message.append("Task No #task#");
			message.append("Complainant Name #name#");
			message.append("Completion dt #date#");
			message.append("NMCGOV");
			}else if(announcementType==null || announcementType==5) {
				message.append("Dear ");
				message.append("#name#");
				message.append("("+schoolName.getSchoolShortCode()+"),");
				message.append("Kindly pay your pending fees of #amount#");
				message.append("as soon as possible.");
				message.append("- ITPL");
			}else {
				message.append("Dear ");
				message.append("#name# ");
				message.append("("+schoolName.getSchoolShortCode()+"), ");
				message.append("You have important notification.");
				message.append("Please visit this link ");
				message.append("\"info.eprashasan.com/#var#/n\" don't share this link- ITPL");
			}
			
			String message1=message.toString();

			List<SavedIdBean> savedIdBean=new ArrayList<>(); 
			savingDataToSentMessagesModel(phoneNumbers,role,message1,staffStudentId,savedIdBean,staffStud,schoolid,announcementId,appUserIds,contactNumbers,priority.getOtherSchoolId());

			responseBody = restCallSendCommunication(phoneNumbers, schoolid,message1,savedIdBean,fcmTokenBean,emailBean,schoolName.getSchoolName(),priority,announcementType);

			}else {
				
				Map<Integer,List<PhoneNumberBean>> phoneNumbersMapToSchool=new LinkedHashMap<>();
				List<PhoneNumberBean> phNoFromAssignToTable=new ArrayList<>();

				if(studentAnnouncementBean.getForPrincipal()==2 ||studentAnnouncementBean.getForTeacher()==2) {
					 phNoFromAssignToTable=studentAnnouncementAssignedToRepository.getPhoneNumbersForBalSanstha(studentAnnouncementBean.getSansthakey(),studentAnnouncementBean.getAnnouncementId());
					}else {
					phNoFromAssignToTable=	commonServiceDaoImpl.getStaffContactNo(studentAnnouncementBean);
					}
				
				for (PhoneNumberBean phoneNumber : phNoFromAssignToTable) {
			        Integer schoolId = phoneNumber.getSchoolid(); // Assuming PhoneNumberBean has a getSchoolId() method
			        phoneNumbersMapToSchool
			            .computeIfAbsent(schoolId, k -> new ArrayList<>())
			            .add(phoneNumber);
			    }
				
				
				for (Map.Entry<Integer, List<PhoneNumberBean>> entry : phoneNumbersMapToSchool.entrySet()) {

					
					Integer schoolId = entry.getKey();
				    List<PhoneNumberBean> phoneNumbersAssign=entry.getValue();
					
				    
				    
				    
				    Integer announcementId=studentAnnouncementBean.getAnnouncementId();
					Integer schoolid=studentAnnouncementBean.getSchoolId();
					Map<Integer,String> role=new HashMap<>();//staffIdStudentids and role mapping
					Map<Integer,Integer> appUserIds=new HashMap<>();
					Map<Integer,String> contactNumbers=new HashMap<>();

					List<Integer> staffStudentId=new ArrayList<>();//usinig these ids we will find roles
					List<staffStudentBean> staffStud=new ArrayList<>();//staffStudentId,name,contact number

					List<TokenMobileNumberBean>fcmTokenBean=new ArrayList<>();
					List<TokenMobileNumberBean>emailBean=new ArrayList<>();
					AnnouncementBean priority=announcement.getPriority(announcementId);

					Integer announcementType=priority.getAnnouncementTypeId();
					
					if(announcementType==null) {
						announcementType=0;
					}

					List<String> phoneNumbers=new ArrayList<>();//we will send text msg on this phone number

				    
				    
				    
				    
				    SchoolNameBean schoolName=schoolRepo.getShortSchoolName(schoolId);

					/*getting phone numbers for sending sms*/
				//	List<PhoneNumberBean> phNoFromAssignToTable=studentAnnouncementAssignedToRepository.getPhoneNumbers(schoolid,announcementId);

					if(!CollectionUtils.isEmpty(phoneNumbersAssign)) {

						assignToTableCode(phoneNumbers, phoneNumbersAssign, priority.getPriority(), role, staffStudentId, staffStud,
								fcmTokenBean,announcementId,appUserIds,contactNumbers,emailBean);
					}
			

					
					
					StringBuilder message=new StringBuilder();
					if( announcementType==null || announcementType==7) {

						
					message.append("Dear Employee, ");
					message.append("The Following task is assigned to you.");
					message.append("Task No #task#");
					message.append("Complainant Name #name#");
					message.append("Completion dt #date#");
					message.append("NMCGOV");
					}else if(announcementType==null || announcementType==5) {
						message.append("Dear ");
						message.append("#name#");
						message.append("("+schoolName.getSchoolShortCode()+"),");
						message.append("Kindly pay your pending fees of #amount#");
						message.append("as soon as possible.");
						message.append("- ITPL");
					}else {
						message.append("Dear ");
						message.append("#name# ");
						message.append("("+schoolName.getSchoolShortCode()+"), ");
						message.append("You have important notification.");
						message.append("Please visit this link ");
						message.append("\"info.eprashasan.com/#var#/n\" don't share this link- ITPL");
					}
					
					String message1=message.toString();

					List<SavedIdBean> savedIdBean=new ArrayList<>(); 
					savingDataToSentMessagesModel(phoneNumbers,role,message1,staffStudentId,savedIdBean,staffStud,schoolid,announcementId,appUserIds,contactNumbers,priority.getOtherSchoolId());

					responseBody = restCallSendCommunication(phoneNumbers, schoolid,message1,savedIdBean,fcmTokenBean,emailBean,schoolName.getSchoolName(),priority,announcementType);
					
				}

				
				

			}
		
		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseBody;
	}

	private void classDivisionTableCode(List<String> phoneNumbers, Integer priority, Map<Integer, String> role,
			List<Integer> staffStudentId, List<staffStudentBean> staffStud, List<TokenMobileNumberBean> fcmTokenBean,
			List<PhoneNumberBean> phoneClassDivision, Integer announcementId, Map<Integer, Integer> appUserIds, Map<Integer, String> contactNumbers, List<TokenMobileNumberBean> emailBean) {
		for (PhoneNumberBean phoneNumberBean : phoneClassDivision) {
			TokenMobileNumberBean tokenMobileNumberBean=new TokenMobileNumberBean();

			String fcmToken=null;
			String roleName="student";
			role.put(phoneNumberBean.getStaffStudentId(),"Student");
			contactNumbers.put(phoneNumberBean.getStaffStudentId(), phoneNumberBean.getStudentContackNumber());
			appUserIds.put(phoneNumberBean.getStaffStudentId(), phoneNumberBean.getStudentUserId());
			staffStudentBean staffStudContact = studentInfoHolder(role, staffStudentId, phoneNumberBean,announcementId);

			//staffStud.add(staffStudContact);
			if(priority==ApplicationConstants.highPriority){
				txtAndNotification(phoneNumbers, staffStud, fcmTokenBean, phoneNumberBean,
						tokenMobileNumberBean, fcmToken, staffStudContact,phoneNumberBean.getStaffStudentId(),roleName,announcementId,phoneNumberBean.getStudentUserId(),emailBean,phoneNumberBean.getStudentName());
			}else if(priority==ApplicationConstants.lowPriority) {

				notificationOrSmsClassDivision(phoneNumbers, staffStud, fcmTokenBean, phoneNumberBean,
						tokenMobileNumberBean, fcmToken, staffStudContact,phoneNumberBean.getStaffStudentId(),roleName,announcementId,phoneNumberBean.getStudentUserId(),emailBean,phoneNumberBean.getStudentName());

			}else if(priority==ApplicationConstants.onlyNotification) {
				onlyNotification(phoneNumbers, staffStud, fcmTokenBean, phoneNumberBean,
						tokenMobileNumberBean, fcmToken, staffStudContact,phoneNumberBean.getStaffStudentId(),roleName,announcementId,phoneNumberBean.getStudentUserId(),emailBean,phoneNumberBean.getStudentName());
			}
		}
	}


	private void phoneNumclassDivision(List<String> phoneNumbers, Integer priority,
			List<PhoneNumberBean> phoneClassDivision) {
		for (PhoneNumberBean phoneNumberBean : phoneClassDivision) {

			if(priority==ApplicationConstants.highPriority){
				txtAndNotificationPhoneNumber(phoneNumbers, phoneNumberBean);
			}else {

				notificationOrSmsClassDivisionPhoneNumber(phoneNumbers  , phoneNumberBean );

			}
		}
	}

	private void assignToTableCode(List<String> phoneNumbers, List<PhoneNumberBean> phNoFromAssignToTable,
			Integer priority, Map<Integer, String> role, List<Integer> staffStudentId, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, Integer announcementId, Map<Integer, Integer> appUserIds, Map<Integer, String> contactNumbers, List<TokenMobileNumberBean> emailBean) {
		for (PhoneNumberBean staffContactNumber : phNoFromAssignToTable) {
			TokenMobileNumberBean tokenMobileNumberBean=new TokenMobileNumberBean();
			String fcmToken=null;
			staffStudentBean staffStudContact=new staffStudentBean();
			if(staffContactNumber.getStaffContact()!=null && !staffContactNumber.getStaffContact().equals("0")) {
				String roleName="staff";
				role.put(staffContactNumber.getStaffId(),"Staff");
				contactNumbers.put(staffContactNumber.getStaffId(), staffContactNumber.getStaffContact());
				appUserIds.put(staffContactNumber.getStaffId(),staffContactNumber.getStaffUserId());

				staffInfoHolder(role, staffStudentId, staffContactNumber, staffStudContact,announcementId);

				if(priority==ApplicationConstants.highPriority){//If priority is high then we have to send text message as well as notification
					
					fcmToken = textMessageAndNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStaffUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStaffContact(),staffContactNumber.getStaffId(),roleName,announcementId,staffContactNumber,emailBean,staffContactNumber.getStaffName());

				}else if(priority==ApplicationConstants.lowPriority) {//if priority is low we will find if token is availbe for the number first if token is not availble then we will send text message on that number
					fcmToken = textMessageOrNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStaffUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStaffContact(),staffContactNumber.getStaffId(),roleName,announcementId,staffContactNumber,emailBean,staffContactNumber.getStaffName());
				}else if(priority==ApplicationConstants.onlyNotification){
					fcmToken = onlyNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStaffUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStaffContact(),staffContactNumber.getStaffId(),roleName,announcementId,staffContactNumber,emailBean,staffContactNumber.getStaffName());
				}


			}
			else if(staffContactNumber.getStudentContackNumber()!=null && !staffContactNumber.getStudentContackNumber().equals("0") ) {

				String roleName="student";

				appUserIds.put(staffContactNumber.getStudentId(),staffContactNumber.getStudentUserId());
				role.put(staffContactNumber.getStudentId(),"Student");
				contactNumbers.put(staffContactNumber.getStudentId(), staffContactNumber.getStudentContackNumber());

				studentInfoHolder(role, staffStudentId, staffContactNumber, staffStudContact,announcementId);

				if(priority==ApplicationConstants.highPriority){


					fcmToken = textMessageAndNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStudentUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStudentContackNumber(),staffContactNumber.getStudentId(),roleName,announcementId,staffContactNumber,emailBean,staffContactNumber.getStudentName());

				}else if(priority==ApplicationConstants.lowPriority){

					fcmToken = textMessageOrNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStudentUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStudentContackNumber(),staffContactNumber.getStudentId(),roleName,announcementId,staffContactNumber,emailBean,staffContactNumber.getStudentName());
				}else if(priority==ApplicationConstants.onlyNotification) {
					fcmToken = onlyNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStudentUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStudentContackNumber(),staffContactNumber.getStudentId(),roleName,announcementId,staffContactNumber,emailBean,staffContactNumber.getStudentName());
				}

			}else {
				if(staffContactNumber.getRole().equals("Student") || staffContactNumber.getRole().equals("Parent")) {
					fcmToken = onlySaveNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStudentUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStudentContackNumber(),staffContactNumber.getStudentId(),"Student",announcementId,staffContactNumber,emailBean,staffContactNumber.getStudentName());

					role.put(staffContactNumber.getStudentId(),"Student");
					studentInfoHolder(role, staffStudentId, staffContactNumber, staffStudContact,announcementId);
					if(priority!=ApplicationConstants.onlyNotification) {
						phoneNumbers.add(staffContactNumber.getStudentContackNumber());
						staffStud.add(staffStudContact);
					}
				}
				else {
					fcmToken = onlySaveNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber.getStaffUserId(),
							tokenMobileNumberBean, fcmToken, staffStudContact,staffContactNumber.getStaffContact(),staffContactNumber.getStaffId(),"Staff",announcementId,staffContactNumber,emailBean,staffContactNumber.getStaffName());

					role.put(staffContactNumber.getStaffId(),"Staff");

					staffInfoHolder(role, staffStudentId, staffContactNumber, staffStudContact,announcementId);

					if(priority!=ApplicationConstants.onlyNotification) {
						phoneNumbers.add(staffContactNumber.getStaffContact());
						staffStud.add(staffStudContact);
					}
				}

			}

		}
	}


	private void phoneNumAssignTo(List<String> phoneNumbers, List<PhoneNumberBean> phNoFromAssignToTable,
			Integer priority) {
		for (PhoneNumberBean staffContactNumber : phNoFromAssignToTable) {
			if(staffContactNumber.getStaffContact()!=null && !staffContactNumber.getStaffContact().equals("0")) {

				if(priority==ApplicationConstants.highPriority){//If priority is high then we have to send text message as well as notification

					textMessageAndNotificationPhoneNumber(phoneNumbers,staffContactNumber.getStaffContact());


				}else {//if priority is low we will find if token is availbe for the number first if token is not availble then we will send text message on that number
					textMessageOrNotificationPhoneNumber(phoneNumbers,staffContactNumber.getStaffContact());
				}
			}
			else if(staffContactNumber.getStudentContackNumber()!=null && !staffContactNumber.getStudentContackNumber().equals("0") ) {


				if(priority==ApplicationConstants.highPriority){


					textMessageAndNotificationPhoneNumber(phoneNumbers,staffContactNumber.getStudentContackNumber());

				}else {

					textMessageOrNotificationPhoneNumber(phoneNumbers,staffContactNumber.getStudentContackNumber());
				}

			}

		}
	}


	private void studentInfoHolder(Map<Integer, String> role, List<Integer> staffStudentId,
			PhoneNumberBean staffContactNumber, staffStudentBean staffStudContact,Integer announcementId) {

		//role.put(staffContactNumber.getStudentId(),"Student");
		staffStudentId.add(staffContactNumber.getStudentId());
		setStaffStudentContactBean(staffContactNumber.getStudentContackNumber(),staffContactNumber.getStudentId(),staffContactNumber.getStudentName(),staffStudContact,staffContactNumber,announcementId);
	}



	private void staffInfoHolder(Map<Integer, String> role, List<Integer> staffStudentId,
			PhoneNumberBean staffContactNumber, staffStudentBean staffStudContact,Integer announcementId) {

		//role.put(staffContactNumber.getStaffId(),"Staff");

		staffStudentId.add(staffContactNumber.getStaffId());
		setStaffStudentContactBean(staffContactNumber.getStaffContact(),staffContactNumber.getStaffId(),staffContactNumber.getStaffName(),staffStudContact,staffContactNumber,announcementId);

	}

	private void txtAndNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, PhoneNumberBean phoneNumberBean,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact, Integer staffStudentId, String roleName, Integer announcementId, Integer userId, List<TokenMobileNumberBean> emailBean,String name) {

		//if(phoneNumberBean.getStudentContackNumber()!=null ) {
		phoneNumbers.add(phoneNumberBean.getStudentContackNumber());
		staffStud.add(staffStudContact);
		Page<String> fcmToken1=fcmRepo.getToken(phoneNumberBean.getStudentContackNumber(), PageRequest.of(0, 1));

		if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {

			fcmToken=fcmToken1.getContent().get(0);
			setNotificationBean(fcmToken, phoneNumberBean.getStudentContackNumber(),userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,phoneNumberBean);

		}else {
			setNotificationBean(fcmToken, phoneNumberBean.getStudentContackNumber(),userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,phoneNumberBean);

		}
		setEmailBean(fcmToken, phoneNumberBean.getStudentContackNumber(),userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,phoneNumberBean,name);

	}

	private void txtAndNotificationPhoneNumber(List<String> phoneNumbers, PhoneNumberBean phoneNumberBean) {

		phoneNumbers.add(phoneNumberBean.getStudentContackNumber());
	}


	private staffStudentBean studentInfoHolder(Map<Integer, String> role, List<Integer> staffStudentId,
			PhoneNumberBean phoneNumberBean,Integer announcementId) {
		staffStudentBean staffStudContact=new staffStudentBean();
		staffStudentId.add(phoneNumberBean.getStaffStudentId());

		setStaffStudentContactBean(phoneNumberBean.getStudentContackNumber(),phoneNumberBean.getStaffStudentId(),phoneNumberBean.getStudentName(),staffStudContact,phoneNumberBean,announcementId);

		return staffStudContact;
	}

	//	private void messageAndNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
	//			List<TokenMobileNumberBean> fcmTokenBean, PhoneNumberBean staffContactNumber,
	//			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact) {
	//		   
	//			txtAndNotification(phoneNumbers, staffStud, fcmTokenBean, staffContactNumber, tokenMobileNumberBean, fcmToken,
	//				staffStudContact);
	//	}

	private String textMessageAndNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, Integer userId,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact,String contactNumber, Integer staffStudentId, String roleName, Integer announcementId, PhoneNumberBean staffContactNumber, List<TokenMobileNumberBean> emailBean,String name) {

		//for text
		phoneNumbers.add(contactNumber);
		staffStud.add(staffStudContact);
		//for notification
		Page<String> fcmToken1=fcmRepo.getToken(contactNumber, PageRequest.of(0, 1));
		if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			fcmToken=fcmToken1.getContent().get(0);
			setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);
		}
		else { 
			setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);

		}
		setEmailBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,staffContactNumber,name);

		return fcmToken;
	}

	private String textMessageAndNotificationPhoneNumber(List<String> phoneNumbers,String contactNumber) {

		phoneNumbers.add(contactNumber);

		return null;
	}

	private String onlySaveNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, Integer userId,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact,String contactNumber, Integer staffStudentId, String roleName, Integer announcementId, PhoneNumberBean staffContactNumber, List<TokenMobileNumberBean> emailBean,String name) {

		setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);
		setEmailBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,staffContactNumber,name);


		return fcmToken;
	}
	private void notificationOrSmsClassDivision(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, PhoneNumberBean staffContactNumber,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact, Integer staffStudentId, String roleName, Integer announcementId, Integer userId, List<TokenMobileNumberBean> emailBean,String name) {
		Page<String> fcmToken1=fcmRepo.getToken(staffContactNumber.getStudentContackNumber(), PageRequest.of(0, 1));

		if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			fcmToken=fcmToken1.getContent().get(0);
			setNotificationBean(fcmToken, staffContactNumber.getStudentContackNumber(), userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);

		}else {
			setNotificationBean(fcmToken, staffContactNumber.getStudentContackNumber(), userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);

			phoneNumbers.add(staffContactNumber.getStudentContackNumber());
			staffStud.add(staffStudContact);
		}
		setEmailBean(fcmToken, staffContactNumber.getStudentContackNumber(), userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,staffContactNumber,name);

	}

	private void onlyNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, PhoneNumberBean staffContactNumber,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact, Integer staffStudentId, String roleName, Integer announcementId, Integer userId, List<TokenMobileNumberBean> emailBean,String name) {
		Page<String> fcmToken1=fcmRepo.getToken(staffContactNumber.getStudentContackNumber(), PageRequest.of(0, 1));

		if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			fcmToken=fcmToken1.getContent().get(0);
			setNotificationBean(fcmToken, staffContactNumber.getStudentContackNumber(), userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);
		}else {
			//here we are setting bean without token only for save purpose
			setNotificationBean(fcmToken, staffContactNumber.getStudentContackNumber(), userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);
		}
		setEmailBean(fcmToken, staffContactNumber.getStudentContackNumber(), userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,staffContactNumber,name);

	}

	private void notificationOrSmsClassDivisionPhoneNumber(List<String> phoneNumbers,
			PhoneNumberBean staffContactNumber) {
		Page<String> fcmToken1=fcmRepo.getToken(staffContactNumber.getStudentContackNumber(), PageRequest.of(0, 1));

		if(CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			phoneNumbers.add(staffContactNumber.getStudentContackNumber());
		}
	}

	private String textMessageOrNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, Integer userId,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact,String contactNumber, Integer staffStudentId, String roleName, Integer announcementId, PhoneNumberBean staffContactNumber, List<TokenMobileNumberBean> emailBean,String name) {
		Page<String> fcmToken1=fcmRepo.getToken(contactNumber, PageRequest.of(0, 1));
		if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			fcmToken=fcmToken1.getContent().get(0);
			setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);

		}
		else {
			//here we are setting bean without token only for save purpose
			setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);
			//if token is empty then insted we will send sms
			phoneNumbers.add(contactNumber);
			staffStud.add(staffStudContact);
		}
		setEmailBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,staffContactNumber,name);

		
		return fcmToken;
	}

	private String onlyNotification(List<String> phoneNumbers, List<staffStudentBean> staffStud,
			List<TokenMobileNumberBean> fcmTokenBean, Integer userId,
			TokenMobileNumberBean tokenMobileNumberBean, String fcmToken, staffStudentBean staffStudContact,String contactNumber, Integer staffStudentId, String roleName, Integer announcementId, PhoneNumberBean staffContactNumber, List<TokenMobileNumberBean> emailBean, String name) {
		Page<String> fcmToken1=fcmRepo.getToken(contactNumber, PageRequest.of(0, 1));
		if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			fcmToken=fcmToken1.getContent().get(0);
			setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);

		}
		else {
			//here we are setting bean without token only for save purpose
			setNotificationBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, fcmTokenBean,staffStudentId,roleName,announcementId,staffContactNumber);

		}
		setEmailBean(fcmToken, contactNumber, userId, tokenMobileNumberBean, emailBean,staffStudentId,roleName,announcementId,staffContactNumber,name);

		return fcmToken;
	}

	private String textMessageOrNotificationPhoneNumber(List<String> phoneNumbers,String contactNumber) {
		Page<String> fcmToken1=fcmRepo.getToken(contactNumber, PageRequest.of(0, 1));
		if(CollectionUtils.isEmpty(fcmToken1.getContent()) ) {
			phoneNumbers.add(contactNumber);

		}

		return null;
	}

	private void savingDataToSentMessagesModel(List<String> phoneNumbers, Map<Integer, String> role,String message, List<Integer> staffStudentId, List<SavedIdBean> savedIdBean, List<staffStudentBean> staffStud, Integer schoolid, Integer announcementId, Map<Integer, Integer> appUserIds, Map<Integer, String> contactNumbers, Integer otherSchoolId) {


		for (staffStudentBean staffStudent : staffStud) {

			try {
			String rCode=null;
			boolean found=true;
//			do {
//				rCode=uniqueCode.generateUniqueCode();
//				List<Integer>entry=sentMessagesRepository.findByRCode(rCode);
//				if(!CollectionUtils.isEmpty(entry)) {
//					found=true;
//				}else {
//					found=false;
//				}
//			}while(found);
			
			do {
			    rCode = uniqueCode.generateUniqueCode();
			} while (!sentMessagesRepository.findByRCode(rCode).isEmpty());
			
			SavedIdBean savedId=new SavedIdBean();
			SentMessagesModel sentMessage=new SentMessagesModel();
			sentMessage.setId(0);
			sentMessage.setMessage(message);
			sentMessage.setRole(role.get(staffStudent.getStaffStudentId()));
			sentMessage.setStaffStudentId(staffStudent.getStaffStudentId());
			System.out.println("staffStudentId::"+staffStudent.getStaffStudentId());
			
			sentMessage.setTemplateTypeId(2);
			sentMessage.setSchoolId(schoolid);
			sentMessage.setAnnouncementId(announcementId);
			sentMessage.setAppUserId(appUserIds.get(staffStudent.getStaffStudentId()));
			System.out.println("contactNumber::"+contactNumbers.get(staffStudent.getStaffStudentId()));
			sentMessage.setMobileNumber(contactNumbers.get(staffStudent.getStaffStudentId()));
			
			sentMessage.setcDate(new Date());
			sentMessage.setRcode(rCode);
			if(otherSchoolId!=null) {
				sentMessage.setOtherSchoolId(otherSchoolId);
			}else {
				sentMessage.setOtherSchoolId(0);
			}
			String id=sentMessagesRepository.save(sentMessage).getRcode();


			savedId.setMobileNumber(staffStudent.getContatctNumber());
			savedId.setSavedId(id);
			savedId.setName(staffStudent.getName());
			savedId.setYearId(staffStudent.getYearId());
			savedId.setYear(staffStudent.getYear());
			savedId.setStandardId(staffStudent.getStandardId());
			savedId.setStaffStudentId(staffStudent.getStaffStudentId());
			savedId.setAnnouncementId(staffStudent.getAnnouncementId());
			savedId.setrCode(rCode);
			savedIdBean.add(savedId);

			//			for (staffStudentBean staffStudentBean : staffStud) {
			//				
			//				if(staffStudentBean.getStaffStudentId()==staffStudent) {
			//					
			//				}
			//			}

			}catch(Exception e) {
				e.printStackTrace();
			}
		}

	}

	private String restCallSendCommunication(List<String> phoneNumbers, Integer schoolid, String message, List<SavedIdBean> savedIdBean, List<TokenMobileNumberBean> fcmTokenBean, List<TokenMobileNumberBean> emailBean, String schoolName, AnnouncementBean priority, Integer announcementType)
			throws JsonProcessingException {

		String responseBody;

		/*json object is created for rest call*/
		String jsonString = jsonObjectCreator(schoolid,phoneNumbers,message,savedIdBean,fcmTokenBean,emailBean,schoolName,priority,announcementType);
		
		System.out.println(jsonString);
		/*seeting headers*/
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> requestEntity = new HttpEntity<>(jsonString, headers);
		String url = "http://65.2.23.210:8051/sendCommunication";
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		responseBody = responseEntity.getBody();
		return responseBody;
	}

	private String jsonObjectCreator(Integer schoolid, List<String> phoneNumbers, String message, List<SavedIdBean> savedIdBean, List<TokenMobileNumberBean> fcmTokenBean, List<TokenMobileNumberBean> emailBean, String schoolName,  AnnouncementBean priority, Integer announcementType)
			throws JsonProcessingException {

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode phoneNumberArray = objectMapper.createArrayNode();
		ArrayNode fcmToken = objectMapper.createArrayNode();
		ArrayNode emails = objectMapper.createArrayNode();


		ArrayNode savedIdArray = objectMapper.createArrayNode();
		//real	
		for (SavedIdBean savedIdB : savedIdBean) {
			String mobileNumber=savedIdB.getMobileNumber();
			String savedId=savedIdB.getSavedId();
			String name=savedIdB.getName();
			ObjectNode savedId1 = objectMapper.createObjectNode();
			savedId1.put("mobileNumber",mobileNumber );
			savedId1.put("savedId", savedId);
			savedId1.put("yearId", savedIdB.getYearId());
			savedId1.put("year", savedIdB.getYear());
			savedId1.put("standardId", savedIdB.getStandardId());
			savedId1.put("staffStudentId", savedIdB.getStaffStudentId());
			savedId1.put("name", name);
			savedId1.put("announcementId", savedIdB.getAnnouncementId());
			savedId1.put("rCode", savedIdB.getrCode());



			savedIdArray.add(savedId1);
		}

		

		//real
		for (String contactNo : phoneNumbers) {
			if(contactNo!=null) {
				phoneNumberArray.add(contactNo);
			}
		}
		


		//real
		for (TokenMobileNumberBean token : fcmTokenBean) {
			ObjectNode tokenObj = objectMapper.createObjectNode();
			tokenObj.put("fcmToken",token.getFcmToken());
			tokenObj.put("mobileNumber", token.getMobileNumber());
			tokenObj.put("appUserId", token.getAppUserId());
			tokenObj.put("staffStudentId",token.getStaffStudentId());
			tokenObj.put("role", token.getRole());
			tokenObj.put("announcementId", token.getAnnouncementId());
			tokenObj.put("yearId", token.getYearId());
			tokenObj.put("year", token.getYear());
			tokenObj.put("standardId", token.getStandardId());
			tokenObj.put("name", token.getName());



			fcmToken.add(tokenObj);
		}
		
		for (TokenMobileNumberBean email : emailBean) {
			ObjectNode tokenObj = objectMapper.createObjectNode();
			tokenObj.put("emailId",email.getEmailId());
			tokenObj.put("mobileNumber", email.getMobileNumber());
			tokenObj.put("appUserId", email.getAppUserId());
			tokenObj.put("staffStudentId",email.getStaffStudentId());
			tokenObj.put("role", email.getRole());
			tokenObj.put("announcementId", email.getAnnouncementId());
			tokenObj.put("name", email.getName());
			tokenObj.put("yearId", email.getYearId());
			tokenObj.put("year", email.getYear());
			tokenObj.put("standardId", email.getStandardId());



			emails.add(tokenObj);
		}
		//test
		//fcmToken.add("eGAL4_EhRZGlGaWoPKx8Kh:APA91bFI5VgsFp33ozz61__SRH4nt_LeYPW9bg8XcaS-P7LgBWueqOYJxt3-Zi-HtwqXxRyCg5Kz1qAFjdOlTK_zzEXm1_khlQtT9FfH_LNABuYISbba46J5tBkMvFK6iSZiSx5i_W2w");

		ObjectNode jsonNode = objectMapper.createObjectNode();
		jsonNode.set("phoneNumber",phoneNumberArray);
		jsonNode.set("savedIdBean", savedIdArray);
		jsonNode.put("schoolId", schoolid);
		jsonNode.put("message", message);
		jsonNode.put("schoolName", schoolName);
		jsonNode.put("otherSchoolId",priority.getOtherSchoolId());
		jsonNode.put("deadLine",""+priority.getDeadline());
		jsonNode.put("complaintBy",priority.getComplaintBy());

		/*template Id 1 is for send post details to staff and students  */
		if(announcementType==5){
			jsonNode.put("templateId", 3); //for fees

		}else if(announcementType==7){
			jsonNode.put("templateId", 5); //for task

		}else {
			jsonNode.put("templateId", 2);
		}
		jsonNode.set("fcmTokens", fcmToken);
		jsonNode.set("emailBean", emails);

		String jsonString = objectMapper.writeValueAsString(jsonNode);
		return jsonString;
	}

	@Override
	public Optional<SentMessagesModel> getMessageDetails(String rcode) {

		Optional<SentMessagesModel> messageDetails=sentMessagesRepository.findByRcode(rcode);
		return messageDetails;
	}

	


	public void setNotificationBean(String fcmToken,String contactNumber,Integer appUserId,TokenMobileNumberBean tokenMobileNumberBean,List<TokenMobileNumberBean> fcmTokenBean,Integer staffStudentId,String role, Integer announcementId, PhoneNumberBean staffContactNumber)
	{
		tokenMobileNumberBean.setFcmToken(fcmToken);
		tokenMobileNumberBean.setMobileNumber(contactNumber);
		tokenMobileNumberBean.setAppUserId(appUserId);
		tokenMobileNumberBean.setStaffStudentId(staffStudentId);
		tokenMobileNumberBean.setRole(role);
		tokenMobileNumberBean.setAnnouncementId(announcementId);
		tokenMobileNumberBean.setYearId(staffContactNumber.getYearId());
		tokenMobileNumberBean.setYear(staffContactNumber.getYear());
		tokenMobileNumberBean.setStandardId(staffContactNumber.getStandardId());
		fcmTokenBean.add(tokenMobileNumberBean);
	}
	
	public void setEmailBean(String fcmToken,String contactNumber,Integer appUserId,TokenMobileNumberBean tokenMobileNumberBean,List<TokenMobileNumberBean> emailBean,Integer staffStudentId,String role, Integer announcementId, PhoneNumberBean staffContactNumber, String name)
	{
		tokenMobileNumberBean.setEmailId(staffContactNumber.getEmailId());
		tokenMobileNumberBean.setMobileNumber(contactNumber);
		tokenMobileNumberBean.setAppUserId(appUserId);
		tokenMobileNumberBean.setStaffStudentId(staffStudentId);
		tokenMobileNumberBean.setRole(role);
		tokenMobileNumberBean.setAnnouncementId(announcementId);
		tokenMobileNumberBean.setName(name);
		tokenMobileNumberBean.setYearId(staffContactNumber.getYearId());
		tokenMobileNumberBean.setYear(staffContactNumber.getYear());
		tokenMobileNumberBean.setStandardId(staffContactNumber.getStandardId());
		emailBean.add(tokenMobileNumberBean);
	}
	private void setStaffStudentContactBean(String studentContackNumber, Integer studentId, String studentName, staffStudentBean staffStudContact, PhoneNumberBean staffContactNumber,Integer announcementId) {
		staffStudContact.setContatctNumber(studentContackNumber);
		staffStudContact.setStaffStudentId(studentId);
		staffStudContact.setName(studentName);
		staffStudContact.setYearId(staffContactNumber.getYearId());
		staffStudContact.setYear(staffContactNumber.getYear());
		staffStudContact.setStandardId(staffContactNumber.getStandardId());
		staffStudContact.setAnnouncementId(announcementId);

	}

	@Override
	public Integer recordMessageReadStatus(SentMessagesReadStatusModel sentMessagesReadStatus) {

		if(sentMessagesReadStatus.getReadStatusId()==1) {
			sentMessagesReadStatus.setReadStatusId(0);
		}

		Integer id=	sentMessagesReadStatusRepo.save(sentMessagesReadStatus).getReadStatusId();
		return id;
	}

	@Override
	public List<SaveSendNotificationModel> getAllNotification(Integer appUserId, String flag, Integer offset,
			Integer limit) {


		Page<SaveSendNotificationModel> saveSendNotification=notificationRepo.getAllNotifications(appUserId, flag,PageRequest.of(offset, limit));

		List<SaveSendNotificationModel> content = saveSendNotification.getContent();
		return content;
	}

	@Override
	public List<SentMessagesModel> getAllMessages(Integer staffStudentId, Integer flag) {

		String role;
		if(flag==0) {
			role="Student";
		}else {
			role="Staff";
		}

		List<SentMessagesModel> sentMessages= sentMessagesRepository.getMessages(staffStudentId,role);

		for (SentMessagesModel sentMessagesModel : sentMessages) {
			Integer sentMessageId=sentMessagesModel.getId();

			List<SentMessagesReadStatusModel> readStatus=sentMessagesReadStatusRepo.findBySentMessageIdOrderBySentMessageIdDesc(sentMessageId);
			sentMessagesModel.setReadStatus(readStatus);
		}

		return sentMessages;
	}

	
	@Override
	public ReportBean getannouncementdeliveryreportassignto(Integer schoolid, Integer announcementId) {


		List<PhoneNumberBean> phNoFromAssignToTable=studentAnnouncementAssignedToRepository.getStaffStudentReport(schoolid,announcementId);


		List<PhoneNumberBean> staffReportBean=new ArrayList<>();
		List<PhoneNumberBean> studentReportBean=new ArrayList<>();
		//		StaffReportBean staff=new StaffReportBean();
		//		StudentReportBean student=new StudentReportBean();
		ReportBean report =new ReportBean();

		for (PhoneNumberBean reports : phNoFromAssignToTable) {


			if(reports.getRole().equals("Teacher")|| reports.getRole().equals("Principal")) {
				SentMessagesModel sentMessage= sentMessagesRepository.getMessagesForRepot(reports.getStaffId(),"Staff",announcementId);

				if(sentMessage!=null) {
					List<SentMessagesReadStatusModel>sentMessagesReadStatusModel=sentMessagesReadStatusRepo.findBySentMessageId(sentMessage.getId());
					//SentMessagesReadStatusModel
					reports.setSentMessage(sentMessage);
					reports.setReadStatus(sentMessagesReadStatusModel);
				}
				List<SaveSendNotificationModel> allNotifications = notification.getAllNotificationsForReport(reports.getStaffUserId(), "2",announcementId);
				reports.setAllNotifications(allNotifications);
				List<SentEmailModel> allEmails = sentEmailRepo.getAllEmails(reports.getStaffUserId(), "staff",announcementId);
				reports.setSentEmail(allEmails);
				staffReportBean.add(reports);

			}else {
				SentMessagesModel sentMessage= sentMessagesRepository.getMessagesForRepot(reports.getStudentId(),"Student",announcementId);
				if(sentMessage!=null) {
					List<SentMessagesReadStatusModel>sentMessagesReadStatusModel=sentMessagesReadStatusRepo.findBySentMessageId(sentMessage.getId());
					reports.setSentMessage(sentMessage);
					reports.setReadStatus(sentMessagesReadStatusModel);

				}
				List<SaveSendNotificationModel> allNotifications = notification.getAllNotificationsForReport(reports.getStudentUserId(), "1",announcementId);
				reports.setAllNotifications(allNotifications);
				List<SentEmailModel> allEmails = sentEmailRepo.getAllEmails(reports.getStudentUserId(), "Student",announcementId);
				reports.setSentEmail(allEmails);

				studentReportBean.add(reports);

			}
		}

		report.setStaffReportBean(staffReportBean);
		report.setStudentReportBean(studentReportBean);
		System.out.println("");
		return report;
	}

	@Override
	public List<SentMessagesModel> getAllMessagesReport(Integer schoolid, String fromDate, String toDate,Integer otherSchoolId, Integer offset, Integer limit) {

		List<SentMessagesModel> sentMessages=new ArrayList<>();
		if(fromDate!=null && toDate!=null) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date fromDate1 = null;
			Date toDate1 =null;
			try {
				fromDate1 = dateFormat.parse(fromDate);
				toDate1= dateFormat.parse(toDate);
			}catch(Exception e) {
				e.printStackTrace();
			}

			if(otherSchoolId!=null && otherSchoolId>0) {
				sentMessages= sentMessagesRepository.getAllMessagesReportDateWise(schoolid,fromDate1,toDate1,otherSchoolId,PageRequest.of(offset, limit));
			
			}else {
				sentMessages= sentMessagesRepository.getAllMessagesReportDateWise(schoolid,fromDate1,toDate1,PageRequest.of(offset, limit));
			}
		}else {
			if(otherSchoolId!=null && otherSchoolId>0) {
				sentMessages= sentMessagesRepository.getAllMessagesReport(schoolid,otherSchoolId,PageRequest.of(offset, limit));
			}else {
				sentMessages= sentMessagesRepository.getAllMessagesReport(schoolid,PageRequest.of(offset, limit));
			}
		}

		for (SentMessagesModel sentMessagesModel : sentMessages) {
			Integer sentMessageId=sentMessagesModel.getId();

			List<SentMessagesReadStatusModel> readStatus=sentMessagesReadStatusRepo.findBySentMessageIdOrderBySentMessageIdDesc(sentMessageId);
			sentMessagesModel.setReadStatus(readStatus);

			List<SentEmailModel> sentEmail=sentEmailRepo.findByAppUserId(sentMessagesModel.getAppUserId());
			
			for (SentEmailModel email : sentEmail) {
				List<SentEmailReadStatusModel> emailReadStatus=sentEmailReadStatusRepository.findBySentEmailId(email.getId());
				email.setEmailReadStatus(emailReadStatus);
			}
			
			sentMessagesModel.setSentEmail(sentEmail);
		}
		return sentMessages;
	}

	//	@Override
	//	public List<SentMessagesModel> getMessageReadStatus(String messageSavedId) {
	//
	//		List<SentMessagesModel> sentMessageDetails=sentMessagesReadStatusRepo.findBySentMessageId(messageSavedId);
	//		
	//		return sentMessageDetails;
	//	}


	@Override
	@Transactional
	public Integer submitAnnouncementApprovalStatus(SubmitApprovalBean studentAnnouncementBean) {

		if(studentAnnouncementBean.getIsSanstha()==null||studentAnnouncementBean.getIsSanstha()!=1  ) {
		List<String> phoneNumbers=new ArrayList<>();//we will send text msg on this phone number
		Integer announcementId=studentAnnouncementBean.getAnnouncementId();
		Integer schoolid=studentAnnouncementBean.getSchoolId();
		/*getting phone numbers for sending sms*/
		List<PhoneNumberBean> phNoFromAssignToTable=studentAnnouncementAssignedToRepository.getPhoneNumbersForBal(schoolid,announcementId);
		AnnouncementBean priority=announcement.getPriority(announcementId,schoolid);
		//String schoolName=schoolRepo.getShortSchoolName(studentAnnouncementBean.getSchoolId());
		
		Integer announcementType=priority.getAnnouncementTypeId();
		if(announcementType==null) {
			announcementType=0;
		}

		List<TokenMobileNumberBean>fcmTokenBean=new ArrayList<>();

		if(!CollectionUtils.isEmpty(phNoFromAssignToTable)) {

			phoneNumAssignTo(phoneNumbers, phNoFromAssignToTable, priority.getPriority());
		}
		/*getting phone numbers for sending sms*/
	 
		List<PhoneNumberBean> phoneClassDivision=announcementAssignToClassDivisionRepository.getPhoneNumbersForBal(schoolid,announcementId);					
	 
		if(!CollectionUtils.isEmpty(phoneClassDivision)) {

			phoneNumclassDivision(phoneNumbers, priority.getPriority() ,
					phoneClassDivision);

		}
	 
		
		if(announcementType!=5) {
		List<PhoneNumberBean> phoneNoGroups=announcementForGroupRepository.getContactNumbersFromGroups(announcementId);
		if(!CollectionUtils.isEmpty(phoneNoGroups)) {
			phoneNumAssignTo(phoneNumbers, phoneNoGroups, priority.getPriority());
		}
		}
		Integer totalMessages=0;

		for (String contact : phoneNumbers) {

			if(!contact.equals("0") && !contact.equals(null)) {
				totalMessages++;
			}
		}

		if(priority.getPriority()!=2) {
		//SmsSchoolDebitModel bal=sendCommunicationRepoImpl.CheckBalance(schoolid,entityManager);
		SmsSchoolCreditModel smsCreditInfo = communicationServiceImpl.getSmsCreditInfo(schoolid);
		// Fetch the account by school ID

		Integer bal=smsCreditInfo.getTotalCredit()-smsCreditInfo.getSmsCount()-totalMessages;
		if(bal<0) {
		return 0;	
		}
		}


		//Integer update=smsSchoolDebitRepo.updateBalance(totalMessages);

		
//		List<Integer> alreadyExist= announcementApprovalStatusRepository.getApprovedStatus(studentAnnouncementBean.getAnnouncementId(),studentAnnouncementBean.getSchoolId());
//
//		if(!CollectionUtils.isEmpty(alreadyExist)) {
		AnnouncementApprovalStatusModel announcementApprovalStatusModel = new AnnouncementApprovalStatusModel();
		AnnouncementModel announcementModel = new AnnouncementModel();
		announcementModel.setAnnouncementId(studentAnnouncementBean.getAnnouncementId());

		AppUserModel appUserModel = new AppUserModel();
		appUserModel.setAppUserRoleId(studentAnnouncementBean.getAppuserId());

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());

		announcementApprovalStatusModel.setAnnouncementModel(announcementModel);
		announcementApprovalStatusModel.setAppUserModel(appUserModel);
		announcementApprovalStatusModel.setSchoolMasterModel(schoolMasterModel);
		announcementApprovalStatusModel.setStatus(studentAnnouncementBean.getStatus());
		announcementApprovalStatusModel.setRemark(studentAnnouncementBean.getRemark());
		Integer id=announcementApprovalStatusRepository.save(announcementApprovalStatusModel).getAnnouncementapprovalStatusId();
		

		announcement.updateFlah(studentAnnouncementBean.getAnnouncementId());
		return id;
		}else {
			
			
			List<String> phoneNumbers=new ArrayList<>();//we will send text msg on this phone number
			Integer announcementId=studentAnnouncementBean.getAnnouncementId();
			Integer schoolid=studentAnnouncementBean.getSchoolId();
			String sansthaKey=studentAnnouncementBean.getSansthakey();
			List<PhoneNumberBean> phNoFromAssignToTable=new ArrayList<>();
				
			if(studentAnnouncementBean.getForPrincipal()==2 ||studentAnnouncementBean.getForTeacher()==2) {
			 phNoFromAssignToTable=studentAnnouncementAssignedToRepository.getPhoneNumbersForBalSanstha(sansthaKey,announcementId);
			}else {
			phNoFromAssignToTable=	commonServiceDaoImpl.getStaffContactNo(studentAnnouncementBean);
			}
		
			AnnouncementBean priority=announcement.getPriority(announcementId);
			
			Integer announcementType=priority.getAnnouncementTypeId();
			if(announcementType==null) {
				announcementType=0;
			}


			if(!CollectionUtils.isEmpty(phNoFromAssignToTable)) {

				phoneNumAssignTo(phoneNumbers, phNoFromAssignToTable, priority.getPriority());
			}
		 
			Integer totalMessages=0;

			for (String contact : phoneNumbers) {

				if(!contact.equals("0") && !contact.equals(null)) {
					totalMessages++;
				}
			}

			if(priority.getPriority()!=2) {
			//SmsSchoolDebitModel bal=sendCommunicationRepoImpl.CheckBalance(schoolid,entityManager);
			SmsSchoolCreditModel smsCreditInfo = communicationServiceImpl.getSmsCreditInfo(schoolid);
			// Fetch the account by school ID

			Integer bal=smsCreditInfo.getTotalCredit()-smsCreditInfo.getSmsCount()-totalMessages;
			if(bal<0) {
			return 0;	
			}
			}


			//Integer update=smsSchoolDebitRepo.updateBalance(totalMessages);

			
//			List<Integer> alreadyExist= announcementApprovalStatusRepository.getApprovedStatus(studentAnnouncementBean.getAnnouncementId(),studentAnnouncementBean.getSchoolId());
	//
//			if(!CollectionUtils.isEmpty(alreadyExist)) {
			AnnouncementApprovalStatusModel announcementApprovalStatusModel = new AnnouncementApprovalStatusModel();
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(studentAnnouncementBean.getAnnouncementId());

			AppUserModel appUserModel = new AppUserModel();
			appUserModel.setAppUserRoleId(studentAnnouncementBean.getAppuserId());

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());

			announcementApprovalStatusModel.setAnnouncementModel(announcementModel);
			announcementApprovalStatusModel.setAppUserModel(appUserModel);
			announcementApprovalStatusModel.setSchoolMasterModel(schoolMasterModel);
			announcementApprovalStatusModel.setStatus(studentAnnouncementBean.getStatus());
			announcementApprovalStatusModel.setRemark(studentAnnouncementBean.getRemark());
			Integer id=announcementApprovalStatusRepository.save(announcementApprovalStatusModel).getAnnouncementapprovalStatusId();
			

			announcement.updateFlah(studentAnnouncementBean.getAnnouncementId());
			return id;
			
			
			
			
			
		}
	//	}
		//			List<AnnouncementApprovalResponseBean> getApprovalStatusList = getApprovalStatusOfAnnouncement(
		//					studentAnnouncementBean.getSchoolId(), studentAnnouncementBean.getAnnouncementId());

		//			List<AnnouncementApprovalResponseBean> approvalList = getApprovalStatusList.parallelStream()
		//					.filter(g -> g.getStatus().equalsIgnoreCase("0")).collect(Collectors.toList());
		//			if (approvalList.size() == 0) {
		//				return studentAnnouncementBean.getAnnouncementId();
		//			} else {
		//				return 1;
		//			}
	//	return 1;

	}


	@Override
	public Optional<SentEmailModel> getEmailDetails(String rcode) {
		
		Optional<SentEmailModel> messageDetails=sentEmailRepository.findByRcode(rcode);
		return messageDetails;
	}


	@Override
	public Integer recordEmailReadStatus(SentEmailReadStatusModel sentEmailReadStatus) {
		Integer id=sentEmailReadStatusRepository.save(sentEmailReadStatus).getSentEmailId();
		return id;
	}


	@Override
	public List<SentMessagesModel> getAllEmails(Integer staffStudentId, Integer flag) {
		String role;
		if(flag==0) {
			role="Student";
		}else {
			role="Staff";
		}

		List<SentMessagesModel> sentMessages= sentEmailRepo.getEmails(staffStudentId,role);

		for (SentMessagesModel sentMessagesModel : sentMessages) {
			Integer sentMessageId=sentMessagesModel.getId();

			List<SentMessagesReadStatusModel> readStatus=sentMessagesReadStatusRepo.findBySentMessageIdOrderBySentMessageIdDesc(sentMessageId);
			sentMessagesModel.setReadStatus(readStatus);
		}

		return sentMessages;
	}
	
	//@Scheduled(cron = "0 0 9 * * *")
//	@Scheduled(cron = "0 */1 * * * ?")
//	public void sendingDailyRutinMsg(){
//		
//		List<AnnouncementModel> ob= announcement.getAllAnouncementForSendingRoutinMsg();
//		
//		for (AnnouncementModel announcementModel : ob) {
//			
//			try {
//			String jsonString="{\"announcementId\":"+announcementModel.getAnnouncementId()+",\"approvalStatusId\":1,\"appuserId\":1253800001,\"postId\":1253800158,\"remark\":\"\",\"schoolId\":1253800001,\"status\":\"approved\"}";
//
//			String responseBody;
//			/*json object is created for rest call*/
//			//String jsonString = jsonObjectCreator(schoolid,phoneNumbers,message,savedIdBean,fcmTokenBean,emailBean,schoolName,otherSchoolId,announcementType);
//			String url = "http://65.2.23.210:8090/submit_announcement_approval_status";
//			/*seeting headers*/
//			HttpHeaders headers = new HttpHeaders();
//			headers.setContentType(MediaType.APPLICATION_JSON);
//			HttpEntity<String> requestEntity = new HttpEntity<>(jsonString, headers);
//			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
//
//			ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
//			responseBody = responseEntity.getBody();
//			}catch(Exception e ) {
//				e.printStackTrace();
//			}
//		}
//	}
	
	@Scheduled(cron = "0 0 9 * * *")
	//@Scheduled(cron = "0 */1 * * * ?")
	public void sendingDailyRutinMsg(){
		
		
		//CompletableFuture<String> future = new CompletableFuture<>();

		CronLogModel cron =new CronLogModel();
		cron.setCronName("sendingDailyRutinMsg");
		cronRepo.save(cron);
		
		CompletableFuture.runAsync(() -> {
			//executor.submit(announcementService.sendNotificationNew(StudentAnnouncementBean));
			sendMessageToStaff();

			//future.complete(result);
		});
	}


	private void sendMessageToStaff() {
		List<AnnouncementModel> ob= announcement.getAllAnouncementForSendingRoutinMsg();
		
		for (AnnouncementModel announcementModel : ob) {
			
			try {
			String jsonString="{\"announcementId\":"+announcementModel.getAnnouncementId()+",\"approvalStatusId\":1,\"appuserId\":1253800001,\"postId\":1253800158,\"remark\":\"\",\"schoolId\":1253800001,\"status\":\"approved\"}";

			String responseBody;
			/*json object is created for rest call*/
			//String jsonString = jsonObjectCreator(schoolid,phoneNumbers,message,savedIdBean,fcmTokenBean,emailBean,schoolName,otherSchoolId,announcementType);
			String url = "http://65.2.23.210:8090/submit_announcement_approval_status";
			/*seeting headers*/
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> requestEntity = new HttpEntity<>(jsonString, headers);
			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

			ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
			responseBody = responseEntity.getBody();
			}catch(Exception e ) {
				e.printStackTrace();
			}
		}
	}

//	@Override
//	public ReportBean getStudentListDeliveryReportForAnnouncement(Integer schoolid, Integer yearId, Integer standardId,
//			Integer divisionId, Integer announcementId, String announcementDate) {
//		List<PhoneNumberBean> phNoFromAssignToTable =studentStandardRenewRepository.getStaffStudentReport( schoolid,  yearId,  standardId,
//				 divisionId,  announcementDate,announcementId);
//
//
//		List<PhoneNumberBean> staffReportBean=new ArrayList<>();
//		List<PhoneNumberBean> studentReportBean=new ArrayList<>();
//
//		ReportBean report =new ReportBean();
//
//		for (PhoneNumberBean reports : phNoFromAssignToTable) {
//				SentMessagesModel sentMessage= sentMessagesRepository.getMessagesForRepot(reports.getStudentId(),"Student",announcementId);
//				if(sentMessage!=null) {
//					List<SentMessagesReadStatusModel>sentMessagesReadStatusModel=sentMessagesReadStatusRepo.findBySentMessageId(sentMessage.getId());
//					reports.setSentMessage(sentMessage);
//					reports.setReadStatus(sentMessagesReadStatusModel);
//				}
//				List<SaveSendNotificationModel> allNotifications = notification.getAllNotificationsForReport(reports.getStudentUserId(), "1",announcementId);
//				reports.setAllNotifications(allNotifications);
//				List<SentEmailModel> allEmails = sentEmailRepo.getAllEmails(reports.getStudentUserId(), "Student",announcementId);
//				reports.setSentEmail(allEmails);
//				studentReportBean.add(reports);
//		}
//
//		report.setStaffReportBean(staffReportBean);
//		report.setStudentReportBean(studentReportBean);
//		System.out.println("");
//		return report;
//	}
	
	@Override
	public ReportBean getStudentListDeliveryReportForAnnouncement(Integer schoolid, Integer yearId, Integer standardId,
			Integer divisionId, Integer announcementId, String announcementDate) {
		List<PhoneNumberBean> phNoFromAssignToTable =studentStandardRenewRepository.getStaffStudentReport( schoolid,  yearId,  standardId,
				 divisionId,  announcementDate,announcementId);

		List<Integer> studentIds=phNoFromAssignToTable
				.stream()
				.map(PhoneNumberBean::getStudentId)
				.collect(Collectors.toList());

		List<PhoneNumberBean> studentReportBean=new ArrayList<>();
		ReportBean report =new ReportBean();

				List<SentMessagesModel> sentMessages= sentMessagesRepository.getMessagesForRepot(studentIds,"Student",announcementId);
				for (PhoneNumberBean reports : phNoFromAssignToTable) {
					for (SentMessagesModel sentMessage : sentMessages) {
						
						if(reports.getStudentId().equals(sentMessage.getStaffStudentId())) {
						if(sentMessage!=null) {
							List<SentMessagesReadStatusModel>sentMessagesReadStatusModel=sentMessagesReadStatusRepo.findBySentMessageId(sentMessage.getId());
							reports.setSentMessage(sentMessage);
							reports.setReadStatus(sentMessagesReadStatusModel);
						}
						List<SaveSendNotificationModel> allNotifications = notification.getAllNotificationsForReport(reports.getStudentUserId(), "1",announcementId);
						reports.setAllNotifications(allNotifications);
						List<SentEmailModel> allEmails = sentEmailRepo.getAllEmails(reports.getStudentUserId(), "Student",announcementId);
						reports.setSentEmail(allEmails);
						studentReportBean.add(reports);
						}
					}
				}

		//report.setStaffReportBean(staffReportBean);
		report.setStudentReportBean(studentReportBean);
		System.out.println("");
		return report;
	}

	
}
