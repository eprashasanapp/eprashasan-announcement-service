package com.ingenio.announcement.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ingenio.announcement.Constants.ApplicationConstants;
import com.ingenio.announcement.bean.AnnnouncementLikeOrDislikeBean;
import com.ingenio.announcement.bean.AnnoucementLabelBean;
import com.ingenio.announcement.bean.AnnouncementApprovalResponseBean;
import com.ingenio.announcement.bean.AnnouncementComentBean;
import com.ingenio.announcement.bean.AnnouncementCommentResponseBean;
import com.ingenio.announcement.bean.AnnouncementLikeResponseBean;
import com.ingenio.announcement.bean.AnnouncementOptionOfQuestionBean;
import com.ingenio.announcement.bean.AnnouncementQuestionAnswersBean;
import com.ingenio.announcement.bean.AnnouncementQuestionOptionBean;
import com.ingenio.announcement.bean.AnnouncementReactionBean;
import com.ingenio.announcement.bean.AnnouncementRequestBean;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementUserToGroupBean;
import com.ingenio.announcement.bean.AnnouncementViewBean;
import com.ingenio.announcement.bean.AnnouncementViewResponseBean;
import com.ingenio.announcement.bean.FCMTokenBean;
import com.ingenio.announcement.bean.FeedbackData;
import com.ingenio.announcement.bean.FeedbackEmailRequestBean;
import com.ingenio.announcement.bean.GroupBean;
import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.StaffDetailsBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.StudentStandardRenewBean;
import com.ingenio.announcement.bean.SubmitApprovalBean;
import com.ingenio.announcement.bean.Tokenbean;
import com.ingenio.announcement.bean.UploadFileBean;
import com.ingenio.announcement.bean.WhatsAppMessageBean;
import com.ingenio.announcement.config.ConfigurationProperties;
import com.ingenio.announcement.model.AndroidNotificationStatusModel;
import com.ingenio.announcement.model.AnnouncementApprovalStatusModel;
import com.ingenio.announcement.model.AnnouncementAssignToClassDivisionModel;
import com.ingenio.announcement.model.AnnouncementAssignToModel;
import com.ingenio.announcement.model.AnnouncementAttachmentModel;
import com.ingenio.announcement.model.AnnouncementCommentModel;
import com.ingenio.announcement.model.AnnouncementGroupModel;
import com.ingenio.announcement.model.AnnouncementLikeorDislikeModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AnnouncementReactionModel;
import com.ingenio.announcement.model.AnnouncementTypeModel;
import com.ingenio.announcement.model.AnnouncementUserToGroupMappingModel;
import com.ingenio.announcement.model.AnnouncementViewsModel;
import com.ingenio.announcement.model.AnnouncmenetApprovalAuthorityModel;
import com.ingenio.announcement.model.AnnouncmenetForGroupModel;
import com.ingenio.announcement.model.AnnouncmenetQuestionsModel;
import com.ingenio.announcement.model.AnnouncmentOptionsOfQuestionsModel;
import com.ingenio.announcement.model.AppLanguageModel;
import com.ingenio.announcement.model.AppUserModel;
import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.FeeSendSmsNumbersMstrModel;
import com.ingenio.announcement.model.FeeSmsHistoryModel;
import com.ingenio.announcement.model.GlobalFcmModel;
import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SaveSendWhatsAppSMSModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.SchoolWiseFcmModel;
import com.ingenio.announcement.model.SchoolWiseWhatsAppSMSModel;
import com.ingenio.announcement.model.SmsMessageMasterModel;
import com.ingenio.announcement.model.StaffBasicDetailsModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.StudentMasterModel;
import com.ingenio.announcement.model.StudentStandardRenewModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AndroidConfigurationRepository;
import com.ingenio.announcement.repository.AndroidFcmTokenRepository;
import com.ingenio.announcement.repository.AndroidNotificationStatusRepository;
import com.ingenio.announcement.repository.AnnouncementApprovalAuthorityRepository;
import com.ingenio.announcement.repository.AnnouncementApprovalAuthorityStatusRepository;
import com.ingenio.announcement.repository.AnnouncementApprovalStatusRepository;
import com.ingenio.announcement.repository.AnnouncementAssignToClassDivisionRepository;
import com.ingenio.announcement.repository.AnnouncementAttachmentRepository;
import com.ingenio.announcement.repository.AnnouncementCommentRepository;
import com.ingenio.announcement.repository.AnnouncementForGroupRepository;
import com.ingenio.announcement.repository.AnnouncementGroupRepository;
import com.ingenio.announcement.repository.AnnouncementLikeOrDislikeRepository;
import com.ingenio.announcement.repository.AnnouncementOptionsForQuestionsRepository;
import com.ingenio.announcement.repository.AnnouncementQuestionRepository;
import com.ingenio.announcement.repository.AnnouncementReactionRepository;
import com.ingenio.announcement.repository.AnnouncementRepository;
import com.ingenio.announcement.repository.AnnouncementTypeRepository;
import com.ingenio.announcement.repository.AnnouncementUserToGroupMappingRepository;
import com.ingenio.announcement.repository.AnnouncementViewRepository;
import com.ingenio.announcement.repository.AppUserRepository;
import com.ingenio.announcement.repository.AppUserRoleRepository;
import com.ingenio.announcement.repository.AssignSubjectToStudentRepository;
import com.ingenio.announcement.repository.AttachmentRepository;
import com.ingenio.announcement.repository.DivisionMasterRepository;
import com.ingenio.announcement.repository.GlobalFcmRepository;
import com.ingenio.announcement.repository.GlobalWhatsAppSMSRepository;
import com.ingenio.announcement.repository.IcardMasterRepository;
import com.ingenio.announcement.repository.SaveSendNotificationRepository;
import com.ingenio.announcement.repository.SaveSendWhatsAppSMSRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.SchoolWiseFcmRepositry;
import com.ingenio.announcement.repository.SmsRepository;
import com.ingenio.announcement.repository.StaffBasicDetailRepository;
import com.ingenio.announcement.repository.StandardMasterRepository;
import com.ingenio.announcement.repository.StudentAnnouncementAssignedToRepository;
import com.ingenio.announcement.repository.StudentAnnouncementAttachmentRepository;
import com.ingenio.announcement.repository.StudentMasterRepository;
import com.ingenio.announcement.repository.StudentSmsAssignedToRepository;
import com.ingenio.announcement.repository.StudentSmsMasterRepository;
import com.ingenio.announcement.repository.StudentStandardRenewRepository;
import com.ingenio.announcement.repository.UserAuthenticationRepository;
import com.ingenio.announcement.repository.impl.FeeRepositoryImpl;
import com.ingenio.announcement.service.AnnouncementService;
import com.ingenio.announcement.util.CommonUtlitity;
import com.ingenio.announcement.util.CryptographyUtil;
import com.ingenio.announcement.util.HttpClientUtil;
import com.ingenio.announcement.util.Utility;

@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")

@PropertySource("classpath:messages_en.properties")

@PropertySource("classpath:messages_mr.properties")

@PropertySource("classpath:messages_hn.properties")

public class AnnouncementServiceImpl implements AnnouncementService {
	@Autowired
	AndroidFcmTokenRepository androidFcmTokenRepository;
	@Autowired
	AssignSubjectToStudentRepository assignSubjectToStudentRepository;

	@Autowired
	StudentAnnouncementAttachmentRepository studentAnnouncementAttachmentRepository;
	
	@Autowired
	IcardMasterRepository icardMasterRepository;

	@Autowired
	AnnouncementRepository announcementRepository;

	@Autowired
	AnnouncementApprovalStatusRepository announcementApprovalStatusRepository;
	
	@Autowired
	AndroidConfigurationRepository androidConfigurationRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	AttachmentRepository attachmentRepository;

	@Autowired
	StudentStandardRenewRepository studentStandardRenewRepository;

	@Autowired
	StudentAnnouncementAssignedToRepository studentAnnouncementAssignedToRepository;

	@Autowired
	StaffBasicDetailRepository staffBasicDetailRepository;

	@Autowired
	StudentSmsAssignedToRepository studentSmsAssignedToRepository;

	@Autowired
	UserAuthenticationRepository userAuthenticationRepository;

	@Autowired
	private Environment env;

	@Autowired
	AnnouncementViewRepository announcementViewRepository;

	@Autowired
	AnnouncementLikeOrDislikeRepository likeDislikeRepo;

	@Autowired
	SchoolMasterRepository schoolRepo;

	@Autowired
	AnnouncementCommentRepository commentRepo;

	@Autowired
	AnnouncementReactionRepository reactionRepo;
	
	@Autowired
	SaveSendNotificationRepository saveSendNotificationRepository;

	CryptographyUtil cryptoUtil = new CryptographyUtil();
	Integer smssaveId = 0;

	@Autowired
	private AndroidNotificationStatusRepository androidNotificationStatusRepository;

	@Autowired
	private FeeRepositoryImpl feeRepositoryImpl;

	@Autowired
	StudentSmsMasterRepository studentSmsMasterRepository;

	@Autowired
	SmsRepository smsRepository;

//	@Autowired
//	private RestTemplate restTemplate;

	@Autowired
	CommonUtlitity commonUtlitity;

	@Autowired
	AmazonS3 amazonS3;

	@Autowired
	AnnouncementForGroupRepository announcementForGroupRepository;

	@Autowired
	AnnouncementTypeRepository announcementTypeRepository;

	@Autowired
	AnnouncementGroupRepository announcementGroupRepository;

	@Autowired
	AnnouncementOptionsForQuestionsRepository announcementOptionsForQuestionsRepository;

	@Autowired
	AnnouncementQuestionRepository announcementQuestionRepository;

	CryptographyUtil cry = new CryptographyUtil();

	@Autowired
	GlobalWhatsAppSMSRepository globalWhatsAppSMSRepository;

	@Autowired
	SaveSendWhatsAppSMSRepository saveSendWhatsAppSMSRepository;

	@Autowired
	AnnouncementAttachmentRepository announcementAttachmentRepository;

	@Autowired
	AppUserRepository appUserRepository;
	
	@Autowired
	AppUserRoleRepository appUserRoleRepository;

	@Autowired
	StudentMasterRepository studentMasterRepository;

	@Autowired
	AnnouncementAssignToClassDivisionRepository announcementAssignToClassDivisionRepository;

	@Autowired
	StandardMasterRepository standardMasterRepository;

	@Autowired
	DivisionMasterRepository divisionMasterRepository;

	@Autowired
	AnnouncementApprovalAuthorityRepository announcementApprovalAuthorityRepository;

	@Autowired
	AnnouncementApprovalAuthorityStatusRepository announcementApprovalAuthorityStatusRepository;

	@Autowired
	AnnouncementUserToGroupMappingRepository userToGoupMappingRepo;
	
	@Autowired
	SchoolWiseFcmRepositry schoolWiseFcmRepositry;
	
	@Autowired
	GlobalFcmRepository globalFcmRepository;
	
	RestTemplate resttemplate=new RestTemplate();

	@Override
	public synchronized Integer saveAnnouncement(AnnouncementRequestBean studentAnnouncementBean, Integer isWhatsAppMsg,
			Integer isDueFeeRemainder) {

		StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
		staffBasicDetailsModel.setstaffId(studentAnnouncementBean.getStaffId());

		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(studentAnnouncementBean.getYearId());

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());

		String announcementId = "0";

		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentAnnouncementBean.getSchoolId());
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

		announcementId = "" + studentAnnouncementBean.getAnnouncementId();
		if (StringUtils.isEmpty(announcementId) || announcementId.equals("0")) {
			String incrementedId = "" + announcementRepository.getMaxId(studentAnnouncementBean.getSchoolId());
			announcementId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
		}

		
		AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(studentAnnouncementBean.getUserId());
		AnnouncementModel announcementModel = new AnnouncementModel();
		AnnouncementTypeModel announcementTypeModel = new AnnouncementTypeModel();
		if (studentAnnouncementBean.getAnnouncement_type_id() != 0) {
			//announcementTypeModel.setAnnouncementTypeId(null);
			announcementTypeModel.setAnnouncementTypeId(studentAnnouncementBean.getAnnouncement_type_id());
			announcementTypeModel.setSchoolMasterModel(schoolMasterModel);	
			announcementModel.setAnnouncementTypeModel(announcementTypeModel);
		} 
		

		
		announcementModel.setUserId(appUserRoleModel);
		announcementModel.setIpAddress(studentAnnouncementBean.getIPAddress());
		announcementModel.setDeviceType(studentAnnouncementBean.getDeviceType());
		announcementModel.setAnnouncementTitle(studentAnnouncementBean.getAnnouncementTitle());
		announcementModel.setAnnouncement(studentAnnouncementBean.getAnnouncement());
		announcementModel.setStartDate(studentAnnouncementBean.getStartDate());
		announcementModel.setSchoolMasterModel(schoolMasterModel);
		announcementModel.setStaffId(studentAnnouncementBean.getStaffId());
		announcementModel.setIsWhatsappMsg(isWhatsAppMsg);
		announcementModel.setIsDueFeeRemainder(isDueFeeRemainder);
		
		announcementModel.setIsSanstha(studentAnnouncementBean.getIsSanstha());
		announcementModel.setSansthakey(studentAnnouncementBean.getSansthakey());
		announcementModel.setForPrincipal(studentAnnouncementBean.getForPrincipal());
		announcementModel.setForTeacher(studentAnnouncementBean.getForTeacher());
		announcementModel.setForstudent(studentAnnouncementBean.getForstudent());
		
		if (StringUtils.isNotEmpty(announcementId) && !announcementId.equals("0")) {
			announcementModel.setAnnouncementId(Integer.parseInt(announcementId));
		}
		announcementModel.setPublishDate(null);
		announcementModel.setIsFeedback(studentAnnouncementBean.getFeedback_status_flag());
		announcementModel.setUserCanComment(studentAnnouncementBean.getUserCanComment());
		announcementModel.setPublishFlag(studentAnnouncementBean.getPublishFlag());
		announcementModel.setSendForApproval(0);
		announcementModel.setPriority(studentAnnouncementBean.getPriority()!=null ?studentAnnouncementBean.getPriority():0);
		if(studentAnnouncementBean.getOtherSchoolId()!=null) {
		announcementModel.setOtherSchoolId(studentAnnouncementBean.getOtherSchoolId());
		}else {
			announcementModel.setOtherSchoolId(0);
		}
		announcementModel.setAssignDate(studentAnnouncementBean.getAssignDate());
		announcementModel.setDeadline(studentAnnouncementBean.getDeadline());
		announcementModel.setActuallyCompletionDate(studentAnnouncementBean.getActuallyCompletionDate());
		announcementModel.setComplaintBy(studentAnnouncementBean.getComplaintBy());
		announcementModel.setCompletionStatus(studentAnnouncementBean.getCompletionStatus());
		announcementModel.setWhatAppSendingFlag(studentAnnouncementBean.getWhatAppSendingFlag());
		
		return announcementRepository.save(announcementModel).getAnnouncementId();
	}

	@Override
	public void postAnnouncementForMembers(AnnouncementRequestBean studentAnnouncementBean, Integer announcementId) {
		StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
		staffBasicDetailsModel.setstaffId(studentAnnouncementBean.getStaffId());

		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(studentAnnouncementBean.getYearId());

		AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(studentAnnouncementBean.getUserId());

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentAnnouncementBean.getSchoolId());
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

		if (studentAnnouncementBean.getApiFlag() != null && studentAnnouncementBean.getApiFlag() == 0) {
			sendAnnouncement(studentAnnouncementBean, yearMasterModel, schoolMasterModel, appUserRoleModel,
					"" + announcementId, globalDbSchoolKey);
		}
		if (studentAnnouncementBean.getApiFlag() != null && studentAnnouncementBean.getApiFlag() == 1) {
			sendSms(studentAnnouncementBean, yearMasterModel, schoolMasterModel, globalDbSchoolKey, "" + announcementId,
					appUserRoleModel);
		}

	}

	@SuppressWarnings("static-access")
	private Integer sendSms(AnnouncementRequestBean studentAnnouncementBean, YearMasterModel yearMasterModel,
			SchoolMasterModel schoolMasterModel, String globalDbSchoolKey, String announcementId,
			AppUserRoleModel appUserRoleModel) {

		SmsMessageMasterModel smsMessageMasterModel = new SmsMessageMasterModel();
		smsMessageMasterModel.setAppUserRoleModel(appUserRoleModel);
		smsMessageMasterModel.setIpAddress(studentAnnouncementBean.getIPAddress());
		smsMessageMasterModel.setDeviceType(studentAnnouncementBean.getDeviceType());
		smsMessageMasterModel.setMessage(studentAnnouncementBean.getSmsMessage());
		smsMessageMasterModel.setSchoolMasterModel(schoolMasterModel);
		String msgMasterNo = "" + studentSmsMasterRepository.getMaxId(studentAnnouncementBean.getSchoolId());
		String sendsmsid1 = Utility.generatePrimaryKey1(globalDbSchoolKey, msgMasterNo);
		smsMessageMasterModel.setSms_msg_master_id(Long.parseLong(sendsmsid1));
		long smsMasterId = studentSmsMasterRepository.save(smsMessageMasterModel).getSms_msg_master_id();

		FeeSmsHistoryModel feeSmsHistoryModel = new FeeSmsHistoryModel();

		feeSmsHistoryModel.setAppUserRoleModel(appUserRoleModel);
		feeSmsHistoryModel.setApprovalBy(appUserRoleModel);
		feeSmsHistoryModel.setAppUserRoleModel(appUserRoleModel);
		feeSmsHistoryModel.setIpAddress(studentAnnouncementBean.getIPAddress());
		feeSmsHistoryModel.setDeviceType(studentAnnouncementBean.getDeviceType());
		feeSmsHistoryModel.setMessage(smsMasterId);
		feeSmsHistoryModel.setSchoolMasterModel(schoolMasterModel);
		if (CollectionUtils.isNotEmpty(studentAnnouncementBean.getAnnoucementResponseList())) {
			studentAnnouncementBean.getAnnoucementResponseList().stream().forEach(responseBean -> {
				responseBean.getDisplayList().stream().forEach(labelBean -> {
					if (responseBean.getRoleName().equals("Teacher")) {
						Date date = new Date();
						String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
						List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
								.findStaff(studentAnnouncementBean.getSchoolId(), labelBean.getId(), currentDate);
						if (CollectionUtils.isNotEmpty(staffList)) {
							staffList.stream().forEach(staff -> {
								feeSmsHistoryModel.setStaffBasicDetailsModel(staff);
								feeSmsHistoryModel.setStudentStandardRenewModel(null);
								try {
									String feeSMSId = "" + studentSmsAssignedToRepository
											.getMaxId(studentAnnouncementBean.getSchoolId());
									String sendsmsid = Utility.generatePrimaryKey(globalDbSchoolKey, feeSMSId);
									feeSmsHistoryModel.setSendSmsId(Integer.parseInt(sendsmsid));
									smssaveId = studentSmsAssignedToRepository.saveAndFlush(feeSmsHistoryModel)
											.getSendSmsId();
								} catch (Exception e) {
									e.printStackTrace();
								}
//								if(smssaveId!=0 && smssaveId!=null){
//						         sendOtpMessage(staff.getContactno(),studentAnnouncementBean.getSmsMessage(),studentAnnouncementBean.getSchoolId(,smssaveId));}	 
							});
						}
					} else if (responseBean.getRoleName().equals("Parent")) {
						String standardId = labelBean.getId().split("-")[0];
						String divisionId = labelBean.getId().split("-")[1];
						List<StudentStandardRenewModel> studentList = studentStandardRenewRepository.findStudent(
								studentAnnouncementBean.getYearId(), Integer.parseInt(standardId),
								Integer.parseInt(divisionId), studentAnnouncementBean.getSchoolId());
						if (CollectionUtils.isNotEmpty(studentList)) {
							studentList.stream().forEach(student -> {
								feeSmsHistoryModel.setStudentStandardRenewModel(student);
								feeSmsHistoryModel.setStaffBasicDetailsModel(null);
								try {
									String feeSMSId = "" + studentSmsAssignedToRepository
											.getMaxId(studentAnnouncementBean.getSchoolId());
									String sendsmsid = Utility.generatePrimaryKey(globalDbSchoolKey, feeSMSId);
									feeSmsHistoryModel.setSendSmsId(Integer.parseInt(sendsmsid));
									smssaveId = studentSmsAssignedToRepository.saveAndFlush(feeSmsHistoryModel)
											.getSendSmsId();
								} catch (Exception e) {
									e.printStackTrace();
								}
//								if(smssaveId!=0 && smssaveId!=null){
//									try {
//							         sendOtpMessage(student.getStudentMasterModel().getMmobile(),studentAnnouncementBean.getSmsMessage(),studentAnnouncementBean.getSchoolId(),smssaveId);}	
//								catch(Exception e){
//									e.printStackTrace();}}	
							});

						}
					} else if (responseBean.getRoleName().equals("Student")) {
//						String standardId = labelBean.getId().split("-")[0];
//						String divisionId = labelBean.getId().split("-")[1];
						List<Integer> studentRenewId = Arrays
								.stream(((StringUtils) studentAnnouncementBean.getSendingList()).split(","))
								.map(Integer::parseInt).collect(Collectors.toList());
						if (CollectionUtils.isNotEmpty(studentRenewId)) {
							studentRenewId.stream().forEach(student -> {
								StudentStandardRenewModel studentRenewModel = new StudentStandardRenewModel();
								studentRenewModel.setRenewStudentId(student);
								feeSmsHistoryModel.setStudentStandardRenewModel(studentRenewModel);
								feeSmsHistoryModel.setStaffBasicDetailsModel(null);
								try {
									String feeSMSId = "" + studentSmsAssignedToRepository
											.getMaxId(studentAnnouncementBean.getSchoolId());
									String sendsmsid = Utility.generatePrimaryKey(globalDbSchoolKey, feeSMSId);
									feeSmsHistoryModel.setSendSmsId(Integer.parseInt(sendsmsid));
									smssaveId = studentSmsAssignedToRepository.saveAndFlush(feeSmsHistoryModel)
											.getSendSmsId();
								} catch (Exception e) {
									e.printStackTrace();
								}
//								if(smssaveId!=0 && smssaveId!=null){
//									try {
//							         sendOtpMessage(student.getStudentMasterModel().getMmobile(),studentAnnouncementBean.getSmsMessage(),studentAnnouncementBean.getSchoolId(),smssaveId);}	
//								catch(Exception e){
//									e.printStackTrace();}}	
							});

						}

					}
				});
			});
		}
		return smssaveId;
	}

	private void sendAnnouncement(AnnouncementRequestBean studentAnnouncementBean, YearMasterModel yearMasterModel,
			SchoolMasterModel schoolMasterModel, AppUserRoleModel appUserRoleModel, String announcementId,
			String globalDbSchoolKey) {

		try {
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(Integer.parseInt(announcementId));
			if (StringUtils.isNotEmpty("" + studentAnnouncementBean.getAnnouncementId())
					&& !("" + studentAnnouncementBean.getAnnouncementId()).equals("0")) {
				studentAnnouncementAssignedToRepository.deleteByAnnouncementModel(announcementModel);
				//announcementForGroupRepository.deleteByAnnouncementModel(announcementModel);
			}

			if (CollectionUtils.isNotEmpty(studentAnnouncementBean.getAnnoucementResponseList())) {
				for (int i = 0; i < studentAnnouncementBean.getAnnoucementResponseList().size(); i++) {
					AnnouncementResponseBean announceemntResponseBean = studentAnnouncementBean
							.getAnnoucementResponseList().get(i);
					if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Parent")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							List<Integer> divisionNames = Arrays
									.stream(announceemntResponseBean.getSendingList().split(",")).map(Integer::parseInt)
									.collect(Collectors.toList());
							saveClassDivisionInfo(divisionNames, studentAnnouncementBean.getYearId(),
									studentAnnouncementBean.getSchoolId(), Integer.parseInt(announcementId), 0);
//							List<StudentStandardRenewBean> studentList = studentStandardRenewRepository
//									.findStudentbydivision(studentAnnouncementBean.getYearId(),
//											studentAnnouncementBean.getSchoolId(), divisionNames);
//							List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
//							if (CollectionUtils.isNotEmpty(studentList)) {
//								for (int s = 0; s < studentList.size(); s++) {
//									StudentStandardRenewBean student = studentList.get(s);
//									AnnouncementAssignToModel AnnouncementAssignToModel = new AnnouncementAssignToModel();
//									AnnouncementAssignToModel.setRole("Parent");
//									AnnouncementAssignToModel.setStaffStudentId(student.getRenewStudentId());
//									AnnouncementAssignToModel.setAnnouncementStatus("0");
//									AnnouncementAssignToModel.setSchoolMasterModel(schoolMasterModel);
//
//									if(yearMasterModel.getYearId()!=null)
//									AnnouncementAssignToModel.setYearMasterModel(yearMasterModel);
//									AnnouncementAssignToModel.setAnnouncementModel(announcementModel);
//									AnnouncementAssignToModel.setUserId(appUserRoleModel);
////					  			     AnnouncementAssignToModel.setAnnouncementAssignToId(Integer.parseInt(assignedToAnnouncementId)+idIndex);
//								
//									if(student.getDivisionId()!=0) {
//									DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
//									divisionMasterModel.setDivisionId(student.getDivisionId());
//									AnnouncementAssignToModel.setDivisionMasterModel(divisionMasterModel);
//									}
//									StandardMasterModel standardMasterModel = new StandardMasterModel();
//									standardMasterModel.setStandardId(student.getStandardId());
//									AnnouncementAssignToModel.setStandardMasterModel(standardMasterModel);
//									announcementModelList.add(AnnouncementAssignToModel);
////						  			idIndex++;
//									studentAnnouncementAssignedToRepository.save(AnnouncementAssignToModel);
//								}
//
//								//studentAnnouncementAssignedToRepository.saveAll(announcementModelList);
//							}
						}

					} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Teacher")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							Date date = new Date();
							String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
							List<String> roleNames = Arrays
									.asList(announceemntResponseBean.getSendingList().split(","));
							List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
									.findStaff(studentAnnouncementBean.getSchoolId(), roleNames, currentDate);
							List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
							for (int s = 0; s < staffList.size(); s++) {
								StaffBasicDetailsModel staff = staffList.get(s);
								AnnouncementAssignToModel AnnouncementAssignToModel = new AnnouncementAssignToModel();
								AnnouncementAssignToModel.setRole("Teacher");
								AnnouncementAssignToModel.setStaffStudentId(staff.getstaffId());
								AnnouncementAssignToModel.setAnnouncementStatus("0");
								AnnouncementAssignToModel.setSchoolMasterModel(schoolMasterModel);
							
								if(yearMasterModel.getYearId()!=null)
								AnnouncementAssignToModel.setYearMasterModel(yearMasterModel);
								AnnouncementAssignToModel.setAnnouncementModel(announcementModel);
//								AnnouncementAssignToModel.setAnnouncementAssignToId(Integer.parseInt(assignedToAnnouncementId)+idIndex);
								announcementModelList.add(AnnouncementAssignToModel);
							}

							studentAnnouncementAssignedToRepository.saveAll(announcementModelList);
						}
//						idIndex++;
					} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Student")) {
						
						System.out.println("hello");
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							List<Integer> studentRenewId = Arrays
									.stream(announceemntResponseBean.getSendingList().split(",")).map(Integer::parseInt)
									.collect(Collectors.toList());
							System.out.println(studentRenewId);
							List<StudentStandardRenewBean> studentList = studentStandardRenewRepository
									.findStudentbyRenewId(studentAnnouncementBean.getSchoolId(), studentRenewId);
							System.out.println(studentList);
							List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
							if (CollectionUtils.isNotEmpty(studentList)) {
								for (int s = 0; s < studentList.size(); s++) {
									StudentStandardRenewBean student = studentList.get(s);
									AnnouncementAssignToModel AnnouncementAssignToModel = new AnnouncementAssignToModel();
									AnnouncementAssignToModel.setRole("Student");
									AnnouncementAssignToModel.setStaffStudentId(student.getRenewStudentId());
									AnnouncementAssignToModel.setAnnouncementStatus("0");
									AnnouncementAssignToModel.setSchoolMasterModel(schoolMasterModel);
									
									if(yearMasterModel.getYearId()!=null)
									AnnouncementAssignToModel.setYearMasterModel(yearMasterModel);
									
									AnnouncementAssignToModel.setAnnouncementModel(announcementModel);
									AnnouncementAssignToModel.setUserId(appUserRoleModel);
//					  			     AnnouncementAssignToModel.setAnnouncementAssignToId(Integer.parseInt(assignedToAnnouncementId)+idIndex);
									if(student.getDivisionId()!=0) {
									DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
									divisionMasterModel.setDivisionId(student.getDivisionId());
									AnnouncementAssignToModel.setDivisionMasterModel(divisionMasterModel);
									}
									StandardMasterModel standardMasterModel = new StandardMasterModel();
									standardMasterModel.setStandardId(student.getStandardId());
									AnnouncementAssignToModel.setStandardMasterModel(standardMasterModel);
									announcementModelList.add(AnnouncementAssignToModel);
//						  			idIndex++;
								}
                                System.out.println(announcementModelList);
								studentAnnouncementAssignedToRepository.saveAll(announcementModelList);
							}
						}
					} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Teacher1")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							Date date = new Date();
							String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
							List<Integer> staffId = Arrays.stream(announceemntResponseBean.getSendingList().split(","))
									.map(Integer::parseInt).collect(Collectors.toList());
							List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
									.findStaff(studentAnnouncementBean.getSchoolId(), currentDate, staffId);
							List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
							for (int s = 0; s < staffList.size(); s++) {
								StaffBasicDetailsModel staff = staffList.get(s);
								AnnouncementAssignToModel AnnouncementAssignToModel = new AnnouncementAssignToModel();
								AnnouncementAssignToModel.setRole("Teacher");
								AnnouncementAssignToModel.setStaffStudentId(staff.getstaffId());
								AnnouncementAssignToModel.setAnnouncementStatus("0");
								AnnouncementAssignToModel.setSchoolMasterModel(schoolMasterModel);

								if(yearMasterModel.getYearId()!=null)
								AnnouncementAssignToModel.setYearMasterModel(yearMasterModel);
								AnnouncementAssignToModel.setAnnouncementModel(announcementModel);
//								AnnouncementAssignToModel.setAnnouncementAssignToId(Integer.parseInt(assignedToAnnouncementId)+idIndex);
								announcementModelList.add(AnnouncementAssignToModel);
							}

							studentAnnouncementAssignedToRepository.saveAll(announcementModelList);
//						idIndex++;
						}
					} 
					else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("GroupList")) {
						
						if(!announceemntResponseBean.getSendingList().equals("")) {
						Date date = new Date();
						String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
						List<String> groupIds = Arrays.asList(announceemntResponseBean.getSendingList().split(","));
						List<AnnouncmenetForGroupModel> announcementModelList = new ArrayList<>();
						groupIds.parallelStream().forEach(groupId -> {
							AnnouncmenetForGroupModel announcmenetForGroupModel = new AnnouncmenetForGroupModel();
							AnnouncementGroupModel announcementGroupModel = new AnnouncementGroupModel();
							announcementGroupModel.setAnnouncementGroupId(Integer.parseInt(groupId));
							announcmenetForGroupModel.setAnnouncementGroupModel(announcementGroupModel);
							announcmenetForGroupModel.setAnnouncementModel(announcementModel);
							announcementModelList.add(announcmenetForGroupModel);
						});
						announcementForGroupRepository.saveAll(announcementModelList);
						}
					} 
					else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("DivisionList")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							List<Integer> divisionNames = Arrays
									.stream(announceemntResponseBean.getSendingList().split(",")).map(Integer::parseInt)
									.collect(Collectors.toList());
							saveClassDivisionInfo(divisionNames, studentAnnouncementBean.getYearId(),
									studentAnnouncementBean.getSchoolId(), Integer.parseInt(announcementId), 1);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void saveClassDivisionInfo(List<Integer> divisionNames, Integer yearId, Integer schoolId,
			int announcementId, int selectionFlag) {
		// TODO Auto-generated method stub
		try {
			List<DivisionMasterModel> divisionStandardList = studentMasterRepository.getStandardList(divisionNames,
					schoolId);
			if (divisionStandardList.size() > 0) {
				divisionStandardList.parallelStream().forEach(divisionList -> {
					AnnouncementAssignToClassDivisionModel announcementAssignToClassDivisionModel = new AnnouncementAssignToClassDivisionModel();
					YearMasterModel yearModel = new YearMasterModel();
					yearModel.setYearId(yearId);
					announcementAssignToClassDivisionModel.setYearMasterModel(yearModel);

					SchoolMasterModel schoolMaster = new SchoolMasterModel();
					schoolMaster.setSchoolid(schoolId);
					announcementAssignToClassDivisionModel.setSchoolMasterModel(schoolMaster);

					AnnouncementModel announcementModel = new AnnouncementModel();
					announcementModel.setAnnouncementId(announcementId);
					announcementAssignToClassDivisionModel.setAnnouncementModel(announcementModel);

					announcementAssignToClassDivisionModel
							.setStandardMasterModel(divisionList.getStandardMasterModel());

					announcementAssignToClassDivisionModel.setSelectionFlag(selectionFlag);

					announcementAssignToClassDivisionModel.setDivisionMasterModel(divisionList);
					announcementAssignToClassDivisionRepository.saveAndFlush(announcementAssignToClassDivisionModel);
				});
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void sendNotification(AnnouncementRequestBean studentAnnouncementBean, Integer announcementId) {
		try {
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());
			SchoolMasterModel schoolModel = schoolMasterRepository
					.findBySchoolid(studentAnnouncementBean.getSchoolId());
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
//			List<String> androidTokenList = new ArrayList<>();
			WhatsAppMessageBean tokenList = new WhatsAppMessageBean();
			String serverkey = "";
			List<SchoolWiseFcmModel> schoolWiseList = schoolWiseFcmRepositry.findBySchoolId(studentAnnouncementBean.getSchoolId());
			if(schoolWiseList.size() >0) {
				serverkey=schoolWiseList.get(0).getFcm_key();
			}else {
				List<GlobalFcmModel> globalFcmModel = globalFcmRepository.findAll();
				serverkey = globalFcmModel.get(0).getFcm_key();
			}
			
			tokenList.setServerKey(serverkey);
			List<FCMTokenBean> Fcmtoken = new ArrayList<>();
//			List<String> IOSTokenList = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(studentAnnouncementBean.getAnnoucementResponseList())) {
				for (int i = 0; i < studentAnnouncementBean.getAnnoucementResponseList().size(); i++) {
					AnnouncementResponseBean announceemntResponseBean = studentAnnouncementBean
							.getAnnoucementResponseList().get(i);
					if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Parent")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							List<Integer> divisionNames = Arrays
									.stream(announceemntResponseBean.getSendingList().split(",")).map(Integer::parseInt)
									.collect(Collectors.toList());
							List<StudentStandardRenewBean> studentList = studentStandardRenewRepository
									.findStudentbydivision(studentAnnouncementBean.getYearId(),
											studentAnnouncementBean.getSchoolId(), divisionNames);
							if (CollectionUtils.isNotEmpty(studentList)) {
								for (int s = 0; s < studentList.size(); s++) {
									StudentStandardRenewBean student = studentList.get(s);
									FCMTokenBean fcmTokenBean = new FCMTokenBean();
									List<FCMTokenBean> tokenList1 = assignSubjectToStudentRepository
											.getTokenList(student.getRenewStudentId(), "2");
//									Integer userId = appUserRepository.
									if (tokenList1.size() > 0) {
										if (!tokenList1.get(0).getToken().equalsIgnoreCase(null)
												&& !tokenList1.get(0).getToken().equalsIgnoreCase("")) {
											fcmTokenBean.setMenuId(7);
											fcmTokenBean.setMenuName("Notices");
											fcmTokenBean.setToken(tokenList1.get(0).getToken());
											fcmTokenBean.setUserId(tokenList1.get(0).getUserId());
											fcmTokenBean.setMobileNo(student.getMobileNo());
											fcmTokenBean.setMessageType("1");
											fcmTokenBean.setRenewStaffId(student.getRenewStudentId());
											fcmTokenBean.setTypeFlag("1");
											fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
											fcmTokenBean.setNotificationId(announcementId);
											Fcmtoken.add(fcmTokenBean);
										}

									} else {
										List<FCMTokenBean> tokenList11 = assignSubjectToStudentRepository
												.getTokenList(student.getRenewStudentId(), "3");
										System.out.println("$$$$$$$$$$ " + tokenList11.size());

										if (tokenList11.size() > 0) {
											fcmTokenBean.setMenuId(7);
											fcmTokenBean.setMenuName("Notices");
											fcmTokenBean.setToken(tokenList11.get(0).getToken());
											fcmTokenBean.setUserId(tokenList11.get(0).getUserId());
											fcmTokenBean.setMobileNo(student.getMobileNo());
											fcmTokenBean.setMessageType("1");
											fcmTokenBean.setRenewStaffId(student.getRenewStudentId());
											fcmTokenBean.setTypeFlag("1");
											fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
											fcmTokenBean.setNotificationId(announcementId);
											Fcmtoken.add(fcmTokenBean);
										}
									}
								}
								if (Fcmtoken != null) {
									tokenList.setTokenList(Fcmtoken);
								}

							}
						}
					} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Teacher")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							Date date = new Date();
							String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
							List<String> roleNames = Arrays
									.asList(announceemntResponseBean.getSendingList().split(","));
							List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
									.findStaff(studentAnnouncementBean.getSchoolId(), roleNames, currentDate);
//							List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
							for (int s = 0; s < staffList.size(); s++) {
								StaffBasicDetailsModel staff = staffList.get(s);
								FCMTokenBean fcmTokenBean = new FCMTokenBean();
								List<FCMTokenBean> tokenList1 = assignSubjectToStudentRepository
										.getTokenListForTeacher(staff.getstaffId(), "2");
								if (tokenList1.size() > 0) {
									fcmTokenBean.setMenuId(7);
									fcmTokenBean.setMenuName("Notices");
									fcmTokenBean.setToken(tokenList1.get(0).getToken());
									fcmTokenBean.setUserId(tokenList1.get(0).getUserId());
									fcmTokenBean.setMobileNo(staff.getContactnosms());
									fcmTokenBean.setMessageType("1");
									fcmTokenBean.setRenewStaffId(staff.getstaffId());
									fcmTokenBean.setTypeFlag("2");
									fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
									fcmTokenBean.setNotificationId(announcementId);
									Fcmtoken.add(fcmTokenBean);
								} else {
									List<FCMTokenBean> tokenList11 = assignSubjectToStudentRepository
											.getTokenListForTeacher(staff.getstaffId(), "3");
									if (tokenList11.size() > 0) {
										fcmTokenBean.setMenuId(7);
										fcmTokenBean.setMenuName("Notices");
										fcmTokenBean.setToken(tokenList11.get(0).getToken());
										fcmTokenBean.setUserId(tokenList11.get(0).getUserId());
										fcmTokenBean.setMobileNo(staff.getContactnosms());
										fcmTokenBean.setMessageType("1");
										fcmTokenBean.setRenewStaffId(staff.getstaffId());
										fcmTokenBean.setTypeFlag("2");
										fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
										fcmTokenBean.setNotificationId(announcementId);
										Fcmtoken.add(fcmTokenBean);
									}
								}

							}
							if (Fcmtoken != null) {
								tokenList.setTokenList(Fcmtoken);
							}

						}

					} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Student")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							List<Integer> studentRenewId = Arrays
									.stream(announceemntResponseBean.getSendingList().split(",")).map(Integer::parseInt)
									.collect(Collectors.toList());
							List<StudentStandardRenewBean> studentList = studentStandardRenewRepository
									.findStudentbyRenewId(
											studentAnnouncementBean.getSchoolId(), studentRenewId);
							if (CollectionUtils.isNotEmpty(studentList)) {
								for (int s = 0; s < studentList.size(); s++) {
									StudentStandardRenewBean student = studentList.get(s);
									FCMTokenBean fcmTokenBean = new FCMTokenBean();
									List<FCMTokenBean> tokenList1 = assignSubjectToStudentRepository
											.getTokenList(student.getRenewStudentId(), "2");
//							Integer userId = appUserRepository.
									if (tokenList1.size() > 0) {
										fcmTokenBean.setMenuId(7);
										fcmTokenBean.setMenuName("Notices");
										fcmTokenBean.setToken(tokenList1.get(0).getToken());
										fcmTokenBean.setUserId(tokenList1.get(0).getUserId());
										fcmTokenBean.setMobileNo(student.getMobileNo());
										fcmTokenBean.setMessageType("1");
										fcmTokenBean.setRenewStaffId(student.getRenewStudentId());
										fcmTokenBean.setTypeFlag("1");
										fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
										fcmTokenBean.setNotificationId(announcementId);
										Fcmtoken.add(fcmTokenBean);
									} else {
										List<FCMTokenBean> tokenList11 = assignSubjectToStudentRepository
												.getTokenList(student.getRenewStudentId(), "3");
										if (tokenList11.size() > 0) {
											fcmTokenBean.setMenuId(7);
											fcmTokenBean.setMenuName("Notices");
											fcmTokenBean.setToken(tokenList11.get(0).getToken());
											fcmTokenBean.setUserId(tokenList11.get(0).getUserId());
											fcmTokenBean.setMobileNo(student.getMobileNo());
											fcmTokenBean.setMessageType("1");
											fcmTokenBean.setRenewStaffId(student.getRenewStudentId());
											fcmTokenBean.setTypeFlag("1");
											fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
											fcmTokenBean.setNotificationId(announcementId);
											Fcmtoken.add(fcmTokenBean);
										}
									}

								}
								if (Fcmtoken != null) {
									tokenList.setTokenList(Fcmtoken);
								}
							}
						}

					} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Teacher1")) {
						if (!announceemntResponseBean.getSendingList().isEmpty()) {
							Date date = new Date();
							String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
							if (!announceemntResponseBean.getSendingList().isEmpty()) {
								List<Integer> staffId = Arrays
										.stream(announceemntResponseBean.getSendingList().split(","))
										.map(Integer::parseInt).collect(Collectors.toList());
								List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
										.findStaff(studentAnnouncementBean.getSchoolId(), currentDate, staffId);
//							List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
								for (int s = 0; s < staffList.size(); s++) {
									StaffBasicDetailsModel staff = staffList.get(s);
									FCMTokenBean fcmTokenBean = new FCMTokenBean();
									List<FCMTokenBean> tokenList1 = assignSubjectToStudentRepository
											.getTokenListForTeacher(staff.getstaffId(), "2");
									if (tokenList1.size() > 0) {
										fcmTokenBean.setMenuId(7);
										fcmTokenBean.setMenuName("Notices");
										fcmTokenBean.setToken(tokenList1.get(0).getToken());
										fcmTokenBean.setUserId(tokenList1.get(0).getUserId());
										fcmTokenBean.setMobileNo(staff.getContactnosms());
										fcmTokenBean.setMessageType("1");
										fcmTokenBean.setRenewStaffId(staff.getstaffId());
										fcmTokenBean.setTypeFlag("2");
										fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
										fcmTokenBean.setNotificationId(announcementId);
										Fcmtoken.add(fcmTokenBean);
									} else {
										List<FCMTokenBean> tokenList11 = assignSubjectToStudentRepository
												.getTokenListForTeacher(staff.getstaffId(), "3");
										if (tokenList11.size() > 0) {
											fcmTokenBean.setMenuId(7);
											fcmTokenBean.setMenuName("Notices");
											fcmTokenBean.setToken(tokenList11.get(0).getToken());
											fcmTokenBean.setUserId(tokenList11.get(0).getUserId());
											fcmTokenBean.setMobileNo(staff.getContactnosms());
											fcmTokenBean.setMessageType("1");
											fcmTokenBean.setRenewStaffId(staff.getstaffId());
											fcmTokenBean.setTypeFlag("2");
											fcmTokenBean.setSchoolId(studentAnnouncementBean.getSchoolId());
											fcmTokenBean.setNotificationId(announcementId);
											Fcmtoken.add(fcmTokenBean);
										}
									}

								}
								if (Fcmtoken != null) {
									tokenList.setTokenList(Fcmtoken);
								}

							}
						}

					}

				}

			}

			String language = Utility.getLanguage(schoolModel.getLanguage().getLanguageId());
			final String title = env.getProperty(language + "." + "Announcement");
			String message1 = env.getProperty(language + "." + "AnnouncementNotification");
			final String message = message1.replaceAll("teacherName", studentAnnouncementBean.getTeacherName())
					.replaceAll("\\{", "").replaceAll("\\}", "")
					.replaceAll("notifTime", studentAnnouncementBean.getTime())
					.concat(" : " + studentAnnouncementBean.getAnnouncement());

//			AndroidNotificationStatusModel androidNotificationStatusModel = new AndroidNotificationStatusModel();
//			String statusIncrementedId = ""
//					+ androidNotificationStatusRepository.getMaxId(studentAnnouncementBean.getSchoolId());
//			String notificationStatusId = Utility.generatePrimaryKey(globalDbSchoolKey, statusIncrementedId);
//			androidNotificationStatusModel.setNotificationStatusId(Integer.parseInt(notificationStatusId));
//			androidNotificationStatusModel.setSchoolMasterModel(schoolMasterModel);
//			androidNotificationStatusModel.setJoinTable("announcement_assign_to");
//			androidNotificationStatusModel.setJoinId("" + announcementId);
//			androidNotificationStatusModel.setNotificationType("Announcement");
//			androidNotificationStatusModel.setStatus(1);
//			androidNotificationStatusModel.setTitle(title);
//			androidNotificationStatusModel.setMessage(message);
//			androidNotificationStatusModel.setCount(Integer.parseInt(statusIncrementedId));
//			androidNotificationStatusModel.setNotificationDate(new Date());
//			String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
//			androidNotificationStatusModel.setNotificationTime(currentTime);
//			androidNotificationStatusRepository.saveAndFlush(androidNotificationStatusModel);

			announcementRepository.updateAnnouncementSentFlag(1, announcementId);

//			Utility.pushNotification(androidTokenList, title, message, "2");
//			Utility.pushNotification(IOSTokenList, title, message, "3");
			tokenList.setTitle(studentAnnouncementBean.getAnnouncementTitle());
			tokenList.setMessage(message);
			tokenList.setCustomizeMessageFlag(0);
			Utility.sendNotificationNew(tokenList);
			announcementRepository.updateNoticeFlagStatus(announcementId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	//@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public  Integer saveAttachments(MultipartFile file, String fileName, Integer assignAnnouncementId,
			String yearName, Integer schoolId) {
		try {
			AnnouncementAttachmentModel attachmentModel = new AnnouncementAttachmentModel();
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(assignAnnouncementId);
			attachmentModel.setAnnouncementModel(announcementModel);
			attachmentModel.setFileName(fileName);

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			attachmentModel.setSchoolMasterModel(schoolMasterModel);
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
//			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
//			String incrementedId = "" + announcementRepository.getMaxAttachmentId(schoolId);
//			String announcementId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
//			attachmentModel.setAnnouncementAttachmentId(Integer.parseInt(announcementId));
			StringBuilder attachmentFileName = new StringBuilder();

			AppLanguageModel appLanguageModel = schoolModel.getLanguage();
			Integer language = appLanguageModel.getLanguageId();

			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				attachmentFileName.append(yearName).append("_").append(fileName);
				String folderName = "cms" + File.separator + "Announcement" + File.separator
						+ schoolModel.getSchoolKey() + File.separator + yearName;
				//String target = HttpClientUtil.uploadImage(file, fileName, folderName, schoolModel.getSchoolKey(),
					//	language);
				try {
					// Upload attachment on s3
					String postUri = env.getProperty("awsUploadUrl");
					URI uri = new URI(postUri);
					File convFile = new File(attachmentFileName.toString());
					convFile.createNewFile();
					FileOutputStream fos = new FileOutputStream(convFile);
					fos.write(file.getBytes());
					fos.close();
					UploadFileBean bean = new UploadFileBean();
					bean.setFileName(fileName);
					bean.setFile(convFile);
					bean.setFilePath("maindb" + "/" + folderName);
//					String path = restTemplate.postForObject(uri, bean, String.class);
					String path = uploadSingleFile(bean);
					attachmentModel.setFileName(attachmentFileName.toString());
					convFile.delete();
					if (path != null) {
						attachmentModel.setServerFilePath(path);
					} 
//					else {
//						attachmentModel.setServerFilePath(target);
//					}
					//
					attachmentModel.setFileName(attachmentFileName.toString());
					attachmentRepository.saveAndFlush(attachmentModel);
					return attachmentModel.getAnnouncementAttachmentId();
				}catch(Exception e ){
					e.printStackTrace();
				}
			}

		} 
		catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return 0;

	}

	public String uploadSingleFile(UploadFileBean fileBean) {
		try {
//			String bucketName = "eprashasan1";
			String bucketName = ApplicationConstants.bucketName;
			String key = fileBean.getFilePath();
			key = key.replace("\\", "/");
			String returnPath = UploadFile(bucketName, key, fileBean.getFile());
			return returnPath;
		} catch (Exception e) {
			return null;
		}
	}

	public String UploadFile(String bucketName, String keyName, File file) {
		try {
			keyName = keyName + "/" + file.getName();
			keyName = keyName.replace(" ", "_");
			amazonS3.putObject(
					new PutObjectRequest(bucketName, keyName, file));
			return ((AmazonS3Client) amazonS3).getResourceUrl(bucketName, keyName);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public List<AnnouncementResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole) {
		List<AnnouncementResponseBean> annoucementResponseList = new ArrayList<>();
		AnnouncementResponseBean annoucementResponseBean = new AnnouncementResponseBean();
		annoucementResponseBean.setRoleName("Teacher");
		List<AnnoucementLabelBean> labelList = new ArrayList<>();
		AnnoucementLabelBean annoucementLabelBean = new AnnoucementLabelBean();
		annoucementLabelBean.setId("ROLE_NONTEACHINGSTAFF");
		annoucementLabelBean.setName("Non Teacher");
		labelList.add(annoucementLabelBean);

		AnnoucementLabelBean annoucementLabelBean1 = new AnnoucementLabelBean();
		annoucementLabelBean1.setId("ROLE_TEACHINGSTAFF");
		annoucementLabelBean1.setName("Teacher");
		labelList.add(annoucementLabelBean1);
		annoucementResponseBean.setDisplayList(labelList);
		annoucementResponseList.add(annoucementResponseBean);

		List<StandardDivisionBean> standardDivisionList = announcementRepository.getStandardDivisionList(schoolId);
		/*
		 * List<AnnoucementLabelBean> labelList1 = new ArrayList<>();
		 * annoucementResponseBean = new AnnouncementResponseBean();
		 * annoucementResponseBean.setRoleName("Student");
		 * standardDivisionList.stream().forEach( standardDivision -> {
		 * AnnoucementLabelBean annoucementLabelBean2 = new AnnoucementLabelBean();
		 * annoucementLabelBean2.setId(""+standardDivision.getStandardId()+"-"+
		 * standardDivision.getDivisionId());
		 * annoucementLabelBean2.setName(""+standardDivision.getStandardName()+"-"+
		 * standardDivision.getDivisionName()); labelList1.add(annoucementLabelBean2);
		 * }); annoucementResponseBean.setDisplayList(labelList1);
		 * annoucementResponseList.add(annoucementResponseBean);
		 */

		List<AnnoucementLabelBean> labelList2 = new ArrayList<>();
		annoucementResponseBean = new AnnouncementResponseBean();
		annoucementResponseBean.setRoleName("Parent");
		standardDivisionList.stream().forEach(standardDivision -> {
			AnnoucementLabelBean annoucementLabelBean2 = new AnnoucementLabelBean();
			annoucementLabelBean2.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
			annoucementLabelBean2
					.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
			labelList2.add(annoucementLabelBean2);
		});
		annoucementResponseBean.setDisplayList(labelList2);
		annoucementResponseList.add(annoucementResponseBean);

		annoucementResponseBean = new AnnouncementResponseBean();
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository.findStaff(schoolId, currentDate);
		List<StaffDetailsBean> staffBeanList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(staffList)) {
			staffList.parallelStream().forEach(staff -> {
				StaffDetailsBean staffDetailsBean = new StaffDetailsBean();
				staffDetailsBean.setStaffId(staff.getstaffId());
				String InitialName = staff.getInitialname();
				String FirstName = staff.getFirstname();
				String SecondName = staff.getSecondname();
				String LastName = staff.getLastname();
				if (InitialName == null || InitialName.length() == 0 || InitialName.equalsIgnoreCase("Null")) {
					InitialName = "";
				}
				if (FirstName == null || FirstName.length() == 0 || FirstName.equalsIgnoreCase("Null")) {
					FirstName = "";
				}
				if (SecondName == null || SecondName.length() == 0 || SecondName.equalsIgnoreCase("Null")) {
					SecondName = "";
				}
				if (LastName == null || LastName.length() == 0 || LastName.equalsIgnoreCase("Null")) {
					LastName = "";
				}

				staffDetailsBean.setStaffName(InitialName + " " + FirstName + " " + SecondName + " " + LastName);
				staffDetailsBean.setRegNo(staff.getSregNo());
				staffBeanList.add(staffDetailsBean);
			});
			annoucementResponseBean.setStaffList(staffBeanList);
			annoucementResponseList.add(annoucementResponseBean);
		}
		return annoucementResponseList;
	}

//	@Override
//	public List<AnnouncementResponseBean> getMyAnnouncement(Integer staffStudentId, Integer yearId, String profileRole,
//			String announcementDate, Integer offset, Integer limit, Integer userId, Integer typeId,Integer schoolId,Integer announcementId) {
//		System.out.println("limit" + limit);
//		if (limit == null || limit == 0) {
//			limit = 10;
//		}
//		List<AnnouncementResponseBean> studentList = new ArrayList<>();
//		try {
//
//			if (StringUtils.isNotEmpty(announcementDate)) {
//				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(announcementDate);
//				if (profileRole.equalsIgnoreCase("Parent")) {
//					if(announcementId!=0) {
//						studentList = announcementRepository.getStudentAnnouncementParentWithAnnouncementId(staffStudentId, yearId,
//								profileRole, date, typeId,announcementId,schoolId ,PageRequest.of(offset, limit));
//					}else {
//					studentList = announcementRepository.getStudentAnnouncementParent(staffStudentId, yearId,
//							profileRole, date, typeId,schoolId, PageRequest.of(offset, limit));
//					}
//				} else {
//					if(announcementId!=0) {
//
//						studentList = announcementRepository.getStudentAnnouncementWithAnnouncementId(staffStudentId, yearId, profileRole,
//								date, typeId,announcementId,schoolId ,PageRequest.of(offset, limit));
//
//					}else {
//					studentList = announcementRepository.getStudentAnnouncement(staffStudentId, yearId, profileRole,
//							date, typeId, schoolId,PageRequest.of(offset, limit));
//					}
//				}
//
//			} else {
//				if (profileRole.equalsIgnoreCase("Parent")) {
//					
//					if(announcementId!=0) {
//						studentList = announcementRepository.getStudentAnnouncementParentWithAnnouncementId(staffStudentId, yearId,
//								profileRole, typeId,announcementId,schoolId , PageRequest.of(offset, limit));
//
//					}else {
//					studentList = announcementRepository.getStudentAnnouncementParent(staffStudentId, yearId,
//							profileRole, typeId,schoolId,PageRequest.of(offset, limit));
//					}
//				} else {
//					if(announcementId!=0) {
//						studentList = announcementRepository.getStudentAnnouncementWithAnnouncementId(staffStudentId, yearId, profileRole,
//								typeId, announcementId,schoolId, PageRequest.of(offset, limit));
//					}else {
//					studentList = announcementRepository.getStudentAnnouncement(staffStudentId, yearId, profileRole,
//							typeId,schoolId,PageRequest.of(offset, limit));
//					}
//				}
//
//			}
//
//			studentList.stream().forEach(student -> {
//
//				student.setTotalCommentCount(1);
//				student.setTotalViewsCount(1);
//
//				AnnouncementCommentResponseBean announcementCommentResponseBean = new AnnouncementCommentResponseBean();
//				announcementCommentResponseBean.setAppUserId(userId);
//				AppUserModel appUser = appUserRepository.getUserDetailsWithUserId(userId,
//						schoolId);
//				announcementCommentResponseBean.setFirstName(appUser.getFirstName());
//				announcementCommentResponseBean.setLastName(appUser.getLastName());
//				announcementCommentResponseBean.setPhotoUrl(
//						"https://eprashasan.s3.ap-south-1.amazonaws.com/maindb/icardImages/0001/1000100001/8149160393.jpg");
//				announcementCommentResponseBean.setSchoolId(schoolId);
//				try {
//				List<AnnouncementCommentModel> comments = commentRepo.findViews(schoolId, student.getAnnouncementId());
//				announcementCommentResponseBean.setCommentId(comments.get(0).getCommentId());
//				announcementCommentResponseBean.setComment(comments.get(0).getComment());
//				student.setLastComment(announcementCommentResponseBean);
//				}catch(Exception e) {
//					System.out.println(e);
//				}
//				student.setUserCanComment(true);
//				student.setUserComment("user comment");
//				List<AnnouncementLikeorDislikeModel> likes = likeDislikeRepo.findLikes(schoolId, student.getAnnouncementId());
//				student.setTotalLikes(likes.size());
//				student.setUserlikestatus(true);
//				student.setPostedTime("posted time");
//				student.setUserfeedBackStatus(true);
//
//				student.setShowcount(false);
//				student.setCompleteCount(0l);
//				student.setDeliveredCount(0l);
//				student.setSeenCount(0l);
//				student.setUnseenCount(0l);
//				student.setTotalStudentCount(0l);
//				student.setAnnouncedByRole(getRoles(student.getAnnouncedByRole()));
//				List<AnnouncementResponseBean> attachmentList = announcementRepository
//						.getAttachments(student.getAnnouncementId(),schoolId);
//				attachmentList.parallelStream().forEach(attachment -> {
//					attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
//				});
//				student.setAttachmentList(attachmentList);
//				student.setAssignmentStatus("" + getAssignmentStatus(Integer.parseInt(student.getAssignmentStatus())));
//				String announcedByUrl = setStaffPhotoUrl(student.getSchoolId(), student.getRegistrationNumber());
//				student.setAnnouncedByImgUrl(announcedByUrl);
//
//				if (student.getStaffId().equals(staffStudentId)) {
//					List<AnnouncementResponseBean> countList = announcementRepository
//							.findAssignedAnnouncementCount(staffStudentId, yearId, student.getAnnouncementId(),schoolId);
//					student.setShowcount(true);
//					student.setCompleteCount(countList.get(0).getCompleteCount());
//					student.setDeliveredCount(countList.get(0).getDeliveredCount());
//					student.setSeenCount(countList.get(0).getSeenCount());
//					student.setUnseenCount(countList.get(0).getUnseenCount());
//					student.setTotalStudentCount(countList.get(0).getTotalStudentCount());
//				}
//			});
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return studentList;
//	}

	private String setStaffPhotoUrl(Integer schoolId, String registartionNo) {
		String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = "" + ConfigurationProperties.staffPhotoUrl + "/" + schoolKey;
		String extension = "jpg";
		return directoryPath + "/" + registartionNo + "." + extension;
	}

	private String getRoles(String userName) {
		switch (userName) {
		case "ROLE_PRINCIPAL":
			return "Principal";
		case "ROLE_ADMIN":
			return "Admin";
		case "ROLE_NONTEACHINGSTAFF":
			return "Non Teaching Staff";
		case "ROLE_TEACHINGSTAFF":
			return "Teacher";
		case "ROLE_PARENT1":
			return "Father";
		case "ROLE_PARENT2":
			return "Mother";
		case "ROLE_SANSTHAOFFICER":
			return "Sanstha Officer";
		case "ROLE_STUDENT":
			return "Student";
		case "ROLE_SUPERADMIN":
			return "Super Admin";
		case "ROLE_USER":
			return "User";
		default:
			return userName;
		}

	}

	private String getAssignmentStatus(int i) {
		switch (i) {
		case 0:
			return "Total";
		case 1:
			return "Delivered";
		case 2:
			return "Seen";
		case 3:
			return "Completed";
		case 4:
			return "Unseen";
		default:
			return "";
		}
	}

	@Override
	public Integer updateAnnouncementStatus(Integer announcementAssignToId, Integer studentId, String statusFlag) {
		AnnouncementAssignToModel studentAnnouncementAssignToModel = studentAnnouncementAssignedToRepository
				.findByAnnouncementAssignToId(announcementAssignToId);
		if (!(statusFlag.equals("1") && (studentAnnouncementAssignToModel.getAnnouncementStatus().equals("2")
				|| studentAnnouncementAssignToModel.getAnnouncementStatus().equals("3")))) {
			studentAnnouncementAssignToModel.setAnnouncementStatus(statusFlag);
			studentAnnouncementAssignToModel = studentAnnouncementAssignedToRepository
					.save(studentAnnouncementAssignToModel);
		}
		return studentAnnouncementAssignToModel.getAnnouncementAssignToId();
	}

	public void sendOtpMessage(String toMobile1, String message, Integer schoolId, Integer smssaveId2) {
		String apikey = null;
		String senderId = null;
		String type = null;
		String mainUrl = null;
		String route = "";
		String country = "";
		String statusUrl = "";
		String response = null;
		String user = null;
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		String toMobile = Utility.convertOtherToEnglish(toMobile1);
		if (toMobile.length() == 10 && toMobile.matches("^[0-9]*$")) {
			try {
				schoolMasterModel.setSchoolid(schoolId);
				List<FeeSendSmsNumbersMstrModel> list1 = userAuthenticationRepository
						.findBySchoolMasterModel(schoolMasterModel);

				for (int i = 0; i < list1.size(); i++) {
					if ("1".equals("" + list1.get(i).getAdminPermission())) {
						apikey = cryptoUtil.decrypt("azxopre", list1.get(i).getApikey());
						user = cryptoUtil.decrypt("mqpghdrz", list1.get(i).getUserName());
						// aPiName = list1.get(i).getApiname();
						// password = cry.decrypt("opqzakgj",list1.get(i).getPassword());
						// messageType= list1.get(i).getMsgType();
						// mobileId = list1.get(i).getMobNoId();
						mainUrl = cryptoUtil.decrypt("yrewpzgl", list1.get(i).getWebsiteUrl());
						statusUrl = cryptoUtil.decrypt("qbxozgl", list1.get(i).getStatusUrl());
						route = list1.get(i).getRoute();
						country = list1.get(i).getCountry();
						senderId = list1.get(i).getSenderId();
					}
				}

				URLConnection myURLConnection = null;
				URL myURL = null;
				BufferedReader reader = null;

				String encodedMessage = URLEncoder.encode(message, "UTF-8");
				StringBuilder sbPostData = new StringBuilder(mainUrl);

				if (country.equalsIgnoreCase("0")) {
					sbPostData.append("authkey=" + apikey);
					sbPostData.append("&mobiles=" + toMobile);
					sbPostData.append("&message=" + encodedMessage);
					sbPostData.append("&route=" + route);
					sbPostData.append("&sender=" + senderId);
					sbPostData.append("&country=" + country);
					sbPostData.append("&unicode=" + 1);
					mainUrl = sbPostData.toString();
					myURL = new URL(mainUrl);
					myURLConnection = myURL.openConnection();
					myURLConnection.connect();
					reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					reader.close();
					type = "4";

					sbPostData = new StringBuilder(statusUrl);
					sbPostData.append("authkey=" + apikey);
					sbPostData.append("&type=" + type);
					mainUrl = sbPostData.toString();
					myURL = new URL(mainUrl);
					myURLConnection = myURL.openConnection();
					myURLConnection.connect();
					reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					reader.close();
				} else {
					if (statusUrl.equalsIgnoreCase("")) {
						URL url = new URL("" + mainUrl + "user=" + user + "&pwd=" + apikey + "&senderid=" + senderId
								+ "&mobileno=" + toMobile + "&msgtext=" + encodedMessage + "");
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						conn.setRequestMethod("GET");
						conn.setDoOutput(true);
						conn.setDoInput(true);
						conn.setUseCaches(false);
						conn.connect();
						BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						String line;
						StringBuilder buffer = new StringBuilder();
						while ((line = rd.readLine()) != null) {
							buffer.append(line).append("\n");
						}

						rd.close();
						conn.disconnect();
					} else {
						StringBuilder sbPostData1 = new StringBuilder(mainUrl);
						sbPostData1.append("user=" + user);
						sbPostData1.append("&apikey=" + apikey);
						sbPostData1.append("&message=" + encodedMessage);
						sbPostData1.append("&mobile=" + toMobile);
						sbPostData1.append("&senderid=" + senderId);
						sbPostData1.append("&type=" + type);
						String msgid = "";
						mainUrl = sbPostData1.toString();
						myURL = new URL(mainUrl);
						myURLConnection = myURL.openConnection();
						myURLConnection.connect();
						reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
						while ((response = reader.readLine()) != null) {
							msgid = response.trim();
						}
						reader.close();

						smsRepository.updateMsgId(msgid, smssaveId2);
						reader.close();

						sbPostData = new StringBuilder(statusUrl);
						sbPostData.append("user=" + user);
						sbPostData.append("&apikey=" + apikey);
						sbPostData.append("&msgid=" + msgid);
						mainUrl = sbPostData.toString();
						myURL = new URL(mainUrl);
						myURLConnection = myURL.openConnection();
						myURLConnection.connect();
						reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
						while ((response = reader.readLine()) != null) {
							String status = response.toString();
							if (!status.equalsIgnoreCase("")) {
								smsRepository.updateStatus(status, smssaveId2);
							} else {
								smsRepository.updateStatus("", smssaveId2);
							}
						}
						reader.close();

						String balncurl = "";
						balncurl = statusUrl.replace("status", "balance");
						sbPostData = new StringBuilder(balncurl);
						sbPostData.append("user=" + user);
						sbPostData.append("&apikey=" + apikey);
						mainUrl = sbPostData.toString();
						myURL = new URL(mainUrl);
						myURLConnection = myURL.openConnection();
						myURLConnection.connect();
						reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
						while ((response = reader.readLine()) != null) {
							System.out.println(response.toString());
						}
						reader.close();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Integer sendFeedbackEmail(FeedbackEmailRequestBean feedbackEmailRequestBean) {

		String userName = Utility.convertInUTFFormat(feedbackEmailRequestBean.getUserName());
		String description = Utility.convertInUTFFormat(feedbackEmailRequestBean.getDescription());
		String schoolName = Utility.convertInUTFFormat(feedbackEmailRequestBean.getSchoolName());

		StringBuilder subject = new StringBuilder();

		subject.append(feedbackEmailRequestBean.getTitle()).append(" from ")
				.append(feedbackEmailRequestBean.getUserName());

		if (StringUtils.isNotEmpty(feedbackEmailRequestBean.getRegNo())) {
			subject.append(" Reg No. ").append(feedbackEmailRequestBean.getRegNo());
		}
		Integer sentMail = 0;

		try {
			sentMail = sendEmail(subject.toString(), description, userName, feedbackEmailRequestBean, schoolName);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return sentMail;
	}

	public Integer sendEmail(String subject, String message, String userName,
			FeedbackEmailRequestBean feedbackEmailRequestBean, String schoolName)
			throws AddressException, MessagingException {

		String host = "smtp.gmail.com";
		String port = "587";
		String mailFrom = "feedback.ingenio@gmail.com";
		String password = "eprashasan";
		String[] recipients = { "feedback.ingenio@gmail.com" };

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.user", mailFrom);
		properties.put("mail.password", password);
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailFrom, password);
			}
		};

		Session session = Session.getInstance(properties, auth);

		// creates a new e-mail message
		MimeMessage msg = new MimeMessage(session);
//		msg.setHeader("Content-Type", "UTF-8");

		msg.setFrom(new InternetAddress(mailFrom));
		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}

		msg.setRecipients(Message.RecipientType.TO, addressTo);
		msg.setSubject(subject, "UTF-8");
		msg.setSentDate(new Date());

		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();

		messageBodyPart.setText(message + "\n" + "\n" + "\n" + "\n" + "User Details" + "\n" + "Name : " + userName
				+ "\n" + "Role  :" + feedbackEmailRequestBean.getRole() + "\n" + "School Name :" + schoolName + "\n"
				+ "Reg No. :" + feedbackEmailRequestBean.getRegNo() + "\n" + "Contact No : "
				+ feedbackEmailRequestBean.getContactNo() + "\n" + "Renew Id : "
				+ feedbackEmailRequestBean.getRenewStudentId() + "\n" + "Order Id : "
				+ feedbackEmailRequestBean.getOrderId() + "\n" + "School Id : " + feedbackEmailRequestBean.getSchoolId()
				+ "\n" + "School Key : " + feedbackEmailRequestBean.getSchoolSansthaKey() + "\n" + "Student Fee Id : "
				+ feedbackEmailRequestBean.getStudFeeID() + "\n" + "Process Id : "
				+ feedbackEmailRequestBean.getProcessId() + "\n" + "Process Id : "
				+ feedbackEmailRequestBean.getProcessId() + "\n" + "Status Code : "
				+ feedbackEmailRequestBean.getStatusCode() + "\n" + "Status Message : "
				+ feedbackEmailRequestBean.getStatusMessage() + "\n" + "Status : "
				+ feedbackEmailRequestBean.getStatus() + "\n");

		multipart.addBodyPart(messageBodyPart);
		msg.setContent(multipart);
		Transport.send(msg);
		/*
		 * try { String filePath = "C:/ePrashasan/MainDB/FeedBack"; File dir = new
		 * File(filePath); if (!dir.exists()) { dir.mkdirs(); }
		 * 
		 * if(CollectionUtils.isNotEmpty(fileList)) { for(int i=0;i<fileList.size();i++)
		 * { MultipartFile file = fileList.get(i); File convFile = new
		 * File(filePath+"/"+file.getOriginalFilename()); convFile.createNewFile();
		 * FileOutputStream fos = new FileOutputStream(convFile);
		 * fos.write(file.getBytes()); fos.close(); messageBodyPart = new
		 * MimeBodyPart(); DataSource source = new
		 * FileDataSource(filePath+"/"+file.getOriginalFilename());
		 * messageBodyPart.setDataHandler(new DataHandler(source));
		 * messageBodyPart.setFileName(filePath+"/"+file.getOriginalFilename());
		 * 
		 * } }
		 * 
		 * 
		 * multipart.addBodyPart(messageBodyPart);
		 */

		/*
		 * }catch(
		 * 
		 * FileNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }catch( IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */
		if (StringUtils.isNotEmpty(feedbackEmailRequestBean.getProcessId())) {
			return feeRepositoryImpl.updateStatus(feedbackEmailRequestBean);
		}
		return 0;

	}

	@Override
	@Transactional
	public Integer deleteAnnouncement(Integer announcementId) {
		AnnouncementModel model = new AnnouncementModel();
		model.setAnnouncementId(announcementId);
		studentAnnouncementAttachmentRepository.deleteByAnnouncementModel(model);
		studentAnnouncementAssignedToRepository.deleteByAnnouncementModel(model);
		announcementRepository.deleteById(announcementId);
		return 1;
	}

	@Override
	@Transactional
	public Integer deleteAttachment(Integer assignmentAttachmentId) {
		studentAnnouncementAttachmentRepository.deleteById(assignmentAttachmentId);
		return 1;
	}

	@Override
	public List<AnnouncementResponseBean> getAttachmentList(Integer announcementId) {
		List<AnnouncementResponseBean> attachmentList = announcementRepository.getAttachments(announcementId);
		attachmentList.parallelStream().forEach(attachment -> {
			attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
		});
		return attachmentList;
	}

	@Override
	public List<StudentDetailsBean> getStudentDetailsForAnnouncement(Integer announcementId, Integer offset) {
		List<StudentDetailsBean> announcementList = new ArrayList<>();
		List<StudentDetailsBean> studentDetailsList = announcementRepository
				.getStudentDetailsForAnnouncement(announcementId);
		List<StudentDetailsBean> staffDetailsList = announcementRepository
				.getStaffDetailsForAnnouncement(announcementId);
		List<StudentDetailsBean> totalList = new ArrayList<>();
		totalList.addAll(studentDetailsList);
		totalList.addAll(staffDetailsList);

		IntStream.range(0, 5).forEach(i -> {
			StudentDetailsBean studentAnnouncementBean = new StudentDetailsBean();
			studentAnnouncementBean.setAnnouncementStatus("" + i);
			studentAnnouncementBean.setAnnouncementStatusText(getAnnouncementStatus(i));
			if (i == 0) {
				totalList.stream().forEach(student -> {
					if (("Student").equals(student.getRole())) {
						student.setPhotoUrl(
								setStudentPhotoUrl(student.getSchoolId(), student.getRegNo(), student.getGrBookId()));
					} else {
						student.setStudentId(0);
						student.setPhotoUrl(setStaffPhotoUrl(student.getSchoolId(), student.getRegNo()));
					}

				});
				studentAnnouncementBean.setStudentDetailsBean(totalList);
			} else {
				List<StudentDetailsBean> updatedStudentDetailsList = new ArrayList<>();
				totalList.stream().forEach(student -> {
					if (("Student").equals(student.getRole())) {
						student.setPhotoUrl(
								setStudentPhotoUrl(student.getSchoolId(), student.getRegNo(), student.getGrBookId()));
					} else {
						student.setStudentId(0);
						student.setPhotoUrl(setStaffPhotoUrl(student.getSchoolId(), student.getRegNo()));
					}
					if (i != 4 && Integer.parseInt(student.getAnnouncementStatus()) >= i) {
						updatedStudentDetailsList.add(student);
					} else if (i == 4 && student.getAnnouncementStatus().equals("1")) {
						updatedStudentDetailsList.add(student);
					}
				});
				studentAnnouncementBean.setStudentDetailsBean(updatedStudentDetailsList);
			}

			announcementList.add(studentAnnouncementBean);
		});

		return announcementList;
	}

	private String setStudentPhotoUrl(Integer schoolId, String registartionNo, Integer grBookId) {
		String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = "" + ConfigurationProperties.studentPhotoUrl + schoolKey + "/" + grBookId;
		String extension = "jpg";
		return directoryPath + "/" + registartionNo + "." + extension;
	}

	private String getAnnouncementStatus(int i) {
		switch (i) {
		case 0:
			return "Total";
		case 1:
			return "Delivered";
		case 2:
			return "Seen";
		case 3:
			return "Noted";
		case 4:
			return "Unseen";
		default:
			return "";
		}
	}

	@Override
	public Integer newSendFeedbackEmail(MultipartFile file, FeedbackEmailRequestBean feedbackEmailRequestBean) {
		String userName = Utility.convertInUTFFormat(feedbackEmailRequestBean.getUserName());
		String description = Utility.convertInUTFFormat(feedbackEmailRequestBean.getDescription());
		String schoolName = Utility.convertInUTFFormat(feedbackEmailRequestBean.getSchoolName());

		StringBuilder subject = new StringBuilder();

		subject.append(feedbackEmailRequestBean.getTitle()).append(" from ")
				.append(feedbackEmailRequestBean.getUserName());

		if (StringUtils.isNotEmpty(feedbackEmailRequestBean.getRegNo())) {
			subject.append(" Reg No. ").append(feedbackEmailRequestBean.getRegNo());
		}
		Integer sentMail = 0;

		try {
			sentMail = newSendEmail(file, subject.toString(), description, userName, feedbackEmailRequestBean,
					schoolName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sentMail;
	}

	private Integer newSendEmail(MultipartFile file, String subject, String message, String userName,
			FeedbackEmailRequestBean feedbackEmailRequestBean, String schoolName)
			throws AddressException, MessagingException {

		String host = "smtp.gmail.com";
		String port = "587";
		String mailFrom = "feedback.ingenio@gmail.com";
		String password = "eprashasan";
		String[] recipients = { "feedback.ingenio@gmail.com" };

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.user", mailFrom);
		properties.put("mail.password", password);
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailFrom, password);
			}
		};

		Session session = Session.getInstance(properties, auth);

		// creates a new e-mail message
		MimeMessage msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(mailFrom));
		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}

		msg.setRecipients(Message.RecipientType.TO, addressTo);
		msg.setSubject(subject, "UTF-8");
		msg.setSentDate(new Date());

		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();

		messageBodyPart.setText(message + "\n" + "\n" + "\n" + "\n" + "User Details" + "\n" + "Name : " + userName
				+ "\n" + "Role  :" + feedbackEmailRequestBean.getRole() + "\n" + "School Name :" + schoolName + "\n"
				+ "Reg No. :" + feedbackEmailRequestBean.getRegNo() + "\n" + "Contact No : "
				+ feedbackEmailRequestBean.getContactNo() + "\n" + "Renew Id : "
				+ feedbackEmailRequestBean.getRenewStudentId() + "\n" + "Order Id : "
				+ feedbackEmailRequestBean.getOrderId() + "\n" + "School Id : " + feedbackEmailRequestBean.getSchoolId()
				+ "\n" + "School Key : " + feedbackEmailRequestBean.getSchoolSansthaKey() + "\n" + "Student Fee Id : "
				+ feedbackEmailRequestBean.getStudFeeID() + "\n" + "Process Id : "
				+ feedbackEmailRequestBean.getProcessId() + "\n" + "Process Id : "
				+ feedbackEmailRequestBean.getProcessId() + "\n" + "Status Code : "
				+ feedbackEmailRequestBean.getStatusCode() + "\n" + "Status Message : "
				+ feedbackEmailRequestBean.getStatusMessage() + "\n" + "Status : "
				+ feedbackEmailRequestBean.getStatus() + "\n");

		multipart.addBodyPart(messageBodyPart);
		try {
			String filePath = "C:/ePrashasan/MainDB/FeedBack";
			File dir = new File(filePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}

			/*
			 * if(CollectionUtils.isNotEmpty(fileList)) { for(int i=0;i<fileList.size();i++)
			 * { MultipartFile file = fileList.get(i);
			 */
			File convFile = new File(filePath + "/" + file.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filePath + "/" + file.getOriginalFilename());
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filePath + "/" + file.getOriginalFilename());
			/*
			 * } }
			 */

			multipart.addBodyPart(messageBodyPart);

			msg.setContent(multipart);
			Transport.send(msg);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (StringUtils.isNotEmpty(feedbackEmailRequestBean.getProcessId())) {
			return feeRepositoryImpl.updateStatus(feedbackEmailRequestBean);
		}
		return 0;

	}

	@Override
	public Integer saveAttachmentsWithId(MultipartFile file, String fileName, Integer announcmentId, Integer yearId,
			Integer schoolId) {
		try {
			AnnouncementAttachmentModel attachmentModel = new AnnouncementAttachmentModel();
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(announcmentId);
			attachmentModel.setAnnouncementModel(announcementModel);
			attachmentModel.setFileName(fileName);

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			attachmentModel.setSchoolMasterModel(schoolMasterModel);
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId = "" + announcementRepository.getMaxAttachmentId(schoolId);
			String announcementId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			attachmentModel.setAnnouncementAttachmentId(Integer.parseInt(announcementId));
			StringBuilder attachmentFileName = new StringBuilder();

			AppLanguageModel appLanguageModel = schoolModel.getLanguage();
			Integer language = appLanguageModel.getLanguageId();

			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				attachmentFileName.append(yearId).append("_").append(fileName);
				String folderName = "cms" + File.separator + "Announcement" + File.separator
						+ schoolModel.getSchoolKey() + File.separator + yearId;
				String target = HttpClientUtil.uploadImage(file, fileName, folderName, schoolModel.getSchoolKey(),
						language);
				if (!target.isEmpty()) {
//					Upload attachment on s3
//					String postUri = env.getProperty("awsUploadUrl");
//					URI uri = new URI(postUri);
					File convFile = new File(attachmentFileName.toString());
					convFile.createNewFile();
					FileOutputStream fos = new FileOutputStream(convFile);
					fos.write(file.getBytes());
					fos.close();
					UploadFileBean bean = new UploadFileBean();
					bean.setFileName(fileName);
					bean.setFile(convFile);
					bean.setFilePath("maindb" + "/" + folderName);
//					String path = restTemplate.postForObject(uri, bean, String.class);
					String path = uploadSingleFile(bean);
					convFile.delete();
					if (path != null) {
						attachmentModel.setServerFilePath(path);
					} else {
						attachmentModel.setServerFilePath(target);
					}
					//
					attachmentModel.setFileName(attachmentFileName.toString());
					attachmentRepository.saveAndFlush(attachmentModel);
					return attachmentModel.getAnnouncementAttachmentId();
				}
			}

		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		return 0;

	}

	@Override
	public List<AnnouncementTypeModel> getAnnouncementType(Integer schoolId) {
			List<AnnouncementTypeModel> announcementTypeModel= announcementTypeRepository.getType(schoolId);
//		
	return announcementTypeModel;
	}

	@Override
	public List<AnnouncementResponseBean> getAnnouncementGroupList(Integer schoolId) {
		List<AnnouncementGroupModel> grouplist = announcementGroupRepository.findBySchoolId(schoolId);
		List<AnnouncementResponseBean> groupsendinList = new ArrayList<AnnouncementResponseBean>();
		grouplist.parallelStream().forEach(group -> {
			AnnouncementResponseBean announcementResponseBean = new AnnouncementResponseBean();
			announcementResponseBean.setGroupId(group.getAnnouncementGroupId());
			announcementResponseBean.setGroupName(group.getAnnouncementGroupName());
			groupsendinList.add(announcementResponseBean);
		});
		return groupsendinList;
	}

	@Override
	public Integer saveQuestionsAndOptions(AnnouncementQuestionOptionBean announcementQuestionOptionBean) {

		AnnouncementModel announcementModel = new AnnouncementModel();
		announcementModel.setAnnouncementId(announcementQuestionOptionBean.getAnnouncementId());
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(announcementQuestionOptionBean.getSchoolid());

		List<FeedbackData> feedbackDataList = announcementQuestionOptionBean.getFeedbackDataList();
		feedbackDataList.stream().forEach(feedback -> {
			AnnouncmenetQuestionsModel announcmenetQuestionsModel = new AnnouncmenetQuestionsModel();
			announcmenetQuestionsModel.setQuestions(feedback.getQuestionName());

			announcmenetQuestionsModel.setAnnouncementModel(announcementModel);
			announcmenetQuestionsModel.setSchoolMasterModel(schoolMasterModel);
			
			if(feedback.getQuestionId() !=0 ) {
				announcmenetQuestionsModel.setAnnouncementQuestionsId(feedback.getQuestionId());
			}
			AnnouncmenetQuestionsModel announcmenetQuestionsModel1 = announcementQuestionRepository
					.save(announcmenetQuestionsModel);
			
			feedback.getQuestionOption().stream().forEach(questionOption -> {
				AnnouncmentOptionsOfQuestionsModel announcmentOptionsOfQuestionsModel = new AnnouncmentOptionsOfQuestionsModel();
				announcmentOptionsOfQuestionsModel.setSchoolMasterModel(schoolMasterModel);
				announcmentOptionsOfQuestionsModel.setAnnouncementModel(announcementModel);
				announcmentOptionsOfQuestionsModel.setAnnouncmenetQuestionsModel(announcmenetQuestionsModel1);
				announcmentOptionsOfQuestionsModel.setOptions(questionOption.getOptionName());
				if(questionOption.getOptionId() != 0 ) {
					announcmentOptionsOfQuestionsModel.setAnnouncementOptionsofQuestionsId(questionOption.getOptionId());	
				}
				announcementOptionsForQuestionsRepository.save(announcmentOptionsOfQuestionsModel);
			});
		});

		return 1;
	}

	public void sendWhatsappSms(AnnouncementRequestBean studentAnnouncementBean, Integer saveId,
			Integer isDueFeeRemainder) {
		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());
			SchoolMasterModel schoolModel = schoolMasterRepository
					.findBySchoolid(studentAnnouncementBean.getSchoolId());
			String schoolName = cry.decrypt("qwertyschoolname",
					StringEscapeUtils.escapeJava(schoolModel.getSchoolName()).replaceAll("[\\r\\n]+", ""));
			List<SchoolWiseWhatsAppSMSModel> schoolWiseList = globalWhatsAppSMSRepository
					.getSchoolWiseSetting(schoolModel.getSchoolid());
			List<AnnouncementAttachmentModel> announcementAttachment = announcementAttachmentRepository
					.getAttachment(saveId);
			String imagePath = "0";
			if (schoolWiseList.size() > 0) {
				String language = Utility.getLanguage(schoolModel.getLanguage().getLanguageId());
				final String title = env.getProperty(language + "." + "Announcement");
				String message = "IMP notice from " + studentAnnouncementBean.getTeacherName() + "(" + schoolName + ")";
				Integer msgCount = 0;
				if (CollectionUtils.isNotEmpty(studentAnnouncementBean.getAnnoucementResponseList())) {
					for (int i = 0; i < studentAnnouncementBean.getAnnoucementResponseList().size(); i++) {
						AnnouncementResponseBean announceemntResponseBean = studentAnnouncementBean
								.getAnnoucementResponseList().get(i);
						if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Parent")) {
							if (!announceemntResponseBean.getSendingList().isEmpty()) {
								List<Integer> divisionNames = Arrays
										.stream(announceemntResponseBean.getSendingList().split(","))
										.map(Integer::parseInt).collect(Collectors.toList());
								List<StudentStandardRenewBean> studentList = studentStandardRenewRepository
										.findStudentbydivision(studentAnnouncementBean.getYearId(),
												studentAnnouncementBean.getSchoolId(), divisionNames);
								if (CollectionUtils.isNotEmpty(studentList)) {
									for (StudentStandardRenewBean parent : studentList) {
										int num = 0;
										if (announcementAttachment.size() > 0) {
											for (AnnouncementAttachmentModel attachmentList : announcementAttachment) {
												imagePath = attachmentList.getServerFilePath();
												sendWhatsAppMessage(parent.getMobileNo(), schoolWiseList, message,
														parent.getRenewStudentId(), msgCount, "1", saveId,
														studentAnnouncementBean.getAnnouncement(), imagePath, num,
														isDueFeeRemainder);
												msgCount++;
												num++;
											}
										} else {
											imagePath = "0";
											sendWhatsAppMessage(parent.getMobileNo(), schoolWiseList, message,
													parent.getRenewStudentId(), msgCount, "1", saveId,
													studentAnnouncementBean.getAnnouncement(), imagePath, num,
													isDueFeeRemainder);
											msgCount++;
										}
									}
									if (schoolWiseList.get(0).getAssignMsgCount() != null) {
										Integer totalCount = Integer.parseInt(schoolWiseList.get(0).getAssignMsgCount())
												- msgCount;
										globalWhatsAppSMSRepository.updateSchoolAssignMessage(schoolModel.getSchoolid(),
												totalCount.toString());
									}
								}
							}
						} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Teacher")) {
							if (!announceemntResponseBean.getSendingList().isEmpty()) {
								Date date = new Date();
								String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
								List<String> roleNames = Arrays
										.asList(announceemntResponseBean.getSendingList().split(","));
								List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
										.findStaff(studentAnnouncementBean.getSchoolId(), roleNames, currentDate);
								if (CollectionUtils.isNotEmpty(staffList)) {
									for (StaffBasicDetailsModel staff : staffList) {
										int num = 0;
										if (announcementAttachment.size() > 0) {
											for (AnnouncementAttachmentModel attachmentList : announcementAttachment) {
												imagePath = attachmentList.getServerFilePath();
												sendWhatsAppMessage(staff.getContactno(), schoolWiseList, message,
														staff.getstaffId(), msgCount, "2", saveId,
														studentAnnouncementBean.getAnnouncement(), imagePath, num,
														isDueFeeRemainder);
												msgCount++;
												num++;
											}
										} else {
											imagePath = "0";
											sendWhatsAppMessage(staff.getContactno(), schoolWiseList, message,
													staff.getstaffId(), msgCount, "2", saveId,
													studentAnnouncementBean.getAnnouncement(), imagePath, num,
													isDueFeeRemainder);
											msgCount++;
										}
									}
									if (schoolWiseList.get(0).getAssignMsgCount() != null) {
										Integer totalCount = Integer.parseInt(schoolWiseList.get(0).getAssignMsgCount())
												- msgCount;
										globalWhatsAppSMSRepository.updateSchoolAssignMessage(schoolModel.getSchoolid(),
												totalCount.toString());
									}
								}
							}

						} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Student")) {
							if (!announceemntResponseBean.getSendingList().isEmpty()) {
								List<Integer> studentRenewId = Arrays
										.stream(announceemntResponseBean.getSendingList().split(","))
										.map(Integer::parseInt).collect(Collectors.toList());
								List<StudentStandardRenewBean> studentList = studentStandardRenewRepository
										.findStudentbyRenewId(
												studentAnnouncementBean.getSchoolId(), studentRenewId);
								if (CollectionUtils.isNotEmpty(studentList)) {
									for (StudentStandardRenewBean student : studentList) {
										int num = 0;
										if (announcementAttachment.size() > 0) {
											for (AnnouncementAttachmentModel attachmentList : announcementAttachment) {
												imagePath = attachmentList.getServerFilePath();
												sendWhatsAppMessage(student.getMobileNo(), schoolWiseList, message,
														student.getRenewStudentId(), msgCount, "1", saveId,
														studentAnnouncementBean.getAnnouncement(), imagePath, num,
														isDueFeeRemainder);
												msgCount++;
												num++;
											}
										} else {
											imagePath = "0";
											sendWhatsAppMessage(student.getMobileNo(), schoolWiseList, message,
													student.getRenewStudentId(), msgCount, "1", saveId,
													studentAnnouncementBean.getAnnouncement(), imagePath, num,
													isDueFeeRemainder);
											msgCount++;
										}
									}
									if (schoolWiseList.get(0).getAssignMsgCount() != null) {
										Integer totalCount = Integer.parseInt(schoolWiseList.get(0).getAssignMsgCount())
												- msgCount;
										globalWhatsAppSMSRepository.updateSchoolAssignMessage(schoolModel.getSchoolid(),
												totalCount.toString());
									}
								}
							}
						} else if (announceemntResponseBean.getRoleName().equalsIgnoreCase("Teacher1")) {
							if (!announceemntResponseBean.getSendingList().isEmpty()) {
								Date date = new Date();
								String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
								List<Integer> staffId = Arrays
										.stream(announceemntResponseBean.getSendingList().split(","))
										.map(Integer::parseInt).collect(Collectors.toList());
								List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository
										.findStaff(studentAnnouncementBean.getSchoolId(), currentDate, staffId);
//								List<AnnouncementAssignToModel> announcementModelList = new ArrayList<>();
								if (CollectionUtils.isNotEmpty(staffList)) {
									for (StaffBasicDetailsModel staff : staffList) {
										int num = 0;
										if (announcementAttachment.size() > 0) {
											for (AnnouncementAttachmentModel attachmentList : announcementAttachment) {
												imagePath = attachmentList.getServerFilePath();
												sendWhatsAppMessage(staff.getContactno(), schoolWiseList, message,
														staff.getstaffId(), msgCount, "2", saveId,
														studentAnnouncementBean.getAnnouncement(), imagePath, num,
														isDueFeeRemainder);
												msgCount++;
												num++;
											}
										} else {
											imagePath = "0";
											sendWhatsAppMessage(staff.getContactno(), schoolWiseList, message,
													staff.getstaffId(), msgCount, "2", saveId,
													studentAnnouncementBean.getAnnouncement(), imagePath, num,
													isDueFeeRemainder);
											msgCount++;
										}
									}
									if (schoolWiseList.get(0).getAssignMsgCount() != null) {
										Integer totalCount = Integer.parseInt(schoolWiseList.get(0).getAssignMsgCount())
												- msgCount;
										globalWhatsAppSMSRepository.updateSchoolAssignMessage(schoolModel.getSchoolid(),
												totalCount.toString());
									}
								}
							}
						}

					}
					announcementRepository.updateWhatsappFlagStatus(saveId);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void sendWhatsAppMessage(String mobileNo, List<SchoolWiseWhatsAppSMSModel> schoolWiseList, String message,
			Integer renewStudentId, Integer msgCount, String typeFlag, Integer saveId, String announcement,
			String imagePath, int num, int isDueFeeRemainder) {
		// TODO Auto-generated method stub

		String instanceId = schoolWiseList.get(0).getInstanceId();
		String accessToken = schoolWiseList.get(0).getAccessToken();
//		String messageFormat=schoolWiseList.get(0).getMessageFormat();	
//		String imagePath=schoolWiseList.get(0).getImagePath();
		String assignMsgCount = schoolWiseList.get(0).getAssignMsgCount();
		boolean flag = false;
		String mobileNoNew = "91" + Utility.convertOtherToEnglish(mobileNo);
//		String mobileNoNew="918149869838";
		String newMsg = "";
		String feemessage = "";
		if (isDueFeeRemainder == 0) {
			String newMssg[] = message.split(" ");
			for (String str : newMssg) {
				newMsg = newMsg.concat("+" + str);
			}
			newMsg = newMsg.concat("+:-");
			for (String str : announcement.split(" ")) {
				newMsg = newMsg.concat("+" + str);
			}
		} else {
			StudentMasterModel studentModel = studentMasterRepository.getStudent(renewStudentId,
					schoolWiseList.get(0).getSchoolMasterModel().getSchoolid());
			AppUserModel appUser = appUserRepository.getUserDetails(studentModel.getStudentId(),
					schoolWiseList.get(0).getSchoolMasterModel().getSchoolid());
			String convertedHex = Integer.toHexString(appUser.getAppUserRoleId());
			feemessage = "Dear " + studentModel.getStudFName() + " " + studentModel.getStudLName()
					+ ", you can pay your fees by click on below link : https://app.eprashasan.com/fees/?mb="
					+ studentModel.getMmobile() + "-" + convertedHex + "-" + saveId + "-1-1";
			String newMssg[] = feemessage.split(" ");
			for (String str : newMssg) {
				newMsg = newMsg.concat("+" + str);
			}
		}

		if (assignMsgCount == null) {
			flag = true;
		} else {
			if (Integer.parseInt(assignMsgCount) > msgCount) {
				flag = true;
			}
		}
		if (flag) {
			try {
				URL obj;
				if (imagePath.equalsIgnoreCase("0") || isDueFeeRemainder == 1) {
					obj = new URL("https://app.wappapi.in/api/send.php?number=" + mobileNoNew + "&type=text&message="
							+ newMsg + "&instance_id=" + instanceId + "&access_token=" + accessToken + "");
				} else {
					if (num == 0) {
						obj = new URL("https://app.wappapi.in/api/send.php?number=" + mobileNoNew
								+ "&type=media&message=" + newMsg + "&media_url=" + imagePath + "&instance_id="
								+ instanceId + "&access_token=" + accessToken + "");
					} else {
						obj = new URL("https://app.wappapi.in/api/send.php?number=" + mobileNoNew
								+ "&type=media&media_url=" + imagePath + "&instance_id=" + instanceId + "&access_token="
								+ accessToken + "");
					}
				}
				HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
				postConnection.addRequestProperty("User-Agent", "Chrome");
				postConnection.setRequestMethod("POST");
				postConnection.setDoOutput(true);
				postConnection.connect();
				if (postConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
					String inputLine;
					StringBuilder response = new StringBuilder();
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
//				    System.out.println("response.toString() = "+response.toString());
					if (!response.toString().equalsIgnoreCase(null) && response.toString() != null) {
						JSONObject jsonObj = new JSONObject(response.toString());
						if (jsonObj.getString("status").equalsIgnoreCase("success")) {
							if (num == 0) {
								SaveSendWhatsAppSMSModel saveSendWhatsAppSMSModel = new SaveSendWhatsAppSMSModel();
								DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
								Date dateobj = new Date();
								String date = df.format(dateobj);
								saveSendWhatsAppSMSModel.setDate(date);
								if (isDueFeeRemainder == 1) {
									saveSendWhatsAppSMSModel.setMessage(feemessage);
								} else {
									saveSendWhatsAppSMSModel.setMessage(message.concat(":- ").concat(announcement));
								}
								saveSendWhatsAppSMSModel.setMessageType("6");
								saveSendWhatsAppSMSModel.setTypeFlag(typeFlag);
								saveSendWhatsAppSMSModel.setRenewStaffId(renewStudentId);
								saveSendWhatsAppSMSModel
										.setSchoolId(schoolWiseList.get(0).getSchoolMasterModel().getSchoolid());
								saveSendWhatsAppSMSModel.setMobileNo(mobileNo);
//									saveSendWhatsAppSMSModel.setCompaignStatusFlag(0);
								AnnouncementModel announcementModel = new AnnouncementModel();
								announcementModel.setAnnouncementId(saveId);
								saveSendWhatsAppSMSModel.setAnnouncementModel(announcementModel);
								saveSendWhatsAppSMSRepository.saveAndFlush(saveSendWhatsAppSMSModel);
							}
						}
					}

				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) { // TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public Integer getWhatsAppApiDetails(Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			List<SchoolWiseWhatsAppSMSModel> schoolWiseList = globalWhatsAppSMSRepository
					.getSchoolWiseSetting(schoolId);
			if (schoolWiseList.size() > 0)
				return 1;
			else
				return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Integer updatedueFeeRemainderStatus(Integer announcementId, Integer renewId, String statusFlag) {
		// TODO Auto-generated method stub
		try {
			saveSendWhatsAppSMSRepository.updateFeeStatus(announcementId, renewId, Integer.parseInt(statusFlag));
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public Integer SendDueFeeRemainderMsg(Integer schoolId, Integer renewId) {
		// TODO Auto-generated method stub
		List<SchoolWiseWhatsAppSMSModel> schoolWiseList = globalWhatsAppSMSRepository.getSchoolWiseSetting(schoolId);
		String instanceId = schoolWiseList.get(0).getInstanceId();
		String accessToken = schoolWiseList.get(0).getAccessToken();
		String assignMsgCount = schoolWiseList.get(0).getAssignMsgCount();
		boolean flag = false;
		int msgCount = 0;
		StudentMasterModel studentModel = studentMasterRepository.getStudent(renewId,
				schoolWiseList.get(0).getSchoolMasterModel().getSchoolid());
		String mobileNoNew = "91" + Utility.convertOtherToEnglish(studentModel.getMmobile());
//		String mobileNoNew="918149869838";
		AppUserModel appUser = appUserRepository.getUserDetails(studentModel.getStudentId(),
				schoolWiseList.get(0).getSchoolMasterModel().getSchoolid());
		String convertedHex = Integer.toHexString(appUser.getAppUserRoleId());
		String feemessage = "Dear " + studentModel.getStudFName() + " " + studentModel.getStudLName()
				+ " you can pay your fees by click on below link : https://app.eprashasan.com/fees/?mb="
				+ mobileNoNew + "-" + convertedHex + "-0-1-1";
		String newMssg[] = feemessage.split(" ");
		String newMsg = "";
		for (String str : newMssg) {
			newMsg = newMsg.concat("+" + str);
		}

		WhatsAppMessageBean whatsAppMessageBean = new WhatsAppMessageBean();
		whatsAppMessageBean.setAccessToken(accessToken);
		whatsAppMessageBean.setInstanceId(instanceId);
		whatsAppMessageBean.setAssignMsgCount(assignMsgCount);
		whatsAppMessageBean.setMobileNo(Utility.convertOtherToEnglish(studentModel.getMmobile()));
		whatsAppMessageBean.setMessage(feemessage);
		whatsAppMessageBean.setEncryptedmessage(newMsg);
		whatsAppMessageBean.setMessageType("6");
		whatsAppMessageBean.setTypeFlag("1");
		whatsAppMessageBean.setRenewId(renewId);
		whatsAppMessageBean.setSchoolId(schoolId);
		whatsAppMessageBean.setIsMediaType(0);
		try {
			ObjectMapper Obj = new ObjectMapper();
			String serverUrl = "3.109.35.243:8073";
//			String serverUrl="localhost:8073";
			String generateMastersUrl = "http://" + serverUrl + "/sendWhatsAppMessageNotification?isSendWhatsAppMsg=1";
			URL url = new URL(generateMastersUrl);
//			http://localhost:8083/sendWhatsAppMessageNotification
			HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
			postConnection.setRequestMethod("POST");
			postConnection.setRequestProperty("Content-Type", "application/json");
			postConnection.setDoOutput(true);
			postConnection.connect();
			OutputStream os = postConnection.getOutputStream();
			String jsonStr = StringUtils.EMPTY;
			try {
				jsonStr = Obj.writeValueAsString(whatsAppMessageBean);
//				System.out.println(jsonStr);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			os.write(jsonStr.getBytes());
			os.flush();
			os.close();
			int responseCode = postConnection.getResponseCode();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 1;
	}

	@Override
	public List<AnnouncementResponseBean> getStandardDivisionList(Integer announcementId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			List<AnnouncementResponseBean> finalList = new ArrayList<>();
			List<AnnouncementAssignToClassDivisionModel> classDivisionList = announcementAssignToClassDivisionRepository
					.findByAnnouncementAndSchoolId(announcementId, schoolId);
			if (classDivisionList.size() > 0) {
				classDivisionList.stream().forEach(divisionList -> {
					AnnouncementResponseBean responseBean = new AnnouncementResponseBean();
					responseBean.setStandardDivisionId(divisionList.getStandardMasterModel().getStandardId() + "-"
							+ divisionList.getDivisionMasterModel().getDivisionId());
					String standardName = standardMasterRepository
							.getStandard(divisionList.getStandardMasterModel().getStandardId());
					String divisionName = divisionMasterRepository
							.getDivision(divisionList.getDivisionMasterModel().getDivisionId());
					responseBean.setStandardDivision(standardName + "-" + divisionName);
					List<AnnouncementResponseBean> studentCountList = announcementRepository
							.getAnnouncementStudentcount(divisionList.getStandardMasterModel().getStandardId(),
									divisionList.getDivisionMasterModel().getDivisionId(),
									divisionList.getYearMasterModel().getYearId(), schoolId, announcementId);
					responseBean.setShowcount(true);
					responseBean.setCompleteCount(studentCountList.get(0).getCompleteCount());
					responseBean.setDeliveredCount(studentCountList.get(0).getDeliveredCount());
					responseBean.setSeenCount(studentCountList.get(0).getSeenCount());
					responseBean.setUnseenCount(studentCountList.get(0).getUnseenCount());
					responseBean.setTotalStudentCount(studentCountList.get(0).getTotalStudentCount());
					finalList.add(responseBean);
				});
			}
			return finalList;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ArrayList<>();
		}

	}

	@Override
	public List<StudentDetailsBean> studentListByStandardDivision(Integer announcementId, Integer standardId,
			Integer divisionId, Integer yearId, Integer schoolId) {
		// TODO Auto-generated method stub
		List<StudentDetailsBean> studentDetailsList = announcementRepository
				.studentListByStandardDivision(announcementId, standardId, divisionId, yearId, schoolId);
		studentDetailsList.stream().forEach(student -> {
			student.setPhotoUrl(setStudentPhotoUrl(student.getSchoolId(), student.getRegNo(), student.getGrBookId()));
		});
		return studentDetailsList;
	}

	@Override
	public Integer saveAnnounceMentView(AnnouncementViewBean announcementViewBean) {

		AnnouncementViewsModel announcementViewsModel = new AnnouncementViewsModel();

		SchoolMasterModel schoolMaster = schoolRepo.findBySchoolid(announcementViewBean.getSchoolid());
		String globalDbSchoolKey = "1".concat("" + schoolMaster.getSchoolKey());

		if (announcementViewBean.getViewsId() != 0) {
			announcementViewsModel.setViewsId(announcementViewBean.getViewsId());
		}

		AppUserModel appUserModel = new AppUserModel();
		appUserModel.setAppUserRoleId(announcementViewBean.getAppUserId());
		announcementViewsModel.setAppUserModel(appUserModel);

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(announcementViewBean.getSchoolid());
		announcementViewsModel.setSchoolMasterModel(schoolMasterModel);

		AnnouncementModel announcementModel = new AnnouncementModel();
		announcementModel.setAnnouncementId(announcementViewBean.getAnnouncementId());
		announcementViewsModel.setAnnouncementModel(announcementModel);

		return announcementViewRepository.save(announcementViewsModel).getViewsId();
	}

	@Override
	public Integer saveAnnnouncementLikeOrDislike(AnnnouncementLikeOrDislikeBean annnouncementLikeOrDislikeBean) {

		AnnouncementLikeorDislikeModel announcementLikeorDislikeModel = new AnnouncementLikeorDislikeModel();

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(annnouncementLikeOrDislikeBean.getSchoolid());
		announcementLikeorDislikeModel.setSchoolMasterModel(schoolMasterModel);

		AnnouncementModel announcementModel = new AnnouncementModel();
		announcementModel.setAnnouncementId(annnouncementLikeOrDislikeBean.getAnnouncementId());
		announcementLikeorDislikeModel.setAnnouncementModel(announcementModel);

		AppUserModel appUserModel = new AppUserModel();
		appUserModel.setAppUserRoleId(annnouncementLikeOrDislikeBean.getAppUserId());
		announcementLikeorDislikeModel.setAppUserModel(appUserModel);

		announcementLikeorDislikeModel.setLikeDislike(annnouncementLikeOrDislikeBean.getLikeDislike());

		if (annnouncementLikeOrDislikeBean.getLikeId() != 0) {
			announcementLikeorDislikeModel.setLikeId(annnouncementLikeOrDislikeBean.getLikeId());

		}
		return likeDislikeRepo.save(announcementLikeorDislikeModel).getLikeId();
	}

	@Override
	public Integer saveComment(AnnouncementComentBean announcementCommentBean) {
		AnnouncementCommentModel announcementCommentModel = new AnnouncementCommentModel();
		if (announcementCommentBean.getCommentId() != 0) {
			announcementCommentModel.setCommentId(announcementCommentBean.getCommentId());
		}
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(announcementCommentBean.getSchoolid());
		announcementCommentModel.setSchoolMasterModel(schoolMasterModel);
		AnnouncementModel announcementModel = new AnnouncementModel();
		announcementModel.setAnnouncementId(announcementCommentBean.getAnnouncementId());
		announcementCommentModel.setAnnouncementModel(announcementModel);
		AppUserModel appUserModel = new AppUserModel();
		appUserModel.setAppUserRoleId(announcementCommentBean.getAppUserId());
		announcementCommentModel.setAppUserModel(appUserModel);
		announcementCommentModel.setComment(announcementCommentBean.getComment());
		return commentRepo.save(announcementCommentModel).getCommentId();
	}

	@Override
	public Integer saveReaction(List<AnnouncementReactionBean> announcementReaction) {

		for (AnnouncementReactionBean announcementReactionBean : announcementReaction) {
			AnnouncementReactionModel announcementReactionModel = new AnnouncementReactionModel();
			if (announcementReactionBean.getReactionId() != 0) {
				announcementReactionModel.setReactionId(announcementReactionBean.getReactionId());
			}
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(announcementReactionBean.getSchoolid());
			announcementReactionModel.setSchoolMasterModel(schoolMasterModel);
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(announcementReactionBean.getAnnouncementId());
			announcementReactionModel.setAnnouncementModel(announcementModel);
			AppUserModel appUserModel = new AppUserModel();
			appUserModel.setAppUserRoleId(announcementReactionBean.getAppUserId());
			announcementReactionModel.setAppUserModel(appUserModel);

			AnnouncmenetQuestionsModel announcmenetQuestionsModel = new AnnouncmenetQuestionsModel();
			announcmenetQuestionsModel.setAnnouncementQuestionsId(announcementReactionBean.getQuestionId());
			announcementReactionModel.setAnnouncmenetQuestionsModel(announcmenetQuestionsModel);

			AnnouncmentOptionsOfQuestionsModel announcmentOptionsOfQuestionsModel = new AnnouncmentOptionsOfQuestionsModel();
			announcmentOptionsOfQuestionsModel
					.setAnnouncementOptionsofQuestionsId(announcementReactionBean.getOptionId());
			announcementReactionModel.setAnnouncmentOptionsOfQuestionsModel(announcmentOptionsOfQuestionsModel);

			reactionRepo.save(announcementReactionModel);

		}
		return 1;

	}

	@Override
	public List<AnnouncementResponseBean> getAnnouncementAuthorityusingGroupId(Integer schoolId, Integer groupId) {
		List<AnnouncmenetApprovalAuthorityModel> approvalList = announcementApprovalAuthorityRepository
				.getApprovalList(schoolId, groupId);
		List<AnnouncementResponseBean> authorityApprovalList = new ArrayList<AnnouncementResponseBean>();
		approvalList.parallelStream().forEach(group -> {
			AnnouncementResponseBean announcementResponseBean = new AnnouncementResponseBean();
			announcementResponseBean.setGroupId(group.getAnnouncementGroupModel().getAnnouncementGroupId());
			announcementResponseBean.setAnnouncementapprovalAuthorityId(group.getAnnouncementapprovalAuthorityId());
			announcementResponseBean.setStaffUserId(group.getAppUserModel().getAppUserRoleId());
			announcementResponseBean.setSchoolId(group.getSchoolMasterModel().getSchoolid());
			authorityApprovalList.add(announcementResponseBean);
		});
		return authorityApprovalList;

	}

	@Override
	public Integer submitAnnouncementApprovalStatus(SubmitApprovalBean studentAnnouncementBean) {

		
			AnnouncementApprovalStatusModel announcementApprovalStatusModel = new AnnouncementApprovalStatusModel();
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(studentAnnouncementBean.getAnnouncementId());

			AppUserModel appUserModel = new AppUserModel();
			appUserModel.setAppUserRoleId(studentAnnouncementBean.getAppuserId());

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentAnnouncementBean.getSchoolId());

			announcementApprovalStatusModel.setAnnouncementModel(announcementModel);
			announcementApprovalStatusModel.setAppUserModel(appUserModel);
			announcementApprovalStatusModel.setSchoolMasterModel(schoolMasterModel);
			announcementApprovalStatusModel.setStatus(studentAnnouncementBean.getStatus());
			announcementApprovalStatusModel.setRemark(studentAnnouncementBean.getRemark());

			
			Integer id=announcementApprovalStatusRepository.save(announcementApprovalStatusModel).getAnnouncementapprovalStatusId();

			
			announcementRepository.updateFlah(studentAnnouncementBean.getAnnouncementId());

			
//			List<AnnouncementApprovalResponseBean> getApprovalStatusList = getApprovalStatusOfAnnouncement(
//					studentAnnouncementBean.getSchoolId(), studentAnnouncementBean.getAnnouncementId());
			
//			List<AnnouncementApprovalResponseBean> approvalList = getApprovalStatusList.parallelStream()
//					.filter(g -> g.getStatus().equalsIgnoreCase("0")).collect(Collectors.toList());
//			if (approvalList.size() == 0) {
//				return studentAnnouncementBean.getAnnouncementId();
//			} else {
//				return 1;
//			}
			return id;
		
	}

	@Override
	public List<AnnouncementCommentResponseBean> getAnnouncementAllComment(Integer schoolId, Integer announcementId) {
		List<AnnouncementCommentModel> comments = commentRepo.findComments(schoolId, announcementId);
		List<AnnouncementCommentResponseBean> commentList = new ArrayList<AnnouncementCommentResponseBean>();
		comments.stream().forEach(comment -> {
			AnnouncementCommentResponseBean announcementCommentResponseBean = new AnnouncementCommentResponseBean();
			announcementCommentResponseBean.setAppUserId(comment.getAppUserModel().getAppUserRoleId());
			System.out.println(comment.getAppUserModel().getAppUserRoleId());
			try {
				AppUserModel appUser = appUserRepository.getUserDetailsWithUserId(
						comment.getAppUserModel().getAppUserRoleId(), comment.getSchoolMasterModel().getSchoolid());
				announcementCommentResponseBean.setFirstName(appUser.getFirstName());
				announcementCommentResponseBean.setLastName(appUser.getLastName());
//				announcementCommentResponseBean.setPhotoUrl(
//						"https://eprashasan1.s3.ap-south-1.amazonaws.com/maindb/icardImages/0001/1000100001/8149160393.jpg");
				
				String  image=null;
				if(!appUser.getRoleName().equals("ROLE_PARENT1")) {
					image=icardMasterRepository.getImageForStaff(appUser.getStaffId());
				}else {
					image=icardMasterRepository.getImageForStudent(appUser.getStaffId());

				} 
				announcementCommentResponseBean.setPhotoUrl(image);
				announcementCommentResponseBean.setSchoolId(schoolId);

			}catch(Exception e) {
				System.out.println(e);
			}
			announcementCommentResponseBean.setSchoolId(comment.getSchoolMasterModel().getSchoolid());
			announcementCommentResponseBean.setCommentId(comment.getCommentId());
			announcementCommentResponseBean.setComment(comment.getComment());
			commentList.add(announcementCommentResponseBean);
		});

		return commentList;
	}

	@Override
	public List<AnnouncementLikeResponseBean> getAnnouncementAllLikes(Integer schoolId, Integer announcementId) {
		List<AnnouncementLikeorDislikeModel> likes = likeDislikeRepo.findLikes(schoolId, announcementId);
		List<AnnouncementLikeResponseBean> commentList = new ArrayList<AnnouncementLikeResponseBean>();
		likes.stream().forEach(comment -> {
			if (comment.getLikeDislike().equalsIgnoreCase("1")) {
				AnnouncementLikeResponseBean announcementCommentResponseBean = new AnnouncementLikeResponseBean();
				announcementCommentResponseBean.setAppUserId(comment.getAppUserModel().getAppUserRoleId());
				AppUserModel appUser = appUserRepository.getUserDetailsWithUserId(
						comment.getAppUserModel().getAppUserRoleId(), comment.getSchoolMasterModel().getSchoolid());
				announcementCommentResponseBean.setFirstName(appUser.getFirstName());
				announcementCommentResponseBean.setLastName(appUser.getLastName());
				String  image=null;
				if(!appUser.getRoleName().equals("ROLE_PARENT1")) {
					image=icardMasterRepository.getImageForStaff(appUser.getStaffId());
				}else {
					image=icardMasterRepository.getImageForStudent(appUser.getStaffId());

				}
				
				   

				
				announcementCommentResponseBean.setPhotoUrl(image);
				announcementCommentResponseBean.setSchoolId(comment.getSchoolMasterModel().getSchoolid());
				announcementCommentResponseBean.setLikeId(comment.getLikeId());
				commentList.add(announcementCommentResponseBean);
			}
		});

		return commentList;
	}

	@Override
	public List<AnnouncementViewResponseBean> getAnnouncementAllViews(Integer schoolId, Integer announcementId) {
		List<AnnouncementViewsModel> likes = announcementViewRepository.findViews(schoolId, announcementId);
		List<AnnouncementViewResponseBean> commentList = new ArrayList<AnnouncementViewResponseBean>();
		likes.stream().forEach(comment -> {
			AnnouncementViewResponseBean announcementCommentResponseBean = new AnnouncementViewResponseBean();
			Integer appUserId=comment.getAppUserModel().getAppUserRoleId();
			if(appUserId!=null && !appUserId.equals(0)) {
			announcementCommentResponseBean.setAppUserId(appUserId);
			AppUserModel appUser = appUserRepository.getUserDetailsWithUserId(
					appUserId, comment.getSchoolMasterModel().getSchoolid());
			announcementCommentResponseBean.setFirstName(appUser.getFirstName());
			announcementCommentResponseBean.setLastName(appUser.getLastName());
			
			String  image=null;
			String roleName=appUser.getRoleName();
			if(!roleName.equals("ROLE_PARENT1")) {
				image=icardMasterRepository.getImageForStaff(appUser.getStaffId());
			}else {
				image=icardMasterRepository.getImageForStudent(appUser.getStaffId());

			}
			announcementCommentResponseBean.setPhotoUrl(image);
			
			announcementCommentResponseBean.setSchoolId(comment.getSchoolMasterModel().getSchoolid());
			announcementCommentResponseBean.setViewsId(comment.getViewsId());
			commentList.add(announcementCommentResponseBean);
			}
		});

		return commentList;
	}

	@Override
	public Integer saveAnnouncementUserToGroup(AnnouncementUserToGroupBean announcementUserToGroupBean) {

		for (Integer groupId : announcementUserToGroupBean.getGroupId()) {
			AnnouncementUserToGroupMappingModel announcementUserToGroupMappingModel = new AnnouncementUserToGroupMappingModel();
			AppUserModel appUserModel = new AppUserModel();
			appUserModel.setAppUserRoleId(announcementUserToGroupBean.getAppUserId());
			announcementUserToGroupMappingModel.setAppUserModel(appUserModel);
			AnnouncementGroupModel announcementGroupModel = new AnnouncementGroupModel();
			announcementGroupModel.setAnnouncementGroupId(groupId);
			announcementUserToGroupMappingModel.setAnnouncementGroupModel(announcementGroupModel);
			userToGoupMappingRepo.save(announcementUserToGroupMappingModel);
		}
		return 1;
	}

	@Override
	public List<AnnouncementResponseBean> getAdminAnnouncement(Integer userId, Integer offset, Integer limit,
			Integer typeId, Integer schoolId) {
		if (limit == null || limit == 0) {
			limit = 10;
		}
		if (offset == null || offset == 0) {
			offset = 0;
		}
		List<AnnouncementResponseBean> announcementList1 = new ArrayList<>();
		if (typeId != 0) {
			announcementList1 = announcementRepository.getAdminAnnouncement(userId, typeId);
		} else {
			announcementList1 = announcementRepository.getAdminAnnouncement(userId);
			for (AnnouncementResponseBean announcementResponseBean : announcementList1) {
				announcementResponseBean.setAnnouncementTypeId(0);
				announcementResponseBean.setAnnouncementTypeName("");
			}
			
		}
		return sendAnnouncementList(announcementList1, offset, limit, schoolId, userId);
	}

	@Override
	public List<AnnouncementResponseBean> getannoucementforapproval(Integer appUserId, Integer offset, Integer limit,
			Integer typeId, Integer schoolId) {
		if (limit == null || limit == 0) {
			limit = 10;
		}
		if (offset == null || offset == 0) {
			offset = 0;
		}
		List<Integer> postIds = announcementApprovalAuthorityStatusRepository.getPostIds(schoolId, appUserId);
		List<AnnouncementResponseBean> announcementList1 = new ArrayList<AnnouncementResponseBean>();
		for (Integer postId : postIds) {
			
			List<String> approvedStatus =announcementApprovalStatusRepository.getApprovalStatus(appUserId,schoolId,postId);
			AnnouncementResponseBean getAnnouncements = new AnnouncementResponseBean();
			if (typeId != 0) {
				getAnnouncements = announcementRepository.getAnnouncements(postId, typeId);
				
				
				if(CollectionUtils.isNotEmpty(approvedStatus)) {
				getAnnouncements.setApprovedStatus(approvedStatus.get(0));
				}else {
					getAnnouncements.setApprovedStatus("notApproved");	
				}
			} else {
				getAnnouncements = announcementRepository.getAnnouncements(postId);
				
					if(CollectionUtils.isNotEmpty(approvedStatus)) {
					getAnnouncements.setApprovedStatus(approvedStatus.get(0));
					}else {
						getAnnouncements.setApprovedStatus("not approved");	
					}
			}
			announcementList1.add(getAnnouncements);
		}
		return sendAnnouncementList(announcementList1, offset, limit, schoolId, appUserId);
	}

	@Override
	public List<AnnouncementApprovalResponseBean> getApprovalStatusOfAnnouncement(Integer schoolId, Integer postId) {
		List<AnnouncementApprovalStatusModel> approvalList = new ArrayList<AnnouncementApprovalStatusModel>();
		approvalList = announcementApprovalStatusRepository.approvalList(schoolId, postId);
		List<AnnouncementApprovalResponseBean> approvalResponseList = new ArrayList<AnnouncementApprovalResponseBean>();
		approvalList.parallelStream().forEach(approval -> {
			AnnouncementApprovalResponseBean announcementApprovalResponseBean = new AnnouncementApprovalResponseBean();
			announcementApprovalResponseBean.setApprovalStatusId(approval.getAnnouncementapprovalStatusId());
			announcementApprovalResponseBean.setRemark(approval.getRemark());
			announcementApprovalResponseBean.setStatus(approval.getStatus());
			announcementApprovalResponseBean.setAppuserId(approval.getAppUserModel().getAppUserRoleId());
			announcementApprovalResponseBean.setSchoolId(approval.getSchoolMasterModel().getSchoolid());
			announcementApprovalResponseBean.setPostId(approval.getAnnouncementModel().getAnnouncementId());
			approvalResponseList.add(announcementApprovalResponseBean);
		});
		return approvalResponseList;
	}

	@Override
	public Integer sendPostForApproval(Integer schoolId, Integer postId) {

		List<Integer> groupIdList = announcementForGroupRepository.getGroupId(postId);

//		if(groupIdList.size() == 0) {
//		groupIdList.parallelStream().forEach(group -> {
//			List<AnnouncmenetApprovalAuthorityModel> approvalList = announcementApprovalAuthorityRepository
//					.getApprovalList(schoolId, group);
//			approvalList.parallelStream().forEach(approve -> {
//
//				AnnouncementApprovalStatusModel announcementApprovalStatusModel = new AnnouncementApprovalStatusModel();
//				AnnouncementModel announcementModel = new AnnouncementModel();
//				announcementModel.setAnnouncementId(postId);
//
//				AppUserModel appUserModel = new AppUserModel();
//				appUserModel.setAppUserRoleId(approve.getAppUserModel().getAppUserRoleId());
//
//				SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
//				schoolMasterModel.setSchoolid(schoolId);
//
//				announcementApprovalStatusModel.setAnnouncementModel(announcementModel);
//				announcementApprovalStatusModel.setAppUserModel(appUserModel);
//				announcementApprovalStatusModel.setSchoolMasterModel(schoolMasterModel);
//				announcementApprovalStatusModel.setStatus("0");
//				announcementApprovalStatusModel.setRemark("Pending");
//
//				announcementApprovalAuthorityStatusRepository.save(announcementApprovalStatusModel);
//
//			});
//		});
//		} else {
			Integer principalId = announcementForGroupRepository.getprincipalUserId(schoolId);
			AnnouncementApprovalStatusModel announcementApprovalStatusModel = new AnnouncementApprovalStatusModel();
			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(postId);

			AppUserModel appUserModel = new AppUserModel();
			appUserModel.setAppUserRoleId(principalId);

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);

			announcementApprovalStatusModel.setAnnouncementModel(announcementModel);
			announcementApprovalStatusModel.setAppUserModel(appUserModel);
			announcementApprovalStatusModel.setSchoolMasterModel(schoolMasterModel);
			announcementApprovalStatusModel.setStatus("0");
			announcementApprovalStatusModel.setRemark("Pending");

			announcementApprovalAuthorityStatusRepository.save(announcementApprovalStatusModel);
//		}
			announcementRepository.updateSendForApproval(postId);

		return 1;
	}

	@Override
	public List<AnnouncementQuestionAnswersBean> getQuestionsOptionsAnswers(Integer schoolId, Integer announcementId,
			Integer appUserId) {

		List<AnnouncementQuestionAnswersBean> questionAnswerList = new ArrayList<AnnouncementQuestionAnswersBean>();
		List<AnnouncmenetQuestionsModel> questions = announcementQuestionRepository.getAllQuestions(schoolId,
				announcementId);
		questions.stream().forEach(question -> {
			List<AnnouncmentOptionsOfQuestionsModel> options1 = announcementOptionsForQuestionsRepository
					.getAllOptions(schoolId, question.getAnnouncementQuestionsId());
			List<AnnouncementOptionOfQuestionBean> options = new ArrayList<>();
			options1.stream().forEach(option -> {
				AnnouncementOptionOfQuestionBean announcementOptionOfQuestionBean = new AnnouncementOptionOfQuestionBean();
				announcementOptionOfQuestionBean.setAnnouncementId(option.getAnnouncementModel().getAnnouncementId());
				announcementOptionOfQuestionBean
						.setAnnouncementOptionOfQuestionId(option.getAnnouncementOptionsofQuestionsId());
				announcementOptionOfQuestionBean
						.setAnnouncementQuestionId(option.getAnnouncmenetQuestionsModel().getAnnouncementQuestionsId());
				announcementOptionOfQuestionBean.setSchoolid(schoolId);
				announcementOptionOfQuestionBean.setOptions(option.getOptions());
				options.add(announcementOptionOfQuestionBean);
			});
			AnnouncementQuestionAnswersBean announcementQuestionAnswersBean = new AnnouncementQuestionAnswersBean();
			announcementQuestionAnswersBean.setQuestion(question.getQuestions());
			announcementQuestionAnswersBean.setQuestionId(question.getAnnouncementQuestionsId());
			announcementQuestionAnswersBean.setOptionList(options);
			List<AnnouncementReactionModel> reactionList = new ArrayList<>();
			if (appUserId == null || appUserId == 0) {
				reactionList = reactionRepo.getOptionId(schoolId, question.getAnnouncementQuestionsId());
			} else {
				reactionList = reactionRepo.getOptionId(schoolId, question.getAnnouncementQuestionsId(), appUserId);
			}
			if (reactionList != null) {
				reactionList.stream().forEach(reaction -> {
					announcementQuestionAnswersBean
							.setAnswer(reaction.getAnnouncmentOptionsOfQuestionsModel().getOptions());
					announcementQuestionAnswersBean.setAnswerId(reaction.getReactionId());
					announcementQuestionAnswersBean.setAnswerOptionId(reaction.getAnnouncmentOptionsOfQuestionsModel().getAnnouncementOptionsofQuestionsId());
				});
			}else {
				announcementQuestionAnswersBean.setAnswer("");
				announcementQuestionAnswersBean.setAnswerId(0);
				announcementQuestionAnswersBean.setAnswerOptionId(0);

			}
			questionAnswerList.add(announcementQuestionAnswersBean);
		});
		return questionAnswerList;
	}

	@Override
	public List<GroupBean> getListofGroupForUser(Integer appUserId) {
		List<GroupBean> groupBean = userToGoupMappingRepo.getGroups(appUserId);
		return groupBean;

	}

	private List<AnnouncementResponseBean> sendAnnouncementList(List<AnnouncementResponseBean> announcementList1,
			Integer offset, Integer limit, Integer schoolId, Integer appUserId) {
		List<AnnouncementResponseBean> announcementList = new ArrayList<>();
		announcementList = announcementList1.parallelStream().skip(offset).limit(limit).collect(Collectors.toList());
		announcementList.stream().forEach(announcement -> {
			
			try {
				List<AnnouncementCommentModel> comments = commentRepo.findComments(schoolId,
						announcement.getAnnouncementId());
				
				AnnouncementCommentResponseBean announcementCommentResponseBean = new AnnouncementCommentResponseBean();
				AppUserModel appUser = appUserRepository.getUserDetailsWithUserId(comments.get(0).getAppUserModel().getAppUserRoleId(), schoolId);
				announcementCommentResponseBean.setAppUserId(comments.get(0).getAppUserModel().getAppUserRoleId());

				announcementCommentResponseBean.setFirstName(appUser.getFirstName());
				announcementCommentResponseBean.setLastName(appUser.getLastName());
				
				String  image=null;
				if(!appUser.getRoleName().equals("ROLE_PARENT1")) {
					image=icardMasterRepository.getImageForStaff(appUser.getStaffId());
				}else {
					image=icardMasterRepository.getImageForStudent(appUser.getStaffId());

				}

				announcementCommentResponseBean.setPhotoUrl(image);
				announcementCommentResponseBean.setSchoolId(schoolId);
				
				announcementCommentResponseBean.setCommentId(comments.get(0).getCommentId());
				announcementCommentResponseBean.setComment(comments.get(0).getComment());
				announcement.setLastComment(announcementCommentResponseBean);
				announcement.setTotalCommentCount(comments.size());

				comments.stream().forEach(commentmodule -> {
					if (commentmodule.getAppUserModel().getAppUserRoleId().equals(appUserId)) {
						announcement.setUserComment(commentmodule.getComment());
					}
				});

			} catch (Exception e) {
				System.out.println(e);
			}

			List<AnnouncementLikeorDislikeModel> likes = likeDislikeRepo.findLikes(schoolId,
					announcement.getAnnouncementId());
			announcement.setTotalLikes(likes.size());
			likes.stream().forEach(likemodule -> {
				if (likemodule.getAppUserModel().getAppUserRoleId().equals(appUserId)) {
					announcement.setUserlikestatus(true);
				}
			});
			List<AnnouncementViewsModel> viewList = announcementViewRepository.findViews(schoolId,
					announcement.getAnnouncementId());
			announcement.setTotalViewsCount(viewList.size());
			viewList.stream().forEach(view -> {
				if (view.getAppUserModel().getAppUserRoleId().equals(appUserId)) {
					announcement.setUserViewStatus(true);
				}
			});

			announcement.setPostedTime(announcement.getPostedTime());
			announcement.setIsfeedback(announcement.getIsfeedback());
			announcement.setUserCanComment(announcement.getUserCanComment());
			announcement.setPublishFlag(announcement.getPublishFlag());
			announcement.setSendForApproval(announcement.getSendForApproval());

			List<AnnouncementResponseBean> attachmentList = announcementRepository
					.getAttachments(announcement.getAnnouncementId(), schoolId);
			attachmentList.stream().forEach(attachment -> {
				attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
				attachment.setFileName(attachment.getFileName());
			});
			announcement.setNoOfAttachments(Long.parseLong(""+attachmentList.size()));
			announcement.setAttachmentList(attachmentList);
		});
		return announcementList;
	}

	@Override
	public Integer saveAnnouncementView(List<AnnouncementViewBean> announViewBeans) {
		for (AnnouncementViewBean announcementViewBean : announViewBeans) {
			AnnouncementViewsModel announcementViewsModel = new AnnouncementViewsModel();
			if (announcementViewBean.getViewsId() != 0) {
				announcementViewsModel.setViewsId(announcementViewBean.getViewsId());
			}

			AppUserModel appUserModel = new AppUserModel();
			appUserModel.setAppUserRoleId(announcementViewBean.getAppUserId());
			announcementViewsModel.setAppUserModel(appUserModel);

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(announcementViewBean.getSchoolid());
			announcementViewsModel.setSchoolMasterModel(schoolMasterModel);

			AnnouncementModel announcementModel = new AnnouncementModel();
			announcementModel.setAnnouncementId(announcementViewBean.getAnnouncementId());
			announcementViewsModel.setAnnouncementModel(announcementModel);

			announcementViewRepository.save(announcementViewsModel);

		}
		return 1;
	}

	@Override
	public Runnable sendNotificationNew(SubmitApprovalBean studentAnnouncementBean) {
		
		
		String name=announcementRepository.getNameOfSender(studentAnnouncementBean.getAnnouncementId());
				
		
				List<Integer>StudentId =studentAnnouncementAssignedToRepository.getStudents(studentAnnouncementBean.getAnnouncementId(),studentAnnouncementBean.getSchoolId());
				
				List<Integer> staffId=studentAnnouncementAssignedToRepository.getStaffs(studentAnnouncementBean.getAnnouncementId(),studentAnnouncementBean.getSchoolId());
				
				if(CollectionUtils.isNotEmpty(staffId))
				StudentId.addAll(staffId);
				
				if(CollectionUtils.isNotEmpty(StudentId)) {
				List<Integer> appUserId=appUserRoleRepository.getAppUserId(StudentId);
				
				List<Tokenbean>fcmToken=androidFcmTokenRepository.getFcmToken(appUserId);
//				String message4=announcementRepository.getMessage(studentAnnouncementBean.getAnnouncementId());
//				String message1=message4.replace('"', '@');
//				String[] message2=message1.split("@:@");
//				String[] message3=message2[1].split("@");
				
				String message="Important Notice from "+name;
				System.out.println(message);

				try {
				
					if(!CollectionUtils.isEmpty(fcmToken)) {
				for (Tokenbean token : fcmToken) {
					if(!token.getToken().equalsIgnoreCase(null) && !token.getToken().equalsIgnoreCase("")) {
						String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
						URL url = new URL(FIREBASE_API_URL);
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();

						conn.setUseCaches(false);
						conn.setDoInput(true);
						conn.setDoOutput(true);

						conn.setRequestMethod("POST");
						conn.setRequestProperty("Authorization", "key=" + "AAAA1ToOLXI:APA91bHhYJ6qNYyccN8ExRJAHPPMGOeiXaUoiVgK83vBRC4sHn1_E_t-FYTfXn0hKvaaIgv9SXIdXBEIy3-mIQBbmO6S7TZYxIVacG2aXOSqm30UY-ipUkCtMYjHI86cnhmhLudDfgA1");
						conn.setRequestProperty("Content-Type", "application/json");
						
													
							JSONObject json = new JSONObject();
							json.put("to",token.getToken());

							JSONObject data = new JSONObject();
							data.put("mobileNo",token.getMobileNumber());
							
							data.put("title", message);
							data.put("body", "Plesase visit your App...");
							data.put("userId", token.getAppUserId().toString());
							data.put("menuId",7);
							data.put("menuName","Notice");
							data.put("SeenStataus", "0");
							json.put("data", data);
							
							
							OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
							wr.write(json.toString());
							wr.flush();

							BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
							String output;
							StringBuffer response = new StringBuffer();
							while ((output = br.readLine()) != null) {
								response.append(output);
							}
							br.close();
							JSONObject jsonObj=new JSONObject(response.toString());
						
//							SaveSendNotificationModel saveSendNotificationModel = new SaveSendNotificationModel();
//							saveSendNotificationModel.setMessageType("1");
//							saveSendNotificationModel.setTypeFlag("1");
//							saveSendNotificationModel.setRenewStaffId(token.getAppUserId());
//							saveSendNotificationModel.setSchoolId(studentAnnouncementBean.getSchoolId());
//							saveSendNotificationModel.setMobileNo(token.getMobileNumber());
//							saveSendNotificationModel.setMessage(message);
//							saveSendNotificationModel.setTitle("Notice");
//							saveSendNotificationModel.setMenuId(7);
//							saveSendNotificationModel.setSeenUnseenStatusFlag(0);
//							saveSendNotificationModel.setNotificationId(studentAnnouncementBean.getAnnouncementId());
//							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//							Date dateobj = new Date();
//							String date = df.format(dateobj);
//							saveSendNotificationModel.setDate(date);
//							
//							if(jsonObj.getInt("success") == 1) {
//								saveSendNotificationModel.setSuccessFailureFlag("1");
//							}else {
//								saveSendNotificationModel.setSuccessFailureFlag("0");
//							}
//							
//							//System.out.println("");
//							saveSendNotificationRepository.saveAndFlush(saveSendNotificationModel);
						
							SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentAnnouncementBean.getSchoolId());
							String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

							AndroidNotificationStatusModel androidNotificationStatusModel = new AndroidNotificationStatusModel();
							String statusIncrementedId = ""
									+ androidNotificationStatusRepository.getMaxId(studentAnnouncementBean.getSchoolId());
							String notificationStatusId = Utility.generatePrimaryKey(globalDbSchoolKey, statusIncrementedId);

							androidNotificationStatusModel.setNotificationStatusId(Integer.parseInt(notificationStatusId));
							androidNotificationStatusModel.setSchoolMasterModel(schoolModel);
							androidNotificationStatusModel.setJoinTable("announcement_assign_to");
							androidNotificationStatusModel.setJoinId("" + studentAnnouncementBean.getAnnouncementId());
							androidNotificationStatusModel.setNotificationType("Announcement");
							androidNotificationStatusModel.setStatus(1);
							androidNotificationStatusModel.setTitle(message);
							androidNotificationStatusModel.setMessage("plese check notice");
							androidNotificationStatusModel.setCount(Integer.parseInt(statusIncrementedId));
							androidNotificationStatusModel.setNotificationDate(new Date());
							Date date = new Date();
							Timestamp ts=new Timestamp(date.getTime());
							androidNotificationStatusModel.setcDate(ts);
							String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
							androidNotificationStatusModel.setNotificationTime(currentTime);
							androidNotificationStatusRepository.saveAndFlush(androidNotificationStatusModel);

							
						}
					}		
				}
				}catch(Exception e) {
					
					e.printStackTrace();
				}
				}
				

return null;

			
	
}

	
	
}	
