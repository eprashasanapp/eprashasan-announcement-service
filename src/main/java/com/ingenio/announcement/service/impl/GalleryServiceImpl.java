package com.ingenio.announcement.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ingenio.announcement.bean.FCMTokenBean;
import com.ingenio.announcement.bean.GalleryLabelBean;
import com.ingenio.announcement.bean.GalleryRequestBean;
import com.ingenio.announcement.bean.GalleryResponseBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.UploadFileBean;
import com.ingenio.announcement.bean.WhatsAppMessageBean;
import com.ingenio.announcement.config.ConfigurationProperties;
import com.ingenio.announcement.model.AndroidNotificationStatusModel;
import com.ingenio.announcement.model.AppLanguageModel;
import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.GalleryAssignToModel;
import com.ingenio.announcement.model.GalleryAttachmentModel;
import com.ingenio.announcement.model.GalleryModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.StaffBasicDetailsModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AndroidConfigurationRepository;
import com.ingenio.announcement.repository.AndroidNotificationStatusRepository;
import com.ingenio.announcement.repository.AssignSubjectToStudentRepository;
import com.ingenio.announcement.repository.GalleryAttachmentRepository;
import com.ingenio.announcement.repository.GalleryRepository;
import com.ingenio.announcement.repository.GlobalWhatsAppSMSRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.StaffBasicDetailRepository;
import com.ingenio.announcement.repository.StudentGalleryAssignedToRepository;
import com.ingenio.announcement.repository.StudentGalleryAttachmentRepository;
import com.ingenio.announcement.repository.StudentSmsAssignedToRepository;
import com.ingenio.announcement.repository.StudentStandardRenewRepository;
import com.ingenio.announcement.repository.UserAuthenticationRepository;
import com.ingenio.announcement.service.GalleryService;
import com.ingenio.announcement.util.CommonUtlitity;
import com.ingenio.announcement.util.CryptographyUtil;
import com.ingenio.announcement.util.HttpClientUtil;
import com.ingenio.announcement.util.Utility;


@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")



  @PropertySource("classpath:messages_en.properties")
  
  @PropertySource("classpath:messages_mr.properties")
  
  @PropertySource("classpath:messages_hn.properties")
 
 
public class GalleryServiceImpl implements GalleryService {

	@Autowired
	AssignSubjectToStudentRepository assignSubjectToStudentRepository;

	@Autowired
	StudentGalleryAttachmentRepository studentGalleryAttachmentRepository;

	@Autowired
	AndroidConfigurationRepository androidConfigurationRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	GalleryAttachmentRepository galleryAttachmentRepository;
	
	@Autowired
	GalleryRepository galleryRepository;

	@Autowired
	StudentStandardRenewRepository studentStandardRenewRepository;

	@Autowired
	StudentGalleryAssignedToRepository studentGalleryAssignedToRepository ;

	@Autowired
	StaffBasicDetailRepository staffBasicDetailRepository;

	@Autowired
	StudentSmsAssignedToRepository studentSmsAssignedToRepository;

	@Autowired
	UserAuthenticationRepository userAuthenticationRepository;
	

	CryptographyUtil cryptoUtil = new CryptographyUtil();
	Integer smssaveId = 0;
	
	@Autowired
	private Environment env;
	
	CryptographyUtil cry = new CryptographyUtil();
	
	@Autowired
	private AndroidNotificationStatusRepository androidNotificationStatusRepository;

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	CommonUtlitity commonUtlitity;
	
	@Autowired
	AmazonS3 amazonS3;
	
	@Autowired
	GlobalWhatsAppSMSRepository globalWhatsAppSMSRepository;
	
	Utility utility = new Utility();

	/*@Override
	public List<GalleryResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole,Integer yearId) {
		List<GalleryResponseBean> galleryResponseList = new ArrayList<>();
		GalleryResponseBean galleryResponseBean = new GalleryResponseBean();
		galleryResponseBean.setRoleName("Teacher");
		
		List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository.findStaff(schoolId);
		List<GalleryLabelBean> labelList = new ArrayList<>();
	
		if (CollectionUtils.isNotEmpty(staffList)) {
		staffList.parallelStream().forEach(staff -> {
		GalleryLabelBean galleryLabelBean = new GalleryLabelBean();
		galleryLabelBean.setId(""+staff.getstaffId());
		galleryLabelBean.setName(staff.getInitialname()+ " " + staff.getFirstname()+" " +staff.getSecondname()+" "+staff.getLastname());
	    galleryLabelBean.setRegNo(staff.getSregNo());
	    galleryLabelBean.setPhotoUrl(setStaffPhotoUrl(schoolId,galleryLabelBean.getRegNo()));
		labelList.add(galleryLabelBean);
		});
	}

		galleryResponseBean.setDisplayList(labelList);
		galleryResponseList.add(galleryResponseBean);

		List<StandardDivisionBean> standardDivisionList = galleryRepository.getStandardDivisionList(schoolId);
	
		List<GalleryLabelBean> labelList2 = new ArrayList<>();
		galleryResponseBean = new GalleryResponseBean();
		galleryResponseBean.setRoleName("Class");
		standardDivisionList.parallelStream().forEach(standardDivision -> {
			GalleryLabelBean galleryLabelBean2 = new GalleryLabelBean();
			galleryLabelBean2.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
			galleryLabelBean2.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
			List<StudentDetailsBean> studentList = studentStandardRenewRepository.findStudent1(yearId,standardDivision.getStandardId(),
					standardDivision.getDivisionId(), schoolId);
			galleryLabelBean2.setStudentDetailsList(studentList);
			labelList2.add(galleryLabelBean2);
			
		});
		galleryResponseBean.setDisplayList(labelList2);
		galleryResponseList.add(galleryResponseBean);

		return galleryResponseList;
	}
*/
	
//	@Override
//	public List<GalleryResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole,Integer yearId) {
//		List<GalleryResponseBean> galleryResponseList = new ArrayList<>();
//		GalleryResponseBean galleryResponseBean = new GalleryResponseBean();
//		galleryResponseBean.setRoleName("Teacher");
//		
//		List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository.findStaff(schoolId);
//		List<GalleryLabelBean> labelList = new ArrayList<>();
//	
//		if (CollectionUtils.isNotEmpty(staffList)) {
//		staffList.parallelStream().forEach(staff -> {
//		GalleryLabelBean galleryLabelBean = new GalleryLabelBean();
//		galleryLabelBean.setId(""+staff.getstaffId());
//		galleryLabelBean.setName(staff.getInitialname()+ " " + staff.getFirstname()+" " +staff.getSecondname()+" "+staff.getLastname());
//	    galleryLabelBean.setRegNo(staff.getSregNo());
//	    galleryLabelBean.setPhotoUrl(setStaffPhotoUrl(schoolId,galleryLabelBean.getRegNo()));
//		labelList.add(galleryLabelBean);
//		});
//	}
//
//		galleryResponseBean.setDisplayList(labelList);
//		galleryResponseList.add(galleryResponseBean);
//
//		List<StandardDivisionBean> standardDivisionList = galleryRepository.getStandardDivisionList(schoolId);
//	
//		List<GalleryLabelBean> labelList2 = new ArrayList<>();
//		galleryResponseBean = new GalleryResponseBean();
//		galleryResponseBean.setRoleName("Class");
//		standardDivisionList.parallelStream().forEach(standardDivision -> {
//			GalleryLabelBean galleryLabelBean2 = new GalleryLabelBean();
//			galleryLabelBean2.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
//			galleryLabelBean2.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
//			List<StudentStandardRenewModel> studentList = studentStandardRenewRepository.findStudent(yearId,standardDivision.getStandardId(),
//					standardDivision.getDivisionId(), schoolId);
//			List<StudentDetailsBean> studentDetailsList = new ArrayList<>();
//			if (CollectionUtils.isNotEmpty(studentList)) {
//				studentList.parallelStream().forEach(student -> {
//				StudentDetailsBean studentDetailsBean =new StudentDetailsBean();
//				StringBuilder fullname = new StringBuilder();
//				fullname.append(student.getStudentMasterModel().getStudFName());
//				fullname.append(" ");
//				fullname.append(student.getStudentMasterModel().getStudMName());
//				fullname.append(" ");
//				fullname.append(student.getStudentMasterModel().getStudLName());
//                String completeName = fullname.toString();
//				studentDetailsBean.setStudentName(completeName);
//				studentDetailsBean.setRollNo(student.getRollNo());
//				studentDetailsBean.setRegNo(student.getStudentMasterModel().getStudentRegNo());
//				studentDetailsBean.setGrBookId(student.getStudentMasterModel().getGrBookName().getGrBookId()); 
//				studentDetailsBean.setRenewStudentId(student.getRenewStudentId());
//				studentDetailsBean.setStudentId(student.getStudentMasterModel().getStudentId());
//				studentDetailsBean.setPhotoUrl(setStudentPhotoUrl(schoolId, studentDetailsBean.getRegNo(),studentDetailsBean.getGrBookId()));
//				studentDetailsList.add(studentDetailsBean);
//			});
//		}
//			galleryLabelBean2.setStudentDetailsList(studentDetailsList);
//			labelList2.add(galleryLabelBean2);
//			
//		});
//		galleryResponseBean.setDisplayList(labelList2);
//		galleryResponseList.add(galleryResponseBean);
//
//		return galleryResponseList;
//	}

	
	@Override
	public List<GalleryResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole,Integer yearId) {
		List<GalleryResponseBean> galleryResponseList = new ArrayList<>();
		GalleryResponseBean galleryResponseBean = new GalleryResponseBean();
		galleryResponseBean.setRoleName("Teacher");
		
		Date date = new Date();
		String currentDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
		List<StaffBasicDetailsModel> staffList = staffBasicDetailRepository.findStaff(schoolId,currentDate);
		List<GalleryLabelBean> labelList = new ArrayList<>();
	
		if (CollectionUtils.isNotEmpty(staffList)) {
		staffList.parallelStream().forEach(staff -> {
		GalleryLabelBean galleryLabelBean = new GalleryLabelBean();
		galleryLabelBean.setId(""+staff.getstaffId());
		String InitialName= staff.getInitialname();
		String FirstName = staff.getFirstname();
		String SecondName= staff.getSecondname();
		String LastName= staff.getLastname();
		if(InitialName==null || InitialName.length()==0 || InitialName.equalsIgnoreCase("Null")){
		 InitialName="";	
		}
		if(FirstName==null || FirstName.length()==0 || FirstName.equalsIgnoreCase("Null")){
			FirstName="";	
		}
		if(SecondName==null || SecondName.length()==0 || SecondName.equalsIgnoreCase("Null")){
			SecondName="";	
		}
		if(LastName==null || LastName.length()==0 || LastName.equalsIgnoreCase("Null")){
			LastName="";	
		}
		
		galleryLabelBean.setName(InitialName+ " " + FirstName+" " +SecondName+" "+LastName);
	    galleryLabelBean.setRegNo(staff.getSregNo());
	    galleryLabelBean.setPhotoUrl(setStaffPhotoUrl(schoolId,galleryLabelBean.getRegNo()));
		labelList.add(galleryLabelBean);
		});
	}

		galleryResponseBean.setDisplayList(labelList);
		galleryResponseList.add(galleryResponseBean);

		List<StandardDivisionBean> standardDivisionList = galleryRepository.getStandardDivisionList(schoolId);
		List<GalleryLabelBean> labelList2 = new ArrayList<>();
		galleryResponseBean = new GalleryResponseBean();
		galleryResponseBean.setRoleName("Class");
		standardDivisionList.stream().forEach(standardDivision -> {
			GalleryLabelBean galleryLabelBean2 = new GalleryLabelBean();
			galleryLabelBean2.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
			galleryLabelBean2.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
			List<StudentDetailsBean> studentList = studentStandardRenewRepository.findStudent1(yearId,standardDivision.getStandardId(),
					standardDivision.getDivisionId(), schoolId);
			galleryLabelBean2.setStudentDetailsList(studentList);
			labelList2.add(galleryLabelBean2);
		});
		galleryResponseBean.setDisplayList(labelList2);
		galleryResponseList.add(galleryResponseBean);

		
	
//		List<GalleryLabelBean> labelList2 = new ArrayList<>();
//		galleryResponseBean = new GalleryResponseBean();
//		galleryResponseBean.setRoleName("Class");
//		standardDivisionList.parallelStream().forEach(standardDivision -> {
//			GalleryLabelBean galleryLabelBean2 = new GalleryLabelBean();
//			galleryLabelBean2.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
//			galleryLabelBean2.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
//			List<StudentStandardRenewModel> studentList = studentStandardRenewRepository.findStudent(yearId,standardDivision.getStandardId(),
//					standardDivision.getDivisionId(), schoolId);
//			List<StudentDetailsBean> studentDetailsList = new ArrayList<>();
//			if (CollectionUtils.isNotEmpty(studentList)) {
//				studentList.parallelStream().forEach(student -> {
//				StudentDetailsBean studentDetailsBean =new StudentDetailsBean();
//				StringBuilder fullname = new StringBuilder();
//				fullname.append(student.getStudentMasterModel().getStudFName());
//				fullname.append(" ");
//				fullname.append(student.getStudentMasterModel().getStudMName());
//				fullname.append(" ");
//				fullname.append(student.getStudentMasterModel().getStudLName());
//                String completeName = fullname.toString();
//				studentDetailsBean.setStudentName(completeName);
//				studentDetailsBean.setRollNo(student.getRollNo());
//				studentDetailsBean.setRegNo(student.getStudentMasterModel().getStudentRegNo());
//				studentDetailsBean.setGrBookId(student.getStudentMasterModel().getGrBookName().getGrBookId()); 
//				studentDetailsBean.setRenewStudentId(student.getRenewStudentId());
//				studentDetailsBean.setStudentId(student.getStudentMasterModel().getStudentId());
//				studentDetailsBean.setPhotoUrl(setStudentPhotoUrl(schoolId, studentDetailsBean.getRegNo(),studentDetailsBean.getGrBookId()));
//				studentDetailsList.add(studentDetailsBean);
//			});
//		}
//			galleryLabelBean2.setStudentDetailsList(studentDetailsList);
//			labelList2.add(galleryLabelBean2);
//			
//		});
//		galleryResponseBean.setDisplayList(labelList2);
//		galleryResponseList.add(galleryResponseBean);

		return galleryResponseList;
	}



	@Override
	public synchronized Integer saveGalleryPost(GalleryRequestBean studentGalleryBean) {

		StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
		staffBasicDetailsModel.setstaffId(studentGalleryBean.getStaffId());

		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(studentGalleryBean.getYearId());

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentGalleryBean.getSchoolId());

		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentGalleryBean.getSchoolId());
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

		String galleryId = "0";
		galleryId = ""+studentGalleryBean.getGalleryId();
		if(StringUtils.isEmpty(galleryId) || galleryId.equals("0")) {
			String incrementedId = "" + galleryRepository.getMaxId(studentGalleryBean.getSchoolId());
			galleryId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);	
		}
		AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(studentGalleryBean.getUserId());
		Integer saveId = 0;

	//	if (studentGalleryBean.getApiFlag() != null && studentGalleryBean.getApiFlag() == 0) {
	//		saveId = sendGallery(studentGalleryBean, yearMasterModel, schoolMasterModel, globalDbSchoolKey,
	//				galleryId, appUserRoleModel);
	//	}
		
		GalleryModel galleryModel = new GalleryModel();
		galleryModel.setUserId(appUserRoleModel);
		galleryModel.setIpAddress(studentGalleryBean.getIPAddress());
		galleryModel.setDeviceType(studentGalleryBean.getDeviceType());
		galleryModel.setGalleryMessage(studentGalleryBean.getGalleryMessage());
		galleryModel.setGalleryTitle(studentGalleryBean.getGalleryTitle());
		galleryModel.setStartDate(studentGalleryBean.getStartDate());
		galleryModel.setSchoolMasterModel(schoolMasterModel);
		galleryModel.setStaffId(studentGalleryBean.getStaffId());
		galleryModel.setGalleryTime(studentGalleryBean.getTime());
		if(StringUtils.isNotEmpty(galleryId) && !galleryId.equals("0")) {
		galleryModel.setGalleryId(Integer.parseInt(galleryId));
		}
		saveId = galleryRepository.save(galleryModel).getGalleryId();
		
	 return saveId;
	}

	private Integer sendGallery(GalleryRequestBean studentGalleryBean, YearMasterModel yearMasterModel,
			SchoolMasterModel schoolMasterModel, String globalDbSchoolKey, String galleryId,AppUserRoleModel appUserRoleModel) {
//		Integer saveId;
//		List<String> androidTokenList = new ArrayList<>();
//		List<String> IOSTokenList = new ArrayList<>();
		
		
			GalleryModel galleryModel = new GalleryModel();
			galleryModel.setGalleryId(Integer.parseInt(galleryId));
			if (StringUtils.isNotEmpty(""+studentGalleryBean.getGalleryId()) && !(""+studentGalleryBean.getGalleryId()).equals("0")) {
				studentGalleryAssignedToRepository.deleteByGalleryModel(galleryModel);
			}
		
		
		
//		SchoolMasterModel schoolModel1 = schoolMasterRepository.findBySchoolid(studentGalleryBean.getSchoolId());
		/* String language = Utility.getLanguage(schoolModel1.getLanguage().getLanguageId());           
	    final String title=env.getProperty(language+"."+"GalleryPost");			
		
		 * String message1=env.getProperty(language+"."+"GalleryNotification");
		 * if(studentGalleryBean.getTime()==null) { String currentTime = new
		 * SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
		 * studentGalleryBean.setTime(currentTime); } final String message =
		 * message1.replaceAll("teacherName",studentGalleryBean.getTeacherName()).
		 * replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("notifTime",
		 * ""+studentGalleryBean.getTime());
		 */
	
		
		if (CollectionUtils.isNotEmpty(studentGalleryBean.getGalleryResponseList())) {
			studentGalleryBean.getGalleryResponseList().stream().forEach(responseBean -> {
					if (responseBean.getRoleName().equals("Teacher")) {
						List<GalleryLabelBean> staffList = responseBean.getDisplayList();
						staffList.stream().forEach(staff -> {
//							List<FCMTokenBean> tokenList = assignSubjectToStudentRepository.getTokenForTeacher(Integer.parseInt(staff.getId()));
							String galleryToIncrementedId = ""+ galleryRepository.getMaxAssignToId(studentGalleryBean.getSchoolId());
							String assignedToGalleryId = Utility.generatePrimaryKey(globalDbSchoolKey,galleryToIncrementedId);
							GalleryAssignToModel galleryAssignToModel = new GalleryAssignToModel();
							galleryAssignToModel.setRole(responseBean.getRoleName());
							galleryAssignToModel.setStaffStudentId(Integer.parseInt(staff.getId()));
							galleryAssignToModel.setGalleryStatus("0");
							galleryAssignToModel.setSchoolMasterModel(schoolMasterModel);
							galleryAssignToModel.setYearMasterModel(yearMasterModel);
							galleryAssignToModel.setGalleryModel(galleryModel);
							galleryAssignToModel.setGalleryAssignToId((Integer.parseInt(assignedToGalleryId)));
							studentGalleryAssignedToRepository.saveAndFlush(galleryAssignToModel);

//							if (CollectionUtils.isNotEmpty(tokenList)) {
//								for (int j = 0; j < tokenList.size(); j++) {
//									if (StringUtils.isNotEmpty(tokenList.get(j).getToken()) && tokenList.get(j).getDeviceType().equals("2")) {
//										androidTokenList.add(tokenList.get(j).getToken());
//									}
//									else if (StringUtils.isNotEmpty(tokenList.get(j).getToken()) && tokenList.get(j).getDeviceType().equals("3")) {
//										IOSTokenList.add(tokenList.get(j).getToken());
//									}
//
//								}
//							}
							
						});
						
					} else if (responseBean.getRoleName().equals("Class")) {
					if (CollectionUtils.isNotEmpty(responseBean.getDisplayList())) {
						for(int i=0;i<responseBean.getDisplayList().size();i++) {
					    List<StudentDetailsBean> studentList = responseBean.getDisplayList().get(i).getStudentDetailsList();		
						String standardId = responseBean.getDisplayList().get(i).getId().split("-")[0];
						String divisionId = responseBean.getDisplayList().get(i).getId().split("-")[1];
						if (CollectionUtils.isNotEmpty(studentList)) {
							studentList.stream().forEach(student -> {
//								List<FCMTokenBean> tokenList = assignSubjectToStudentRepository.getToken(student.getRenewStudentId());
								String galleryToIncrementedId = "" + galleryRepository.getMaxAssignToId(studentGalleryBean.getSchoolId());
								String assignedToGalleryId = Utility.generatePrimaryKey(globalDbSchoolKey,galleryToIncrementedId);
								GalleryAssignToModel galleryAssignToModel = new GalleryAssignToModel();
								galleryAssignToModel.setRole("Parent");
								galleryAssignToModel.setStaffStudentId(student.getRenewStudentId());
								galleryAssignToModel.setGalleryStatus("0");
								galleryAssignToModel.setSchoolMasterModel(schoolMasterModel);
								galleryAssignToModel.setYearMasterModel(yearMasterModel);
								galleryAssignToModel.setGalleryModel(galleryModel);
								galleryAssignToModel.setGalleryAssignToId(Integer.parseInt(assignedToGalleryId));
								DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
								divisionMasterModel.setDivisionId(Integer.parseInt(divisionId));
								galleryAssignToModel.setDivisionMasterModel(divisionMasterModel);
								StandardMasterModel standardMasterModel = new StandardMasterModel();
								standardMasterModel.setStandardId(Integer.parseInt(standardId));
								galleryAssignToModel.setStandardMasterModel(standardMasterModel);
								studentGalleryAssignedToRepository.saveAndFlush(galleryAssignToModel);

									/*
									 * if (CollectionUtils.isNotEmpty(tokenList)) { for (int j = 0; j <
									 * tokenList.size(); j++) { if
									 * (StringUtils.isNotEmpty(tokenList.get(j).getToken()) &&
									 * tokenList.get(j).getDeviceType().equals("2")) {
									 * androidTokenList.add(tokenList.get(j).getToken()); } else if
									 * (StringUtils.isNotEmpty(tokenList.get(j).getToken()) &&
									 * tokenList.get(j).getDeviceType().equals("3")) {
									 * IOSTokenList.add(tokenList.get(j).getToken()); }
									 * 
									 * } }
									 */
							});

							
						}
					}
				}		
					
			}
					
		});
			
		/*
		 * AndroidNotificationStatusModel androidNotificationStatusModel = new
		 * AndroidNotificationStatusModel(); String statusIncrementedId = "" +
		 * androidNotificationStatusRepository.getMaxId(studentGalleryBean.getSchoolId()
		 * ); String notificationStatusId =
		 * Utility.generatePrimaryKey(globalDbSchoolKey,statusIncrementedId);
		 * androidNotificationStatusModel.setNotificationStatusId(Integer.parseInt(
		 * notificationStatusId));
		 * androidNotificationStatusModel.setSchoolMasterModel(schoolMasterModel);
		 * androidNotificationStatusModel.setJoinTable("gallery");
		 * androidNotificationStatusModel.setJoinId(""+galleryModel.getGalleryId());
		 * androidNotificationStatusModel.setNotificationType("Gallery");
		 * androidNotificationStatusModel.setStatus(1);
		 * androidNotificationStatusModel.setTitle(title);
		 * androidNotificationStatusModel.setMessage(message);
		 * androidNotificationStatusModel.setCount(Integer.parseInt(statusIncrementedId)
		 * ); androidNotificationStatusModel.setNotificationDate(new Date()); String
		 * currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new
		 * Date()); androidNotificationStatusModel.setNotificationTime(currentTime);
		 * androidNotificationStatusRepository.saveAndFlush(
		 * androidNotificationStatusModel); Utility.pushNotification(androidTokenList,
		 * title, message,"2"); Utility.pushNotification(IOSTokenList, title,
		 * message,"3");
		 */
		
		}
		return 1;
	}


	@Override
	public List<GalleryResponseBean> getMyGalleryPosts(Integer staffId, Integer yearId, String profileRole,Integer offset,String standardDivision) {
		
		Integer limit = androidConfigurationRepository.findByAndroidId(1).getOffset();
		
		List<GalleryResponseBean> studentList = new ArrayList<>();
		if(StringUtils.isEmpty(standardDivision)) {		
			studentList=galleryRepository.getStudentGallery(staffId,yearId, profileRole, PageRequest.of(offset, limit));
		}else
		{
			List<Integer>standardList=new ArrayList<>();
			List<Integer>divisionList=new ArrayList<>();
			List<String>standatrdDivisionList = new ArrayList<String>();
			
			String[] arrayOfStandardDivision = standardDivision.split("\\s*,\\s*");
			if(arrayOfStandardDivision != null && arrayOfStandardDivision.length>0) {
				standatrdDivisionList = Arrays.asList(arrayOfStandardDivision);	
			}
			standatrdDivisionList.stream().forEach(bean -> {
					standardList.add(Integer.parseInt(bean.split("-")[0]));	
					divisionList.add(Integer.parseInt(bean.split("-")[1]));	
				
			});
			studentList=galleryRepository.getStudentGallery(staffId,yearId, profileRole, PageRequest.of(offset, limit),standardList,divisionList);
		}

		studentList.stream().forEach(student -> {
			student.setShowcount(false);
			student.setCompleteCount(0l);
			student.setDeliveredCount(0l);
			student.setSeenCount(0l);
			student.setUnseenCount(0l);
			student.setTotalStudentCount(0l);
			student.setPostedByRole(getRoles(student.getPostedByRole()));
			List<GalleryResponseBean> attachmentList = galleryRepository.getAttachments(student.getGalleryId());
			attachmentList.parallelStream().forEach(attachment->{
				attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
			});
			student.setAttachmentList(attachmentList);
			student.setPostedGalleryStatus(("" + getAssignmentStatus(Integer.parseInt(student.getPostedGalleryStatus()))));
			String postedByImgUrl = setStaffPhotoUrl(student.getSchoolId(), student.getRegistrationNumber());
			student.setPostedByImgUrl(postedByImgUrl);

			if (student.getStaffId().equals(staffId)) {
				List<GalleryResponseBean> countList = galleryRepository.findAssignedGalleryCount(staffId, yearId, student.getGalleryId());
				student.setShowcount(true);
				student.setCompleteCount(countList.get(0).getCompleteCount());
				student.setDeliveredCount(countList.get(0).getDeliveredCount());
				student.setSeenCount(countList.get(0).getSeenCount());
				student.setUnseenCount(countList.get(0).getUnseenCount());
				student.setTotalStudentCount(countList.get(0).getTotalStudentCount());
			}
		});

		return studentList;
	}


	@Override
	public Integer updateGalleryStatus(Integer galleryAssignedtoId, Integer studentId, String statusFlag) {
		GalleryAssignToModel studentGalleryAssignToModel = studentGalleryAssignedToRepository.findByGalleryAssignToId(galleryAssignedtoId);
		if (!(statusFlag.equals("1") && (studentGalleryAssignToModel.getGalleryStatus().equals("2")
				|| studentGalleryAssignToModel.getGalleryStatus().equals("3")))) {
			studentGalleryAssignToModel.setGalleryStatus(statusFlag);
			studentGalleryAssignToModel = studentGalleryAssignedToRepository.save(studentGalleryAssignToModel);
		}
		return studentGalleryAssignToModel.getGalleryAssignToId();
	}
	

	@Override
	@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public synchronized Integer saveGalleryAttachments(MultipartFile file, String fileName, Integer galleryId, String yearName,
			Integer schoolId) {
		try {
			GalleryAttachmentModel galleryAttachmentModel   = new GalleryAttachmentModel();
			GalleryModel galleryModel = new GalleryModel();
			
			galleryModel.setGalleryId(galleryId);
			galleryAttachmentModel.setGalleryModel(galleryModel);
			galleryAttachmentModel.setFileName(fileName);
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			galleryAttachmentModel.setSchoolMasterModel(schoolMasterModel);
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			
			String incrementedId = "" + galleryRepository.getMaxAttachmentId(schoolId);
			String galleryAttchmentId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			galleryAttachmentModel.setGalleryAttachmentId(Integer.parseInt(galleryAttchmentId));
			
			AppLanguageModel appLanguageModel = schoolModel.getLanguage();
			Integer language = appLanguageModel.getLanguageId();
			
			StringBuilder attachmentFileName = new StringBuilder();
			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				attachmentFileName.append(yearName).append("_").append(fileName);
				String folderName = "cms" + File.separator + "Gallery" + File.separator+ schoolModel.getSchoolKey() + File.separator + yearName;
				String target = HttpClientUtil.uploadImage(file, fileName, folderName, schoolModel.getSchoolKey(),language);
				if (!target.isEmpty()) {
				//Upload attachment to S3
					String postUri= env.getProperty("awsUploadUrl");
					URI uri = new URI(postUri);
					File convFile = new File(attachmentFileName.toString());
				    convFile.createNewFile();
				    FileOutputStream fos = new FileOutputStream(convFile);
				    fos.write(file.getBytes());
				    fos.close();
					UploadFileBean bean = new UploadFileBean();
					bean.setFileName(fileName);
					bean.setFile(convFile);
					bean.setFilePath("maindb"+"/"+folderName);
					String path = restTemplate.postForObject(uri, bean, String.class);
				 	convFile.delete();
				 	if(path!=null)
				 	{
				 		galleryAttachmentModel.setServerFilePath(path);
				 	}else {
				 		galleryAttachmentModel.setServerFilePath(target);
				 	}
				//
					galleryAttachmentModel.setFileName(attachmentFileName.toString());
					galleryAttachmentRepository.saveAndFlush(galleryAttachmentModel);
					return galleryAttachmentModel.getGalleryAttachmentId();
				}
			}
		
		} catch (NumberFormatException | URISyntaxException | IOException e) {
			e.printStackTrace();
		} 
		return 0;
	}


	@Override
	@Transactional
	public Integer deleteGalleryPost(Integer galleryId) {
		GalleryModel model = new GalleryModel();
		model.setGalleryId(galleryId);
		studentGalleryAssignedToRepository.deleteByGalleryModel(model);
		studentGalleryAttachmentRepository.deleteByGalleryModel(model);
		galleryRepository.deleteById(galleryId);
		return 1;
	}


	@Override
	@Transactional
	public Integer deleteGalleryAttachment(Integer galleryAttachmentId) {
	studentGalleryAttachmentRepository.deleteById(galleryAttachmentId);
	return 1;
	}


	@Override
	public List<GalleryResponseBean> getGalleryAttachmentList(Integer galleryId) {
		List<GalleryResponseBean> attachmentList =  galleryRepository.getAttachments(galleryId);
		attachmentList.parallelStream().forEach(attachment->{
			attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
		});
		return attachmentList;
	}


	private String setStaffPhotoUrl(Integer schoolId,String registartionNo) {
		String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = ""+ConfigurationProperties.staffPhotoUrl+"/"+schoolKey;
		String extension="jpg";
		return directoryPath+"/"+registartionNo+"."+extension;
	}
	
	private String setStudentPhotoUrl(Integer schoolId, String registartionNo,Integer grBookId) {
		String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = ""+ConfigurationProperties.studentPhotoUrl+schoolKey +"/"+ grBookId;
		String extension="jpg";
		return directoryPath+"/"+registartionNo+"."+extension;
	}

	private String getRoles(String userName) {
		switch (userName) {
		case "ROLE_PRINCIPAL":
			return "Principal";
		case "ROLE_ADMIN":
			return "Admin";
		case "ROLE_NONTEACHINGSTAFF":
			return "Non Teaching Staff";
		case "ROLE_TEACHINGSTAFF":
			return "Teacher";
		case "ROLE_PARENT1":
			return "Father";
		case "ROLE_PARENT2":
			return "Mother";
		case "ROLE_SANSTHAOFFICER":
			return "Sanstha Officer";
		case "ROLE_STUDENT":
			return "Student";
		case "ROLE_SUPERADMIN":
			return "Super Admin";
		case "ROLE_USER":
			return "User";
		default:
			return userName;
		}

	}

	private String getAssignmentStatus(int i) {
		switch (i) {
		case 0:
			return "Total";
		case 1:
			return "Delivered";
		case 2:
			return "Seen";
		case 3:
			return "Completed";
		case 4:
			return "Unseen";
		default:
			return "";
		}
	}


	@Override
	public List<GalleryResponseBean> getdivisionWiseFilterList(Integer schoolId, Integer yearId, Integer staffId) {
		GalleryResponseBean galleryResponseBean =new GalleryResponseBean();
		List<GalleryResponseBean> galleryResponseList = new ArrayList<>();
		List<GalleryLabelBean> labelList2 = new ArrayList<>();
		List<StandardDivisionBean> standardDivisionList = galleryRepository.getdivisionWiseFilterList(schoolId,staffId);
		standardDivisionList.stream().forEach(standardDivision -> {
			GalleryLabelBean galleryLabelBean2 = new GalleryLabelBean();
			galleryLabelBean2.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
			galleryLabelBean2.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
			labelList2.add(galleryLabelBean2);
		});
		galleryResponseBean.setDisplayList(labelList2);
		galleryResponseList.add(galleryResponseBean);
        return galleryResponseList;
	}


	@Override
	public List<GalleryResponseBean> getMyGalleryPostDetails(Integer galleryId, Integer yearId,
			Integer studentStaffId,String profileRole) {
		List<GalleryResponseBean> studentList = new ArrayList<>();
		studentList=galleryRepository.getMyGalleryPostDetails(galleryId,yearId, studentStaffId,profileRole);

		studentList.stream().forEach(student -> {
			student.setShowcount(false);
			student.setCompleteCount(0l);
			student.setDeliveredCount(0l);
			student.setSeenCount(0l);
			student.setUnseenCount(0l);
			student.setTotalStudentCount(0l);
			student.setPostedByRole(getRoles(student.getPostedByRole()));
			List<GalleryResponseBean> attachmentList = galleryRepository.getAttachments(student.getGalleryId());
			attachmentList.parallelStream().forEach(attachment->{
				attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
			});
			student.setAttachmentList(attachmentList);
			student.setPostedGalleryStatus(("" + getAssignmentStatus(Integer.parseInt(student.getPostedGalleryStatus()))));
			String postedByImgUrl = setStaffPhotoUrl(student.getSchoolId(), student.getRegistrationNumber());
			student.setPostedByImgUrl(postedByImgUrl);

			if (student.getStaffId().equals(studentStaffId)) {
				List<GalleryResponseBean> countList = galleryRepository.findAssignedGalleryCount(studentStaffId, yearId, student.getGalleryId());
				student.setShowcount(true);
				student.setCompleteCount(countList.get(0).getCompleteCount());
				student.setDeliveredCount(countList.get(0).getDeliveredCount());
				student.setSeenCount(countList.get(0).getSeenCount());
				student.setUnseenCount(countList.get(0).getUnseenCount());
				student.setTotalStudentCount(countList.get(0).getTotalStudentCount());
			}
		});

		return studentList;
	}

	@Override
	public List<StudentDetailsBean> getStudentDetailsForGallery(Integer galleryId, Integer offset) {
		List<StudentDetailsBean> galleryList = new ArrayList<>();
		List<StudentDetailsBean> studentDetailsList = galleryRepository.getStudentDetailsForGallery(galleryId);
		List<StudentDetailsBean> staffDetailsList = galleryRepository.getStaffDetailsForGallery(galleryId);
		List<StudentDetailsBean> totalList = new ArrayList<>();
		totalList.addAll(studentDetailsList);
		totalList.addAll(staffDetailsList);
		
		IntStream.range(0, 5).forEach(i -> {
			StudentDetailsBean studentGalleryBean = new StudentDetailsBean();
			studentGalleryBean.setGalleryStatus(""+i);
			studentGalleryBean.setGalleryStatusText(getGalleryStatus(i));
			if(i==0) {
				totalList.stream().forEach( student -> {
					if(("Student").equals(student.getRole())) {
						student.setPhotoUrl(setStudentPhotoUrl(student.getSchoolId(),student.getRegNo(),student.getGrBookId()));
					}
					else {
						student.setStudentId(0);
						student.setPhotoUrl(setStaffPhotoUrl(student.getSchoolId(),student.getRegNo()));
					}
					
				});
				studentGalleryBean.setStudentDetailsBean(totalList);
			}
			else {
				List<StudentDetailsBean> updatedStudentDetailsList = new ArrayList<>();
				totalList.stream().forEach( student -> {
					if(("Student").equals(student.getRole())) {
						student.setPhotoUrl(setStudentPhotoUrl(student.getSchoolId(),student.getRegNo(),student.getGrBookId()));
					}
					else {
						student.setStudentId(0);
						student.setPhotoUrl(setStaffPhotoUrl(student.getSchoolId(),student.getRegNo()));
					}
					if(i!=4 && Integer.parseInt(student.getGalleryStatus())>=i) {
						updatedStudentDetailsList.add(student);
					}
					else if(i==4 && student.getGalleryStatus().equals("1")) {
						updatedStudentDetailsList.add(student);
					}
				});
				studentGalleryBean.setStudentDetailsBean(updatedStudentDetailsList);
			}
			
			galleryList.add(studentGalleryBean);
		}); 
		
		
		return galleryList;
	}
	
	@Override
	public List<GalleryResponseBean> getDivisionWiseStudentList(Integer schoolId, String assignedByRole, Integer yearId,
			String standardDivision,String standardDivisionName) {
		
		List<GalleryResponseBean> galleryResponseList = new ArrayList<>();
		GalleryResponseBean galleryResponseBean = new GalleryResponseBean();
		galleryResponseBean.setRoleName("Class");
		GalleryLabelBean galleryLabelBean2 = new GalleryLabelBean();
		List<GalleryLabelBean> labelList2 = new ArrayList<>();
		galleryLabelBean2.setId(standardDivision.split("-")[0] + "-" + standardDivision.split("-")[1]);
		galleryLabelBean2.setName(standardDivisionName.split("-")[0] + "-" + standardDivisionName.split("-")[1]);
		List<StudentDetailsBean> studentList = studentStandardRenewRepository.findStudent1(yearId,Integer.parseInt(standardDivision.split("-")[0]),
				Integer.parseInt(standardDivision.split("-")[1]), schoolId);
//		List<StudentDetailsBean> studentDetailsList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(studentList)) {
			studentList.stream().forEach(student -> {
/*			StudentDetailsBean studentDetailsBean =new StudentDetailsBean();
			StringBuilder fullname = new StringBuilder();
			fullname.append(student.getStudentMasterModel().getStudFName());
			fullname.append(" ");
			fullname.append(student.getStudentMasterModel().getStudMName());
			fullname.append(" ");
			fullname.append(student.getStudentMasterModel().getStudLName());
            String completeName = fullname.toString();
			studentDetailsBean.setStudentName(completeName);
			studentDetailsBean.setRollNo(student.getRollNo());
			studentDetailsBean.setRegNo(student.getStudentMasterModel().getStudentRegNo());
			studentDetailsBean.setGrBookId(student.getStudentMasterModel().getGrBookName().getGrBookId()); 
			studentDetailsBean.setRenewStudentId(student.getRenewStudentId());
			studentDetailsBean.setStudentId(student.getStudentMasterModel().getStudentId());*/
			student.setPhotoUrl(setStudentPhotoUrl(schoolId, student.getRegNo(),student.getGrBookId()));
//			studentDetailsList.add(studentDetailsBean);
		});
	}
		galleryLabelBean2.setStudentDetailsList(studentList);
		labelList2.add(galleryLabelBean2);
		galleryResponseBean.setDisplayList(labelList2);
		galleryResponseList.add(galleryResponseBean);
		return galleryResponseList;
	}


	private String getGalleryStatus(int i) {
		switch(i) {
		case 0 :
			return "Total";
		case 1 :
			return "Delivered";
		case 2 :
			return "Seen";
		case 3 :
			return "Completed";
		case 4 :
			return "Unseen";
		default: 
			return "";
		}
	}



	@SuppressWarnings("unused")
	@Override
	public Integer saveGalleryAttachmentsWithId(MultipartFile file, String fileName, Integer galleryId, Integer yeraId,
			Integer schoolId) {
		try {
			GalleryAttachmentModel galleryAttachmentModel   = new GalleryAttachmentModel();
			GalleryModel galleryModel = new GalleryModel();
			
			galleryModel.setGalleryId(galleryId);
			galleryAttachmentModel.setGalleryModel(galleryModel);
			galleryAttachmentModel.setFileName(fileName);
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			galleryAttachmentModel.setSchoolMasterModel(schoolMasterModel);
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			
			String incrementedId = "" + galleryRepository.getMaxAttachmentId(schoolId);
			String galleryAttchmentId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			galleryAttachmentModel.setGalleryAttachmentId(Integer.parseInt(galleryAttchmentId));
			
			AppLanguageModel appLanguageModel = schoolModel.getLanguage();
			Integer language = appLanguageModel.getLanguageId();
			
			StringBuilder attachmentFileName = new StringBuilder();
			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				attachmentFileName.append(yeraId).append("_").append(fileName);
				String folderName = "cms" + File.separator + "Gallery" + File.separator+ schoolModel.getSchoolKey() + File.separator + yeraId;
				String target = HttpClientUtil.uploadImage(file, fileName, folderName, schoolModel.getSchoolKey(),language);
				if (!target.isEmpty()) {
				//Upload attachment to S3
					String postUri= env.getProperty("awsUploadUrl");
					URI uri = new URI(postUri);
					File convFile = new File(attachmentFileName.toString());
				    convFile.createNewFile();
				    FileOutputStream fos = new FileOutputStream(convFile);
				    fos.write(file.getBytes());
				    fos.close();
					UploadFileBean bean = new UploadFileBean();
					bean.setFileName(fileName);
					bean.setFile(convFile);
					bean.setFilePath("maindb"+"/"+folderName);
//					String path = restTemplate.postForObject(uri, bean, String.class);
					String path = uploadSingleFile(bean);
				 	convFile.delete();
				 	if(path!=null)
				 	{
				 		galleryAttachmentModel.setServerFilePath(path);
				 	}else {
				 		galleryAttachmentModel.setServerFilePath(target);
				 	}
				//
					galleryAttachmentModel.setFileName(attachmentFileName.toString());
					galleryAttachmentRepository.saveAndFlush(galleryAttachmentModel);
					return galleryAttachmentModel.getGalleryAttachmentId();
				}
			}
		
		} catch (NumberFormatException | URISyntaxException | IOException e) {
			e.printStackTrace();
		} 
		return 0;

	}

	public String uploadSingleFile(UploadFileBean fileBean) {
		try {
//			String bucketName = "eprashasan1";
			String bucketName = "eprashasan2";
			String key = fileBean.getFilePath();
			key = key.replace("\\", "/");
			String returnPath = UploadFile(bucketName, key, fileBean.getFile());
			return returnPath;
		} catch (Exception e) {
			return null;
		}
	}

	public String UploadFile(String bucketName, String keyName, File file) {
		try {
			keyName = keyName + "/" + file.getName();
			keyName = keyName.replace(" ", "_");
			amazonS3.putObject(
					new PutObjectRequest(bucketName, keyName, file).withCannedAcl(CannedAccessControlList.PublicRead));
			return ((AmazonS3Client) amazonS3).getResourceUrl(bucketName, keyName);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		}
		return "";
	}


	@Override
	public void postGalleryForMembers(GalleryRequestBean studentGalleryBean, Integer galleryId) {
		
		StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
		staffBasicDetailsModel.setstaffId(studentGalleryBean.getStaffId());

		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(studentGalleryBean.getYearId());

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentGalleryBean.getSchoolId());

		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentGalleryBean.getSchoolId());
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

		
		AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(studentGalleryBean.getUserId());	

		if (studentGalleryBean.getApiFlag() != null && studentGalleryBean.getApiFlag() == 0) {
		 sendGallery(studentGalleryBean, yearMasterModel, schoolMasterModel, globalDbSchoolKey,
					""+galleryId, appUserRoleModel);
		}
		
	}



	@Override
	public void sendNotification(GalleryRequestBean studentGalleryBean, Integer galleryId) {
		
		
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentGalleryBean.getSchoolId());
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentGalleryBean.getSchoolId());
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
//		List<String> androidTokenList = new ArrayList<>();
//		List<String> IOSTokenList = new ArrayList<>();
		WhatsAppMessageBean tokenList = new WhatsAppMessageBean();
		List<FCMTokenBean> Fcmtoken=new ArrayList<>();
		
		
	    String language = Utility.getLanguage(schoolModel.getLanguage().getLanguageId());           
	    final String title=env.getProperty(language+"."+"GalleryPost");			
		String message1=env.getProperty(language+"."+"GalleryNotification");	
		if(studentGalleryBean.getTime()==null) {
			String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
			studentGalleryBean.setTime(currentTime);
		}
		final String message = message1.replaceAll("teacherName",studentGalleryBean.getTeacherName()).replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("notifTime", ""+studentGalleryBean.getTime()).concat(" : "+studentGalleryBean.getGalleryMessage());
	
		
		if (CollectionUtils.isNotEmpty(studentGalleryBean.getGalleryResponseList())) {
			for (GalleryResponseBean responseBean : studentGalleryBean.getGalleryResponseList()) {

				if (responseBean.getRoleName().equals("Teacher")) {
					List<GalleryLabelBean> staffList = responseBean.getDisplayList();
					staffList.stream().forEach(staff -> {
						List<FCMTokenBean> FcmtokenList = assignSubjectToStudentRepository.getTokenForTeacher(Integer.parseInt(staff.getId()));							
						if (CollectionUtils.isNotEmpty(FcmtokenList)) {
							for (int j = 0; j < FcmtokenList.size(); j++) {
								if(!FcmtokenList.get(j).getToken().equalsIgnoreCase(null) && !FcmtokenList.get(j).getToken().equalsIgnoreCase("")) {
									FCMTokenBean fcmTokenBean =new FCMTokenBean();
									fcmTokenBean.setMenuId(11);
									fcmTokenBean.setMenuName("Gallery");
									fcmTokenBean.setToken(FcmtokenList.get(j).getToken());
									fcmTokenBean.setUserId(FcmtokenList.get(j).getUserId());
									fcmTokenBean.setMobileNo(FcmtokenList.get(j).getMobileNo());
									fcmTokenBean.setMessageType("2");
									fcmTokenBean.setRenewStaffId(Integer.parseInt(staff.getId()));
									fcmTokenBean.setTypeFlag("2");
									fcmTokenBean.setSchoolId(studentGalleryBean.getSchoolId());
									fcmTokenBean.setNotificationId(galleryId);
									Fcmtoken.add(fcmTokenBean);
								}
								
							}
							if(Fcmtoken != null) {
								tokenList.setTokenList(Fcmtoken);
							}
						}
						
					});
					
				} else if (responseBean.getRoleName().equals("Class")) {
				if (CollectionUtils.isNotEmpty(responseBean.getDisplayList())) {
					for(int i=0;i<responseBean.getDisplayList().size();i++) {
				    List<StudentDetailsBean> studentList = responseBean.getDisplayList().get(i).getStudentDetailsList();		
//					String standardId = responseBean.getDisplayList().get(i).getId().split("-")[0];
//					String divisionId = responseBean.getDisplayList().get(i).getId().split("-")[1];
					if (CollectionUtils.isNotEmpty(studentList)) {
						studentList.stream().forEach(student -> {
							List<FCMTokenBean> FcmtokenList = assignSubjectToStudentRepository.getToken(student.getRenewStudentId());				
							if (CollectionUtils.isNotEmpty(FcmtokenList)) {
								for (int j = 0; j < FcmtokenList.size(); j++) {
									if(!FcmtokenList.get(j).getToken().equalsIgnoreCase(null) && !FcmtokenList.get(j).getToken().equalsIgnoreCase("")) {
									FCMTokenBean fcmTokenBean =new FCMTokenBean();
									fcmTokenBean.setMenuId(11);
									fcmTokenBean.setMenuName("Gallery");
									fcmTokenBean.setToken(FcmtokenList.get(j).getToken());
									fcmTokenBean.setUserId(FcmtokenList.get(j).getUserId());
									fcmTokenBean.setMobileNo(FcmtokenList.get(j).getMobileNo());
									fcmTokenBean.setMessageType("2");
									fcmTokenBean.setRenewStaffId(student.getRenewStudentId());
									fcmTokenBean.setTypeFlag("1");
									fcmTokenBean.setSchoolId(studentGalleryBean.getSchoolId());
									fcmTokenBean.setNotificationId(galleryId);
									Fcmtoken.add(fcmTokenBean);
									}
								}
								if(Fcmtoken != null) {
									tokenList.setTokenList(Fcmtoken);
								}
							}
						});

						
					}
				}
			}		
				
		}
				
	
			}
			
			AndroidNotificationStatusModel androidNotificationStatusModel = new AndroidNotificationStatusModel();
			String statusIncrementedId = "" + androidNotificationStatusRepository.getMaxId(studentGalleryBean.getSchoolId());
			String notificationStatusId = Utility.generatePrimaryKey(globalDbSchoolKey,statusIncrementedId);
			androidNotificationStatusModel.setNotificationStatusId(Integer.parseInt(notificationStatusId));
			androidNotificationStatusModel.setSchoolMasterModel(schoolMasterModel);
			androidNotificationStatusModel.setJoinTable("gallery"); 
			androidNotificationStatusModel.setJoinId(""+galleryId);
			androidNotificationStatusModel.setNotificationType("Gallery");									
			androidNotificationStatusModel.setStatus(1);	
			androidNotificationStatusModel.setTitle(title);
			androidNotificationStatusModel.setMessage(message);							
			androidNotificationStatusModel.setCount(Integer.parseInt(statusIncrementedId));	
			androidNotificationStatusModel.setNotificationDate(new Date());
			String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
			androidNotificationStatusModel.setNotificationTime(currentTime);
			androidNotificationStatusRepository.saveAndFlush(androidNotificationStatusModel);		
			
			
			galleryRepository.updateGallerySentFlag(1 , galleryId);	
			
			tokenList.setTitle(studentGalleryBean.getGalleryTitle());
			tokenList.setMessage(message);
			tokenList.setCustomizeMessageFlag(0);
			Utility.sendNotificationNew(tokenList);
//			Utility.pushNotification(androidTokenList, title, message,"2");
//			Utility.pushNotification(IOSTokenList, title, message,"3");
		
		}
	}

}
