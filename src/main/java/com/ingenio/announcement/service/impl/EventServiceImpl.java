package com.ingenio.announcement.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.BannerImages;
import com.ingenio.announcement.bean.EventLabelBean;
import com.ingenio.announcement.bean.EventRequestBean;
import com.ingenio.announcement.bean.EventResponse;
import com.ingenio.announcement.bean.EventResponseBean;
import com.ingenio.announcement.bean.EventTypeBean;
import com.ingenio.announcement.bean.PersonalNoteRequestBean;
import com.ingenio.announcement.bean.StaffDetailsBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.UploadFileBean;
import com.ingenio.announcement.config.ConfigurationProperties;
import com.ingenio.announcement.model.AppLanguageModel;
import com.ingenio.announcement.model.AppUserModel;
import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.EventAssignToModel;
import com.ingenio.announcement.model.EventAttachmentModel;
import com.ingenio.announcement.model.EventCalendarAssignToModel;
import com.ingenio.announcement.model.EventModel;
import com.ingenio.announcement.model.EventTypeMasterModel;
import com.ingenio.announcement.model.PersonalNoteModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.StaffBasicDetailsModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AndroidConfigurationRepository;
import com.ingenio.announcement.repository.AndroidNotificationStatusRepository;
import com.ingenio.announcement.repository.AssignSubjectToStudentRepository;
import com.ingenio.announcement.repository.AttachmentRepository;
import com.ingenio.announcement.repository.EventAttachmentRepository;
import com.ingenio.announcement.repository.EventCalendarRepository;
import com.ingenio.announcement.repository.EventRepository;
import com.ingenio.announcement.repository.EventTypeRepository;
import com.ingenio.announcement.repository.PersonalNoteRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.StaffBasicDetailRepository;
import com.ingenio.announcement.repository.StudentEventAssignedToRepository;
import com.ingenio.announcement.repository.StudentEventSmsAssignedToRepository;
import com.ingenio.announcement.repository.StudentStandardRenewRepository;
import com.ingenio.announcement.repository.UserAuthenticationRepository;
import com.ingenio.announcement.service.EventService;
import com.ingenio.announcement.util.CommonUtlitity;
import com.ingenio.announcement.util.CryptographyUtil;
import com.ingenio.announcement.util.HttpClientUtil;
import com.ingenio.announcement.util.Utility;

@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")



  @PropertySource("classpath:messages_en.properties")
  
  @PropertySource("classpath:messages_mr.properties")
  
  @PropertySource("classpath:messages_hn.properties")
 
 
public class EventServiceImpl implements EventService {

	@Autowired
	AssignSubjectToStudentRepository assignSubjectToStudentRepository;

	@Autowired
	AndroidConfigurationRepository androidConfigurationRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	AttachmentRepository attachmentRepository;

	@Autowired
	StudentStandardRenewRepository studentStandardRenewRepository;

	@Autowired
	StudentEventAssignedToRepository studentEventAssignedToRepository;

	@Autowired
	StaffBasicDetailRepository staffBasicDetailRepository;

	@Autowired
	StudentEventSmsAssignedToRepository studentEventSmsAssignedToRepository;

	@Autowired
	UserAuthenticationRepository userAuthenticationRepository;

	CryptographyUtil cryptoUtil = new CryptographyUtil();
	Integer smssaveId = 0;

	@Autowired
	EventRepository eventRepository;

	@Autowired
	EventTypeRepository eventTypeRepository;

	@Autowired
	PersonalNoteRepository personalNoteRepository;
	
	@Autowired
	EventCalendarRepository eventCalendarRepository;

	@Autowired
	private Environment env;

	@Autowired
	private RestTemplate restTemplate;

	Integer assignedToIncrementedId = 0;
	Integer assignedToIncrementedId1 = 0;

	@Autowired
	private AndroidNotificationStatusRepository androidNotificationStatusRepository;

	@Autowired
	EventAttachmentRepository eventAttachmentRepository;
	
	@Autowired
	CommonUtlitity commonUtlitity;

	@Override
	public List<EventResponseBean> getClassDivisisionList(Integer schoolId, String assignedByRole) {
		List<EventResponseBean> eventResponseList = new ArrayList<>();
		EventResponseBean eventResponseBean = new EventResponseBean();
		List<StandardDivisionBean> standardDivisionList = eventRepository.getStandardDivisionList(schoolId);
		List<EventLabelBean> labelList2 = new ArrayList<>();
		standardDivisionList.stream().forEach(standardDivision -> {
			EventLabelBean eventLabelBean = new EventLabelBean();
			eventLabelBean.setId("" + standardDivision.getStandardId() + "-" + standardDivision.getDivisionId());
			eventLabelBean.setName("" + standardDivision.getStandardName() + "-" + standardDivision.getDivisionName());
			labelList2.add(eventLabelBean);
		});
		eventResponseBean.setDisplayList(labelList2);
		eventResponseList.add(eventResponseBean);

		return eventResponseList;
	}

	@Override
	public synchronized Integer saveEvent(EventRequestBean eventRequestBean) {

		String eventId = "0";
		Integer saveId = 0;
		StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
		staffBasicDetailsModel.setstaffId(eventRequestBean.getStaffId());

		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(eventRequestBean.getYearId());

		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(eventRequestBean.getSchoolId());

		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(eventRequestBean.getSchoolId());
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

		AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(eventRequestBean.getUserId());
		
		EventModel eventModel = new EventModel();
		eventId = "" + eventRequestBean.getEventId();
		if (StringUtils.isEmpty(eventId) || eventId.equals("0")) {
			String incrementedId = "" + eventRepository.getMaxId(eventRequestBean.getSchoolId());
			eventId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			eventModel.setEventId(Integer.parseInt(eventId));
		}else {
			eventModel.setEventId(Integer.parseInt(eventId));
			eventCalendarRepository.deleteByEventModel(eventModel);
			eventAttachmentRepository.deleteByEventModel(eventModel);
		}

		eventModel.setUserId(appUserRoleModel);
		eventModel.setIpAddress(eventRequestBean.getIPAddress());
		eventModel.setDeviceType(eventRequestBean.getDeviceType());
		eventModel.setEventStartDate(eventRequestBean.getEventStartDate());
		eventModel.setEventStartTime(eventRequestBean.getEventStartTime());
		eventModel.setEventEndDate(eventRequestBean.getEventEndDate());
		eventModel.setEventEndtime(eventRequestBean.getEventEndtime());
		eventModel.setEventVenue(eventRequestBean.getEventVenue());
		eventModel.setEventTitle(eventRequestBean.getEventTitle());
		eventModel.setEventReminderMessage(eventRequestBean.getEventReminderMessage());
		eventModel.setEventDescription(eventRequestBean.getEventDescription());
		eventModel.setSchoolMasterModel(schoolMasterModel);
		eventModel.setStaffId(eventRequestBean.getStaffId());
		eventModel.setEventId(Integer.parseInt(eventId));
		
        EventTypeMasterModel eventTypeMasterModel =new EventTypeMasterModel();
        eventTypeMasterModel.setEventTypeId(eventRequestBean.getEventTypeId());
        eventModel.setEventTypeMasterModel(eventTypeMasterModel);
		
		saveId = eventRepository.save(eventModel).getEventId();

		for (int i = 0; i < 1; i++) {
			for (int j = 0; j < eventRequestBean.getEventResponseList().get(i).getDisplayList().size(); j++) {

				String stdDiv = eventRequestBean.getEventResponseList().get(i).getDisplayList().get(j).getId();
				EventCalendarAssignToModel eventCalendarAssignToModel = new EventCalendarAssignToModel();
				String standardIdArray[] = stdDiv.split("-");
				Integer standardId = Integer.valueOf(standardIdArray[0]);
				Integer divisionId = Integer.valueOf(standardIdArray[1]);
				StandardMasterModel standardMasterModel1 = new StandardMasterModel();
				standardMasterModel1.setStandardId(standardId);
				eventCalendarAssignToModel.setStandardMasterModel(standardMasterModel1);

				DivisionMasterModel divisionMasterModel1 = new DivisionMasterModel();
				divisionMasterModel1.setDivisionId(divisionId);
				eventCalendarAssignToModel.setDivisionMasterModel(divisionMasterModel1);
				
				EventModel eventModel1 = new EventModel();
				eventModel1.setEventId(saveId);
				eventCalendarAssignToModel.setEventModel(eventModel1);
				
				YearMasterModel yearMasterModel2 =new YearMasterModel();
				yearMasterModel2.setYearId(1069900001);
				eventCalendarAssignToModel.setYearMasterModel(yearMasterModel2);
				eventCalendarRepository.save(eventCalendarAssignToModel);
			}
		}

		return saveId;
	}

	@Override
	public EventResponse getMyEvent(Integer day, Integer month, Integer yearId, String year, String profileRole,
			Integer studentStaffId, Integer schoolId, Integer standardId, Integer divisionId, Integer userId) {
		List<EventResponseBean> eventList = new ArrayList<EventResponseBean>();
		EventResponse eventResponse = new EventResponse();
		if (day != null && day != 0) {
			eventList = eventRepository.getStudentEvent(day, yearId, profileRole, studentStaffId, month,
					Integer.parseInt(year));
		} else {
			eventList = eventRepository.getStudentEvent(yearId, profileRole, studentStaffId, month,
					Integer.parseInt(year));
		}

		List<PersonalNoteRequestBean> personalRequestList = new ArrayList<PersonalNoteRequestBean>();

		if (schoolId == 1069900001) {
			eventList = eventRepository.getStudentEvent(schoolId, standardId, divisionId, month);
			personalRequestList = eventRepository.getPersonalNote(userId, month);
			personalRequestList.stream().forEach(personal -> {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				personal.setReminderDate(simpleDateFormat.format(personal.getReminderDate1()));
			});
		}

		eventList.stream().forEach(student -> {
			student.setShowcount(false);
			student.setCompleteCount(0l);
			student.setDeliveredCount(0l);
			student.setSeenCount(0l);
			student.setUnseenCount(0l);
		student.setGoingCount(0l);
			student.setNotGoingCount(0l);
			student.setMaybeCount(0l);
			student.setEventStatusFlag(student.getEventStatus());
			List<AnnouncementResponseBean> attachmentList = eventRepository.getAttachments(student.getEventId());
			attachmentList.parallelStream().forEach(attachment->{
				attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
			});
			student.setAttachmentList(attachmentList);
			student.setEventStatus("1");

		});

		eventResponse.setEventList(eventList);
		eventResponse.setPersonalRequestList(personalRequestList);
		return eventResponse;

	}

	private String getAssignmentStatus(int i) {
		switch (i) {
		case 0:
			return "Total";
		case 1:
			return "Delivered";
		case 2:
			return "Seen";
		case 3:
			return "Completed";
		case 4:
			return "Unseen";
		case 5:
			return "Going";
		case 6:
			return "Not Going";
		case 7:
			return "Maybe";
		default:
			return "";
		}
	}

	@Override
	public List<EventResponseBean> getMyEventDetails(Integer eventId, Integer yearId, Integer studentStaffId,
			Integer schoolId) {
		List<EventResponseBean> studentList = new ArrayList<EventResponseBean>();
		studentList = eventRepository.getStudentEventDetails(eventId, studentStaffId);
		if (schoolId == 1069900001) {
			studentList = eventRepository.getStudentEventDetails(eventId);
		}
		studentList.stream().forEach(student -> {
			student.setShowcount(false);
			student.setCompleteCount(0l);
			student.setDeliveredCount(0l);
			student.setSeenCount(0l);
			student.setUnseenCount(0l);
			student.setTotalStudentCount(0l);
			student.setGoingCount(0l);
			student.setNotGoingCount(0l);
			student.setMaybeCount(0l);
//			student.setEventStatusFlag(student.getEventStatus());
//			student.setEventStatus("" + getAssignmentStatus(Integer.parseInt(student.getEventStatus())));	
			student.setPostedBy(student.getPostedBy());
			student.setPostedByRole(getRoles(student.getPostedByRole()));
			List<AnnouncementResponseBean> attachmentList = eventRepository.getAttachments(eventId);
			student.setAttachmentList(attachmentList);
			String postedByImgUrl = setStaffPhotoUrl(student.getSchoolId(), student.getSregNo());
			student.setPostedByImgUrl(postedByImgUrl);

			if (student.getStaffId().equals(studentStaffId)) {
				List<EventResponseBean> countList = eventRepository.findAssignedEventCount(studentStaffId, yearId,
						student.getEventId());
				student.setShowcount(true);
				student.setCompleteCount(0L);
				student.setDeliveredCount(0L);
				student.setSeenCount(0L);
				student.setUnseenCount(0L);
				student.setTotalStudentCount(0L);
				student.setGoingCount(0L);
				student.setNotGoingCount(0L);
				List<AnnouncementResponseBean> attachmentList1 = eventRepository.getAttachments(student.getEventId());
				attachmentList.parallelStream().forEach(attachment->{
					attachment.setFilePath(commonUtlitity.getS3PreSignedUrl(attachment.getFilePath()));
				});
				student.setAttachmentList(attachmentList1);
				student.setMaybeCount(0L);

			}

		});

		return studentList;
	}

	private String getRoles(String userName) {
		switch (userName) {
		case "ROLE_PRINCIPAL":
			return "Principal";
		case "ROLE_ADMIN":
			return "Admin";
		case "ROLE_NONTEACHINGSTAFF":
			return "Non Teaching Staff";
		case "ROLE_TEACHINGSTAFF":
			return "Teacher";
		case "ROLE_PARENT1":
			return "Father";
		case "ROLE_PARENT2":
			return "Mother";
		case "ROLE_SANSTHAOFFICER":
			return "Sanstha Officer";
		case "ROLE_STUDENT":
			return "Student";
		case "ROLE_SUPERADMIN":
			return "Super Admin";
		case "ROLE_USER":
			return "User";
		default:
			return userName;
		}

	}

	private String setStaffPhotoUrl(Integer schoolId, String registartionNo) {
		String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = "" + ConfigurationProperties.staffPhotoUrl + "/" + schoolKey;
		String extension = "jpg";
		return directoryPath + "/" + registartionNo + "." + extension;
	}

	@Override
	public Integer updateEventStatus(Integer eventAssignId, Integer studentStaffId, String statusFlag) {
		EventAssignToModel eventAssignToModel = studentEventAssignedToRepository.findByEventAssignToId(eventAssignId);
		if (!(statusFlag == "1" && (eventAssignToModel.getEventStatus().endsWith("2")
				|| eventAssignToModel.getEventStatus().endsWith("3")
				|| eventAssignToModel.getEventStatus().endsWith("4")
				|| eventAssignToModel.getEventStatus().endsWith("5")
				|| eventAssignToModel.getEventStatus().endsWith("6")
				|| eventAssignToModel.getEventStatus().endsWith("7")))) {
			eventAssignToModel.setEventStatus(statusFlag);
			eventAssignToModel = studentEventAssignedToRepository.save(eventAssignToModel);
		}
		return eventAssignToModel.getEventAssignToId();
	}

	@Override
	@Transactional
	public Integer deleteEvent(Integer eventId) {

		EventModel model = new EventModel();
		model.setEventId(eventId);
		eventCalendarRepository.deleteByEventModel(model);
		eventAttachmentRepository.deleteByEventModel(model);
		eventRepository.deleteById(eventId);
		return 1;
	}

	@Override
	public List<EventResponseBean> getStudentDetailsForEventStatus(Integer schoolId, Integer eventId,
			String eventStatus) {
		List<EventResponseBean> eventStudentList = new ArrayList<>();
		List<StudentDetailsBean> studentDetailsList = studentEventAssignedToRepository
				.getStudentDetailsForEventStatus(eventId, eventStatus, schoolId);
		List<StaffDetailsBean> staffDetailsList = studentEventAssignedToRepository
				.getStaffDetailsForEventStatus(eventId, eventStatus, schoolId);
		EventResponseBean eventResponseBean = new EventResponseBean();
		eventResponseBean.setStudentDetails(studentDetailsList);
		eventResponseBean.setStaffDetails(staffDetailsList);
		eventStudentList.add(eventResponseBean);
		return eventStudentList;

	}

	@Override
	public List<StudentDetailsBean> getStudentDetailsForEvent(Integer eventId, Integer offset) {
		List<StudentDetailsBean> galleryList = new ArrayList<>();
		List<StudentDetailsBean> studentDetailsList = eventRepository.getStudentDetailsForEvent(eventId);
		List<StudentDetailsBean> staffDetailsList = eventRepository.getStaffDetailsForEvent(eventId);
		List<StudentDetailsBean> totalList = new ArrayList<>();
		totalList.addAll(studentDetailsList);
		totalList.addAll(staffDetailsList);

		IntStream.range(0, 7).forEach(i -> {
			if (i == 0) {
				StudentDetailsBean studentEventBean = new StudentDetailsBean();
				studentEventBean.setEventStatus("" + i);
				studentEventBean.setEventStatusText(getEventStatus(i));
				totalList.stream().forEach(student -> {
					if (("Student").equals(student.getRole())) {
						student.setPhotoUrl(
								setStudentPhotoUrl(student.getSchoolId(), student.getRegNo(), student.getGrBookId()));
					} else {
						student.setStudentId(0);
						student.setPhotoUrl(setStaffPhotoUrl(student.getSchoolId(), student.getRegNo()));
					}

				});
				studentEventBean.setStudentDetailsBean(totalList);
				galleryList.add(studentEventBean);
			} else if (i == 5 || i == 6 || i == 7) {
				StudentDetailsBean studentEventBean = new StudentDetailsBean();
				studentEventBean.setEventStatus("" + i);
				studentEventBean.setEventStatusText(getEventStatus(i));
				List<StudentDetailsBean> updatedStudentDetailsList = new ArrayList<>();
				totalList.stream().forEach(student -> {
					if (("Student").equals(student.getRole())) {
						student.setPhotoUrl(
								setStudentPhotoUrl(student.getSchoolId(), student.getRegNo(), student.getGrBookId()));
					} else {
						student.setStudentId(0);
						student.setPhotoUrl(setStaffPhotoUrl(student.getSchoolId(), student.getRegNo()));
					}
					/*
					 * if((i==1 || i==2) && Integer.parseInt(student.getEventStatus())>=i) {
					 * updatedStudentDetailsList.add(student); } else if(i==4 &&
					 * student.getEventStatus().equals("1")) {
					 * updatedStudentDetailsList.add(student); }
					 */
					if ((i == 5 || i == 6 || i == 7) && Integer.parseInt(student.getEventStatus()) == i) {
						updatedStudentDetailsList.add(student);
					}
				});
				studentEventBean.setStudentDetailsBean(updatedStudentDetailsList);
				galleryList.add(studentEventBean);
			}

		});

		return galleryList;
	}

	private String getEventStatus(int i) {
		switch (i) {
		case 0:
			return "Total";
		case 1:
			return "Delivered";
		case 2:
			return "Seen";
		case 3:
			return "Completed";
		case 4:
			return "Unseen";
		case 5:
			return "Going";
		case 6:
			return "Not Going";
		case 7:
			return "Maybe";
		default:
			return "";
		}
	}

	private String setStudentPhotoUrl(Integer schoolId, String registartionNo, Integer grBookId) {
		String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = "" + ConfigurationProperties.studentPhotoUrl + schoolKey + "/" + grBookId;
		String extension = "jpg";
		return directoryPath + "/" + registartionNo + "." + extension;
	}

	@Override
	public List<EventTypeBean> getAllEventTypes() {
		List<EventTypeBean> eventTypeList = eventTypeRepository.getAllEventTypes();
		return eventTypeList;
	}

	@Override
	public Integer savePersonalNote(PersonalNoteRequestBean personalNoteRequestBean) {

		try {
			PersonalNoteModel personalNoteModel = new PersonalNoteModel();
			personalNoteModel.setPersonal_note(personalNoteRequestBean.getPersonalNote());
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			personalNoteModel.setReminderDate(simpleDateFormat.parse(personalNoteRequestBean.getReminderDate()));
			personalNoteModel.setReminderTime(personalNoteRequestBean.getReminderTime());

			if (personalNoteRequestBean.getPersonalNoteId() != 0) {
				personalNoteModel.setPersonalNoteId(personalNoteRequestBean.getPersonalNoteId());
			}

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(personalNoteRequestBean.getSchoolId());

			AppUserModel appUserRoleModel = new AppUserModel();
			appUserRoleModel.setAppUserRoleId(personalNoteRequestBean.getUserId());

			personalNoteModel.setSchoolMasterModel(schoolMasterModel);
			personalNoteModel.setUserId(appUserRoleModel);

			StandardMasterModel standardMasterModel = new StandardMasterModel();
			standardMasterModel.setStandardId(personalNoteRequestBean.getStandardId());
			personalNoteModel.setStandardMasterModel(standardMasterModel);

			DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
			divisionMasterModel.setDivisionId(personalNoteRequestBean.getDivisionId());
			personalNoteModel.setDivisionMasterModel(divisionMasterModel);

			PersonalNoteModel personalNoteModel1 = personalNoteRepository.save(personalNoteModel);

			return personalNoteModel1.getPersonalNoteId();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public Integer saveEventAttachments(MultipartFile file, String fileName, Integer eventId, String yearName,
			Integer schoolId) {
		try {
			EventAttachmentModel eventAttachmentModel = new EventAttachmentModel();
			EventModel eventModel = new EventModel();
			eventModel.setEventId(eventId);
			eventAttachmentModel.setEventModel(eventModel);
			eventAttachmentModel.setFileName(fileName);

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			eventAttachmentModel.setSchoolMasterModel(schoolMasterModel);
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId = "" + eventRepository.getMaxAttachmentId(schoolId);
			String eventAttachmentId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			eventAttachmentModel.setEventAttachmentId(Integer.parseInt(eventAttachmentId));
			StringBuilder attachmentFileName = new StringBuilder();

			AppLanguageModel appLanguageModel = schoolModel.getLanguage();
			Integer language = appLanguageModel.getLanguageId();

			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				attachmentFileName.append(yearName).append("_").append(fileName);
				String folderName = "cms" + File.separator + "Event" + File.separator + schoolModel.getSchoolKey()
						+ File.separator + yearName;
				String target = HttpClientUtil.uploadImage(file, fileName, folderName, schoolModel.getSchoolKey(),
						language);
				if (!target.isEmpty()) {
					// Upload attachment on s3
					String postUri = env.getProperty("awsUploadUrl");
					URI uri = new URI(postUri);
					File convFile = new File(attachmentFileName.toString());
					convFile.createNewFile();
					FileOutputStream fos = new FileOutputStream(convFile);
					fos.write(file.getBytes());
					fos.close();
					UploadFileBean bean = new UploadFileBean();
					bean.setFileName(fileName);
					bean.setFile(convFile);
					bean.setFilePath("maindb" + "/" + folderName);
					String path = restTemplate.postForObject(uri, bean, String.class);
					convFile.delete();
					if (path != null) {
						eventAttachmentModel.setServerFilePath(path);
					} else {
						eventAttachmentModel.setServerFilePath(target);
					}
					//
					eventAttachmentModel.setFileName(attachmentFileName.toString());
					eventAttachmentRepository.saveAndFlush(eventAttachmentModel);
					return eventAttachmentModel.getEventAttachmentId();
				}
			}

		} catch (NumberFormatException | URISyntaxException | IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<StandardDivisionBean> getAllStandardsList(Integer schoolId) {
		List<StandardDivisionBean> standardList = eventTypeRepository.getAllStandardsList(schoolId);
		return standardList;
	}

	@Override
	public List<StandardDivisionBean> getAllDivisionList(Integer standardId) {
		List<StandardDivisionBean> divisionList = eventTypeRepository.getAllDivisionList(standardId);
		return divisionList;
	}

	@Override
	public Integer deleteAttachment(Integer eventDeleteAttachmentId) {
		eventAttachmentRepository.deleteById(eventDeleteAttachmentId);
		return 1;
	}

	@Override
	public List<BannerImages> getAllBannerList(Integer schoolId, Integer yearId, Integer monthId, String yearName) {
		List<BannerImages> bannerImagesList = eventTypeRepository.getAllBannerList(schoolId, yearId, yearName, monthId);
		bannerImagesList.parallelStream().forEach(attachment->{
			attachment.setImagePath(commonUtlitity.getS3PreSignedUrl(attachment.getImagePath()));
		});
//		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String date1 = "2021-01-19 21:17:44";
//		Date date11;
//		try {
//			Timestamp a = new Timestamp(dateformat.parse(date1).getTime());
//			System.out.println(a);
//			List<BannerImages> bannerImagesList1 = new ArrayList<BannerImages>();
//			for (int i = 0; i < bannerImagesList.size(); i++) {
//				int b1 = a.compareTo(bannerImagesList.get(i).getcDate());
//				if (b1 > 0) {
//					bannerImagesList1.add(bannerImagesList.get(i));
//				}
//			}
		return bannerImagesList;
	}
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//
//	}

	@Override
	public Integer deletePersonalNote(Integer personalNoteId) {
		PersonalNoteModel model = new PersonalNoteModel();
		model.setPersonalNoteId(personalNoteId);
		personalNoteRepository.deleteById(personalNoteId);
		return 1;
	}

}
