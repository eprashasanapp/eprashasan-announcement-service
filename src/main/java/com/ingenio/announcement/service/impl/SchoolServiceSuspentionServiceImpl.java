package com.ingenio.announcement.service.impl;

import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingenio.announcement.Constants.ApplicationConstants;
import com.ingenio.announcement.bean.CashBankAmountBean;
import com.ingenio.announcement.bean.CountBean;
import com.ingenio.announcement.bean.EditFeeReceiptBean;
import com.ingenio.announcement.bean.EmailBean;
import com.ingenio.announcement.bean.FeeReceiptBean;
import com.ingenio.announcement.bean.FeeReceiptDetailsBean;
import com.ingenio.announcement.bean.RequestBean;
import com.ingenio.announcement.bean.ResponceBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.SchoolNameBean;
import com.ingenio.announcement.bean.StaffInfoBean;
import com.ingenio.announcement.bean.StudentBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.StudentDifferenceBean;
import com.ingenio.announcement.bean.SubheadBean;
import com.ingenio.announcement.bean.ValidateBean;
import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.AppUserTempPassword;
import com.ingenio.announcement.model.FeeHeadMasterModel;
import com.ingenio.announcement.model.FeePaidFeeModel;
import com.ingenio.announcement.model.FeeReceiptEditLogModel;
import com.ingenio.announcement.model.FeeReceiptModel;
import com.ingenio.announcement.model.FeeSendEmailIdsMasterModel;
import com.ingenio.announcement.model.FeeSettingMultiModel;
import com.ingenio.announcement.model.FeeStudentFeeModel;
import com.ingenio.announcement.model.FeeSubheadMasterModel;
import com.ingenio.announcement.model.GlobalEmailConfigModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.SchoolSuspentionDetailsModel;
import com.ingenio.announcement.model.SmsTemplateGlobalModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AndroidFcmTokenRepository;
import com.ingenio.announcement.repository.AppUserRepository;
import com.ingenio.announcement.repository.AppUserTempPasswordRepository;
import com.ingenio.announcement.repository.DivisionMasterRepository;
import com.ingenio.announcement.repository.FeeReceiptEditLogRepository;
import com.ingenio.announcement.repository.FeeRepository;
import com.ingenio.announcement.repository.FeeSendEmailIdsMasterRepository;
import com.ingenio.announcement.repository.GlobalEmailConfigRepo;
import com.ingenio.announcement.repository.PaidFeeRepository;
import com.ingenio.announcement.repository.ReceiptRepository;
import com.ingenio.announcement.repository.SchoolMasterRepository;
import com.ingenio.announcement.repository.SchoolSuspentionDetailsRepository;
import com.ingenio.announcement.repository.StandardMasterRepository;
import com.ingenio.announcement.repository.impl.CarryForwardSubjectExamDaoImpl;
import com.ingenio.announcement.repository.impl.CommonServiceDaoImpl;
import com.ingenio.announcement.service.SchoolSuspentionService;
import com.ingenio.announcement.util.CryptographyUtil;
import com.ingenio.announcement.util.EmailService;
import com.ingenio.announcement.util.NotificationUtility;
import com.ingenio.announcement.util.SmsUtility;
import com.ingenio.announcement.util.Utility;

@Component
public class SchoolServiceSuspentionServiceImpl implements SchoolSuspentionService {

	@Autowired
	SchoolSuspentionDetailsRepository schoolSuspentionDetailsRepository;
	@Autowired
	SchoolMasterRepository schoolRepo;
	
	@Autowired
	StandardMasterRepository standardMasterRepo;
	
	@Autowired
	AppUserTempPasswordRepository appUserTempPasswordRepository;
	
	@Autowired
	FeeSendEmailIdsMasterRepository feeSendEmailIdsMasterRepository;
	@Autowired
	GlobalEmailConfigRepo globalEmailConfigRepo;
	
	
	@Autowired
	CarryForwardSubjectExamDaoImpl carry;
	
	CryptographyUtil cryptoUtil = new CryptographyUtil();
	SmsUtility sms=new SmsUtility();
	
	EmailService emailService=new EmailService(); 
	@Autowired
	NotificationUtility notification;
	@Autowired
	AndroidFcmTokenRepository fcmRepo;
	
	@Autowired
	AppUserRepository appUserRepo;
	@Autowired
	CommonServiceDaoImpl commonServiceDaoImpl;
	
	
	Utility utility=new Utility();
	
	@Autowired
	FeeRepository feeRepository;

	
	@Autowired
	PaidFeeRepository paidFeeRepository;

	
	@Autowired
	ReceiptRepository receiptRepository;

	
	CryptographyUtil cry = new CryptographyUtil();

	@Autowired 
	SchoolMasterRepository schoolMasterRepo;
	@Autowired
	FeeReceiptEditLogRepository feeReceiptEditLogRepository;
	@Autowired
	DivisionMasterRepository divisionRepo;
	
	RestTemplate restTemplate=new RestTemplate();

	@Value("${otp.url}")
    private String otpUrl;
	
	
	@Value("${validate.url}")
	private String validateUrl;

	@Value("${update.url}")
	private String update;

	@Override
	public void sendSuspensionMessage() {
		
		 
		Date date=new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		List<SchoolSuspentionDetailsModel> suspensionDetails=schoolSuspentionDetailsRepository.getDetails(""+localDate);
		
		for (SchoolSuspentionDetailsModel suspention : suspensionDetails) {
			
			try {	
			SchoolNameBean schoolName=schoolRepo.getShortSchoolName(suspention.getSchoolid());
			List<StaffInfoBean> appUser=appUserRepo.getRolePrincipalDetails(suspention.getSchoolid());

			Set<String> mobileNumber=new HashSet<>();
			Set<String> emailId=new HashSet<>();
			for (StaffInfoBean staff : appUser) {
			
				String var1=suspention.getVar1();
				String var2=suspention.getVar2();

				if(!staff.getMobileNumber().equals(null) && mobileNumber.add(staff.getMobileNumber())) {
				
					String message1 ="this is var1 #var1# and this is var2 #var2# ";
					String message2=message1.replace("#var1#",var1);
					String message3=message2.replace("#var2#", schoolName.getSchoolName());

					List<Object[]> smsConfig=carry.getSmsConfig(6);
					SmsTemplateGlobalModel smsTemplateGlobal=setObject(smsConfig);

					String apikey = cryptoUtil.decrypt("azxopre", smsTemplateGlobal.getApiKey());
					String user = cryptoUtil.decrypt("mqpghdrz", smsTemplateGlobal.getUser());
					String mainUrl = cryptoUtil.decrypt("yrewpzgl", smsTemplateGlobal.getWebsiteUrl());
					String statusUrl = cryptoUtil.decrypt("qbxozgl",smsTemplateGlobal.getStatusUrl());
					String senderId = smsTemplateGlobal.getSender();
					String templateID = smsTemplateGlobal.getTemplateId();
					String temp=null;
					
//						sms.sendTextMessage(message3,apikey, senderId,temp, mainUrl,
//								statusUrl,templateID, user,"8483963648");

						sms.sendTextMessage(message3,apikey, senderId,temp, mainUrl,
								statusUrl,templateID, user,staff.getMobileNumber());
						
						
						String fcmToken=null;
						Page<String> fcmToken1=fcmRepo.getToken(staff.getMobileNumber(), PageRequest.of(0, 1));
						if(!CollectionUtils.isEmpty(fcmToken1.getContent()) ) {

							fcmToken=fcmToken1.getContent().get(0);
						
						
							HttpURLConnection conn = null;
							String serverKey="AAAAG3Lald4:APA91bFqPBjiTRaOyYShZhFlZEKXE7piSYsJnO5_OTLCyhlK1KL7gWOo1OSWQajuazdzNlHiusgqQJ_gfGflhRTUANmioOtxTb62DAMUQhO79B_djpOVIwn4noYpOf7Vf88D_pTAP7n4";
							
						        HttpClient httpClient = HttpClientBuilder.create().build();
						        
						        
								try {
									//Integer userId=token.getAppUserId();
									String mobileNO=staff.getMobileNumber();
									String body="You have Important Notification";
									String title="Notice";
									notification.sendNotification(httpClient, serverKey, mobileNO , fcmToken,conn,body,title);
									}catch(Exception e) {
										e.printStackTrace();	
									}
							

						}
						
						
						
				}
				
				if(!staff.getEmailId().equals(null) && emailId.add(staff.getEmailId())) {
					
					String emessage1 ="this is var1 #var1# and this is var2 #var2# ";
					String emessage2=emessage1.replace("#var1#",var1);
					String emessage3=emessage2.replace("#var2#", schoolName.getSchoolName());

					//emailService.sendEmail("eprashasansoftware@gmail.com","uarhvxcomgxytbmh",staff.getEmailId(), emessage3,suspention.getSubject());
					emailService.sendEmail("eprashasansoftware@gmail.com","uarhvxcomgxytbmh","amoldhole1234@gmail.com", "test message","yo wassp");


				}
									
					
			}
			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	}
	
	
	
	
	public SmsTemplateGlobalModel setObject(List<Object[]> objects) {

		SmsTemplateGlobalModel ob =new SmsTemplateGlobalModel();
		//	templateId,message,templateName,websiteUrl,statusUrl,apiKey,'user',sender

		ob.setTemplateId(objects.get(0)[0] != null ?  objects.get(0)[0].toString() : null);
		ob.setMessage(objects.get(0)[1] != null ?  objects.get(0)[1].toString() : null);
		ob.setTemplateName(objects.get(0)[2] != null ?  objects.get(0)[2].toString() : null);
		ob.setWebsiteUrl(objects.get(0)[3] != null ?  objects.get(0)[3].toString() : null);
		ob.setStatusUrl(objects.get(0)[4] != null ?  objects.get(0)[4].toString() : null);
		ob.setApiKey(objects.get(0)[5] != null ?  objects.get(0)[5].toString() : null);
		ob.setUser(objects.get(0)[6] != null ?  objects.get(0)[6].toString() : null);
		ob.setSender(objects.get(0)[7] != null ?  objects.get(0)[7].toString() : null);
		return ob;
	}




	@Override
	public List<CountBean> getAllClassAllCategoryStudentCountClasswise(Integer schoolId, Integer yearId,
			String date,Integer catOrRelignOrMinotyOrConc) {
		
		List<CountBean> standardmaster=standardMasterRepo.getAllStandards(schoolId);
		
		List<CountBean> categoryIds=commonServiceDaoImpl.getAllCategories(schoolId,catOrRelignOrMinotyOrConc);
		CountBean na=new CountBean();
		na.setCategoryId("NA");
		na.setCategoryName("NA");
		categoryIds.add(na);
		
		List<CountBean> list =commonServiceDaoImpl.getCount(schoolId,yearId,date,catOrRelignOrMinotyOrConc);
		
		// Create a Map to store lists based on standardId
		Map<Integer, List<CountBean>> standardseparatedLists = new LinkedHashMap<>();

		for (CountBean item : list) {
		    int standardId = item.getStandardId();

		    // If the standardId is not in the Map, create a new list
		    standardseparatedLists.computeIfAbsent(standardId, k -> new ArrayList<>());

		    // Add the current item to the corresponding standardId list
		    standardseparatedLists.get(standardId).add(item);
		}

		// Convert the Map values to a List of Lists
		List<List<CountBean>> standardseparatedListsResult = new ArrayList<>(standardseparatedLists.values());
		List<CountBean> list1=new ArrayList<>();
		
		for (CountBean ob : standardmaster) {


			for (List<CountBean> standardWiseList : standardseparatedListsResult) {
				if(ob.getStandardId().equals(standardWiseList.get(0).getStandardId() )) {
					Map<String, List<CountBean>> separatedCat = new LinkedHashMap<>();
					for (CountBean countBean : standardWiseList) {
						separatedCat.computeIfAbsent( countBean.getCategoryId() , k -> new ArrayList<>());
						separatedCat.get(countBean.getCategoryId()).add(countBean);
					}
					List<List<CountBean>> separatedCategory = new ArrayList<>(separatedCat.values());
					List<CountBean> categoryWiseList=new ArrayList<>();
					
					for (CountBean category1 : categoryIds) {
						int count=0;
						CountBean category=new CountBean();

						for (List<CountBean> categoryy : separatedCategory) {
							if(category1.getCategoryId().equals(categoryy.get(0).getCategoryId())) {
							//	CountBean category=new CountBean();
								category.setCategoryId(categoryy.get(0).getCategoryId() );
								category.setCategoryName(categoryy.get(0).getCategoryName());
								for (CountBean seprate : categoryy) {

									if(seprate.getStudGender().equalsIgnoreCase("Male")) {
										category.setMaleCount(seprate.getCount());
									}else if(seprate.getStudGender().equalsIgnoreCase("Female")) {
										category.setFeMaleCount(seprate.getCount());
									}else if(seprate.getStudGender().equalsIgnoreCase("NA")) {
										category.setNoGenderCount(seprate.getCount());
									}else if(seprate.getStudGender().equalsIgnoreCase("Transgender")) {
										category.setTransGenderCount(seprate.getTransGenderCount());
									}
								}
								categoryWiseList.add(category);
							}
							count++;
						}
						
						if(count==separatedCategory.size() && category.getCategoryId()==null ) {
							categoryWiseList.add(category1);	
						}
					}
					ob.setCounts(categoryWiseList);
				}
			}
			list1.add(ob);
		}
		return list1;
	}

//	@Override
//	public List<CountBean> getAllClassAllCategoryStudentCountClasswise(Integer schoolId, Integer yearId, String date, Integer catOrRelignOrMinotyOrConc) {
//	    
//	    // Fetch all standards for the school
//	    List<CountBean> standardMaster = standardMasterRepo.getAllStandards(schoolId);
//
//	    // Fetch all categories for the school
//	    List<CountBean> categoryIds = commonServiceDaoImpl.getAllCategories(schoolId, catOrRelignOrMinotyOrConc);
//
//	    // Add "NA" as a default category
//	    CountBean naCategory = new CountBean();
//	    naCategory.setCategoryId("NA");
//	    naCategory.setCategoryName("NA");
//	    categoryIds.add(naCategory);
//
//	    // Fetch counts from the database
//		List<CountBean> countList =commonServiceDaoImpl.getCount(schoolId,yearId,date,catOrRelignOrMinotyOrConc);
//
//	    // Group counts by standardId
//	    Map<Integer, List<CountBean>> groupedByStandard = countList.stream()
//	        .collect(Collectors.groupingBy(CountBean::getStandardId, LinkedHashMap::new, Collectors.toList()));
//
//	    // Result list to hold final data
//	    List<CountBean> result = new ArrayList<>();
//
//	    // Process each standard
//	    for (CountBean standard : standardMaster) {
//	        // Get the list of counts for the current standard
//	        List<CountBean> standardWiseCounts = groupedByStandard.getOrDefault(standard.getStandardId(), new ArrayList<>());
//
//	        // Group counts by categoryId for the current standard
//	        Map<String, List<CountBean>> groupedByCategory = standardWiseCounts.stream()
//	            .collect(Collectors.groupingBy(CountBean::getCategoryId, LinkedHashMap::new, Collectors.toList()));
//
//	        // Prepare the category-wise list for the current standard
//	        List<CountBean> categoryWiseList = new ArrayList<>();
//	        for (CountBean category : categoryIds) {
//	            CountBean categoryBean = new CountBean();
//	            categoryBean.setCategoryId(category.getCategoryId());
//	            categoryBean.setCategoryName(category.getCategoryName());
//
//	            // Fetch counts for the current category
//	            List<CountBean> categoryCounts = groupedByCategory.getOrDefault(category.getCategoryId(), new ArrayList<>());
//
//	            // Initialize counts to 0
//	            categoryBean.setMaleCount(BigInteger.ZERO);
//	            categoryBean.setFeMaleCount(BigInteger.ZERO);
//	            categoryBean.setNoGenderCount(BigInteger.ZERO);
//	            categoryBean.setTransGenderCount(BigInteger.ZERO);
//
//	            // Sum up the counts by gender
//	            for (CountBean count : categoryCounts) {
//	                switch (count.getStudGender()) {
//	                    case "Male":
//	                        categoryBean.setMaleCount(count.getCount());
//	                        break;
//	                    case "Female":
//	                        categoryBean.setFeMaleCount(count.getCount());
//	                        break;
//	                    case "NA":
//	                        categoryBean.setNoGenderCount(count.getCount());
//	                        break;
//	                    case "Transgender":
//	                        categoryBean.setTransGenderCount(count.getCount());
//	                        break;
//	                }
//	            }
//
//	            // Add the category-wise data to the list
//	            categoryWiseList.add(categoryBean);
//	        }
//
//	        // Set the counts for the standard
//	        standard.setCounts(categoryWiseList);
//
//	        // Add the standard to the result
//	        result.add(standard);
//	    }
//
//	    return result;
//	}



	@Override
	public List<CashBankAmountBean> getCashBankAmountFromFeeForTrailb(List<RequestBean> requestBean) {
		
		
		
		List<CashBankAmountBean> totalAmountBankAndCash=new ArrayList<>();
		for (RequestBean requestBean1 : requestBean) {
			
			CashBankAmountBean ob=new CashBankAmountBean();
			
			List<CashBankAmountBean> cashBankAmountBean= commonServiceDaoImpl.getBankDetailsAndTotalAmount(requestBean1);
			
			List<CashBankAmountBean> subheadDetails= commonServiceDaoImpl.getSubHeadDetails(requestBean1);
			ob.setSubheadDetails(subheadDetails);

			Double cash=0.0;
			
			Iterator<CashBankAmountBean> iterator = cashBankAmountBean.iterator();
			while (iterator.hasNext()) {
			    CashBankAmountBean cashob = iterator.next();
			    if (cashob.getPayTypeName().equals("Cash")) {
			        cash = cash + cashob.getPaidFee();
			        iterator.remove(); // This removes the object from the original list.
			    }
			}

			ob.setCash(cash);
			ob.setSchoolid(requestBean1.getSchoolid());
			//ob.setPayTypeWisebifurcation(cashBankAmountBean);
			//totalAmountBankAndCash.add(cashBankAmountBean);
			totalAmountBankAndCash.add(ob);
	       
			Map<String, List<CashBankAmountBean>> bankIdMap = new HashMap<>();

			 for (CashBankAmountBean item : cashBankAmountBean) {
		            String bankId = item.getBankId();
		            bankIdMap.computeIfAbsent(bankId, k -> new ArrayList<>()).add(item);
		        }
				List<List<CashBankAmountBean>> separatedCategory = new ArrayList<>(bankIdMap.values());

				List<CashBankAmountBean> bankDetailsBean=new ArrayList<>();
				
				for (List<CashBankAmountBean> addAllBankPay : separatedCategory) {

					CashBankAmountBean ob1=new CashBankAmountBean();
					Double paid=0.0;
					
					ob1.setBankId(addAllBankPay.get(0).getBankId());
					ob1.setBankName(addAllBankPay.get(0).getBankName());
					ob1.setBankAccNo(addAllBankPay.get(0).getBankAccNo());
					ob1.setIfscNo(addAllBankPay.get(0).getIfscNo());
					ob1.setPayTypeWisebifurcation(addAllBankPay);
					
					for (CashBankAmountBean bankId : addAllBankPay) {
						
						paid=paid+bankId.getPaidFee();
						bankId.setBankId(null);
						bankId.setBankName(null);
						bankId.setBankAccNo(null);
						bankId.setIfscNo(null);
					}
					ob1.setPaidFee(paid);
					
					bankDetailsBean.add(ob1) ;
				}
				
				
				ob.setBankDetailsBean(bankDetailsBean);
		}

		
		return totalAmountBankAndCash;
	}




	@Override
	public Integer updateTempPassword(AppUserTempPassword appUserTempPassword) {

		Integer tempPassId= appUserTempPasswordRepository.save(appUserTempPassword).getTempPassId();

		HttpHeaders headers = new HttpHeaders();
        
		 headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);  // Adjust content type if needed
	     String requestBody = String.format("mobileNum=%s&schoolId=%s", appUserTempPassword.getMobileNumber(),appUserTempPassword.getSchoolid());

            // Create HttpEntity with headers and body
            HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);

	        // Make the GET request and capture the response
	        //String response = restTemplate.getForObject(fullUrl, String.class);

	        try {

	            ResponseEntity<String> responseEntity = restTemplate.postForEntity(otpUrl, requestEntity, String.class);

	           System.out.println(responseEntity.getStatusCode());
	           
	           if(responseEntity.getStatusCode()==HttpStatus.OK) {
	        	   return tempPassId;
	           }else {
	        	   return null;
	           }
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	        
	        
		return null;
	}




	@Override
	public Integer validateOtpForResetPassword(ValidateBean validateBean) {

		 //RestTemplate restTemplate = new RestTemplate();

	        // Build the URL with parameters
	        String fullUrl = String.format("%s?schoolId=%s&otpnum=%s&mobileNum=%s", validateUrl,validateBean.getSchoolId(),validateBean.getOtp(), validateBean.getMobileNumber());

	        // Make the GET request and capture the response
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(fullUrl, String.class);

            String statusCode=null;
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                JsonNode rootNode = objectMapper.readTree(responseEntity.getBody());
                 statusCode = rootNode.path("statusCode").asText();
                System.out.println("Status Code: " + statusCode);
            } catch (Exception e) {
                e.printStackTrace();
                // Handle the exception appropriately
            }
            
            
            if(statusCode.equals("200")) {

            	
            	Optional<AppUserTempPassword> findById = appUserTempPasswordRepository.findById(validateBean.getTempPassId());
            	
            	HttpHeaders headers = new HttpHeaders();
                
       		 headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);  // Adjust content type if needed
       		String requestBody=null;
       		 if(findById.get().getSchoolid()==0) {
       			 requestBody = String.format("mobileNumber=%s&password=%s&tempPassId=%s", validateBean.getMobileNumber(),findById.get().getPassword(),validateBean.getTempPassId());
       		 }else {
       	     requestBody = String.format("mobileNumber=%s&schoolid=%s&password=%s&tempPassId=%s", validateBean.getMobileNumber(),findById.get().getSchoolid(),findById.get().getPassword(),validateBean.getTempPassId());
       		 }
                   // Create HttpEntity with headers and body
                   HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);

       	        // Make the GET request and capture the response
       	        //String response = restTemplate.getForObject(fullUrl, String.class);

       	        try {

       	            ResponseEntity<String> responseEntity1 = restTemplate.postForEntity(update, requestEntity, String.class);

       	           System.out.println(responseEntity1.getStatusCode());
       	           
       	        } catch (Exception e) {
       	        	e.printStackTrace();
       	        }
       	        
       	        
       	     appUserTempPasswordRepository.updateFlag(validateBean.getTempPassId());
            	return 1;
            }else {
            	return 0;
            }
		
	}




	@Override
	public Integer editFeeReceipt(EditFeeReceiptBean editFeeReceiptBean) {

		
		Integer logId=feeReceiptEditLogRepository.save(editFeeReceiptBean.getFeeReceiptEditLogModel()).getReceiptLogId();
		
		receiptRepository.updateDate(editFeeReceiptBean.getReceiptId(),editFeeReceiptBean.getFeeReceiptEditLogModel().getReceiptNewDate());
		
		Date dateStr = editFeeReceiptBean.getFeeReceiptEditLogModel().getReceiptNewDate();

		LocalDate localDateObj = dateStr.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		String convertedDate = localDateObj.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		System.out.println(convertedDate);
		
		paidFeeRepository.updatePaidDateAgainstReceiptId(editFeeReceiptBean.getReceiptId(),convertedDate);
		
		
		if(!CollectionUtils.isEmpty(editFeeReceiptBean.getSubheadBean())) {
		for (SubheadBean subheadBean : editFeeReceiptBean.getSubheadBean()) {
			
			if(subheadBean.getPaidId()==null || subheadBean.getPaidId()==0) {
			String schoolKey = schoolMasterRepo.findBySchoolid(editFeeReceiptBean.getSchoolid())
					.getSchoolKey();
			String globalDbSchoolKey = "1".concat("" + schoolKey);
			String incrementedId = "" + feeRepository.getMaxPaidId(editFeeReceiptBean.getSchoolid());
			String paidId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
//			System.out.println("paidId" +paidId);
			FeePaidFeeModel feeModel = new FeePaidFeeModel();
			feeModel.setPaidId(Integer.parseInt(paidId));
			
			FeeReceiptModel feeReceiptModel=new FeeReceiptModel();
			feeReceiptModel.setReceiptId(editFeeReceiptBean.getReceiptId());
			feeModel.setFeeReceiptModel(feeReceiptModel);
			
			Date date11 = new Date();
			SimpleDateFormat format11 = new SimpleDateFormat("dd-MM-yyyy");
			String date111 = format11.format(date11);
			feeModel.setCdate(date111);

			FeeSettingMultiModel feeSettingMultiModel = new FeeSettingMultiModel();
			feeSettingMultiModel.setFeeSetMultiId(subheadBean.getFeeSettingMultiId());
			feeModel.setFeeSettingMultiModel(feeSettingMultiModel);
			FeeStudentFeeModel feeStudentFeeModel = new FeeStudentFeeModel();
			feeStudentFeeModel.setStudFeeId(subheadBean.getStudFeeId());
			feeModel.setFeeStudentFeeModel(feeStudentFeeModel);
			
			SchoolMasterModel schoolMasterModel=new SchoolMasterModel(); 
			schoolMasterModel.setSchoolid(editFeeReceiptBean.getSchoolid());
			feeModel.setSchoolMasterModel(schoolMasterModel);
			FeeHeadMasterModel feeHeadMasterModel = new FeeHeadMasterModel();
			feeHeadMasterModel.setHeadId(subheadBean.getHeadId());
			feeModel.setFeeHeadMasterModel(feeHeadMasterModel);
			FeeSubheadMasterModel feeSubheadMasterModel = new FeeSubheadMasterModel();
			feeSubheadMasterModel.setSubheadId(subheadBean.getSubheadId());
			feeModel.setFeeSubheadMasterModel(feeSubheadMasterModel);
			feeModel.setPayFee(subheadBean.getPaidFee());
			feeModel.setPaidfeeDate(subheadBean.getPaidFeeDate());

			feeModel.setPayType(subheadBean.getPayType());
			feeModel.setDiscount(subheadBean.getDiscount());
			feeModel.setOrderId(subheadBean.getOrderId());
			feeModel.setTranscationId(subheadBean.getTranscationId());
			feeModel.setDdchqno(subheadBean.getDdchqno());
			feeModel.setNarration(subheadBean.getNarration());
			feeModel.setBankId(subheadBean.getBankId());
			
			YearMasterModel yearMasterModel1 = new YearMasterModel();
			if (subheadBean.getYearId() != 0 && subheadBean.getYearId() != null) {
				yearMasterModel1.setYearId(subheadBean.getYearId());
				feeModel.setYearMasterModel(yearMasterModel1);
			}
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			feeModel.setAppUserRoleModel(appUserRoleModel);
			paidFeeRepository.saveAndFlush(feeModel);
			}else {
				paidFeeRepository.updatePaidFee(subheadBean.getPaidId(),subheadBean.getPaidFee());
			}

		}
		}
		
		return 1;
	}




	@Override
	public List<FeeReceiptBean> getEditedReceipt(Integer schoolId) {

		List<FeeReceiptBean> receiptDetails = commonServiceDaoImpl.getReceiptDetails(schoolId);
		//FeeReceiptDetailsBean studentInfo=feeReceiptEditLogRepository.getAllReceiptData(schoolId);
		
		return receiptDetails;
	}




//	@Override
//	public List<CountBean> getDivisisionList(Integer schoolId, Integer yearId, String date,
//			Integer catOrRelignOrMinotyOrConc) {
//		// List<CountBean> standardmaster=standardMasterRepo.getAllStandards(schoolId);
//		
//		List<CountBean> standardmaster=standardMasterRepo.getAllStandards(schoolId);
//		List<CountBean> division=divisionRepo.getAllDivisions(schoolId);
//
//		
//		List<CountBean> categoryIds=commonServiceDaoImpl.getAllCAtegories(schoolId,catOrRelignOrMinotyOrConc);
//		CountBean na=new CountBean();
//		na.setCategoryId("NA");
//		na.setCategoryName("NA");
//		categoryIds.add(na);
//		
//		List<CountBean> list =commonServiceDaoImpl.getDivisionWiseCount(schoolId,yearId,date,catOrRelignOrMinotyOrConc);
//		
//		// Create a Map to store lists based on standardId
//		Map<Integer, List<CountBean>> standardseparatedLists = new LinkedHashMap<>();
//
//		for (CountBean item : list) {
//		    int standardId = item.getStandardId();
//
//		    // If the standardId is not in the Map, create a new list
//		    standardseparatedLists.computeIfAbsent(standardId, k -> new ArrayList<>());
//
//		    // Add the current item to the corresponding standardId list
//		    standardseparatedLists.get(standardId).add(item);
//		}
//
//		// Convert the Map values to a List of Lists
//		List<List<CountBean>> standardseparatedListsResult = new ArrayList<>(standardseparatedLists.values());
//		List<CountBean> list1=new ArrayList<>();
//		
//		for (CountBean ob : standardmaster) {
//
//
//			for (List<CountBean> standardWiseList : standardseparatedListsResult) {
//				if(ob.getStandardId().equals(standardWiseList.get(0).getStandardId() )) {
//					Map<String, List<CountBean>> separatedCat = new LinkedHashMap<>();
//					for (CountBean countBean : standardWiseList) {
//						separatedCat.computeIfAbsent( countBean.getCategoryId() , k -> new ArrayList<>());
//						separatedCat.get(countBean.getCategoryId()).add(countBean);
//					}
//					List<List<CountBean>> separatedCategory = new ArrayList<>(separatedCat.values());
//					List<CountBean> categoryWiseList=new ArrayList<>();
//					
//					for (CountBean category1 : categoryIds) {
//						int count=0;
//						CountBean category=new CountBean();
//
//						for (List<CountBean> categoryy : separatedCategory) {
//							if(category1.getCategoryId().equals(categoryy.get(0).getCategoryId())) {
//							//	CountBean category=new CountBean();
//								category.setCategoryId(categoryy.get(0).getCategoryId() );
//								category.setCategoryName(categoryy.get(0).getCategoryName());
//								for (CountBean seprate : categoryy) {
//
//									if(seprate.getStudGender().equals("Male")) {
//										category.setMaleCount(seprate.getCount());
//									}else if(seprate.getStudGender().equals("Female")) {
//										category.setFeMaleCount(seprate.getCount());
//									}else if(seprate.getStudGender().equals("NA")) {
//										category.setNoGenderCount(seprate.getCount());
//									}
//								}
//								categoryWiseList.add(category);
//							}
//							count++;
//						}
//						
//						if(count==separatedCategory.size() && category.getCategoryId()==null ) {
//							categoryWiseList.add(category1);	
//						}
//					}
//					ob.setCounts(categoryWiseList);
//				}
//			}
//			list1.add(ob);
//		}
//		return list1;
//
//
//	}


	@Override
	public List<CountBean> getDivisionList(Integer schoolId, Integer yearId, String date,
	        Integer catOrRelignOrMinotyOrConc) {
	    List<CountBean> standardMaster = standardMasterRepo.getAllStandards(schoolId);
	    List<CountBean> categoryIds = commonServiceDaoImpl.getAllCategories(schoolId, catOrRelignOrMinotyOrConc);
	    CountBean na = new CountBean();
	    na.setCategoryId("NA");
	    na.setCategoryName("NA");
	    categoryIds.add(na);
	    List<CountBean> list = commonServiceDaoImpl.getDivisionWiseCount(schoolId, yearId, date, catOrRelignOrMinotyOrConc);

	    // Create a Map to store lists based on standardId
	    Map<Integer, List<CountBean>> standardSeparatedLists = new LinkedHashMap<>();

	    for (CountBean item : list) {
	        int standardId = item.getStandardId();

	        // If the standardId is not in the Map, create a new list
	        standardSeparatedLists.computeIfAbsent(standardId, k -> new ArrayList<>());

	        // Add the current item to the corresponding standardId list
	        standardSeparatedLists.get(standardId).add(item);
	    }

	    List<CountBean> result = new ArrayList<>();

	    for (CountBean standard : standardMaster) {
	        CountBean standardWithDivisions = new CountBean();
	        standardWithDivisions.setStandardId(standard.getStandardId());
	        standardWithDivisions.setStandardName(standard.getStandardName());

	        List<CountBean> divisionsForStandard = new ArrayList<>();

	        // Fetch divisions for the current standard only
	        List<CountBean> divisions = divisionRepo.getAllDivisions(schoolId, standard.getStandardId());
	        CountBean d=new CountBean();
	        d.setDivisionId("NA");
	        d.setDivisionName("NA");
	        divisions.add(d);
	        // Check if there are divisions available for the standard
	        if (!divisions.isEmpty()) {
	            for (CountBean div : divisions) {
	                CountBean divisionWithCategories = new CountBean();
	                divisionWithCategories.setDivisionId(div.getDivisionId());
	                divisionWithCategories.setDivisionName(div.getDivisionName());

	                List<CountBean> categoriesForDivision = new ArrayList<>();
	                for (CountBean category : categoryIds) {
	                    CountBean categoryWithCounts = new CountBean();
	                    categoryWithCounts.setCategoryId(category.getCategoryId());
	                    categoryWithCounts.setCategoryName(category.getCategoryName());

	                    // Find the corresponding count for this category, division, and standard
	                    for (CountBean count : list) {
	                        if (count.getStandardId().equals(standard.getStandardId()) &&
	                                count.getDivisionId().equals(div.getDivisionId()) &&
	                                count.getCategoryId().equals(category.getCategoryId())) {

	                            // Iterate over the counts for different genders within the category
	                        	
//								for (CountBean seprate : categoryy) {
	                        	        if (count.getStudGender().equals("Male")) {
	                        	            categoryWithCounts.setMaleCount(count.getCount());
	                        	        } else if (count.getStudGender().equals("Female")) {
	                        	            categoryWithCounts.setFeMaleCount(count.getCount());
	                        	        } else if (count.getStudGender().equals("NA")) {
	                        	            categoryWithCounts.setNoGenderCount(count.getCount());
	                        	        }
	                        	  //  }
	                        	
	                        	

	                           // break; // Exit the loop after processing counts
	                        }
	                    }

	                    categoriesForDivision.add(categoryWithCounts);
	                }

	                divisionWithCategories.setCounts(categoriesForDivision);
	                divisionsForStandard.add(divisionWithCategories);
	            }
	        }

	        standardWithDivisions.setCounts(divisionsForStandard);
	        result.add(standardWithDivisions);
	    }

	    return result;
	}




	@Override
	public List<CountBean> getAgeStandardWise(Integer schoolId, Integer yearId, String date) {

		List<CountBean> standardMaster = standardMasterRepo.getAllStandards(schoolId);

		List<CountBean> ageInyears=commonServiceDaoImpl.getAgeStandardWise(schoolId,yearId,date);

		Map<Integer, List<CountBean>> standardSeparatedLists = new LinkedHashMap<>();
		for (CountBean item : ageInyears) {
			int standardId = item.getStandardId();

			// If the standardId is not in the Map, create a new list
			standardSeparatedLists.computeIfAbsent(standardId, k -> new ArrayList<>());

			// Add the current item to the corresponding standardId list
			standardSeparatedLists.get(standardId).add(item);
		}

		for (CountBean standards : standardMaster) {

			List<CountBean> list = standardSeparatedLists.get(standards.getStandardId());
			Map<String, List<CountBean>> ageWiseCountList = new LinkedHashMap<>();

			if(list!=null)
				for (CountBean ageWiseList : list) {
					ageWiseCountList.computeIfAbsent(ageWiseList.getAge_in_years(), k -> new ArrayList<>());
					ageWiseCountList.get(ageWiseList.getAge_in_years()).add(ageWiseList);

				}

			List<CountBean> ageWiseCount=new ArrayList<>();
			for (Map.Entry<String, List<CountBean>> entry : ageWiseCountList.entrySet()) {

				CountBean ob =new CountBean();
				String key = entry.getKey();
				List<CountBean> ageWise = entry.getValue();
				ob.setAge_in_years(key);
				CountBean counts=new CountBean();
				counts.setAge_in_years(key);
				for (CountBean count : ageWise) {

					if (count.getStudGender().equals("Male")) {
						counts.setMaleCount(count.getStudentCount());
					} else if (count.getStudGender().equals("Female")) {
						counts.setFeMaleCount(count.getStudentCount());
					} else if (count.getStudGender().equals("NA")) {
						counts.setNoGenderCount(count.getStudentCount());
					}
				}
				ob.setAgeWisecounts(counts);
				ageWiseCount.add(ob);
			}
			standards.setCounts(ageWiseCount);
		}
		System.out.println("");

		return standardMaster;
	}




	@Override
	public List<CountBean> getAgeDivisionWise(Integer schoolId, Integer yearId, String date) {
		List<CountBean> standardMaster = standardMasterRepo.getAllStandards(schoolId);
		List<CountBean> ageInyears=commonServiceDaoImpl.getAgeDivisionWise(schoolId,yearId,date);

		Map<Integer, List<CountBean>> standardSeparatedLists = new LinkedHashMap<>();
		for (CountBean item : ageInyears) {
			int standardId = item.getStandardId();
			// If the standardId is not in the Map, create a new list
			standardSeparatedLists.computeIfAbsent(standardId, k -> new ArrayList<>());

			// Add the current item to the corresponding standardId list
			standardSeparatedLists.get(standardId).add(item);
		}

		for (CountBean standards : standardMaster) {

			List<CountBean> divisions = divisionRepo.getAllDivisions(schoolId, standards.getStandardId());
			CountBean d=new CountBean();
			d.setDivisionId("NA");
			d.setDivisionName("NA");
			divisions.add(d);
			List<CountBean> list = standardSeparatedLists.get(standards.getStandardId());

			Map<String, List<CountBean>> divisionWiseSepration = new LinkedHashMap<>();

			if(list!=null)
				for (CountBean divisionWise : list) {
					divisionWiseSepration.computeIfAbsent(divisionWise.getDivisionId(), k -> new ArrayList<>());
					divisionWiseSepration.get(divisionWise.getDivisionId()).add(divisionWise);
				}

			for (CountBean divisoinId : divisions) {

				List<CountBean>divisionList=divisionWiseSepration.get(divisoinId.getDivisionId());

				Map<String, List<CountBean>> ageWiseCountList = new LinkedHashMap<>();

				if(divisionList!=null)
					for (CountBean ageWiseList : divisionList) {
						ageWiseCountList.computeIfAbsent(ageWiseList.getAge_in_years(), k -> new ArrayList<>());
						ageWiseCountList.get(ageWiseList.getAge_in_years()).add(ageWiseList);

					}
				List<CountBean> ageWiseCount=new ArrayList<>();
				for (Map.Entry<String, List<CountBean>> entry : ageWiseCountList.entrySet()) {

					CountBean ob =new CountBean();
					String key = entry.getKey();
					List<CountBean> ageWise = entry.getValue();
					ob.setAge_in_years(key);

					CountBean counts=new CountBean();
					counts.setAge_in_years(key);
					for (CountBean count : ageWise) {

						if (count.getStudGender().equals("Male")) {
							counts.setMaleCount(count.getStudentCount());
						} else if (count.getStudGender().equals("Female")) {
							counts.setFeMaleCount(count.getStudentCount());
						} else if (count.getStudGender().equals("NA")) {
							counts.setNoGenderCount(count.getStudentCount());
						}
					}

					ob.setAgeWisecounts(counts);

					//  ob.setAgeWisecounts(ageWise);
					ageWiseCount.add(ob);

				}

				divisoinId.setDivisionBean(ageWiseCount);
			}

			standards.setCounts(divisions);

		}
		System.out.println("");

		return standardMaster;
	}

	
	public ResponseBodyBean<String> sendEmail(EmailBean emailBean,String senderEmail,String senderPassword) {
//		String senderEmail=null;
//		String senderPassword=null;
//		        // SMTP server details
        String host = "smtp.gmail.com";
        String port = "587"; // or your SMTP port

        // Email content
        String subject = emailBean.getSubject();
        String body = emailBean.getBody();
//        String subject = "महत्वाची सूचना";
//        String body = "<div style=\"color: red; font-weight: bold;\">महत्वाची सूचना</div>" +
//                "<div style=\"color: black; font-weight: bold;\">सर्व विद्यार्थ्यांना कळविण्यात येते की, ज्या विद्यार्थ्यांना सन 2023-2024 या वर्षात डॉ. बाबासाहेब आंबेडकर स्वाधार योजनेचे अर्ज ऑनलाईन पद्धतीने भरलेले आहे, त्यांनी कोणतेही \"शासकीय शासकीय व खाजगी नौकरी करीत नसल्याबाबतचे प्रमाणपत्र\" (Original) UPLOAD करणे अनिवार्य आहे. सदर प्रमाणपत्र 10/05/2024 पर्यंत अपलोड करावे, अन्यथा आपला अर्ज Reject करण्यात येईल याची गांभीर्याने नोंद घ्यावी</div>";

        
        // Set properties for SMTP server
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);

        
        Session session = createSession(properties, senderEmail, senderPassword);


        try {
            // Create a MimeMessage object
            Message message = new MimeMessage(session);

            // Set sender and recipients
            message.setFrom(new InternetAddress(senderEmail));
            InternetAddress[] recipients = new InternetAddress[emailBean.getToEmailIds().size()];
           
            for (int i = 0; i < emailBean.getToEmailIds().size(); i++) {
            	recipients[i] = new InternetAddress(emailBean.getToEmailIds().get(i).trim());
			}
            //
            message.setRecipients(Message.RecipientType.TO, recipients);
            
//            InternetAddress[] ccRecipients = new InternetAddress[emailBean.getCc().size()];
//            for (int i = 0; i < emailBean.getCc().size(); i++) {
//                ccRecipients[i] = new InternetAddress(emailBean.getCc().get(i).trim());
//            }
//            message.setRecipients(Message.RecipientType.CC, ccRecipients);
//
//            // Add BCC recipients
//            InternetAddress[] bccRecipients = new InternetAddress[emailBean.getBcc().size()];
//            for (int i = 0; i < emailBean.getBcc().size(); i++) {
//                bccRecipients[i] = new InternetAddress(emailBean.getBcc().get(i).trim());
//            }
//            message.setRecipients(Message.RecipientType.BCC, bccRecipients);

            
            // Set email subject and body
            message.setSubject(subject);
           // message.setText(body);
            message.setContent(body, "text/html; charset=utf-8");

            // Send the email
            Transport.send(message);
            
            //--------------------------//---------------------------------
            //notificatin
    		
            HttpClient httpClient = HttpClientBuilder.create().build();
    		String serverKey=ApplicationConstants.serverKey;

            HttpPost httpPost = new HttpPost("https://fcm.googleapis.com/fcm/send");
    		httpPost.setHeader("Authorization", "key=" + serverKey);
    		httpPost.setHeader("Content-Type", "application/json");
    		Integer success=0;
    		if(!CollectionUtils.isEmpty(emailBean.getTokens())) {
    			try {

    				JSONObject json = new JSONObject();
    				json.put("registration_ids",emailBean.getTokens());

    				JSONObject data = new JSONObject();
    				data.put("title", emailBean.getNotificationTitle());
    				data.put("body", emailBean.getNotificationSubject());
    				//data.put("userId",1059000001);
    				//data.put("menuId",7);
    				//data.put("menuName","Notice");
    				//data.put("SeenStataus", "0");
    				json.put("data", data);

    				StringEntity stringEntity = new StringEntity(json.toString());
    				httpPost.setEntity(stringEntity);
    				HttpResponse response = httpClient.execute(httpPost);
    				success = response.getStatusLine().getStatusCode();
    				String statusMessage = response.getStatusLine().getReasonPhrase();
    				Thread.sleep(1000);

    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}

            
            
            
			return new ResponseBodyBean<String>("200", HttpStatus.OK, "Email sent successfully.");

         //  System.out.println("Email sent successfully.");
        } catch (MessagingException e) {
        	return new ResponseBodyBean<String>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					"Error occurred while sending email");
           // System.out.println("Error occurred while sending email: " + e.getMessage());
        }
	}

	private static Session createSession(Properties properties, final String senderEmail, final String senderPassword) {
        return Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });
    }




	@Override
	public List<StudentDetailsBean> studDifference(Integer schoolId, Integer yearId, Integer standardId, Integer divisionId,
			String date) {

		List<StudentDifferenceBean> ageInyears=commonServiceDaoImpl.getStudDiff(schoolId,yearId,standardId,divisionId,date);
	
		Map<Integer, List<StudentDifferenceBean>> ageWiseCountList = new LinkedHashMap<>();

		if(ageInyears!=null)
			for (StudentDifferenceBean ageWiseList : ageInyears) {
				ageWiseCountList.computeIfAbsent(ageWiseList.getStudentId(), k -> new ArrayList<>());
				ageWiseCountList.get(ageWiseList.getStudentId()).add(ageWiseList);

			}
		
		
		List<StudentDetailsBean> list=new ArrayList<>(); 
		for (Map.Entry<Integer, List<StudentDifferenceBean>> entry : ageWiseCountList.entrySet()) {
			StudentDetailsBean ob=new StudentDetailsBean();
			Integer studentId=entry.getKey();
			
			if(!CollectionUtils.isEmpty(entry.getValue())) {
				StudentDifferenceBean studInfo =entry.getValue().get(0);
			

				ob.setRenewStudentId(studInfo.getRenewstudentId());
				//ob.setRollNo(studInfo.getRollNo());
				ob.setStudentFName(studInfo.getStudFName());
				ob.setStudentLName(studInfo.getStudLName());
				ob.setRegNo(studInfo.getStudent_RegNo());
				
			}
			
			ob.setStudentId(studentId);
			List<StudentDifferenceBean> studentDiffList=new ArrayList<>();

			for (StudentDifferenceBean studentDifferenceBean : entry.getValue()) {
				StudentDifferenceBean ob1=new StudentDifferenceBean();
				ob1.setStdId(studentDifferenceBean.getStdId());
				ob1.setDivId(studentDifferenceBean.getDivId());
				ob1.setStandardName(studentDifferenceBean.getStandardName());
				ob1.setDivisionName(studentDifferenceBean.getDivisionName());
				ob1.setMonthId(studentDifferenceBean.getMonthId());
				ob1.setRollNo(studentDifferenceBean.getRollNo());
				studentDiffList.add(ob1);
			}
			ob.setStudentDiffList(studentDiffList);
			list.add(ob);
			
		}
		System.out.println("");
		return list;
	}




	@Override
	public List<StudentDifferenceBean> getStudentsPresentInAttendanceCatalogButNotInStudentRenew(Integer schoolId,
			Integer yearId,String date) {


		List<StudentDifferenceBean> ageInyears=commonServiceDaoImpl.getStudentsPresentInAttendanceCatalogButNotInStudentRenew(schoolId,yearId,date);

		
		return ageInyears;
	}




	@Override
	public List<ResponceBean> getCashBankAmountFromFeeForTrailbDateWise(List<RequestBean> requestBean) {
		
		List<ResponceBean> responceBean=new ArrayList<>(); 
		//List<List<CashBankAmountBean>>overallDateWiseResponce=new ArrayList<>();
		
		
		for (RequestBean requestBean1 : requestBean) {
			ResponceBean ob3=new ResponceBean();
			ob3.setSchoolid(requestBean1.getSchoolid());
			
			List<CashBankAmountBean> totalAmountBankAndCash=new ArrayList<>();

			List<CashBankAmountBean> cashBankAmountBeanNew= commonServiceDaoImpl.getBankDetailsAndTotalAmountDateWise(requestBean1);
			
			
			
			Map<String, List<CashBankAmountBean>> paidFeeDateMap = new LinkedHashMap<>();

			 for (CashBankAmountBean item : cashBankAmountBeanNew) {
		            String paidFeeDate = item.getPaidfeeDate();
		            paidFeeDateMap.computeIfAbsent(paidFeeDate, k -> new ArrayList<>()).add(item);
		        }
				List<CashBankAmountBean> subheadDetailsNew= commonServiceDaoImpl.getSubHeadDetailsDateWise(requestBean1);

				Map<String, List<CashBankAmountBean>> paidFeeDateSubheadMap = new LinkedHashMap<>();

			 for (CashBankAmountBean item : subheadDetailsNew) {
				 
		            String paidFeeDate = item.getPaidfeeDate();
		            paidFeeDateSubheadMap.computeIfAbsent(paidFeeDate, k -> new ArrayList<>()).add(item);

			 }
			
			 List<CashBankAmountBean> list= new ArrayList<>();
			 for (Map.Entry<String, List<CashBankAmountBean>> entry : paidFeeDateMap.entrySet()) {
				 
					CashBankAmountBean ob=new CashBankAmountBean();

				 String date = entry.getKey(); // This is the key (the date in your map)
			
				 ob.setDate(date);
				 List<CashBankAmountBean> cashBankAmountBean = entry.getValue(); // This is the list of CashBankAmountBean objects
				    //List<CashBankAmountBean> subheadDetails= paidFeeDateSubheadMap.get(date);
					
					ob.setSubheadDetails(paidFeeDateSubheadMap.get(date));

					 Double cash=0.0;
						
						Iterator<CashBankAmountBean> iterator = cashBankAmountBean.iterator();
						while (iterator.hasNext()) {
						    CashBankAmountBean cashob = iterator.next();
						    if (cashob.getPayTypeName().equals("Cash")) {
						        cash = cash + cashob.getPaidFee();
						        iterator.remove(); // This removes the object from the original list.
						    }
						}

						ob.setCash(cash);
						ob.setSchoolid(requestBean1.getSchoolid());
						//ob.setPayTypeWisebifurcation(cashBankAmountBean);
						//totalAmountBankAndCash.add(cashBankAmountBean);
						totalAmountBankAndCash.add(ob);
				       
						Map<String, List<CashBankAmountBean>> bankIdMap = new HashMap<>();

						 for (CashBankAmountBean item : cashBankAmountBean) {
					            String bankId = item.getBankId();
					            bankIdMap.computeIfAbsent(bankId, k -> new ArrayList<>()).add(item);
					        }
							List<List<CashBankAmountBean>> separatedCategory = new ArrayList<>(bankIdMap.values());

							List<CashBankAmountBean> bankDetailsBean=new ArrayList<>();
							
							for (List<CashBankAmountBean> addAllBankPay : separatedCategory) {

								CashBankAmountBean ob1=new CashBankAmountBean();
								Double paid=0.0;
								
								ob1.setBankId(addAllBankPay.get(0).getBankId());
								ob1.setBankName(addAllBankPay.get(0).getBankName());
								ob1.setBankAccNo(addAllBankPay.get(0).getBankAccNo());
								ob1.setIfscNo(addAllBankPay.get(0).getIfscNo());
								ob1.setPayTypeWisebifurcation(addAllBankPay);
								
								for (CashBankAmountBean bankId : addAllBankPay) {
									
									paid=paid+bankId.getPaidFee();
									bankId.setBankId(null);
									bankId.setBankName(null);
									bankId.setBankAccNo(null);
									bankId.setIfscNo(null);
								}
								ob1.setPaidFee(paid);
								
								bankDetailsBean.add(ob1) ;
							}
							ob.setBankDetailsBean(bankDetailsBean);
							list.add(ob);
					
			 }
			 ob3.setCashBankAmountList(totalAmountBankAndCash);
			 responceBean.add(ob3);
			// overallDateWiseResponce.add(totalAmountBankAndCash);
				}

		
		return responceBean;

	}
}
