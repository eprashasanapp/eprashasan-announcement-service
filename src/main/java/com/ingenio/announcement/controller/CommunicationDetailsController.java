package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.ReportBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.StandardDivisionReportBean;
import com.ingenio.announcement.model.SentEmailModel;
import com.ingenio.announcement.model.SentEmailReadStatusModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;
import com.ingenio.announcement.model.SmsSchoolCreditModel;
import com.ingenio.announcement.service.CommunicationDetailsService;
import com.ingenio.announcement.service.SendCommunicationService;

@RestController
@CrossOrigin
public class CommunicationDetailsController {

	@Autowired
	CommunicationDetailsService communicationService;
	
	@Autowired
	private SendCommunicationService sendCommunicationService;
	
	@GetMapping(value = "/getSmsSchoolCreditInfo")
	public @ResponseBody ResponseBodyBean<SmsSchoolCreditModel> getAnnouncementAuthorityusingGroupId(
			@RequestParam("schoolId") Integer schoolId) {
		SmsSchoolCreditModel approvalAuthorityList = new SmsSchoolCreditModel();
		try {
			approvalAuthorityList = communicationService.getSmsCreditInfo(schoolId);
			if (approvalAuthorityList!=null) {
				return new ResponseBodyBean<SmsSchoolCreditModel>("200", HttpStatus.OK, approvalAuthorityList);
			} else {
				return new ResponseBodyBean<SmsSchoolCreditModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SmsSchoolCreditModel>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	

	@GetMapping(value = "/announcementDivisionReport")
	public @ResponseBody ResponseBodyBean<StandardDivisionReportBean> getClassDevisionReport(
			@RequestParam("schoolId") Integer schoolId,@RequestParam("announcementId") Integer announcementId) {
		List<StandardDivisionReportBean> classdivisionReport = new ArrayList<>();
		try {
			classdivisionReport = communicationService.getClassDevisionReport(schoolId,announcementId);
			if (CollectionUtils.isNotEmpty(classdivisionReport)) {
				return new ResponseBodyBean<StandardDivisionReportBean>("200", HttpStatus.OK, classdivisionReport);
			} else {
				return new ResponseBodyBean<StandardDivisionReportBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StandardDivisionReportBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	

	
	@GetMapping("/getAnnouncementDeliveryReportGroup")
	public ResponseBodyBean<ReportBean> getannouncementdeliveryReportGroup(@RequestParam("schoolid") Integer schoolid,@RequestParam("announcementId") Integer announcementId) {

		try {
			List<ReportBean> sentMessages = communicationService.getannouncementdeliveryReportGroup(schoolid,announcementId);
			if (CollectionUtils.isNotEmpty(sentMessages)) {
				return new ResponseBodyBean<ReportBean>("200", HttpStatus.OK, sentMessages);
			} else {
				return new ResponseBodyBean<ReportBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<ReportBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	@GetMapping("/getEmailDetails")
	public ResponseBodyBean<SentEmailModel> getEmailDetails(@RequestParam("rcode") String rcode) {

		try {
			Optional<SentEmailModel> groupBean = sendCommunicationService.getEmailDetails(rcode);
			if (groupBean.get()!=null) {
				return new ResponseBodyBean<SentEmailModel>("200", HttpStatus.OK, groupBean.get());
			} else {
				return new ResponseBodyBean<SentEmailModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SentEmailModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	@PostMapping("/recordEmailReadStatus")
	public ResponseBodyBean<Integer> recordEmailReadStatus(@RequestBody SentEmailReadStatusModel sentEmailReadStatus) {
		try {
			Integer id = sendCommunicationService.recordEmailReadStatus(sentEmailReadStatus);
			if (id!=null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	
	
	
	@GetMapping("/getMessageDetails")
	public ResponseBodyBean<SentMessagesModel> getMessageDetails(@RequestParam("rcode") String rcode) {

		try {
			Optional<SentMessagesModel> groupBean = sendCommunicationService.getMessageDetails(rcode);
			if (groupBean.get()!=null) {
				return new ResponseBodyBean<SentMessagesModel>("200", HttpStatus.OK, groupBean.get());
			} else {
				return new ResponseBodyBean<SentMessagesModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SentMessagesModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	@PostMapping("/recordMessageReadStatus")
	public ResponseBodyBean<Integer> recordMessageReadStatus(@RequestBody SentMessagesReadStatusModel sentMessagesReadStatus) {
		try {
			Integer id = sendCommunicationService.recordMessageReadStatus(sentMessagesReadStatus);
			if (id!=null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	@GetMapping("/getAllMessages")
	public ResponseBodyBean<SentMessagesModel> getAllMessages(@RequestParam("staffStudentId") Integer staffStudentId,@RequestParam("flag") Integer flag) {

		try {
			List<SentMessagesModel> sentMessages = sendCommunicationService.getAllMessages(staffStudentId,flag);
			if (!CollectionUtils.isEmpty(sentMessages)) {
				return new ResponseBodyBean<SentMessagesModel>("200", HttpStatus.OK, sentMessages);
			} else {
				return new ResponseBodyBean<SentMessagesModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SentMessagesModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	@GetMapping("/getAllEmails")
	public ResponseBodyBean<SentMessagesModel> getAllEmails(@RequestParam("staffStudentId") Integer staffStudentId,@RequestParam("flag") Integer flag) {

		try {
			List<SentMessagesModel> sentMessages = sendCommunicationService.getAllEmails(staffStudentId,flag);
			if (!CollectionUtils.isEmpty(sentMessages)) {
				return new ResponseBodyBean<SentMessagesModel>("200", HttpStatus.OK, sentMessages);
			} else {
				return new ResponseBodyBean<SentMessagesModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SentMessagesModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
		
	@GetMapping("/getAnnouncementDeliveryReportAssignto")
	public ResponseBodyBean<ReportBean> getannouncementdeliveryreportassignto(@RequestParam("schoolid") Integer schoolid,@RequestParam("announcementId") Integer announcementId) {

		try {
			ReportBean sentMessages = sendCommunicationService.getannouncementdeliveryreportassignto(schoolid,announcementId);
			if (sentMessages!=null) {
				return new ResponseBodyBean<ReportBean>("200", HttpStatus.OK, sentMessages);
			} else {
				return new ResponseBodyBean<ReportBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<ReportBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	@GetMapping("/getStudentListDeliveryReportForAnnouncement")//2024-06-04 date format
	public ResponseBodyBean<ReportBean> getStudentListDeliveryReportForAnnouncement(@RequestParam("schoolid") Integer schoolid,@RequestParam("yearId") Integer yearId,
			@RequestParam("standardId") Integer standardId,
			@RequestParam("divisionId") Integer divisionId,
			@RequestParam("announcementId") Integer announcementId,@RequestParam("announcementDate") String announcementDate) {

		try {
			ReportBean sentMessages = sendCommunicationService.getStudentListDeliveryReportForAnnouncement(schoolid,yearId,standardId,divisionId,announcementId,announcementDate);
			if (sentMessages!=null) {
				return new ResponseBodyBean<ReportBean>("200", HttpStatus.OK, sentMessages);
			} else {
				return new ResponseBodyBean<ReportBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<ReportBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	
	@GetMapping("/getAllMessagesReport")//mail details
	public ResponseBodyBean<SentMessagesModel> getAllMessagesReport(@RequestParam("schoolid") Integer schoolid,@RequestParam(value="fromDate",required = false) String fromDate,@RequestParam(value="toDate",required = false) String toDate,@RequestParam("offset") Integer offset,@RequestParam("limit") Integer limit,@RequestParam(value="otherSchoolId",required = false) Integer otherSchoolId) {

		try {
			List<SentMessagesModel> sentMessages = sendCommunicationService.getAllMessagesReport(schoolid,fromDate,toDate,otherSchoolId,offset,limit);
			if (!CollectionUtils.isEmpty(sentMessages)) {
				return new ResponseBodyBean<SentMessagesModel>("200", HttpStatus.OK, sentMessages);
			} else {
				return new ResponseBodyBean<SentMessagesModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SentMessagesModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	
	
}
