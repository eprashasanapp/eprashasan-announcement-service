package com.ingenio.announcement.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.bean.BannerImages;
import com.ingenio.announcement.bean.EventRequestBean;
import com.ingenio.announcement.bean.EventResponse;
import com.ingenio.announcement.bean.EventResponseBean;
import com.ingenio.announcement.bean.EventTypeBean;
import com.ingenio.announcement.bean.PersonalNoteRequestBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.repository.EventRepository;
import com.ingenio.announcement.service.EventService;
import com.ingenio.announcement.util.Utility;

@Controller
@CrossOrigin
public class EventController {
	
	@Autowired 
	private EventService eventService;
	
	
	@Autowired 
	private EventRepository eventRepository;
	
	@GetMapping(value="/eventClassDivisionList")
	public @ResponseBody ResponseBodyBean<EventResponseBean> getClassDivisisionList(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("assignedByRole") String assignedByRole){
		List<EventResponseBean> eventResponseBean = new ArrayList<>();
		try {
			eventResponseBean=eventService.getClassDivisisionList(schoolId,assignedByRole);
			if(eventResponseBean != null) {
				return new ResponseBodyBean<EventResponseBean>("200",HttpStatus.OK,eventResponseBean);
			}
			else {
				return new ResponseBodyBean<EventResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<EventResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@PostMapping(value="/addEvent")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveEvent(@RequestBody EventRequestBean eventRequestBean){
		try {			
			System.out.println("eventRequestBean"+eventRequestBean);
			System.out.println(eventRequestBean.getEventId());
				Integer saveId = eventService.saveEvent(eventRequestBean);
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}		
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@GetMapping(value="/myEvent")
	public @ResponseBody  ResponseBodyBean<EventResponse> getMyEvent(@RequestParam(value="day",required = false) Integer day,@RequestParam("month") Integer month,
			@RequestParam("yearId") Integer yearId,@RequestParam("year") String year, 
			@RequestParam("profileRole") String profileRole,
			@RequestParam("studentStaffId") Integer studentStaffId,
			@RequestParam(value="schoolId",required = false) Integer schoolId,
			@RequestParam(value="standardId",required = false) Integer standardId ,
			@RequestParam(value="divisionId",required = false) Integer divisionId,
			@RequestParam(value="userId",required = false) Integer userId,
			@RequestParam(value="lastUpdateTime",required = false) String lastUpdateTime){
		EventResponse eventResponse = new EventResponse ();
		System.out.println("std"+standardId + "div "+divisionId);
		try {
			eventResponse=eventService.getMyEvent(day,month,yearId,year,profileRole,studentStaffId,schoolId,standardId,divisionId,userId);
			if(eventResponse!=null) {
				return new ResponseBodyBean<EventResponse>("200",HttpStatus.OK,eventResponse);	
			}
			else {
				return new ResponseBodyBean<EventResponse>("404",HttpStatus.NOT_FOUND,eventResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<EventResponse>("500",HttpStatus.INTERNAL_SERVER_ERROR,eventResponse);
		}
	}
	
	@GetMapping(value="/myEventDetails")
	public @ResponseBody ResponseBodyBean<EventResponseBean> getMyEventDetails(@RequestParam("eventId") Integer eventId,@RequestParam("yearId") Integer yearId,
		@RequestParam("studentStaffId") Integer studentStaffId,@RequestParam(value="standardId",required = false) Integer standardId,
		@RequestParam(value="schoolId",required = false) Integer schoolId,@RequestParam(value="divisionId",required = false) Integer divisionId){
		List<EventResponseBean> eventDetailsList = new ArrayList<EventResponseBean>();
		try {
			eventDetailsList=eventService.getMyEventDetails(eventId,yearId,studentStaffId,schoolId);
			if(CollectionUtils.isNotEmpty(eventDetailsList)) {
//				eventService.updateEventStatus(eventDetailsList.get(0).getEventId(), 0, "1");
				return new ResponseBodyBean<EventResponseBean>("200",HttpStatus.OK,eventDetailsList);
			}
			else {
				return new ResponseBodyBean<EventResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<EventResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}	

	
	@PostMapping(value = "/eventStatus")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> updateEventStatus(@RequestParam("eventAssignId") Integer eventAssignId, @RequestParam("studentStaffId") Integer studentStaffId,
			@RequestParam("statusFlag") String statusFlag){
		try {
			Integer saveId = eventService.updateEventStatus(eventAssignId,studentStaffId,statusFlag);
			if(saveId>0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value="/deleteEvent")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> deleteEvent(@RequestParam("eventId") Integer eventId){
			try{
				Integer deleteId = eventService.deleteEvent(eventId);
				if(deleteId!=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
				}
			  
	        } catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
			}
	}
	
	@PostMapping(value="/deletePersonalNote")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> deletePersonalNote(@RequestParam("personalNoteId") Integer personalNoteId){
			try{
				Integer deleteId = eventService.deletePersonalNote(personalNoteId);
				if(deleteId!=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
				}
			  
	        } catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
			}
	}
	
	@GetMapping(value="/eventStatusBasedStudentDetails")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<EventResponseBean> getStudentDetailsForEventStatus(@RequestParam("schoolId") Integer schoolId,@RequestParam("eventId") Integer eventId, @RequestParam("eventStatus") String eventStatus){
		List<EventResponseBean> eventStatusBasedList = new ArrayList<>();
			try{
				eventStatusBasedList = eventService.getStudentDetailsForEventStatus(schoolId,eventId,eventStatus);
				if(CollectionUtils.isNotEmpty(eventStatusBasedList)) {
					return new ResponseBodyBean<EventResponseBean>("200",HttpStatus.OK,eventStatusBasedList);
				}
				else {
					return new ResponseBodyBean<EventResponseBean>("404",HttpStatus.NOT_FOUND,eventStatusBasedList);
				}				
			  
	        } catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<EventResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,eventStatusBasedList);
			}
	}
	
	@GetMapping(value="/eventStudentDetails")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<StudentDetailsBean> getStudentDetailsForevent(@RequestParam("eventId") Integer eventId,
			@RequestParam("offset") Integer offset){
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try{
			studentList = eventService.getStudentDetailsForEvent(eventId,offset);
			if(CollectionUtils.isNotEmpty(studentList)) {
				return new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studentList);
			}
			else {
				return new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,studentList);
			}
			
		  
        } catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,studentList);
		}
	}
	
	
	
	@GetMapping(value="/eventTypes")
	public @ResponseBody ResponseBodyBean<EventTypeBean> getAllEventTypes(@RequestParam(value="lastUpdateTime",required = false) String lastUpdateTime){
		List<EventTypeBean> eventTypeList = new ArrayList<EventTypeBean>();
		try {
			eventTypeList=eventService.getAllEventTypes();
			if(CollectionUtils.isNotEmpty(eventTypeList)) {
				return new ResponseBodyBean<EventTypeBean>("200",HttpStatus.OK,eventTypeList);	
			}
			else {
				return new ResponseBodyBean<EventTypeBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<EventTypeBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/divisionList")
	public @ResponseBody ResponseBodyBean<StandardDivisionBean> getAllDivisionList(@RequestParam("standardId") Integer standardId){
		List<StandardDivisionBean> divisionList = new ArrayList<StandardDivisionBean>();
		try {
			divisionList=eventService.getAllDivisionList(standardId);
			if(CollectionUtils.isNotEmpty(divisionList)) {
				return new ResponseBodyBean<StandardDivisionBean>("200",HttpStatus.OK,divisionList);	
			}
			else {
				return new ResponseBodyBean<StandardDivisionBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StandardDivisionBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@GetMapping(value="/standardList")
	public @ResponseBody ResponseBodyBean<StandardDivisionBean> getAllStandardsList(@RequestParam("schoolId") Integer schoolId){
		List<StandardDivisionBean> standardList = new ArrayList<StandardDivisionBean>();
		try {
			standardList=eventService.getAllStandardsList(schoolId);
			if(CollectionUtils.isNotEmpty(standardList)) {
				return new ResponseBodyBean<StandardDivisionBean>("200",HttpStatus.OK,standardList);	
			}
			else {
				return new ResponseBodyBean<StandardDivisionBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StandardDivisionBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@GetMapping(value="/allBannerList")
	public @ResponseBody ResponseBodyBean<BannerImages> getAllBannerList(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("yearId") Integer yearId, 
			@RequestParam("monthId") Integer monthId,@RequestParam("yearName") String yearName,
			@RequestParam(value="lastUpdateTime",required = false) String lastUpdateTime){
		List<BannerImages> bannerImagesList = new ArrayList<BannerImages>();
		try {
			bannerImagesList=eventService.getAllBannerList(schoolId,yearId,monthId,yearName);
			if(CollectionUtils.isNotEmpty(bannerImagesList)) {
				return new ResponseBodyBean<BannerImages>("200",HttpStatus.OK,bannerImagesList);	
			}
			else {
				return new ResponseBodyBean<BannerImages>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<BannerImages>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	
	
	@PostMapping(value="/addPersonalNote")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> savePersonalNote(@RequestBody PersonalNoteRequestBean personalNoteRequestBean){
		try {		
			System.out.println(personalNoteRequestBean.getReminderDate());
				Integer saveId = eventService.savePersonalNote(personalNoteRequestBean);
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}		
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@PostMapping(value = "/eventAttachments")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveEventAttachments(@RequestParam MultipartFile file,@RequestParam String fileName,
			@RequestParam Integer eventId,
			@RequestParam  String yearName,@RequestParam Integer schoolId){
		try {
				Integer saveId = eventService.saveEventAttachments(file,fileName,eventId,yearName,schoolId);
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@PostMapping(value="/eventDeleteAttachment")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> EventdeleteAttachment(@RequestParam("eventDeleteAttachmentId") Integer eventDeleteAttachmentId){
			try{
				Integer deleteId = eventService.deleteAttachment(eventDeleteAttachmentId);
				if(deleteId!=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
				}
				
			  
	        } catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
			}
	}
	
	@PostMapping(value = "/sendNotification")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<String> saveEventAttachments(){
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date dateobj = new Date();
			System.out.println("Job started");
			String date = df.format(dateobj);
			List<EventResponseBean> eventList = eventRepository.findEvents(date);
			eventList.stream().forEach(event -> {
				List<String> androidTokenList = new ArrayList<>();
				androidTokenList = eventRepository.getAndroidTokenList(event.getStandardId(), event.getDivisionId());
				Utility.pushNotification(androidTokenList, "Welcome to calendar App", "Welcome to calendar App", "2");

			});
		
			String abc = "a";
				if(abc!=null &abc != ""){
					return new ResponseBodyBean<String>("200",HttpStatus.OK,abc);
				}
				else {
					return new ResponseBodyBean<String>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	
	
	
	
}
