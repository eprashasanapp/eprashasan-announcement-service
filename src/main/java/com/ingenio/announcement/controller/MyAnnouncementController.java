package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementSendingListResponseBean;
import com.ingenio.announcement.bean.FeedbackAnalyticsBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.service.MyAnnouncementService;

@Controller
@CrossOrigin
public class MyAnnouncementController {

	@Autowired
	MyAnnouncementService myAnnouncementService;

	@GetMapping(value = "/myAnnouncement")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getMyAnnouncement(
			@RequestParam("staffStudentId") Integer staffId, @RequestParam("yearId") Integer yearId,
			@RequestParam("profileRole") String profileRole, @RequestParam("offset") Integer offset,
			@RequestParam(value = "announcementDate", required = false) String announcementDate,
			@RequestParam(value = "appUserId", required = false) Integer appUserId,
			@RequestParam(value = "typeId", required = false, defaultValue = "0") Integer typeId,
			@RequestParam(value = "limit", required = false) Integer limit, @RequestParam("schoolId") Integer schoolId,
			@RequestParam(value = "announcementId", required = false) Integer announcementId) {
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {

			if (announcementId == null) {
				announcementId = 0;
			}
			assignedAnnouncementList = myAnnouncementService.getMyAnnouncement(staffId, yearId, profileRole,
					announcementDate, offset, limit, appUserId, typeId, schoolId, announcementId);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)
					&& assignedAnnouncementList.get(0).getAnnouncementId() != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/myAnnouncementGroupWise")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getMyAnnouncementGroupWise(
			@RequestParam("groupId") String groupId, @RequestParam("offset") Integer offset,
			@RequestParam(value = "appUserId", required = false) Integer appUserId,
			@RequestParam(value = "typeId", required = false, defaultValue = "0") Integer typeId,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam("schoolId") Integer schoolId) {
		List<AnnouncementResponseBean> announcementResponseBeanList = new ArrayList<AnnouncementResponseBean>();
		try {
			announcementResponseBeanList = myAnnouncementService.getMyAnnouncementGroupWise(groupId, typeId, schoolId,
					offset, limit,appUserId);
			if (CollectionUtils.isNotEmpty(announcementResponseBeanList)) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, announcementResponseBeanList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	
	@GetMapping(value = "/myAnnouncementClassWise")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getMyAnnouncementClassWise(
			@RequestParam("standardId") Integer standardId, @RequestParam("offset") Integer offset,
			@RequestParam("yearId") Integer yearId,
			@RequestParam("divisionId") Integer divisionId,
			@RequestParam(value = "appUserId", required = false) Integer appUserId,
			@RequestParam(value = "typeId", required = false, defaultValue = "0") Integer typeId,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam("schoolId") Integer schoolId) {
		List<AnnouncementResponseBean> announcementResponseBeanList = new ArrayList<AnnouncementResponseBean>();
		try {
			announcementResponseBeanList = myAnnouncementService.getMyAnnouncementClassWise(standardId, offset, yearId,
					divisionId, appUserId,typeId,limit,schoolId);
			if (CollectionUtils.isNotEmpty(announcementResponseBeanList)) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, announcementResponseBeanList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	

	@GetMapping(value = "/getFeedbackAnalyticsOfAnnouncement")
	public @ResponseBody ResponseBodyBean<FeedbackAnalyticsBean> getFeedbackAnalyticsOfAnnouncement(@RequestParam("schoolId") Integer schoolId, @RequestParam("announcementId") Integer announcementId) {
		
	
		try {
			FeedbackAnalyticsBean feedbackAnalyticsBean=myAnnouncementService.getFeedbackAnalytics(schoolId,announcementId);
			if (feedbackAnalyticsBean!=null) {
				return new ResponseBodyBean<FeedbackAnalyticsBean>("200", HttpStatus.OK, feedbackAnalyticsBean);
			} else {
				return new ResponseBodyBean<FeedbackAnalyticsBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeedbackAnalyticsBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/announcementUsersList")
	public @ResponseBody ResponseBodyBean<AnnouncementSendingListResponseBean> getAnnouncementUsersList(
			@RequestParam("announcementId") Integer announcementId,
			@RequestParam("schoolId") Integer schoolId) {
		try {
			AnnouncementSendingListResponseBean announcementSendingListResponseBean = 
					myAnnouncementService.getAnnouncementUsersList(announcementId, schoolId);
			if (announcementSendingListResponseBean!=null) {
				return new ResponseBodyBean<AnnouncementSendingListResponseBean>("200", HttpStatus.OK, announcementSendingListResponseBean);
			} else {
				return new ResponseBodyBean<AnnouncementSendingListResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementSendingListResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	
	@GetMapping(value = "/getAnnouncement")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAnnouncement(
			@RequestParam(value = "announcementId") Integer announcementId,@RequestParam("schoolId") Integer schoolId,@RequestParam("appUserId") Integer appUserId) {
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {

			if (announcementId == null) {
				announcementId = 0;
			}
			assignedAnnouncementList = myAnnouncementService.getAnnouncement(announcementId,schoolId,appUserId);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)
					&& assignedAnnouncementList.get(0).getAnnouncementId() != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}	

	@GetMapping(value = "/getAnnouncementSchoolWise")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAnnouncementSchoolWise(@RequestParam("schoolId") Integer schoolId,@RequestParam(value = "appUserId", required = false) Integer appUserId,@RequestParam(value = "announcementCategory", required = false)Integer announcementCategory) {
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {

			
			assignedAnnouncementList = myAnnouncementService.getAnnouncementSchoolWise(schoolId,appUserId,announcementCategory);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)
					&& assignedAnnouncementList.get(0).getAnnouncementId() != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}	
	@GetMapping(value="/testing123")
	public String testing() {
		  return "new changes checking ci cd";
	  }
}
