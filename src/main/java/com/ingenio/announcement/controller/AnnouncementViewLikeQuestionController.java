package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;
import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.announcement.bean.AnnnouncementLikeOrDislikeBean;
import com.ingenio.announcement.bean.AnnouncementComentBean;
import com.ingenio.announcement.bean.AnnouncementCommentResponseBean;
import com.ingenio.announcement.bean.AnnouncementLikeResponseBean;
import com.ingenio.announcement.bean.AnnouncementQuestionAnswersBean;
import com.ingenio.announcement.bean.AnnouncementQuestionOptionBean;
import com.ingenio.announcement.bean.AnnouncementReactionBean;
import com.ingenio.announcement.bean.AnnouncementViewBean;
import com.ingenio.announcement.bean.AnnouncementViewResponseBean;
import com.ingenio.announcement.bean.FeedbackAnalyticsBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.service.AnnouncementService;

@CrossOrigin
@RestController
public class AnnouncementViewLikeQuestionController {

	@Autowired
	private AnnouncementService announcementService;

	private ExecutorService executor = Executors.newSingleThreadExecutor();

	@PreDestroy
	public void shutdonw() {
		executor.shutdown();
	}

	@PostMapping("/saveAnnouncementView")
	public ResponseBodyBean<Integer> saveAnnouncementView(@RequestBody AnnouncementViewBean announcementViewBean) {

		try {
			Integer id = announcementService.saveAnnounceMentView(announcementViewBean);
			if (id != 0 || id != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	@PostMapping("/saveAnnouncementViewList")
	public ResponseBodyBean<Integer> saveAnnouncementView(
			@RequestBody List<AnnouncementViewBean> announcementReactionBean) {

		try {
			Integer id = announcementService.saveAnnouncementView(announcementReactionBean);
			if (id != 0 || id != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	

	@PostMapping("/saveAnnnouncementLike")
	public ResponseBodyBean<Integer> saveAnnnouncementLikeOrDislike(
			@RequestBody AnnnouncementLikeOrDislikeBean annnouncementLikeOrDislikeBean) {
		try {
			Integer id = announcementService.saveAnnnouncementLikeOrDislike(annnouncementLikeOrDislikeBean);
			if (id != 0 || id != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}

	@PostMapping("/saveAnnouncementComment")
	public ResponseBodyBean<Integer> saveAnnouncementComment(
			@RequestBody AnnouncementComentBean announcementComentBean) {

		try {
			Integer id = announcementService.saveComment(announcementComentBean);
			if (id != 0 || id != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	@PostMapping(value = "/saveAnnouncement_Questions_Options")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveQuestionsAndOptions(
			@RequestBody AnnouncementQuestionOptionBean announcementQuestionOptionBean) {
		try {
			Integer saveId = announcementService.saveQuestionsAndOptions(announcementQuestionOptionBean);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping("/saveAnnouncementReaction")
	public ResponseBodyBean<Integer> saveAnnouncementReaction(
			@RequestBody List<AnnouncementReactionBean> announcementReactionBean) {

		try {
			Integer id = announcementService.saveReaction(announcementReactionBean);
			if (id != 0 || id != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	

	@GetMapping(value = "/getAnnouncementAllComment")
	public @ResponseBody ResponseBodyBean<AnnouncementCommentResponseBean> getAnnouncementAllComment(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("announcementId") Integer announcementId) {
		List<AnnouncementCommentResponseBean> commentList = new ArrayList<AnnouncementCommentResponseBean>();
		try {
			commentList = announcementService.getAnnouncementAllComment(schoolId, announcementId);
			if (CollectionUtils.isNotEmpty(commentList)) {
				return new ResponseBodyBean<AnnouncementCommentResponseBean>("200", HttpStatus.OK,
						commentList);
			} else {
				return new ResponseBodyBean<AnnouncementCommentResponseBean>("404", HttpStatus.NOT_FOUND,
						new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementCommentResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	
	@GetMapping(value = "/getAnnouncementAllLikes")
	public @ResponseBody ResponseBodyBean<AnnouncementLikeResponseBean> getAnnouncementAllLikes(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("announcementId") Integer announcementId) {
		List<AnnouncementLikeResponseBean> likeList = new ArrayList<AnnouncementLikeResponseBean>();
		try {
			likeList = announcementService.getAnnouncementAllLikes(schoolId, announcementId);
			if (CollectionUtils.isNotEmpty(likeList)) {
				return new ResponseBodyBean<AnnouncementLikeResponseBean>("200", HttpStatus.OK,
						likeList);
			} else {
				return new ResponseBodyBean<AnnouncementLikeResponseBean>("404", HttpStatus.NOT_FOUND,
						new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementLikeResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	
	@GetMapping(value = "/getAnnouncementAllViews")
	public @ResponseBody ResponseBodyBean<AnnouncementViewResponseBean> getAnnouncementAllViews(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("announcementId") Integer announcementId) {
		List<AnnouncementViewResponseBean> viewList = new ArrayList<AnnouncementViewResponseBean>();
		try {
			viewList = announcementService.getAnnouncementAllViews(schoolId, announcementId);
			if (CollectionUtils.isNotEmpty(viewList)) {
				return new ResponseBodyBean<AnnouncementViewResponseBean>("200", HttpStatus.OK,
						viewList);
			} else {
				return new ResponseBodyBean<AnnouncementViewResponseBean>("404", HttpStatus.NOT_FOUND,
						new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementViewResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	
	@GetMapping(value = "/getQuestions_Options_Answers")
	public @ResponseBody ResponseBodyBean<AnnouncementQuestionAnswersBean> getQuestionsOptionsAnswers(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("announcementId") Integer announcementId,
			@RequestParam(value="appUserId",required = false) Integer appUserId) {
		List<AnnouncementQuestionAnswersBean> answerList = new ArrayList<AnnouncementQuestionAnswersBean>();
		try {
			answerList = announcementService.getQuestionsOptionsAnswers(schoolId, announcementId,appUserId);
			if (CollectionUtils.isNotEmpty(answerList)) {
				return new ResponseBodyBean<AnnouncementQuestionAnswersBean>("200", HttpStatus.OK,
					answerList);
			} else {
				return new ResponseBodyBean<AnnouncementQuestionAnswersBean>("404", HttpStatus.NOT_FOUND,
						new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementQuestionAnswersBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	
}
