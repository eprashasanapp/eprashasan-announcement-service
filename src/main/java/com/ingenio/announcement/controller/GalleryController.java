package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.bean.GalleryRequestBean;
import com.ingenio.announcement.bean.GalleryResponseBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.service.GalleryService;



@Controller
@CrossOrigin
public class GalleryController {
	
	@Autowired 
	private GalleryService galleryService;
	
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	
	@GetMapping(value="/galleryClassDivisionList")
	public @ResponseBody ResponseBodyBean<GalleryResponseBean> getClassDivisisionList(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("yearId") Integer yearId,@RequestParam("assignedByRole") String assignedByRole){
		List<GalleryResponseBean> galleryResponseBean = new ArrayList<>();
		try {
			galleryResponseBean=galleryService.getClassDivisisionList(schoolId,assignedByRole,yearId);
			if(galleryResponseBean != null) {
				return new ResponseBodyBean<GalleryResponseBean>("200",HttpStatus.OK,galleryResponseBean);
			}
			else {
				return new ResponseBodyBean<GalleryResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GalleryResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/studentListForGallery")
	public @ResponseBody ResponseBodyBean<GalleryResponseBean> getDivisionWiseStudentList(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("yearId") Integer yearId,@RequestParam("assignedByRole") String assignedByRole,
			@RequestParam(value ="standardDivision", required=false)String standardDivision,@RequestParam(value ="standardDivisionName", required=false)String standardDivisionName){
		List<GalleryResponseBean> galleryResponseBean = new ArrayList<>();
		try {
			galleryResponseBean=galleryService.getDivisionWiseStudentList(schoolId,assignedByRole,yearId,standardDivision,standardDivisionName);
			if(galleryResponseBean != null) {
				return new ResponseBodyBean<GalleryResponseBean>("200",HttpStatus.OK,galleryResponseBean);
			}
			else {
				return new ResponseBodyBean<GalleryResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GalleryResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	
	
	@PostMapping(value="/saveGalleryPost")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer>saveGalleryPost(@RequestBody GalleryRequestBean studentGalleryBean){
		try {			
				Integer saveId = galleryService.saveGalleryPost(studentGalleryBean);
				executor.submit(() -> galleryService.postGalleryForMembers(studentGalleryBean,saveId));
				executor.submit(() -> galleryService.sendNotification(studentGalleryBean,saveId));
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	

	@GetMapping(value="/myGalleryPosts")
	public @ResponseBody ResponseBodyBean<GalleryResponseBean> getMyGalleryPosts(@RequestParam("staffStudentId") Integer staffId,
			@RequestParam("yearId") Integer yearId,@RequestParam("profileRole") String profileRole,@RequestParam("offset") Integer offset,
			@RequestParam(value ="standardDivision", required=false)String standardDivision){
		List<GalleryResponseBean> assignedGalleryList = new ArrayList<GalleryResponseBean>();
		try {			
			assignedGalleryList=galleryService.getMyGalleryPosts(staffId,yearId,profileRole,offset,standardDivision);
			if(CollectionUtils.isNotEmpty(assignedGalleryList) && assignedGalleryList.get(0).getGalleryId()!=null) {
				for(int i=0;i<assignedGalleryList.size();i++) {
					galleryService.updateGalleryStatus(assignedGalleryList.get(i).getGalleryAssignedtoId(), 0, "1");
					}
				return new ResponseBodyBean<GalleryResponseBean>("200",HttpStatus.OK,assignedGalleryList);
			}
			else {
				return new ResponseBodyBean<GalleryResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GalleryResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@GetMapping(value="/myGalleryPostDetails")
	public @ResponseBody ResponseBodyBean<GalleryResponseBean> getMyGalleryPostDetails(@RequestParam("galleryId") Integer galleryId,@RequestParam("yearId") Integer yearId,
			@RequestParam("studentStaffId") Integer studentStaffId,@RequestParam("profileRole") String profileRole){
		List<GalleryResponseBean> galleryDetailsList = new ArrayList<GalleryResponseBean>();
		try {
			galleryDetailsList=galleryService.getMyGalleryPostDetails(galleryId,yearId,studentStaffId,profileRole);
			if(CollectionUtils.isNotEmpty(galleryDetailsList) && galleryDetailsList.get(0).getGalleryId()!=null) {
//				galleryService.updateGalleryStatus(assignedGalleryList.get(0).getGalleryAssignedtoId(), 0, "1");
				return new ResponseBodyBean<GalleryResponseBean>("200",HttpStatus.OK,galleryDetailsList);
			}
			else {
				return new ResponseBodyBean<GalleryResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GalleryResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	

	@PostMapping(value = "/galleryAttachments")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveGalleryAttachments(@RequestParam MultipartFile file,@RequestParam String fileName,
			@RequestParam Integer galleryId,
			@RequestParam  String yearName,@RequestParam Integer schoolId){
		try {			
				Integer saveId = galleryService.saveGalleryAttachments(file,fileName,galleryId,yearName,schoolId);
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}	
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value = "/galleryAttachmentsWithId")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveGalleryAttachmentsWithId(@RequestParam MultipartFile file,@RequestParam String fileName,
			@RequestParam Integer galleryId,
			@RequestParam  Integer yearId,@RequestParam Integer schoolId){
		try {
			Integer saveId = galleryService.saveGalleryAttachmentsWithId(file,fileName,galleryId,yearId,schoolId);
			if(saveId>0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value="/deleteGalleryPost")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> deleteGalleryPost(@RequestParam("galleryId") Integer galleryId){
			try{
				Integer deleteId = galleryService.deleteGalleryPost(galleryId);
				if(deleteId!=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
				}
				
			  
	        } catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
			}
	}
	
	@PostMapping(value="/deleteGalleryAttachment")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> deleteGalleryAttachment(@RequestParam("galleryAttachmentId") Integer galleryAttachmentId){
			try{
				Integer deleteId = galleryService.deleteGalleryAttachment(galleryAttachmentId);
				if(deleteId!=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
				}
				
			  
	        } catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
			}
	}
	
	@GetMapping(value="/galleryAttachmentList")
	public @ResponseBody ResponseBodyBean<GalleryResponseBean> getGalleryAttachmentList(@RequestParam("galleryId") Integer galleryId){
		List<GalleryResponseBean> assignedGalleryList = new ArrayList<GalleryResponseBean>();
		try {
			assignedGalleryList=galleryService.getGalleryAttachmentList(galleryId);
			if(CollectionUtils.isNotEmpty(assignedGalleryList)) {
				return new ResponseBodyBean<GalleryResponseBean>("200",HttpStatus.OK,assignedGalleryList);
			}
			else {
				return new ResponseBodyBean<GalleryResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GalleryResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	


	@PostMapping(value = "/galleryStatus")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> updateGalleryStatus(@RequestParam("galleryAssignId") Integer galleryAssignId,@RequestParam("studentId") Integer studentId,@RequestParam("statusFlag") String statusFlag){
		try {
			Integer saveId = galleryService.updateGalleryStatus(galleryAssignId,studentId,statusFlag);
			if(saveId>0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/divisionFilter")
	public @ResponseBody ResponseBodyBean<GalleryResponseBean> getdivisionWiseFilterList(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("yearId") Integer yearId,@RequestParam("staffStudentId") Integer staffId){
		List<GalleryResponseBean> galleryResponseBean = new ArrayList<>();
		try {
			galleryResponseBean=galleryService.getdivisionWiseFilterList(schoolId,yearId,staffId);
			if(galleryResponseBean != null) {
				return new ResponseBodyBean<GalleryResponseBean>("200",HttpStatus.OK,galleryResponseBean);
			}
			else {
				return new ResponseBodyBean<GalleryResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GalleryResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/studentDetails")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<StudentDetailsBean> getStudentDetailsForGallery(@RequestParam("galleryId") Integer galleryId,
			@RequestParam("offset") Integer offset){
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try{
			studentList = galleryService.getStudentDetailsForGallery(galleryId,offset);
			if(CollectionUtils.isNotEmpty(studentList)) {
				return new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studentList);
			}
			else {
				return new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,studentList);
			}
			
		  
        } catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,studentList);
		}
	}
}
