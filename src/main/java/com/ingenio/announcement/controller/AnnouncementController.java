package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;
import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ingenio.announcement.bean.AnnouncementRequestBean;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementUserToGroupBean;
import com.ingenio.announcement.bean.FeedbackEmailRequestBean;
import com.ingenio.announcement.bean.JsonRequestBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.AnnouncementTypeModel;
import com.ingenio.announcement.model.JsonTestingModel;
import com.ingenio.announcement.repository.JsontestingRepository;
import com.ingenio.announcement.service.AnnouncementService;

@CrossOrigin
@RestController
public class AnnouncementController {

	@Autowired
	private AnnouncementService announcementService;

	@Autowired
	private JsontestingRepository jsontestingRepository;

	private ExecutorService executor = Executors.newSingleThreadExecutor();

	@PreDestroy
	public void shutdonw() {
		executor.shutdown();
	}

	@GetMapping(value = "/classDivisionList")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getClassDivisisionList(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("assignedByRole") String assignedByRole) {
		List<AnnouncementResponseBean> annoucementResponseBean = new ArrayList<>();
		try {
			annoucementResponseBean = announcementService.getClassDivisisionList(schoolId, assignedByRole);
			if (annoucementResponseBean != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/announcement_type")
	public @ResponseBody ResponseBodyBean<AnnouncementTypeModel> getAnnouncementType(
			@RequestParam("schoolId") Integer schoolId) {
		List<AnnouncementTypeModel> annoucementResponseBean = new ArrayList<>();
		try {
			annoucementResponseBean = announcementService.getAnnouncementType(schoolId);
			if (annoucementResponseBean != null) {
				return new ResponseBodyBean<AnnouncementTypeModel>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<AnnouncementTypeModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementTypeModel>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/announcement_group_list")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAnnouncementGroupList(
			@RequestParam("schoolId") Integer schoolId) {
		List<AnnouncementResponseBean> annoucementResponseBean = new ArrayList<>();
		try {
			annoucementResponseBean = announcementService.getAnnouncementGroupList(schoolId);
			if (annoucementResponseBean != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@PostMapping(value = "/announcement")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveAnnouncement(
			@RequestBody AnnouncementRequestBean StudentAnnouncementBean,
			@RequestParam(value = "isWhatsApp", required = false, defaultValue = "0") Integer isWhatsApp,
			@RequestParam(value = "isDueFeeRemainder", required = false, defaultValue = "0") Integer isDueFeeRemainder) {
		try {
			Integer saveId = announcementService.saveAnnouncement(StudentAnnouncementBean, isWhatsApp,
					isDueFeeRemainder);
			
			executor.submit(() -> announcementService.postAnnouncementForMembers(StudentAnnouncementBean, saveId));
			
			
//          announcementService.postAnnouncementForMembers(StudentAnnouncementBean, saveId);
			
//			executor.submit(() -> announcementService.sendNotification(StudentAnnouncementBean, saveId));

//			if (isWhatsApp == 1 || isDueFeeRemainder == 1) {
//				executor.submit(
//						() -> announcementService.sendWhatsappSms(StudentAnnouncementBean, saveId, isDueFeeRemainder));
//			}
			

			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/attachments")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveAttachments(@RequestParam MultipartFile file,
			@RequestParam String fileName, @RequestParam Integer announcmentId, @RequestParam String yearName,
			@RequestParam Integer schoolId) {
		try {
			Integer saveId = announcementService.saveAttachments(file, fileName, announcmentId, yearName, schoolId);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/attachmentsWithId")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveAttachmentsWithId(@RequestParam MultipartFile file,
			@RequestParam String fileName, @RequestParam Integer announcmentId, @RequestParam Integer yearId,
			@RequestParam Integer schoolId) {
		try {
			Integer saveId = announcementService.saveAttachmentsWithId(file, fileName, announcmentId, yearId, schoolId);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/announcementStatus")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> updateAnnouncementStatus(
			@RequestParam("announcementAssignId") Integer announcementAssignId,
			@RequestParam("studentId") Integer studentId, @RequestParam("statusFlag") String statusFlag) {
		try {
			Integer saveId = announcementService.updateAnnouncementStatus(announcementAssignId, studentId, statusFlag);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/newFeedbackEmail")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> sendFeedbackEmail(@RequestParam("file") MultipartFile file,
			@RequestParam("feedbackEmailRequestBean") String requestbean) {
		try {
			Gson gson = new Gson();
			FeedbackEmailRequestBean feedbackEmailRequestBean = gson.fromJson(requestbean,
					FeedbackEmailRequestBean.class);
			Integer sentMail = announcementService.newSendFeedbackEmail(file, feedbackEmailRequestBean);
			if (sentMail > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, sentMail);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/feedbackEmail")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> sendFeedbackEmail(
			@RequestBody FeedbackEmailRequestBean feedbackEmailRequestBean) {
		try {
			Integer sentMail = announcementService.sendFeedbackEmail(feedbackEmailRequestBean);
			if (sentMail > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, sentMail);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/deleteAnnouncement")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> deleteAnnouncement(
			@RequestParam("announcementId") Integer announcementId) {
		try {
			Integer deleteId = announcementService.deleteAnnouncement(announcementId);
			if (deleteId != 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, deleteId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, 0);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, 0);
		}
	}

	@PostMapping(value = "/deleteAttachment")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> deleteAttachment(
			@RequestParam("announcementAttachmentId") Integer announcementAttachmentId) {
		try {
			Integer deleteId = announcementService.deleteAttachment(announcementAttachmentId);
			if (deleteId != 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, deleteId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, 0);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, 0);
		}
	}

	@GetMapping(value = "/attachmentList")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAttachmentList(
			@RequestParam("announcementId") Integer announcementId) {
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {
			assignedAnnouncementList = announcementService.getAttachmentList(announcementId);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/announcementStudentDetails")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<StudentDetailsBean> getStudentDetailsForAnnouncement(
			@RequestParam("announcementId") Integer announcementId, @RequestParam("offset") Integer offset) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
			studentList = announcementService.getStudentDetailsForAnnouncement(announcementId, offset);
			if (CollectionUtils.isNotEmpty(studentList)) {
				return new ResponseBodyBean<StudentDetailsBean>("200", HttpStatus.OK, studentList);
			} else {
				return new ResponseBodyBean<StudentDetailsBean>("404", HttpStatus.NOT_FOUND, studentList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, studentList);
		}
	}

	@GetMapping(value = "/getWhatsAppApiDetails")
	public @ResponseBody ResponseBodyBean<Integer> getWhatsAppApiDetails(@RequestParam("schoolId") Integer schoolId) {
		try {
			Integer present = announcementService.getWhatsAppApiDetails(schoolId);
			if (present != 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, present);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/DueFeeRemainderStatus")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> DueFeeRemainderStatus(
			@RequestParam("announcementId") Integer announcementId, @RequestParam("renewId") Integer renewId,
			@RequestParam("statusFlag") String statusFlag) {
		try {
			Integer saveId = announcementService.updatedueFeeRemainderStatus(announcementId, renewId, statusFlag);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "SendDueFeeRemainderMsg")
	public @ResponseBody ResponseBodyBean<Integer> SendDueFeeRemainderMsg(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("renewId") Integer renewId) {
		try {
			Integer saveId = announcementService.SendDueFeeRemainderMsg(schoolId, renewId);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/getStandardDivisionList")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getStandardDivisionList(
			@RequestParam("announcementId") Integer announcementId, @RequestParam("schoolId") Integer schoolId) {
		List<AnnouncementResponseBean> standardDivisionList = new ArrayList<AnnouncementResponseBean>();
		try {
			standardDivisionList = announcementService.getStandardDivisionList(announcementId, schoolId);
			if (CollectionUtils.isNotEmpty(standardDivisionList)) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, standardDivisionList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/studentListByStandardDivision")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<StudentDetailsBean> studentListByStandardDivision(
			@RequestParam("announcementId") Integer announcementId, @RequestParam("standardId") Integer standardId,
			@RequestParam("divisionId") Integer divisionId, @RequestParam("yearId") Integer yearId,
			@RequestParam("schoolId") Integer schoolId) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
			studentList = announcementService.studentListByStandardDivision(announcementId, standardId, divisionId,
					yearId, schoolId);
			if (CollectionUtils.isNotEmpty(studentList)) {
				return new ResponseBodyBean<StudentDetailsBean>("200", HttpStatus.OK, studentList);
			} else {
				return new ResponseBodyBean<StudentDetailsBean>("404", HttpStatus.NOT_FOUND, studentList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, studentList);
		}
	}

	@PostMapping("/saveAnnouncementUserToGroup")
	public ResponseBodyBean<Integer> announcementUserToGroup(
			@RequestBody AnnouncementUserToGroupBean announcementUserToGroupBean) {

		try {
			Integer id = announcementService.saveAnnouncementUserToGroup(announcementUserToGroupBean);
			if (id != 0 || id != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, id);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	

}
