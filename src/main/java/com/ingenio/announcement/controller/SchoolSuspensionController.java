package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.announcement.Constants.ApplicationConstants;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.CashBankAmountBean;
import com.ingenio.announcement.bean.CountBean;
import com.ingenio.announcement.bean.EditFeeReceiptBean;
import com.ingenio.announcement.bean.EmailBean;
import com.ingenio.announcement.bean.FeeReceiptBean;
import com.ingenio.announcement.bean.RequestBean;
import com.ingenio.announcement.bean.ResponceBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.StudentDifferenceBean;
import com.ingenio.announcement.bean.ValidateBean;
import com.ingenio.announcement.model.AppUserTempPassword;
import com.ingenio.announcement.model.FeeReceiptEditLogModel;
import com.ingenio.announcement.model.FeeSendEmailIdsMasterModel;
import com.ingenio.announcement.model.GlobalEmailConfigModel;
import com.ingenio.announcement.repository.FeeSendEmailIdsMasterRepository;
import com.ingenio.announcement.repository.GlobalEmailConfigRepo;
import com.ingenio.announcement.repository.StudentMasterRepository;
import com.ingenio.announcement.service.SchoolSuspentionService;
import com.ingenio.announcement.service.impl.MessageSender;
import com.ingenio.announcement.service.impl.SchoolServiceSuspentionServiceImpl;

@RestController
@CrossOrigin
public class SchoolSuspensionController {

	@Autowired
	StudentMasterRepository studentRepo;
	
	@Autowired
	FeeSendEmailIdsMasterRepository feeSendEmailIdsMasterRepository;
	@Autowired
	GlobalEmailConfigRepo globalEmailConfigRepo;
	
	@Autowired
	SchoolSuspentionService schoolService;
	@Autowired
	StudentMasterRepository studentMasterRepository;
	
	@Autowired
	SchoolServiceSuspentionServiceImpl schoolServiceSuspentionServiceImpl;
	
	@GetMapping("schoolSuspension")
	public void runSchoolSuspension() {
		schoolService.sendSuspensionMessage();
	}
	//sendSuspensionMessage
	
	@GetMapping(value = "/getAllClassAllCategoryStudentCountClasswise")
	public @ResponseBody ResponseBodyBean<CountBean> getClassDivisisionList(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("yearId") Integer yearId,@RequestParam("date") String date,@RequestParam("categoryorreligionorminorityorconc") Integer catOrRelignOrMinotyOrConc) {
		  
		try {
			List<CountBean> annoucementResponseBean = schoolService.getAllClassAllCategoryStudentCountClasswise(schoolId,yearId,date, catOrRelignOrMinotyOrConc);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<CountBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<CountBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<CountBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/getAllClassAllCategoryStudentCountDivisionwise")
	public @ResponseBody ResponseBodyBean<CountBean> getDivisisionList(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("yearId") Integer yearId,@RequestParam("date") String date,@RequestParam("categoryorreligionorminorityorconc") Integer catOrRelignOrMinotyOrConc) {
		  
		try {
			List<CountBean> 	annoucementResponseBean = schoolService.getDivisionList(schoolId,yearId,date, catOrRelignOrMinotyOrConc);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<CountBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<CountBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<CountBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	
	@PostMapping(value = "/getCashBankAmountFromFeeForTrailb")
	public @ResponseBody ResponseBodyBean<CashBankAmountBean> getCashBankAmountFromFeeForTrailb(@RequestBody List<RequestBean> requestBean) 		{  
		try {
			List<CashBankAmountBean> 	annoucementResponseBean = schoolService.getCashBankAmountFromFeeForTrailb(requestBean);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<CashBankAmountBean>("200", HttpStatus.OK, annoucementResponseBean);
			} 
			else {
				return new ResponseBodyBean<CashBankAmountBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<CashBankAmountBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	@PostMapping(value = "/getCashBankAmountFromFeeForTrailbDateWise")
	public @ResponseBody ResponseBodyBean<ResponceBean> getCashBankAmountFromFeeForTrailbDateWise(@RequestBody List<RequestBean> requestBean) 		{  
		try {
			List<ResponceBean> 	annoucementResponseBean = schoolService.getCashBankAmountFromFeeForTrailbDateWise(requestBean);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<ResponceBean>("200", HttpStatus.OK, annoucementResponseBean);
			} 
			else {
				return new ResponseBodyBean<ResponceBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<ResponceBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	@PostMapping(value = "/updateTempPassword")
	public @ResponseBody ResponseBodyBean<Integer> updateTempPassword(@RequestBody AppUserTempPassword appUserTempPassword) 		{  
		try {
			Integer 	otpsent = schoolService.updateTempPassword(appUserTempPassword);
			if (otpsent>0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, otpsent);
			} 
			else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>(),"operation failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	
	@PostMapping(value = "/validateOtpForResetPassword")
	public @ResponseBody ResponseBodyBean<Integer> validateOtpForResetPassword(@RequestBody ValidateBean validateBean) 		{  
		try {
			Integer otpsent = schoolService.validateOtpForResetPassword(validateBean);
			if (otpsent>0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, otpsent);
			} 
			else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>(),"Invalid OTP");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	
	@PostMapping(value = "/editFeeReceipt")
	public @ResponseBody ResponseBodyBean<Integer> editFeeReceipt(@RequestBody EditFeeReceiptBean editFeeReceiptBean) 		{  
		try {
			Integer otpsent = schoolService.editFeeReceipt(editFeeReceiptBean);
			if (otpsent>0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, otpsent);
			} 
			else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>(),"Invalid OTP");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
	
	@GetMapping(value = "/getEditedReceipt")
	public @ResponseBody ResponseBodyBean<FeeReceiptBean> getEditedReceipt(
			@RequestParam("schoolId") Integer schoolId) {
		  
		try {
			List<FeeReceiptBean> 	annoucementResponseBean = schoolService.getEditedReceipt(schoolId);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<FeeReceiptBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<FeeReceiptBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeeReceiptBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/getAgeStandardWise")
	public @ResponseBody ResponseBodyBean<CountBean> getAgeStandardWise(
			@RequestParam("schoolId") Integer schoolId, 
			@RequestParam("yearId") Integer yearId,@RequestParam("date") String date//yyyy-mm-dd format
			) {
		  
		try {
			List<CountBean> annoucementResponseBean = schoolService.getAgeStandardWise(schoolId,yearId,date);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<CountBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<CountBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<CountBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/getAgeDivisionWise")
	public @ResponseBody ResponseBodyBean<CountBean> getAgeDivisionWise(
			@RequestParam("schoolId") Integer schoolId, 
			@RequestParam("yearId") Integer yearId,@RequestParam("date") String date//yyyy-mm-dd format
			) {
		  
		try {
			List<CountBean> annoucementResponseBean = schoolService.getAgeDivisionWise(schoolId,yearId,date);
			if (!CollectionUtils.isEmpty(annoucementResponseBean)) {
				return new ResponseBodyBean<CountBean>("200", HttpStatus.OK, annoucementResponseBean);
			} else {
				return new ResponseBodyBean<CountBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<CountBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/getStudentDifferenceBetweenStudentRenewAndClasswiseCatlog")
	public @ResponseBody ResponseBodyBean<StudentDetailsBean> studDifference(
			@RequestParam("schoolId") Integer schoolId, 
			@RequestParam("yearId") Integer yearId,@RequestParam("standardId") Integer standardId,@RequestParam("divisionId") Integer divisionId,@RequestParam("date") String date//yyyy-mm-dd format
			) {
		  
		try {
			List<StudentDetailsBean> studentDifference = schoolService.studDifference(schoolId,yearId,standardId,divisionId,date);
			if (!CollectionUtils.isEmpty(studentDifference)) {
				return new ResponseBodyBean<StudentDetailsBean>("200", HttpStatus.OK, studentDifference);
			} else {
				return new ResponseBodyBean<StudentDetailsBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}
		@GetMapping(value = "/getStudentsPresentInAttendanceCatalogButNotInStudentRenew")
		public @ResponseBody ResponseBodyBean<StudentDifferenceBean> getStudentsPresentInAttendanceCatalogButNotInStudentRenew(
				@RequestParam("schoolId") Integer schoolId, 
				@RequestParam("yearId") Integer yearId,@RequestParam("date") String date//yyyy-mm-dd format
				) {
			  
			try {
				List<StudentDifferenceBean> studentDifference = schoolService.getStudentsPresentInAttendanceCatalogButNotInStudentRenew(schoolId,yearId,date);
				if (!CollectionUtils.isEmpty(studentDifference)) {
					return new ResponseBodyBean<StudentDifferenceBean>("200", HttpStatus.OK, studentDifference);
				} else {
					return new ResponseBodyBean<StudentDifferenceBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
				}
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<StudentDifferenceBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
						new ArrayList<>());
			}
	}

	
	@PostMapping("/sendEmail")
	public ResponseBodyBean<String> SendEmail(@RequestBody EmailBean emailBean) {
		// Sender's email details
      
		return sendEmail(emailBean);

	}

	private ResponseBodyBean<String> sendEmail(EmailBean emailBean) {
		String senderEmail=null;
		String senderPassword=null;
		FeeSendEmailIdsMasterModel credentials = feeSendEmailIdsMasterRepository.getCredentials(emailBean.getSchoolid());
		
		if(credentials!=null) {
			senderEmail=credentials.getUserName();
			senderPassword=credentials.getPassword();
		}else {
			List<GlobalEmailConfigModel> globalConfig = globalEmailConfigRepo.findAll();
			senderEmail=globalConfig.get(0).getEmailId();
			senderPassword=globalConfig.get(0).getPassword();

		}
        // SMTP server details
        String host = "smtp.gmail.com";
        String port = "587"; // or your SMTP port

        // Email content
        String subject = emailBean.getSubject();
        String body = emailBean.getBody();

        // Set properties for SMTP server
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);

        
        Session session = createSession(properties, senderEmail, senderPassword);


        try {
            // Create a MimeMessage object
            Message message = new MimeMessage(session);

            // Set sender and recipients
            message.setFrom(new InternetAddress(senderEmail));
            InternetAddress[] recipients = new InternetAddress[emailBean.getToEmailIds().size()];
           
            for (int i = 0; i < emailBean.getToEmailIds().size(); i++) {
            	recipients[i] = new InternetAddress(emailBean.getToEmailIds().get(i).trim());
			}
            //
            message.setRecipients(Message.RecipientType.TO, recipients);
            
            InternetAddress[] ccRecipients = new InternetAddress[emailBean.getCc().size()];
            for (int i = 0; i < emailBean.getCc().size(); i++) {
                ccRecipients[i] = new InternetAddress(emailBean.getCc().get(i).trim());
            }
            message.setRecipients(Message.RecipientType.CC, ccRecipients);

            // Add BCC recipients
            InternetAddress[] bccRecipients = new InternetAddress[emailBean.getBcc().size()];
            for (int i = 0; i < emailBean.getBcc().size(); i++) {
                bccRecipients[i] = new InternetAddress(emailBean.getBcc().get(i).trim());
            }
            message.setRecipients(Message.RecipientType.BCC, bccRecipients);

            
            // Set email subject and body
            message.setSubject(subject);
            message.setText(body);

            // Send the email
            Transport.send(message);
            
            //--------------------------//---------------------------------
            //notificatin
    		
            HttpClient httpClient = HttpClientBuilder.create().build();
    		String serverKey=ApplicationConstants.serverKey;

            HttpPost httpPost = new HttpPost("https://fcm.googleapis.com/fcm/send");
    		httpPost.setHeader("Authorization", "key=" + serverKey);
    		httpPost.setHeader("Content-Type", "application/json");
    		Integer success=0;
    		if(!CollectionUtils.isEmpty(emailBean.getTokens())) {
    			try {

    				JSONObject json = new JSONObject();
    				json.put("registration_ids",emailBean.getTokens());

    				JSONObject data = new JSONObject();
    				data.put("title", emailBean.getNotificationTitle());
    				data.put("body", emailBean.getNotificationSubject());
    				//data.put("userId",1059000001);
    				//data.put("menuId",7);
    				//data.put("menuName","Notice");
    				//data.put("SeenStataus", "0");
    				json.put("data", data);

    				StringEntity stringEntity = new StringEntity(json.toString());
    				httpPost.setEntity(stringEntity);
    				HttpResponse response = httpClient.execute(httpPost);
    				success = response.getStatusLine().getStatusCode();
    				String statusMessage = response.getStatusLine().getReasonPhrase();
    				Thread.sleep(1000);

    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}

            
            
            
			return new ResponseBodyBean<String>("200", HttpStatus.OK, "Email sent successfully.");

         //  System.out.println("Email sent successfully.");
        } catch (MessagingException e) {
        	return new ResponseBodyBean<String>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					"Error occurred while sending email");
           // System.out.println("Error occurred while sending email: " + e.getMessage());
        }
	}

	private static Session createSession(Properties properties, final String senderEmail, final String senderPassword) {
        return Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });
    }
	
	
	@PostMapping("/sendEmailToAllStudentsFromClass")
	public void sendEmailToAllStudentsFromClass(@RequestParam("schoolId")Integer schoolId,@RequestParam("religionId")Integer religionId,@RequestParam("subject")String subject,@RequestParam("body")String body ){
	
		List<String> emailIds=studentMasterRepository.getAllStudents(schoolId,religionId);
		
//		List<String> emailIds=new ArrayList<>();
//		emailIds.add("amoldhole1234@gmail.com");
//		emailIds.add("pachghare.aman4262@gmail.com ");
		int batchSize = 100;
        int totalRecords = emailIds.size();
        int batches = (int) Math.ceil((double) totalRecords / batchSize);
      //  String message ="Dear #var1#,Your installment #var2# Please pay fees before due date to avoid fine.Ingore if already Paid. -ITPL";			

        //String message="Dear #var1# Your Fees are overdue.Fine of #var2# has been applied.Please pay your outstanding fees immediately to avoid further penalties -ITPL";
        
        String senderEmail;
        String senderPassword;
         FeeSendEmailIdsMasterModel credentials = feeSendEmailIdsMasterRepository.getCredentials(schoolId);
		
		if(credentials!=null) {
			senderEmail=credentials.getUserName();
			senderPassword=credentials.getPassword();
		}else {
			List<GlobalEmailConfigModel> globalConfig = globalEmailConfigRepo.findAll();
			senderEmail=globalConfig.get(0).getEmailId();
			senderPassword=globalConfig.get(0).getPassword();

		}

        ExecutorService executorService = Executors.newFixedThreadPool(batches);
        											//300		
        for (int i = 0; i < batches; i++) {
            int start = i * batchSize;     //0//50//100//150//200//250
            int end = Math.min(start + batchSize, totalRecords);//50//100//150//200//250//300
            List<String> batch = emailIds.subList(start, end);
            executorService.submit(new MessageSender(batch,schoolServiceSuspentionServiceImpl,schoolId,subject,body,senderEmail,senderPassword));
        }

        executorService.shutdown();

		
	}
	

}
