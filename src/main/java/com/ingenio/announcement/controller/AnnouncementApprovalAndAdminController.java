package com.ingenio.announcement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;
import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.ingenio.announcement.bean.AnnnouncementLikeOrDislikeBean;
import com.ingenio.announcement.bean.AnnouncementApprovalResponseBean;
import com.ingenio.announcement.bean.AnnouncementComentBean;
import com.ingenio.announcement.bean.AnnouncementCommentResponseBean;
import com.ingenio.announcement.bean.AnnouncementLikeResponseBean;
import com.ingenio.announcement.bean.AnnouncementReactionBean;
import com.ingenio.announcement.bean.AnnouncementRequestBean;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.AnnouncementUserToGroupBean;
import com.ingenio.announcement.bean.AnnouncementViewBean;
import com.ingenio.announcement.bean.AnnouncementViewResponseBean;
import com.ingenio.announcement.bean.FeedbackEmailRequestBean;
import com.ingenio.announcement.bean.GroupBean;
import com.ingenio.announcement.bean.ReportBean;
import com.ingenio.announcement.bean.ResponseBodyBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.SubmitApprovalBean;
import com.ingenio.announcement.model.AnnouncementTypeModel;
import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;
import com.ingenio.announcement.service.AnnouncementService;
import com.ingenio.announcement.service.SendCommunicationService;

@CrossOrigin
@RestController
public class AnnouncementApprovalAndAdminController {

	@Autowired
	private AnnouncementService announcementService;

	@Autowired
	private SendCommunicationService sendCommunicationService;

	private ExecutorService executor = Executors.newSingleThreadExecutor();

	RestTemplate ob=new RestTemplate();

	@PreDestroy
	public void shutdonw() {
		executor.shutdown();
	}

	@GetMapping(value = "/announcement_approvalAuthority_bygroupId")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAnnouncementAuthorityusingGroupId(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("groupId") Integer groupId) {
		List<AnnouncementResponseBean> approvalAuthorityList = new ArrayList<AnnouncementResponseBean>();
		try {
			approvalAuthorityList = announcementService.getAnnouncementAuthorityusingGroupId(schoolId, groupId);
			if (CollectionUtils.isNotEmpty(approvalAuthorityList)) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, approvalAuthorityList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@PostMapping(value = "/submit_announcement_approval_status")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> submitAnnouncementApprovalStatus(
			@RequestBody SubmitApprovalBean StudentAnnouncementBean) {
		try {
			Integer saveId = sendCommunicationService.submitAnnouncementApprovalStatus(StudentAnnouncementBean);
			if (saveId > 1 ) {
				CompletableFuture<String> future = new CompletableFuture<>();

				CompletableFuture.runAsync(() -> {
					//executor.submit(announcementService.sendNotificationNew(StudentAnnouncementBean));
					String result=sendCommunicationService.sendCommunication(StudentAnnouncementBean);
					future.complete(result);
				});
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} 
			else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>(),"Insufficient SMS Balence");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/getAdminAnnouncement")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAdminAnnouncement(
			@RequestParam("appuserId") Integer appuserId, @RequestParam("schoolId") Integer schoolId,
			@RequestParam("offset") Integer offset, @RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "typeId", required = false, defaultValue = "0") Integer typeId) {
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {
			assignedAnnouncementList = announcementService.getAdminAnnouncement(appuserId, offset, limit, typeId,
					schoolId);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)
					&& assignedAnnouncementList.get(0).getAnnouncementId() != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/getannoucementforapproval")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getannoucementforapproval(
			@RequestParam("appuserId") Integer appuserId, @RequestParam("schoolId") Integer schoolId,
			@RequestParam("offset") Integer offset, @RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "typeId", required = false) Integer typeId) {
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {
			assignedAnnouncementList = announcementService.getannoucementforapproval(appuserId, offset, limit, typeId,
					schoolId);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)
					&& assignedAnnouncementList.get(0).getAnnouncementId() != null) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200", HttpStatus.OK, assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@GetMapping(value = "/getApprovalStatusOfAnnouncement")
	public @ResponseBody ResponseBodyBean<AnnouncementApprovalResponseBean> getApprovalStatusOfAnnouncement(
			@RequestParam("schoolId") Integer schoolId, @RequestParam("postId") Integer postId) {
		List<AnnouncementApprovalResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementApprovalResponseBean>();
		try {
			assignedAnnouncementList = announcementService.getApprovalStatusOfAnnouncement(schoolId, postId);
			if (CollectionUtils.isNotEmpty(assignedAnnouncementList)) {
				return new ResponseBodyBean<AnnouncementApprovalResponseBean>("200", HttpStatus.OK,
						assignedAnnouncementList);
			} else {
				return new ResponseBodyBean<AnnouncementApprovalResponseBean>("404", HttpStatus.NOT_FOUND,
						new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementApprovalResponseBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@PostMapping(value = "/send_post_for_approval")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> sendPostForApproval(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("postId") Integer postId) {
		try {
			Integer saveId = announcementService.sendPostForApproval(schoolId, postId);
			if (saveId > 0) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, saveId);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping("/getListofGroupForUser")
	public ResponseBodyBean<GroupBean> getListofGroupForUser(@RequestParam("appUserId") Integer appUserId) {

		try {
			List<GroupBean> groupBean = announcementService.getListofGroupForUser(appUserId);
			if (CollectionUtils.isNotEmpty(groupBean)) {
				return new ResponseBodyBean<GroupBean>("200", HttpStatus.OK, groupBean);
			} else {
				return new ResponseBodyBean<GroupBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<GroupBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	
	
//	@GetMapping("/getMessageReadStatus")
//	public ResponseBodyBean<SentMessagesModel> getMessageReadStatus(@RequestParam("rcode") String messageSavedId) {
//
//		try {
//			List<SentMessagesModel> groupBean = sendCommunicationService.getMessageReadStatus(messageSavedId);
//			if (CollectionUtils.isEmpty(groupBean)) {
//				return new ResponseBodyBean<SentMessagesModel>("200", HttpStatus.OK, groupBean.get());
//			} else {
//				return new ResponseBodyBean<SentMessagesModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseBodyBean<SentMessagesModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
//		}

//	}
	
	@GetMapping("/getAllNotificationsNew")
	public ResponseBodyBean<SaveSendNotificationModel> getAllNotification(@RequestParam("appUserId") Integer appUserId,@RequestParam("flag") String flag,@RequestParam("offset") Integer offset,@RequestParam("limit") Integer limit) {

		try {
			List<SaveSendNotificationModel> allNotifications = sendCommunicationService.getAllNotification(appUserId,flag,offset,limit);
			if (!CollectionUtils.isEmpty(allNotifications)) {
				return new ResponseBodyBean<SaveSendNotificationModel>("200", HttpStatus.OK, allNotifications);
			} else {
				return new ResponseBodyBean<SaveSendNotificationModel>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SaveSendNotificationModel>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}

	}
	
	
	

}
