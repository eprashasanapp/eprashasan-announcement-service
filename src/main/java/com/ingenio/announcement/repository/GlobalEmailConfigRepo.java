package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.GlobalEmailConfigModel;


public interface GlobalEmailConfigRepo extends JpaRepository<GlobalEmailConfigModel, Integer> {

}
