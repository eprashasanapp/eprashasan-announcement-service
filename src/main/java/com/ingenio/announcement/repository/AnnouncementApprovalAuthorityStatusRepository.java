package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementApprovalStatusModel;


public interface AnnouncementApprovalAuthorityStatusRepository extends JpaRepository<AnnouncementApprovalStatusModel, Integer>{

	@Query(value="SELECT a from AnnouncementApprovalStatusModel a "
			+ "where a.schoolMasterModel.schoolid= :schoolId and a.announcementModel.announcementId =:postId ")
	List<AnnouncementApprovalStatusModel> approvalList(Integer schoolId,Integer postId);

	@Query(value="SELECT a.announcementModel.announcementId from AnnouncementApprovalStatusModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.appUserModel.appUserRoleId=:appUserId group by a.announcementModel.announcementId order by a.announcementModel.announcementId desc  ")
	List<Integer> getPostIds(Integer schoolId,Integer appUserId);

	
	





	

}
