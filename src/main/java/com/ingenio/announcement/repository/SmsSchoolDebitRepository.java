package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.SmsSchoolDebitModel;

public interface SmsSchoolDebitRepository extends JpaRepository<SmsSchoolDebitModel, Integer> {

	
	
	//Integer updateBalance(Integer totalMessages);

}
