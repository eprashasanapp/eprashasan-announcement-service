package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.AnnouncementAttachmentModel;
import com.ingenio.announcement.model.AnnouncementModel;

@Repository
public interface StudentAnnouncementAttachmentRepository extends JpaRepository<AnnouncementAttachmentModel, Integer> {

	List<AnnouncementAttachmentModel> findByAnnouncementModel(AnnouncementModel studentAssignAnnouncementModel);

	void deleteByAnnouncementModel(AnnouncementModel model);
}
