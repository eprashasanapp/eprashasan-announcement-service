package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementApprovalStatusModel;
import com.ingenio.announcement.model.AnnouncmenetApprovalAuthorityModel;


public interface AnnouncementApprovalAuthorityRepository extends JpaRepository<AnnouncmenetApprovalAuthorityModel, Integer>{


	@Query(value="SELECT a from AnnouncmenetApprovalAuthorityModel a where a.schoolMasterModel.schoolid=:schoolId and "
			+ " a.announcementGroupModel.announcementGroupId=:groupId ")
	List<AnnouncmenetApprovalAuthorityModel> getApprovalList(Integer schoolId, Integer groupId);

	

}
