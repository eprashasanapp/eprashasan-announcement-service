package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.EventSmsHistoryModel;

@Repository
public interface StudentEventSmsAssignedToRepository extends JpaRepository<EventSmsHistoryModel, Integer>{
	
	
	@Query(value="SELECT COALESCE(MAX(a.eventSmsID),0)+1 from EventSmsHistoryModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
