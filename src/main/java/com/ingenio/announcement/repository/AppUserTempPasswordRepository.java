package com.ingenio.announcement.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AppUserTempPassword;

public interface AppUserTempPasswordRepository extends JpaRepository<AppUserTempPassword, Integer> {

	@Transactional
	@Modifying
	@Query("update AppUserTempPassword a set a.updateTimeStamp=NOW(),a.isPassUpdate=1 where a.tempPassId=:tempPassId ")
	void updateFlag(Integer tempPassId);

}
