/*package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.StaffSubjectDetailsBean;
import com.ingenio.announcement.model.StaffSubjectDetailsModel;

public interface StaffSubjectDetailsRepository extends JpaRepository<StaffSubjectDetailsModel, Integer> {
	

	@Query(value="select new com.ingenio.announcement.bean.StaffSubjectDetailsBean(f.staffId,a.standardId,a.standardName,b.divisionId,b.divisionName,c.subjectId,c.subjectName,e.staffSubId,e.isClassTeacher ) "
		  + "FROM StandardMasterModel a LEFT JOIN DivisionMasterModel b ON b.standardMasterModel.standardId=a.standardId AND b.isDel='0' AND b.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid " 
		  + "LEFT JOIN SubjectModel c ON c.standardMasterModel.standardId=a.standardId AND c.isDel='0' AND c.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid "
		  + "LEFT JOIN StaffSubjectDetailsModel e ON e.standardMasterModel.standardId=a.standardId AND e.divisionMasterModel.divisionId=b.divisionId AND e.subjectModel.subjectId=c.subjectId AND e.isDel='0' "
		  + "AND e.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid  LEFT OUTER JOIN StaffBasicDetailsModel f ON f.staffId=e.staffBasicDetailsModel.staffId AND f.isDel='0' AND f.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid "
		  + "LEFT JOIN YearMasterModel d ON d.yearId=c.yearMasterModel.yearId AND d.isDel='0' AND d.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid "
		  + "WHERE d.schoolMasterModel.schoolid=?2 AND d.isDel='0' AND d.yearId=?3 and e.staffBasicDetailsModel.staffId=?1 ORDER BY a.standardPriority,b.divisionId,c.subjectId ")
	List<StaffSubjectDetailsBean> findAssignedSubjects(Integer staffId,Integer schoolId,Integer yearId, Pageable pageable);

}
*/