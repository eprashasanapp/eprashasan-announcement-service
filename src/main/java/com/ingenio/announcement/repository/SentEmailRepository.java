package com.ingenio.announcement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.SentEmailModel;
import com.ingenio.announcement.model.SentMessagesModel;

public interface SentEmailRepository extends JpaRepository<SentEmailModel, Integer>{

	Optional<SentEmailModel> findByRcode(String rcode);

	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.emailId,a.rcode,a.cDate,b.announcement,b.announcementTitle,COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'')"
			+ ",COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'')) from SentEmailModel a "
			+ "left join StaffBasicDetailsModel stf on stf.staffId=a.staffStudentId and a.role='Staff'  "
			+ "left join StudentMasterModel stud on stud.studentId=a.staffStudentId and a.role='Student'  "
			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.staffStudentId=:staffStudentId and a.role=:role order by id desc  ")
	List<SentMessagesModel> getEmails(Integer staffStudentId, String role);

	@Query("select a from SentEmailModel a where a.appUserId=:staffUserId and a.role=:role and a.announcementId=:announcementId")
	List<SentEmailModel> getAllEmails(Integer staffUserId, String role, Integer announcementId);

	List<SentEmailModel> findByAppUserId(Integer appUserId);
}
