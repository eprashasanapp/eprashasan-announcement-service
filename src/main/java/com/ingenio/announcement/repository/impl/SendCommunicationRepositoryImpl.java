package com.ingenio.announcement.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.SmsSchoolDebitModel;


@Repository
@Transactional
public class SendCommunicationRepositoryImpl {

	public SmsSchoolDebitModel CheckBalance(Integer schoolId, EntityManager entityManager) {


		Query query = entityManager.createQuery("SELECT a FROM SmsSchoolDebitModel a WHERE a.schoolid = :schoolId");
        query.setParameter("schoolId", schoolId);
        SmsSchoolDebitModel account = (SmsSchoolDebitModel) query.getSingleResult();
        return account;
		
	}

}
