package com.ingenio.announcement.repository.impl;

import java.util.List;
import org.springframework.stereotype.Component;

import com.ingenio.announcement.repository.CarryForwardSubjectExamDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Component
public class CarryForwardSubjectExamDaoImpl implements CarryForwardSubjectExamDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Object[]> getSmsConfig(Integer templateTypeId) {
		//SELECT subGroupId,subGroupName,groupId,schoolid,standardId,yearId,selectionType,maxSubjects,minSubjects,userId,sinkingFlag,isCompulsory FROM exam_subgroup_properties WHERE isDel=0 AND schoolid=1059000001 AND groupId=1059000001;
		String sql="SELECT templateId,message,templateName,websiteUrl,statusUrl,apiKey,user,sender "
				+ "FROM sms_template_global "
				+ "WHERE templateTypeId = :templateTypeId "
				+ "LIMIT 1";
		
		
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("templateTypeId", templateTypeId); // Set the value for param1
		List<Object[]> rows = query.getResultList();
		return rows;
	}
	
	
}
