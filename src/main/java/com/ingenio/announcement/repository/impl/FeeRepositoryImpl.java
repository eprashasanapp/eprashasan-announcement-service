package com.ingenio.announcement.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.ingenio.announcement.bean.FeedbackEmailRequestBean;

@Repository
@Transactional
public class FeeRepositoryImpl {
	  @PersistenceContext
	  private EntityManager entitymanager;
	  
	  public int updateStatus(FeedbackEmailRequestBean feedbackEmailRequestBean) {
		  StringBuffer feeReportQuery = new StringBuffer();
		  feeReportQuery.append("update fee_payment_gateway a set a.paymentStatus='"+feedbackEmailRequestBean.getStatus()+"', "
		  		+ "a.paymentStatusCode='"+feedbackEmailRequestBean.getStatusCode()+"',"
		  		+ "a.paymentStatusMessage='"+feedbackEmailRequestBean.getStatusMessage()+"' "
		  		+ "where a.processId='"+feedbackEmailRequestBean.getProcessId()+"' ");
		  return entitymanager.createNativeQuery(feeReportQuery.toString()).executeUpdate();
	  }
}
