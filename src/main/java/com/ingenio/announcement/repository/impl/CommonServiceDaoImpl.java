package com.ingenio.announcement.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.bean.CashBankAmountBean;
import com.ingenio.announcement.bean.CountBean;
import com.ingenio.announcement.bean.FeeReceiptBean;
import com.ingenio.announcement.bean.FeeReceiptDetailsBean;
import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.RequestBean;
import com.ingenio.announcement.bean.StudentDifferenceBean;
import com.ingenio.announcement.bean.SubmitApprovalBean;


@SuppressWarnings("unchecked")
@Repository
public class CommonServiceDaoImpl {

	@PersistenceContext
	private EntityManager entitymanager;

public List<CountBean> getCount(Integer schoolid,Integer yearId,String date, Integer catOrRelignOrMinotyOrConc) {
		
		StringBuilder que=new StringBuilder();
		que.append("SELECT  COUNT(DISTINCT a.studentId) as count, ");
		que.append("a.standardId as standardId,d.standardName as standardName,ifnull(b.stud_gender,'NA') as studGender ");
		if(catOrRelignOrMinotyOrConc==0) {
		que.append(",ifnull(b.categoryId,'NA') as categoryId,ifnull(c.categoryName,'NA') as categoryName ");
		}
		if(catOrRelignOrMinotyOrConc==1) {
		que.append(",ifnull(b.religionId,'NA') as categoryId,ifnull(c.religionName,'NA') as categoryName ");
		} 
		if(catOrRelignOrMinotyOrConc==2) {
			que.append(",ifnull(b.caste_id,'NA') as categoryId,ifnull(c.cast_name,'NA') as categoryName ");
		} 
		if(catOrRelignOrMinotyOrConc==3) {
			que.append(",ifnull(b.status,'NA') as categoryId,ifnull(c.minority_type,'NA') as categoryName ");
		} 
		if(catOrRelignOrMinotyOrConc==4) {
			que.append(",ifnull(b.previousState,'NA') as categoryId,ifnull(c.concession_type,'NA') as categoryName ");
		} 
		que.append("FROM student_master b  ");
//		que.append("LEFT JOIN (select a.studentId as studentId,a.yearId as yearId "
//				+ "from student_master y "
//				+ "left join student_standard_renew x ON y.stud_id=x.studentId "
//				+ "where x.yearId="+yearId+" and ((x.college_leavingdate is  null and  str_to_date(a.renewAdmissiondate,'%d-%m-%Y') <= str_to_date('"+date+"','%d-%m-%Y') and x.isLeft=0) or ( str_to_date(x.renewAdmissiondate,'%d-%m-%Y') <= str_to_date('"+date+"','%d-%m-%Y') and str_to_date(x.college_leavingdate,'%d-%m-%Y') >=str_to_date('"+date+"','%d-%m-%Y') )  ) "
//						+ "AND ((x.isOnlineAdmission=1 and  x.isApproval=1) or x.isOnlineAdmission=0 OR (x.isRegistration='1' AND x.isRegistrationApprovedFlag='1'))  and y.isAdmissionCancelled=1 and x.isDel=0 and y.isDel=0 group by studentId) as a ");
//		
		
		que.append(" LEFT JOIN ( "
			+"	SELECT x.studentId AS studentId,x.yearId AS yearId,x.standardId AS standardId ,x.schoolid AS schoolid,x.college_leavingdate AS college_leavingdate,x.renewAdmissiondate AS renewAdmissiondate,x.isLeft AS isLeft " 
			+"	,x.isOnlineAdmission AS isOnlineAdmission,x.isApproval AS isApproval,x.isRegistration AS isRegistration,x.isRegistrationApprovedFlag AS isRegistrationApprovedFlag,x.isDel AS xisDel,y.isDel AS  yisDel "
			+"	FROM student_master y "
			+"	LEFT JOIN student_standard_renew x ON y.stud_id=x.studentId "
			+"	WHERE x.yearId="+yearId+" AND ((x.college_leavingdate IS NULL AND (STR_TO_DATE(x.renewAdmissiondate,'%d-%m-%Y')  <= STR_TO_DATE('"+date+"','%d-%m-%Y') or x.renewAdmissiondate is null) AND x.isLeft=0) OR ((STR_TO_DATE(x.renewAdmissiondate,'%d-%m-%Y') <= STR_TO_DATE('"+date+"','%d-%m-%Y') or x.renewAdmissiondate is null) AND STR_TO_DATE(x.college_leavingdate,'%d-%m-%Y') >= STR_TO_DATE('"+date+"','%d-%m-%Y'))) AND ((x.isOnlineAdmission=1 AND x.isApproval=1) OR x.isOnlineAdmission=0 OR (x.isRegistration='1' AND x.isRegistrationApprovedFlag='1')) AND y.isAdmissionCancelled=1 AND x.isDel=0 AND y.isDel=0 "
			+"	GROUP BY studentId) AS a ON a.studentId=b.stud_id ");

		//que.append("LEFT JOIN division div ON div.divisionId=a.divisionId ");

		if(catOrRelignOrMinotyOrConc==0) {
			que.append("LEFT JOIN category c ON c.categoryId=b.categoryId ");
		}
		if(catOrRelignOrMinotyOrConc==1) {
			que.append("LEFT JOIN religion c ON c.religionId=b.religionId ");
		}
		if(catOrRelignOrMinotyOrConc==2) {
			que.append("LEFT JOIN castemaster c ON c.casteId=b.caste_id ");
		}
		if(catOrRelignOrMinotyOrConc==3) {
			que.append("LEFT JOIN minority_master c ON c.minority_Id=b.status ");
		}
		if(catOrRelignOrMinotyOrConc==4) {
			que.append("LEFT JOIN concession_master c ON c.concession_Id=b.previousState ");
		}
		que.append("LEFT JOIN standardmaster d ON d.standardId=a.standardId ");
		que.append(" WHERE a.yearId="+yearId+" AND a.schoolid="+schoolid+"   and ((a.college_leavingdate is  null and  (str_to_date(a.renewAdmissiondate,'%d-%m-%Y') <= str_to_date('"+date+"','%d-%m-%Y') OR a.renewAdmissiondate IS NULL) and a.isLeft=0) or ( ( a.renewAdmissiondate IS NULL or str_to_date(a.renewAdmissiondate,'%d-%m-%Y') <= str_to_date('"+date+"','%d-%m-%Y')) and str_to_date(a.college_leavingdate,'%d-%m-%Y') >=str_to_date('"+date+"','%d-%m-%Y') )  )   ");
		que.append( "AND ((a.isOnlineAdmission=1 and  a.isApproval=1) or a.isOnlineAdmission=0 OR (a.isRegistration='1' AND a.isRegistrationApprovedFlag='1'))  and b.isAdmissionCancelled=1 and a.xisDel=0 and a.yisDel=0 ");//a.college_leavingdate is null or

		que.append("GROUP BY a.standardId,b.stud_gender  ");

		if(catOrRelignOrMinotyOrConc==0) {
			que.append(",c.categoryName ");
			que.append(" ORDER BY a.standardId,b.categoryId ");
		}
		if(catOrRelignOrMinotyOrConc==1) {
			que.append(",c.religionName ");
			que.append(" ORDER BY a.standardId,b.religionId ");
		} 
		if(catOrRelignOrMinotyOrConc==2) {
			que.append(",c.cast_name ");
			que.append(" ORDER BY a.standardId,b.caste_id ");
		}
		 if(catOrRelignOrMinotyOrConc==3) {
				que.append(",c.minority_type ");
				que.append(" ORDER BY a.standardId,b.status ");
			}
		 if(catOrRelignOrMinotyOrConc==4) {
				que.append(",c.concession_type ");
				que.append(" ORDER BY a.standardId,b.previousState ");
			}

		Query query = entitymanager.createNativeQuery(que.toString());
		
		List<CountBean> list = query.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.aliasToBean(CountBean.class)).getResultList();

		return list;
	}

public List<CountBean> getDivisionWiseCount(Integer schoolid,Integer yearId,String date, Integer catOrRelignOrMinotyOrConc) {
	
	StringBuilder que=new StringBuilder();
	que.append("SELECT  COUNT(DISTINCT a.studentId) as count, ");
	que.append("a.standardId as standardId,d.standardName as standardName,cast(ifnull(a.divisionId,'NA') as CHAR) as divisionId,ifnull(divs.divisionName,'NA') as divisionName,ifnull(b.stud_gender,'NA') as studGender ");
	if(catOrRelignOrMinotyOrConc==0) {
	que.append(",ifnull(b.categoryId,'NA') as categoryId,ifnull(c.categoryName,'NA') as categoryName ");
	}
	if(catOrRelignOrMinotyOrConc==1) {
	que.append(",ifnull(b.religionId,'NA') as categoryId,ifnull(c.religionName,'NA') as categoryName ");
	} 
	if(catOrRelignOrMinotyOrConc==2) {
		que.append(",ifnull(b.caste_id,'NA') as categoryId,ifnull(c.cast_name,'NA') as categoryName ");
	} 
	if(catOrRelignOrMinotyOrConc==3) {
		que.append(",ifnull(b.status,'NA') as categoryId,ifnull(c.minority_type,'NA') as categoryName ");
	} 
	if(catOrRelignOrMinotyOrConc==4) {
		que.append(",ifnull(b.previousState,'NA') as categoryId,ifnull(c.concession_type,'NA') as categoryName ");
	} 
	//que.append("FROM student_standard_renew a  ");
	que.append("from  student_master b  ");
	
	que.append(" LEFT JOIN ( "
			+"	SELECT x.studentId AS studentId,x.divisionId as divisionId,x.yearId AS yearId,x.standardId AS standardId ,x.schoolid AS schoolid,x.college_leavingdate AS college_leavingdate,x.renewAdmissiondate AS renewAdmissiondate,x.isLeft AS isLeft " 
			+"	,x.isOnlineAdmission AS isOnlineAdmission,x.isApproval AS isApproval,x.isRegistration AS isRegistration,x.isRegistrationApprovedFlag AS isRegistrationApprovedFlag,x.isDel AS xisDel,y.isDel AS  yisDel "
			+"	FROM student_master y "
			+"	LEFT JOIN student_standard_renew x ON y.stud_id=x.studentId "
			+"	WHERE x.yearId="+yearId+" AND ((x.college_leavingdate IS NULL AND (STR_TO_DATE(x.renewAdmissiondate,'%d-%m-%Y')  <= STR_TO_DATE('"+date+"','%d-%m-%Y') or x.renewAdmissiondate is null) AND x.isLeft=0) OR ((STR_TO_DATE(x.renewAdmissiondate,'%d-%m-%Y') <= STR_TO_DATE('"+date+"','%d-%m-%Y') or x.renewAdmissiondate is null) AND STR_TO_DATE(x.college_leavingdate,'%d-%m-%Y') >= STR_TO_DATE('"+date+"','%d-%m-%Y'))) AND ((x.isOnlineAdmission=1 AND x.isApproval=1) OR x.isOnlineAdmission=0 OR (x.isRegistration='1' AND x.isRegistrationApprovedFlag='1')) AND y.isAdmissionCancelled=1 AND x.isDel=0 AND y.isDel=0 "
			+"	GROUP BY studentId) AS a ON a.studentId=b.stud_id ");

	que.append("LEFT JOIN division divs ON divs.divisionId=a.divisionId ");

	if(catOrRelignOrMinotyOrConc==0) {
		que.append("LEFT JOIN category c ON c.categoryId=b.categoryId ");
	}
	if(catOrRelignOrMinotyOrConc==1) {
		que.append("LEFT JOIN religion c ON c.religionId=b.religionId ");
	}
	if(catOrRelignOrMinotyOrConc==2) {
		que.append("LEFT JOIN castemaster c ON c.casteId=b.caste_id ");
	}
	if(catOrRelignOrMinotyOrConc==3) {
		que.append("LEFT JOIN minority_master c ON c.minority_Id=b.status ");
	}
	if(catOrRelignOrMinotyOrConc==4) {
		que.append("LEFT JOIN concession_master c ON c.concession_Id=b.previousState ");
	}
	que.append("LEFT JOIN standardmaster d ON d.standardId=a.standardId ");
	que.append(" WHERE a.yearId="+yearId+" AND a.schoolid="+schoolid+"   and ((a.college_leavingdate is  null and  (str_to_date(a.renewAdmissiondate,'%d-%m-%Y') <= str_to_date('"+date+"','%d-%m-%Y') OR a.renewAdmissiondate IS NULL) and a.isLeft=0) or ( ( a.renewAdmissiondate IS NULL or str_to_date(a.renewAdmissiondate,'%d-%m-%Y') <= str_to_date('"+date+"','%d-%m-%Y')) and str_to_date(a.college_leavingdate,'%d-%m-%Y') >=str_to_date('"+date+"','%d-%m-%Y') )  )   ");
	que.append( "AND ((a.isOnlineAdmission=1 and  a.isApproval=1) or a.isOnlineAdmission=0 OR (a.isRegistration='1' AND a.isRegistrationApprovedFlag='1'))  and b.isAdmissionCancelled=1 and a.xisDel=0 and a.yisDel=0 ");//a.college_leavingdate is null or

	que.append("GROUP BY a.standardId,b.stud_gender ");

	if(catOrRelignOrMinotyOrConc==0) {
		que.append(",b.categoryId ");
		que.append(" ORDER BY a.standardId,a.divisionId,b.categoryId ");
	}
	if(catOrRelignOrMinotyOrConc==1) {
		que.append(",b.religionId ");
		que.append(" ORDER BY a.standardId,a.divisionId,b.religionId ");
	} 
	if(catOrRelignOrMinotyOrConc==2) {
		que.append(",b.caste_id ");
		que.append(" ORDER BY a.standardId,a.divisionId,b.caste_id ");
	}
	 if(catOrRelignOrMinotyOrConc==3) {
			que.append(",b.status ");
			que.append(" ORDER BY a.standardId,a.divisionId,b.status ");
		}
	 if(catOrRelignOrMinotyOrConc==4) {
			que.append(",b.previousState ");
			que.append(" ORDER BY a.standardId,a.divisionId,b.previousState ");
		}

	Query query = entitymanager.createNativeQuery(que.toString());
	
	List<CountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CountBean.class)).getResultList();

	return list;
}



public List<CountBean> getAllCategories(Integer schoolId, Integer catOrRelignOrMinotyOrConc) {
	
	StringBuilder que=new StringBuilder();
	
	if(catOrRelignOrMinotyOrConc==0) {
	que.append("SELECT cast(a.categoryId as CHAR) as categoryId,a.categoryName as categoryName from category a "
			+ "where a.schoolid="+schoolId+" ");
	}
	if(catOrRelignOrMinotyOrConc==1) {
		que.append("SELECT cast(a.religionId as CHAR) as categoryId,a.religionName as categoryName from religion a "
				+ "where a.schoolid="+schoolId+" ");
	}
	if(catOrRelignOrMinotyOrConc==2) {
		que.append("SELECT cast(a.casteId as CHAR) as categoryId,a.cast_name as categoryName from castemaster a "
				+ "where a.schoolid="+schoolId+" ");
	}
	if(catOrRelignOrMinotyOrConc==3) {
		que.append("SELECT cast(a.minority_Id as CHAR) as categoryId,a.minority_type as categoryName from minority_master a "
				+ "where a.schoolid="+schoolId+" ");
	}
	if(catOrRelignOrMinotyOrConc==4) {
		que.append("SELECT cast(a.concession_Id as CHAR) as categoryId,a.concession_type as categoryName from concession_master a "
				+ "where a.schoolid="+schoolId+" ");
	}
	
	
	Query query = entitymanager.createNativeQuery(que.toString());
	List<CountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CountBean.class)).getResultList();


	
	return list;
}



public List<CashBankAmountBean> getBankDetailsAndTotalAmount(RequestBean requestBean) {
	
	StringBuilder que=new StringBuilder();

	que.append("SELECT (sum(a.payFee)-SUM(a.discount)) AS paidFee,a.payType AS payType,e.payTypeName AS payTypeName,");
	que.append("a.bankId AS bankId,f.bankName AS bankName,f.bankAccNo AS bankAccNo,f.ifscNo AS ifscNo,cast(f.accBankLegderId as CHAR) AS accBankLegderId,f.key AS akey,f.salt AS asalt ");
	que.append("FROM fee_paidfeetable a  ");
	que.append("LEFT JOIN fee_receipt b ON a.receiptID=b.receiptID ");
	que.append("LEFT JOIN fee_studentfee c ON a.studFeeID=c.studFeeID ");
	que.append("LEFT JOIN fee_setting d ON c.feeSettingID=d.feeSettingID AND b.yearId=d.yearID   ");
	que.append("LEFT JOIN fee_pay_type e ON e.payTypeID=a.payType ");
	que.append("LEFT JOIN challanbankdetails f ON a.bankId=f.bankId ");
	que.append("WHERE a.schoolID="+requestBean.getSchoolid()+"   "); //and b.typeOfReceipt in (:typeOfReceipt)
	que.append(" and b.isDel=0 and a.isDel=0 ");
	
	que.append(" AND a.paidfeeDate BETWEEN '"+requestBean.getFromDate()+"' AND '"+requestBean.getToDate()+"' ");
//	que.append("AND (str_to_date(a.paidfeeDate,'%d-%m-%Y') >= STR_TO_DATE('"+requestBean.getFromDate()+"','%d-%m-%Y') ");  
//	que.append("AND str_to_date(a.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+requestBean.getToDate()+"','%d-%m-%Y')) ");		 

	if(requestBean.getIsSocityReceipt()!=null) {
		que.append(" AND b.isSocietyReceipt=:isSocityReceipt ");//"+requestBean.getIsSocityReceipt()+"	
	}
	
	if(requestBean.getTypeOfReceipt()!=null) {
	que.append("AND b.typeOfReceipt in (:typeOfReceipt) ");
	}
	if(requestBean.getStandardId()!=null) {
	que.append("AND d.standardID in (:standardIds) ");
	}
	if(requestBean.getSubheadId()!=null) {
		que.append(" and a.subheadID in (:subheadIds) ");

	}
//	que.append(" AND b.typeOfUnitId IS NOT NULL  ");
	que.append("GROUP BY a.bankId,e.payTypeName ");
	
	
	Query query = entitymanager.createNativeQuery(que.toString());
	
	if(requestBean.getIsSocityReceipt()!=null) {
		query.setParameter("isSocityReceipt",requestBean.getIsSocityReceipt());	
	}
	if(requestBean.getStandardId()!=null) {
	query.setParameter("standardIds", requestBean.getStandardId()); // Set your array or collection of standardIds
	}	
	if(requestBean.getTypeOfReceipt()!=null) {
	query.setParameter("typeOfReceipt", requestBean.getTypeOfReceipt());
	}
	if(requestBean.getSubheadId()!=null) {
		query.setParameter("subheadIds", requestBean.getSubheadId());

	}
	
	List<CashBankAmountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CashBankAmountBean.class)).getResultList();
	return list;
}
public List<CashBankAmountBean> getBankDetailsAndTotalAmountDateWise(RequestBean requestBean) {
	
	StringBuilder que=new StringBuilder();

	que.append("SELECT (sum(a.payFee)-SUM(a.discount)) AS paidFee,a.payType AS payType,e.payTypeName AS payTypeName,");
	que.append("a.bankId AS bankId,f.bankName AS bankName,f.bankAccNo AS bankAccNo,f.ifscNo AS ifscNo,cast(f.accBankLegderId as CHAR) AS accBankLegderId,f.key AS akey,f.salt AS asalt,a.paidfeeDate as paidfeeDate ");
	que.append("FROM fee_paidfeetable a  ");
	que.append("LEFT JOIN fee_receipt b ON a.receiptID=b.receiptID ");
	que.append("LEFT JOIN fee_studentfee c ON a.studFeeID=c.studFeeID ");
	que.append("LEFT JOIN fee_setting d ON c.feeSettingID=d.feeSettingID AND b.yearId=d.yearID   ");
	que.append("LEFT JOIN fee_pay_type e ON e.payTypeID=a.payType ");
	que.append("LEFT JOIN challanbankdetails f ON a.bankId=f.bankId ");
	que.append("WHERE a.schoolID="+requestBean.getSchoolid()+"   "); //and b.typeOfReceipt in (:typeOfReceipt)
	que.append(" and b.isDel=0 and a.isDel=0 ");
	que.append(" AND a.paidfeeDate BETWEEN '"+requestBean.getFromDate()+"' AND '"+requestBean.getToDate()+"' ");

	//que.append("AND (str_to_date(a.paidfeeDate,'%d-%m-%Y') >= STR_TO_DATE('"+requestBean.getFromDate()+"','%d-%m-%Y') ");  
	//que.append("AND str_to_date(a.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+requestBean.getToDate()+"','%d-%m-%Y')) ");		 

	if(requestBean.getIsSocityReceipt()!=null) {
		que.append(" AND b.isSocietyReceipt=:isSocityReceipt ");//"+requestBean.getIsSocityReceipt()+"	
	}
	
	if(requestBean.getTypeOfReceipt()!=null) {
	que.append("AND b.typeOfReceipt in (:typeOfReceipt) ");
	}
	if(requestBean.getStandardId()!=null) {
	que.append("AND d.standardID in (:standardIds) ");
	}
	if(requestBean.getSubheadId()!=null) {
		que.append(" and a.subheadID in (:subheadIds) ");

	}
//	que.append(" AND b.typeOfUnitId IS NOT NULL  ");
	que.append("GROUP BY a.bankId,e.payTypeName,str_to_date(a.paidfeeDate,'%d-%m-%Y') ");
	
	
	Query query = entitymanager.createNativeQuery(que.toString());
	
	if(requestBean.getIsSocityReceipt()!=null) {
		query.setParameter("isSocityReceipt",requestBean.getIsSocityReceipt());	
	}
	if(requestBean.getStandardId()!=null) {
	query.setParameter("standardIds", requestBean.getStandardId()); // Set your array or collection of standardIds
	}	
	if(requestBean.getTypeOfReceipt()!=null) {
	query.setParameter("typeOfReceipt", requestBean.getTypeOfReceipt());
	}
	if(requestBean.getSubheadId()!=null) {
		query.setParameter("subheadIds", requestBean.getSubheadId());

	}
	
	List<CashBankAmountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CashBankAmountBean.class)).getResultList();
	return list;
}
	
public List<CashBankAmountBean> getSubHeadDetails(RequestBean requestBean) {
	
	StringBuilder que=new StringBuilder();

	que.append("SELECT cast(a.subheadID as CHAR) as subheadID ,(sum(a.payFee)-sum(a.discount)) as paidFee,cast(a.headID as CHAR) as headID,h.headName as headName,cast(h.isSocietyFlag as CHAR) as isSocietyFlag,cast(h.isOtherFee as CHAR) as isOtherFee ");
	que.append(",i.subheadName as subheadName,cast(i.sinkingFlag as CHAR) as sinkingFlag,cast(i.isScholarshipFlag as CHAR) as isScholarshipFlag ");
	que.append("FROM fee_paidfeetable a  ");
	que.append("LEFT JOIN fee_receipt b ON a.receiptID=b.receiptID ");
	que.append("LEFT JOIN fee_studentfee c ON a.studFeeID=c.studFeeID ");
	que.append("LEFT JOIN fee_setting d ON c.feeSettingID=d.feeSettingID AND b.yearId=d.yearID   ");
	que.append("LEFT JOIN fee_pay_type e ON e.payTypeID=a.payType ");
	que.append("LEFT JOIN challanbankdetails f ON a.bankId=f.bankId ");
	que.append("LEFT JOIN fee_head_master h ON h.headID=a.headID ");        
	que.append("LEFT JOIN fee_subhead_master i ON i.subheadID=a.subheadID ");		

	que.append("WHERE a.schoolID="+requestBean.getSchoolid()+"      "); //and b.typeOfReceipt in (:typeOfReceipt)//and b.isSocietyReceipt="+requestBean.getIsSocityReceipt()+"
	que.append("  and b.isDel=0 and a.isDel=0 ");
	que.append(" AND a.paidfeeDate BETWEEN '"+requestBean.getFromDate()+"' AND '"+requestBean.getToDate()+"' ");

//	que.append("AND (str_to_date(a.paidfeeDate,'%d-%m-%Y') >= STR_TO_DATE('"+requestBean.getFromDate()+"','%d-%m-%Y') ");  
//	que.append("AND str_to_date(a.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+requestBean.getToDate()+"','%d-%m-%Y')) ");		 

	if(requestBean.getIsSocityReceipt()!=null) {
		que.append(" AND b.isSocietyReceipt=:isSocityReceipt ");//"+requestBean.getIsSocityReceipt()+"	
	}
	
	if(requestBean.getTypeOfReceipt()!=null) {
		que.append("AND b.typeOfReceipt in (:typeOfReceipt) ");
		}
		
	if(requestBean.getStandardId()!=null) {
		que.append("AND d.standardID in (:standardIds) ");
		}
		
	if(requestBean.getSubheadId()!=null) {
		que.append(" AND a.subheadID in (:subheadIds) ");

	}
	//que.append(" AND b.typeOfUnitId IS NOT NULL  ");
	que.append("GROUP BY a.subheadID ");
	
	
	Query query = entitymanager.createNativeQuery(que.toString());
	
//	query.setParameter("standardIds", requestBean.getStandardId()); // Set your array or collection of standardIds
//	query.setParameter("typeOfReceipt", requestBean.getTypeOfReceipt());
	
	if(requestBean.getIsSocityReceipt()!=null) {
		query.setParameter("isSocityReceipt",requestBean.getIsSocityReceipt());	
	}
	if(requestBean.getStandardId()!=null) {
		query.setParameter("standardIds", requestBean.getStandardId()); // Set your array or collection of standardIds
		}	
		if(requestBean.getTypeOfReceipt()!=null) {
		query.setParameter("typeOfReceipt", requestBean.getTypeOfReceipt());
		}
	if(requestBean.getSubheadId()!=null) {
		query.setParameter("subheadIds", requestBean.getSubheadId());

	}
	
	List<CashBankAmountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CashBankAmountBean.class)).getResultList();
	return list;
}

public List<CashBankAmountBean> getSubHeadDetailsDateWise(RequestBean requestBean) {
	
	StringBuilder que=new StringBuilder();

	que.append("SELECT cast(a.subheadID as CHAR) as subheadID ,(sum(a.payFee)-sum(a.discount)) as paidFee,cast(a.headID as CHAR) as headID,h.headName as headName,cast(h.isSocietyFlag as CHAR) as isSocietyFlag,cast(h.isOtherFee as CHAR) as isOtherFee ");
	que.append(",i.subheadName as subheadName,cast(i.sinkingFlag as CHAR) as sinkingFlag,cast(i.isScholarshipFlag as CHAR) as isScholarshipFlag,a.paidfeeDate as paidfeeDate ");
	que.append("FROM fee_paidfeetable a  ");
	que.append("LEFT JOIN fee_receipt b ON a.receiptID=b.receiptID ");
	que.append("LEFT JOIN fee_studentfee c ON a.studFeeID=c.studFeeID ");
	que.append("LEFT JOIN fee_setting d ON c.feeSettingID=d.feeSettingID AND b.yearId=d.yearID   ");
	que.append("LEFT JOIN fee_pay_type e ON e.payTypeID=a.payType ");
	que.append("LEFT JOIN challanbankdetails f ON a.bankId=f.bankId ");
	que.append("LEFT JOIN fee_head_master h ON h.headID=a.headID ");        
	que.append("LEFT JOIN fee_subhead_master i ON i.subheadID=a.subheadID ");		

	que.append("WHERE a.schoolID="+requestBean.getSchoolid()+"      "); //and b.typeOfReceipt in (:typeOfReceipt)//and b.isSocietyReceipt="+requestBean.getIsSocityReceipt()+"
	que.append("  and b.isDel=0 and a.isDel=0 ");
	que.append(" AND a.paidfeeDate BETWEEN '"+requestBean.getFromDate()+"' AND '"+requestBean.getToDate()+"' ");

	//	que.append("AND (str_to_date(a.paidfeeDate,'%d-%m-%Y') >= STR_TO_DATE('"+requestBean.getFromDate()+"','%d-%m-%Y') ");  
//	que.append("AND str_to_date(a.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+requestBean.getToDate()+"','%d-%m-%Y')) ");		 

	if(requestBean.getIsSocityReceipt()!=null) {
		que.append(" AND b.isSocietyReceipt=:isSocityReceipt ");//"+requestBean.getIsSocityReceipt()+"	
	}
	
	if(requestBean.getTypeOfReceipt()!=null) {
		que.append("AND b.typeOfReceipt in (:typeOfReceipt) ");
		}
		
	if(requestBean.getStandardId()!=null) {
		que.append("AND d.standardID in (:standardIds) ");
		}
		
	if(requestBean.getSubheadId()!=null) {
		que.append(" AND a.subheadID in (:subheadIds) ");

	}
	//que.append(" AND b.typeOfUnitId IS NOT NULL  ");
	que.append("GROUP BY a.subheadID,str_to_date(a.paidfeeDate,'%d-%m-%Y') ");
	
	
	Query query = entitymanager.createNativeQuery(que.toString());
	
//	query.setParameter("standardIds", requestBean.getStandardId()); // Set your array or collection of standardIds
//	query.setParameter("typeOfReceipt", requestBean.getTypeOfReceipt());
	
	if(requestBean.getIsSocityReceipt()!=null) {
		query.setParameter("isSocityReceipt",requestBean.getIsSocityReceipt());	
	}
	if(requestBean.getStandardId()!=null) {
		query.setParameter("standardIds", requestBean.getStandardId()); // Set your array or collection of standardIds
		}	
		if(requestBean.getTypeOfReceipt()!=null) {
		query.setParameter("typeOfReceipt", requestBean.getTypeOfReceipt());
		}
	if(requestBean.getSubheadId()!=null) {
		query.setParameter("subheadIds", requestBean.getSubheadId());

	}
	
	List<CashBankAmountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CashBankAmountBean.class)).getResultList();
	return list;
}

public List<FeeReceiptBean> getReceiptDetails(Integer schoolid) {
	
	StringBuilder que=new StringBuilder();

		que.append("SELECT a.receiptLogId as receiptLogId ,a.receiptOldDate as receiptOldDate ,a.receiptNewDate as receiptNewDate,a.receiptId as receiptId,cast(b.receiptNo as CHAR) as receiptNo, ");
		que.append("cast(a.userRemark as CHAR) as userRemark,cast(a.approvalStatus as CHAR) as approvalStatus,a.cDate as cDate,a.oldAmount as oldAmount,a.newAmmount as newAmmount,cast(a.subheadRemark as CHAR) as subheadRemark,  ");
		que.append("b.studentId as studentId,b.yearId as yearId,cast(c.studInitialName as CHAR) as studInitialName,cast(c.studFName as CHAR) as studFName,cast(c.studMName as CHAR) as studMName,cast(c.studLName as CHAR) as studLName        ");
		que.append(",d.standardId as standardId,cast(e.standardName as CHAR) as standardName,cast(f.year as CHAR) as year                                                     ");
		que.append("FROM fee_receipt_edit_log a                                                      ");
		que.append("LEFT JOIN fee_receipt b ON a.receiptId=b.receiptID                               ");
		que.append("LEFT JOIN student_master c ON c.stud_id=b.studentId                              ");
		que.append("LEFT JOIN student_standard_renew d ON d.studentId=c.stud_id AND d.yearId=b.yearId ");
		que.append("LEFT JOIN standardmaster e ON e.standardId=d.standardId                          ");
		que.append("LEFT JOIN yearmaster f ON f.yearId=b.yearId ");
		que.append(" where a.schoolid=:schoolid ");
		Query query = entitymanager.createNativeQuery(que.toString());


		query.setParameter("schoolid", schoolid);
		
		List<FeeReceiptBean> list = query.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.aliasToBean(FeeReceiptBean.class)).getResultList();
		return list;
}

public List<CountBean> getAgeStandardWise(Integer schoolId, Integer yearId, String date) {
	
	StringBuilder que=new StringBuilder();
                                                                                                                         
//	que.append("SELECT  ");
//	que.append("ifnull(a.stud_gender,'NA') AS studGender,b.standardId as standardId,c.standardName as standardName, ");
//	que.append("count(a.stud_id) as studentCount,");
//	que.append("cast(IFNULL(FLOOR(DATEDIFF(STR_TO_DATE('"+date+"', '%d-%m-%Y'),STR_TO_DATE(a.birth_date, '%d-%m-%Y') ) / 365),'NA') as CHAR) AS age_in_years  ");
//	que.append("FROM student_master a ");
//	que.append("LEFT JOIN student_standard_renew b ON a.stud_id=b.studentId ");
//	que.append("LEFT JOIN standardmaster c ON c.standardId=b.standardId ");
//	que.append("WHERE b.schoolid="+schoolId+" AND b.yearId="+yearId+" AND b.isDel=0 ");
//	que.append("GROUP BY age_in_years,b.standardId,studGender ORDER BY b.standardId,age_in_years ASC ");
//	
	
	que.append(" SELECT ifnull(a.stud_gender,'NA') AS studGender,b.standardId as standardId,c.standardName as standardName,");
	que.append(" count(a.stud_id) as studentCount, ");
	que.append(" cast(IFNULL(FLOOR(DATEDIFF(STR_TO_DATE('"+date+"', '%d-%m-%Y'),STR_TO_DATE(a.birth_date, '%d-%m-%Y') ) / 365),'NA') as CHAR) AS age_in_years  ");
	que.append("  FROM student_standard_renew b ");
	que.append("  LEFT JOIN student_master a ON a.stud_id=b.studentId ");
	que.append(" INNER JOIN  ");
	que.append(" (SELECT b.studentId AS studentId,b.renewstudentId FROM student_standard_renew b "); 
	que.append(" LEFT JOIN student_master a ON a.stud_id=b.studentId ");
	que.append(" WHERE  a.isAdmissionCancelled=1 ");
	que.append(" AND ((b.isOnlineAdmission=1 and b.isApproval=1) or b.isOnlineAdmission=0 ) ");
	que.append(" AND   STR_TO_DATE(a.admission_date, '%d-%m-%Y')<= STR_TO_DATE('"+date+"', '%d-%m-%Y') ");
	que.append(" AND (b.college_leavingdate IS NULL OR  STR_TO_DATE(b.college_leavingdate, '%d-%m-%Y')>= STR_TO_DATE('"+date+"', '%d-%m-%Y')) ");
	que.append(" AND b.yearId="+yearId+" AND b.schoolid="+schoolId+" AND a.isDel=0  AND b.isDel=0  GROUP BY b.studentId) as k ");
	que.append(" ON k.studentId=b.studentId AND b.renewstudentId=k.renewstudentId ");
	que.append(" LEFT JOIN standardmaster c ON c.standardId=b.standardId ");
	que.append(" WHERE b.schoolid="+schoolId+" AND b.yearId="+yearId+" AND b.isDel=0 GROUP BY age_in_years,b.standardId,studGender ");

	Query query = entitymanager.createNativeQuery(que.toString());

	List<CountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CountBean.class)).getResultList();
	return list;

}

public List<CountBean> getAgeDivisionWise(Integer schoolId, Integer yearId, String date) {
	
	StringBuilder que=new StringBuilder();
                                                                                                                         
//	que.append("SELECT  ");
//	que.append("ifnull(a.stud_gender,'NA') AS studGender,b.standardId as standardId,c.standardName as standardName, ");
//	que.append("count(a.stud_id) as studentCount,cast(ifnull(b.divisionId,'NA') as CHAR) as divisionId,d.divisionName, ");
//	que.append("cast(IFNULL(FLOOR(DATEDIFF(STR_TO_DATE('"+date+"', '%d-%m-%Y'),STR_TO_DATE(a.birth_date, '%d-%m-%Y') ) / 365),'NA') as CHAR) AS age_in_years  ");
//	que.append("FROM student_master a ");
//	que.append("LEFT JOIN student_standard_renew b ON a.stud_id=b.studentId ");
//	que.append("LEFT JOIN standardmaster c ON c.standardId=b.standardId ");
//	que.append("LEFT JOIN division d ON d.divisionId=b.divisionId ");
//	que.append("WHERE b.schoolid="+schoolId+" AND b.yearId="+yearId+" AND b.isDel=0 ");
//	que.append("GROUP BY age_in_years,b.standardId,b.divisionId,studGender ORDER BY b.standardId,age_in_years ASC ");
	
	
	
	
	
	que.append(" SELECT ifnull(a.stud_gender,'NA') AS studGender,b.standardId as standardId,c.standardName as standardName,");
	que.append(" count(a.stud_id) as studentCount,cast(ifnull(b.divisionId,'NA') as CHAR) as divisionId,d.divisionName, ");
	que.append(" cast(IFNULL(FLOOR(DATEDIFF(STR_TO_DATE('"+date+"', '%d-%m-%Y'),STR_TO_DATE(a.birth_date, '%d-%m-%Y') ) / 365),'NA') as CHAR) AS age_in_years  ");
	que.append("  FROM student_standard_renew b ");
	que.append("  LEFT JOIN student_master a ON a.stud_id=b.studentId ");
	que.append(" INNER JOIN  ");
	que.append(" (SELECT b.studentId AS studentId,b.renewstudentId FROM student_standard_renew b "); 
	que.append(" LEFT JOIN student_master a ON a.stud_id=b.studentId ");
	que.append(" WHERE  a.isAdmissionCancelled=1 ");
	que.append(" AND ((b.isOnlineAdmission=1 and b.isApproval=1) or b.isOnlineAdmission=0 ) ");
	que.append(" AND   STR_TO_DATE(a.admission_date, '%d-%m-%Y')<= STR_TO_DATE('"+date+"', '%d-%m-%Y') ");
	que.append(" AND (b.college_leavingdate IS NULL OR  STR_TO_DATE(b.college_leavingdate, '%d-%m-%Y')>= STR_TO_DATE('"+date+"', '%d-%m-%Y')) ");
	que.append(" AND b.yearId="+yearId+" AND b.schoolid="+schoolId+" AND a.isDel=0  AND b.isDel=0  GROUP BY b.studentId) as k ");
	que.append(" ON k.studentId=b.studentId AND b.renewstudentId=k.renewstudentId ");
	que.append(" LEFT JOIN standardmaster c ON c.standardId=b.standardId ");
	que.append("LEFT JOIN division d ON d.divisionId=b.divisionId ");
	que.append(" WHERE b.schoolid="+schoolId+" AND b.yearId="+yearId+" AND b.isDel=0 GROUP BY age_in_years,b.standardId,b.divisionId,studGender ");

	
	
	
	Query query = entitymanager.createNativeQuery(que.toString());

	List<CountBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(CountBean.class)).getResultList();
	return list;

}


public List<StudentDifferenceBean> getStudDiff(Integer schoolId, Integer yearId,Integer standardId,Integer divisionId, String date) {
	
	StringBuilder que=new StringBuilder();
                                                                                                                         

	
	que.append("  SELECT b.studentId,k.renewstudentId,Student_RegNo,studFName,studLName ");
	que.append("   ,e.monthId,e.rollNo,e.stdId,c.standardName,e.divId,d.divisionName,e.yearId ");
	que.append("  FROM student_standard_renew b ");
	que.append("  INNER JOIN ");
	que.append("  (SELECT b.studentId AS studentId,a.Student_RegNo AS Student_RegNo ,b.renewstudentId AS renewstudentId ,a.studFName AS studFName,a.studLName AS studLName  FROM student_standard_renew b "); 
	que.append("  LEFT JOIN student_master a ON a.stud_id=b.studentId ");
	que.append("  WHERE  a.isAdmissionCancelled=1 ");
	que.append("  AND ((b.isOnlineAdmission=1 and b.isApproval=1) or b.isOnlineAdmission=0 ) ");
	que.append("  AND   STR_TO_DATE(a.admission_date, '%d-%m-%Y')<= STR_TO_DATE('"+date+"', '%d-%m-%Y') ");
	que.append("  AND (b.college_leavingdate IS NULL OR  STR_TO_DATE(b.college_leavingdate, '%d-%m-%Y')>= STR_TO_DATE('"+date+"', '%d-%m-%Y')) ");
	que.append("  AND b.yearId="+yearId+" AND b.schoolid="+schoolId+" AND a.isDel=0  AND b.isDel=0  GROUP BY b.studentId) as k ");
	que.append("  ON k.studentId=b.studentId AND b.renewstudentId=k.renewstudentId ");
	que.append("   LEFT JOIN attendance_classwise_catalog e ON e.renewstudentId=k.renewstudentId "); 
	que.append("  LEFT JOIN standardmaster c ON e.stdId=c.standardId ");
	que.append("  LEFT JOIN division d ON e.divId=d.divisionId ");
	que.append("  WHERE b.schoolid="+schoolId+" AND b.yearId="+yearId+" AND b.standardId="+standardId+" AND b.divisionId="+divisionId+" AND b.isDel=0 ");
	que.append("  GROUP BY Student_RegNo,e.monthId ");
	Query query = entitymanager.createNativeQuery(que.toString());

	List<StudentDifferenceBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(StudentDifferenceBean.class)).getResultList();
	return list;

}

public List<StudentDifferenceBean> getStudentsPresentInAttendanceCatalogButNotInStudentRenew(Integer schoolId,
		Integer yearId, String date) {
	StringBuilder que=new StringBuilder();

	que.append(" SELECT a.stud_id as studentId,stud.Student_RegNo,stud.studFName,stud.studLName,a.stdId,st.standardName,a.divId,di.divisionName,a.rollNo FROM attendance_classwise_catalog a "); 
	que.append(" INNER JOIN student_master stud ON stud.stud_id=a.stud_id ");
	que.append(" LEFT JOIN standardmaster st ON st.standardId=a.stdId ");
	que.append(" LEFT JOIN division di ON di.divisionId=a.divId ");
	que.append(" WHERE a.schoolid="+schoolId+" AND a.yearId="+yearId+" "); 
	que.append(" AND a.stud_id NOT IN (SELECT k.studentId FROM student_standard_renew b ");
	que.append(" INNER JOIN  ");
	que.append(" (SELECT b.studentId AS studentId,b.renewstudentId FROM student_standard_renew b "); 
	que.append(" LEFT JOIN student_master a ON a.stud_id=b.studentId ");
	que.append(" WHERE  a.isAdmissionCancelled=1 ");
	que.append(" AND ((b.isOnlineAdmission=1 and b.isApproval=1) or b.isOnlineAdmission=0 ) ");
	que.append(" AND   STR_TO_DATE(a.admission_date, '%d-%m-%Y')<= STR_TO_DATE('"+date+"', '%d-%m-%Y') ");
	que.append(" AND (b.college_leavingdate IS NULL OR  STR_TO_DATE(b.college_leavingdate, '%d-%m-%Y')>= STR_TO_DATE('"+date+"', '%d-%m-%Y')) ");
	que.append(" AND b.yearId="+yearId+" AND b.schoolid="+schoolId+" AND a.isDel=0  AND b.isDel=0  GROUP BY b.studentId) as k ");
	que.append(" ON k.studentId=b.studentId AND b.renewstudentId=k.renewstudentId ");
	que.append(" WHERE b.schoolid="+schoolId+" AND b.yearId="+yearId+" AND b.isDel=0 ) ");
	que.append(" GROUP BY a.stud_id ");
	Query query = entitymanager.createNativeQuery(que.toString());


	List<StudentDifferenceBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(StudentDifferenceBean.class)).getResultList();
	return list;
}

public List<PhoneNumberBean> getStaffContactNo(SubmitApprovalBean studentAnnouncementBean) {
	StringBuilder que=new StringBuilder();
//b.contactnosms,b.staffId,a.role,b.firstname
	que.append("SELECT c.contactnosms as staffContact,c.staaffId as staffId,c.firstname as staffName,a.schoolid as schoolid,b.APPUSERROLEID as staffUserId FROM school_master a "); 
	que.append("left join announcement ann on ann.sansthakey=a.sansthaKey ");
	que.append("LEFT JOIN app_user b ON a.schoolid=b.schoolid ");
	que.append("LEFT JOIN staff_basicdetails c ON b.staffID=c.staaffId ");
	que.append("WHERE a.sansthaKey='"+studentAnnouncementBean.getSansthakey()+"' and ann.announcementId="+studentAnnouncementBean.getAnnouncementId()+" ");
	if(studentAnnouncementBean.getForPrincipal()==1 && studentAnnouncementBean.getForTeacher()==1)
	 que.append(" AND (b.RoleName='ROLE_PRINCIPAL' or b.RoleName='ROLE_TEACHINGSTAFF')  "); 
	 
	if(studentAnnouncementBean.getForPrincipal()==1 && studentAnnouncementBean.getForTeacher()==0)
	 que.append(" AND b.RoleName='ROLE_PRINCIPAL' ");
	if(studentAnnouncementBean.getForPrincipal()==0 && studentAnnouncementBean.getForTeacher()==1)
		 que.append(" AND b.RoleName='ROLE_TEACHINGSTAFF'  "); 

	
		Query query = entitymanager.createNativeQuery(que.toString());


	List<PhoneNumberBean> list = query.unwrap(org.hibernate.query.Query.class)
			.setResultTransformer(Transformers.aliasToBean(PhoneNumberBean.class)).getResultList();
	return list;
}



}
