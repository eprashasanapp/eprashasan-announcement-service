package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.GroupBean;
import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.model.AnnouncementGroupModel;
import com.ingenio.announcement.model.AnnouncementUserToGroupMappingModel;

public interface AnnouncementUserToGroupMappingRepository extends JpaRepository<AnnouncementUserToGroupMappingModel, Integer> {

	
	@Query(value="SELECT a.announcementGroupModel.announcementGroupId from AnnouncementUserToGroupMappingModel a where a.appUserModel.appUserRoleId=:appuserId ")
	List<Integer> getGroupList(Integer appuserId);

	@Query("select new com.ingenio.announcement.bean.GroupBean(g.userToGroupId,g.announcementGroupModel.announcementGroupId,g.announcementGroupModel.announcementGroupName ) "
			+ "from  AnnouncementUserToGroupMappingModel g where g.appUserModel.appUserRoleId=:appUserId ")
	List<GroupBean> getGroups(Integer appUserId);

//	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,case when a.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') THEN 'Staff' WHEN a.roleName = 'ROLE_PARENT1' THEN 'Student' END,c.mmobile,concat(COALESCE(c.studInitialName,''),' ',COALESCE(c.studFName,''),' ',COALESCE(c.studMName,''),' ',COALESCE(c.studLName,'')),concat(COALESCE(b.initialname,''),' ',COALESCE(b.firstname,''),' ',COALESCE(b.secondname,''),' ',COALESCE(b.lastname,'')),a.appUserRoleId "
//			+ ",b.femailId,c.memail,fcm.token,c.studentRegNo,std.standardId,std.standardName,div.divisionId,div.divisionName,y.announcementGroupModel.announcementGroupId,y.announcementGroupModel.announcementGroupName,(CASE WHEN s.viewsId IS NOT NULL THEN 1 ELSE 0 END), COALESCE(t.likeDislike,''),(CASE WHEN count(u.comment) > 0 THEN 1 ELSE 0 END) , "
//			+ "(CASE WHEN COUNT(v.reactionId) > 0 THEN 1 ELSE 0 END) ) from AnnouncmenetForGroupModel y "
//			+ "left join AnnouncementUserToGroupMappingModel z on z.announcementGroupModel.announcementGroupId=y.announcementGroupModel.announcementGroupId  "
//			+ "left join AppUserModel a on z.appUserModel.appUserRoleId=a.appUserRoleId  "
//			+ "left join StaffBasicDetailsModel b on b.staffId=a.staffId and a.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') "
//			+ "left join StudentStandardRenewModel r on r.renewStudentId=a.staffId and a.roleName = 'ROLE_PARENT1' "
//			+ "left join StudentMasterModel c on c.studentId=r.studentMasterModel.studentId and a.roleName = 'ROLE_PARENT1' "
//			+ "left join AndroidFCMTokenMaster fcm on fcm.notificationUserId=a.appUserRoleId "
//			+ "left join StandardMasterModel std on std.standardId=r.standardMasterModel.standardId "
//			+ "left join DivisionMasterModel div on div.divisionId=r.divisionMasterModel.divisionId  "
//			+ "left join AnnouncementViewsModel s on s.announcementModel.announcementId=:announcementId and s.appUserModel.appUserRoleId=a.appUserRoleId "
//			+ "left join AnnouncementLikeorDislikeModel t on t.announcementModel.announcementId=:announcementId and t.appUserModel.appUserRoleId=a.appUserRoleId "
//			+ "left join AnnouncementCommentModel u on u.announcementModel.announcementId=:announcementId and u.appUserModel.appUserRoleId=a.appUserRoleId  "
//			+ "left join AnnouncementReactionModel v on v.announcementModel.announcementId=:announcementId and v.appUserModel.appUserRoleId=a.appUserRoleId "
//			+ "where  y.announcementModel.announcementId=:announcementId  ")
//		List<PhoneNumberBean> getUsersAssignToGroup(Integer announcementId);

	//	@Query("SELECT NEW com.ingenio.announcement.bean.PhoneNumberBean("
//	        + "b.contactnosms, b.staffId, c.studentId, "
//	        + "CASE WHEN a.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') THEN 'Staff' "
//	        + "WHEN a.roleName = 'ROLE_PARENT1' THEN 'Student' END, "
//	        + "c.mmobile, "
//	        + "CONCAT(COALESCE(c.studInitialName,''), ' ', COALESCE(c.studFName,''), ' ', "
//	        + "COALESCE(c.studMName,''), ' ', COALESCE(c.studLName,'')), "
//	        + "CONCAT(COALESCE(b.initialname,''), ' ', COALESCE(b.firstname,''), ' ', "
//	        + "COALESCE(b.secondname,''), ' ', COALESCE(b.lastname,'')), "
//	        + "a.appUserRoleId, b.femailId, c.memail, fcm.token, c.studentRegNo, "
//	        + "std.standardId, std.standardName, div.divisionId, div.divisionName, "
//	        + "y.announcementGroupModel.announcementGroupId, y.announcementGroupModel.announcementGroupName, "
//	        + "(CASE WHEN s.viewsId IS NOT NULL THEN 1 ELSE 0 END), "
//	        + "t.likeDislike, "
//	        + "(CASE WHEN count(u.comment) > 0 THEN 1 ELSE 0 END), "
//	        + "(CASE WHEN COUNT(v.reactionId) > 0 THEN 1 ELSE 0 END)"
//	        + ") "
//	        + "FROM com.ingenio.announcement.model.AnnouncmenetForGroupModel y "
//	        + "LEFT JOIN com.ingenio.announcement.model.AnnouncementUserToGroupMappingModel z ON "
//	        + "z.announcementGroupModel.announcementGroupId = y.announcementGroupModel.announcementGroupId "
//	        + "LEFT JOIN com.ingenio.announcement.model.AppUserModel a ON "
//	        + "z.appUserModel.appUserRoleId = a.appUserRoleId "
//	        + "LEFT JOIN com.ingenio.announcement.model.StaffBasicDetailsModel b ON "
//	        + "b.staffId = a.staffId AND a.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') "
//	        + "LEFT JOIN com.ingenio.announcement.model.StudentStandardRenewModel r ON "
//	        + "r.renewStudentId = a.staffId AND a.roleName = 'ROLE_PARENT1' "
//	        + "LEFT JOIN com.ingenio.announcement.model.StudentMasterModel c ON "
//	        + "c.studentId = r.studentMasterModel.studentId AND a.roleName = 'ROLE_PARENT1' "
//	        + "LEFT JOIN com.ingenio.announcement.model.AndroidFCMTokenMaster fcm ON "
//	        + "fcm.notificationUserId = a.appUserRoleId "
//	        + "LEFT JOIN com.ingenio.announcement.model.StandardMasterModel std ON "
//	        + "std.standardId = r.standardMasterModel.standardId "
//	        + "LEFT JOIN com.ingenio.announcement.model.DivisionMasterModel div ON "
//	        + "div.divisionId = r.divisionMasterModel.divisionId "
//	        + "LEFT JOIN com.ingenio.announcement.model.AnnouncementViewsModel s ON "
//	        + "s.announcementModel.announcementId = :announcementId AND s.appUserModel.appUserRoleId = a.appUserRoleId "
//	        + "LEFT JOIN com.ingenio.announcement.model.AnnouncementLikeorDislikeModel t ON "
//	        + "t.announcementModel.announcementId = :announcementId AND t.appUserModel.appUserRoleId = a.appUserRoleId "
//	        + "LEFT JOIN com.ingenio.announcement.model.AnnouncementCommentModel u ON "
//	        + "u.announcementModel.announcementId = :announcementId AND u.appUserModel.appUserRoleId = a.appUserRoleId "
//	        + "LEFT JOIN com.ingenio.announcement.model.AnnouncementReactionModel v ON "
//	        + "v.announcementModel.announcementId = :announcementId AND v.appUserModel.appUserRoleId = a.appUserRoleId "
//	        + "WHERE y.announcementModel.announcementId = :announcementId")
	//List<PhoneNumberBean> getUsersAssignToGroup( Integer announcementId);
	
//	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,case when st.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') THEN 'Staff' WHEN st.roleName = 'ROLE_PARENT1' THEN 'Student' END,c.mmobile,c.studFName,b.firstname,st.appUserRoleId as staffUserId,st.appUserRoleId as studUserId) from AnnouncmenetForGroupModel a "
//			+ "left join AnnouncementUserToGroupMappingModel z on a.announcementGroupModel.announcementGroupId=z.announcementGroupModel.announcementGroupId "
//			+ "left join AppUserModel st on st.appUserRoleId=z.appUserModel.appUserRoleId  "
//			//+ "left join AppUserModel stud on stud.appUserRoleId=z.appUserModel.appUserRoleId and stud.roleName='ROLE_PARENT1' "
//			+ "left join StudentMasterModel c on c.studentId=st.staffId  and st.roleName='ROLE_PARENT1' "
//			+ "left join StaffBasicDetailsModel b on b.staffId=st.staffId and (st.roleName='ROLE_TEACHINGSTAFF' or st.roleName='ROLE_PRINCIPAL') "
//			+ "where a.announcementModel.announcementId=:announcementId ")
//	List<PhoneNumberBean> getContactNumbersFromGroups(Integer announcementId);
//	
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,case when a.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') THEN 'Staff' WHEN a.roleName = 'ROLE_PARENT1' THEN 'Student' END,c.mmobile,concat(COALESCE(c.studInitialName,''),' ',COALESCE(c.studFName,''),' ',COALESCE(c.studMName,''),' ',COALESCE(c.studLName,'')),concat(COALESCE(b.initialname,''),' ',COALESCE(b.firstname,''),' ',COALESCE(b.secondname,''),' ',COALESCE(b.lastname,'')),a.appUserRoleId "
			+ ",b.femailId,c.memail,fcm.token,c.studentRegNo,std.standardId,std.standardName,div.divisionId,div.divisionName,y.announcementGroupModel.announcementGroupId,y.announcementGroupModel.announcementGroupName,"
			+ "coalesce((select CASE WHEN s.viewsId IS NOT NULL THEN 1 ELSE 0 END from AnnouncementViewsModel s where s.announcementModel.announcementId=:announcementId and s.appUserModel.appUserRoleId=a.appUserRoleId),'') "
			+ ",COALESCE(t.likeDislike,''),( select CASE WHEN count(u.comment) > 0 THEN 1 ELSE 0 END from AnnouncementCommentModel u where u.announcementModel.announcementId=:announcementId and u.appUserModel.appUserRoleId=a.appUserRoleId ) , "
			+ "( select CASE WHEN COUNT(v.reactionId) > 0 THEN 1 ELSE 0 END from AnnouncementReactionModel v where v.announcementModel.announcementId=:announcementId and v.appUserModel.appUserRoleId=a.appUserRoleId) ) from AnnouncmenetForGroupModel y "
			+ "left join AnnouncementUserToGroupMappingModel z on z.announcementGroupModel.announcementGroupId=y.announcementGroupModel.announcementGroupId  "
			+ "left join AppUserModel a on z.appUserModel.appUserRoleId=a.appUserRoleId  "
			+ "left join StaffBasicDetailsModel b on b.staffId=a.staffId and a.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') "
			+ "left join StudentStandardRenewModel r on r.renewStudentId=a.staffId and a.roleName = 'ROLE_PARENT1' "
			+ "left join StudentMasterModel c on c.studentId=r.studentMasterModel.studentId and a.roleName = 'ROLE_PARENT1' "
			+ "left join AndroidFCMTokenMaster fcm on fcm.notificationUserId=a.appUserRoleId "
			+ "left join StandardMasterModel std on std.standardId=r.standardMasterModel.standardId "
			+ "left join DivisionMasterModel div on div.divisionId=r.divisionMasterModel.divisionId  "
			//+ "left join AnnouncementViewsModel s on s.announcementModel.announcementId=:announcementId and s.appUserModel.appUserRoleId=a.appUserRoleId "
			+ "left join AnnouncementLikeorDislikeModel t on t.announcementModel.announcementId=:announcementId and t.appUserModel.appUserRoleId=a.appUserRoleId "
			//+ "left join AnnouncementCommentModel u on u.announcementModel.announcementId=:announcementId and u.appUserModel.appUserRoleId=a.appUserRoleId  "
			//+ "left join AnnouncementReactionModel v on v.announcementModel.announcementId=:announcementId and v.appUserModel.appUserRoleId=a.appUserRoleId "
			+ "where  y.announcementModel.announcementId=:announcementId  ")
		List<PhoneNumberBean> getUsersAssignToGroup(Integer announcementId);

}
