package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.EventResponseBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.EventModel;
import com.ingenio.announcement.model.PersonalNoteModel;

public interface PersonalNoteRepository extends JpaRepository<PersonalNoteModel, Integer>{
	
	
	

}
