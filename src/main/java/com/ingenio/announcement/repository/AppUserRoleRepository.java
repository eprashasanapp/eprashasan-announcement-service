package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AppUserRoleModel;

public interface AppUserRoleRepository extends JpaRepository<AppUserRoleModel, Integer>{

	@Query("select a.appUserRoleId from AppUserModel a where a.staffId in :studentId ")
	List<Integer> getAppUserId(List<Integer> studentId);

}
