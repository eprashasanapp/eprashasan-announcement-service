package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.IcardMaster;

public interface IcardMasterRepository extends JpaRepository<IcardMaster, Integer>{

	@Query("select a.imagePath from IcardMaster a where a.studId=:staffId and (a.renewId=0 or a.renewId is null) group by a.studId order by icardId desc ")
	String getImageForStaff(Integer staffId);

	@Query("select a.imagePath from IcardMaster a where a.studId=:staffId and a.renewId>0 group by a.studId order by icardId desc ")
	String getImageForStudent(Integer staffId);

}
