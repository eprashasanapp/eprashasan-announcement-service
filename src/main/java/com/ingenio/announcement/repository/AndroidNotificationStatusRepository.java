package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.AndroidNotificationStatusModel;



@Repository
public interface AndroidNotificationStatusRepository extends JpaRepository<AndroidNotificationStatusModel, Integer> {

	
	@Query(value="SELECT COALESCE(MAX(a.notificationStatusId),0)+1 from AndroidNotificationStatusModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	

}
