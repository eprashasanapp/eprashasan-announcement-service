package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.AnnouncementViewBean;
import com.ingenio.announcement.model.AnnouncementLikeorDislikeModel;
import com.ingenio.announcement.model.AnnouncementViewsModel;

public interface AnnouncementViewRepository extends JpaRepository<AnnouncementViewsModel, Integer>{

	@Query(value="SELECT coalesce(MAX(a.viewsId),0)+1 from AnnouncementViewsModel a where a.schoolMasterModel.schoolid=:schoolid ")
	String getMaxId(Integer schoolid);


	@Query(value="SELECT a from AnnouncementViewsModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId=:announcementId order by a.viewsId desc ")
	List<AnnouncementViewsModel> findViews(Integer schoolId, Integer announcementId);


	@Query("select count(a.viewsId) from AnnouncementViewsModel a where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId=:announcementId ")
	Integer getViewCount(Integer schoolId, Integer announcementId);

	
	
}
