package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementTypeModel;
import com.ingenio.announcement.model.SchoolMasterModel;

public interface AnnouncementTypeRepository extends JpaRepository<AnnouncementTypeModel, Integer>{


///	List<AnnouncementTypeModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

	@Query("select new com.ingenio.announcement.model.AnnouncementTypeModel(a.announcementTypeId,a.announcementTypeName) from  "
			+ "AnnouncementTypeModel a where a.schoolMasterModel.schoolid=:schoolId")
	List<AnnouncementTypeModel> getType(Integer schoolId);

	
}
