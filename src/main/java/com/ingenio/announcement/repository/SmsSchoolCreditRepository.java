package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.SmsSchoolCreditModel;

public interface SmsSchoolCreditRepository  extends JpaRepository<SmsSchoolCreditModel, Integer>{

	List<SmsSchoolCreditModel> findBySchoolid(Integer schoolId);

	@Query("select sum(a.credit) from SmsSchoolCreditModel a where a.schoolid=:schoolId " )
	Integer totalCredit(Integer schoolId);

}
