package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.SchoolSuspentionDetailsModel;

public interface SchoolSuspentionDetailsRepository extends JpaRepository<SchoolSuspentionDetailsModel, Integer>{

	@Query("select a from SchoolSuspentionDetailsModel a where  a.suspensionStatus=0 and "
			+ "a.fromDate<=:date and a.toDate>=:date ")
	List<SchoolSuspentionDetailsModel> getDetails(String date);

	
}
