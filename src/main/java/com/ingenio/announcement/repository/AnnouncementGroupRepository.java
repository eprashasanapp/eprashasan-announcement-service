package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementGroupModel;
import com.ingenio.announcement.model.AnnouncementModel;

public interface AnnouncementGroupRepository extends JpaRepository<AnnouncementGroupModel, Integer>{

	@Query(value="SELECT a from AnnouncementGroupModel a where a.schoolMasterModel.schoolid=:schoolId ")
	List<AnnouncementGroupModel> findBySchoolId(Integer schoolId);



	

	
}
