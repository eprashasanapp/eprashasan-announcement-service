package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.SaveSendNotificationModel;


public interface SaveSendNotificationRepository extends JpaRepository<SaveSendNotificationModel, Integer>{

	@Query(value="select a from SaveSendNotificationModel a where a.renewStaffId=:renewId and a.typeFlag=:typeFlag and a.successFailureFlag='1' ")
	List<SaveSendNotificationModel> getAllNotifications(Integer renewId, String typeFlag);

	@Query(value="select a from SaveSendNotificationModel a where a.renewStaffId=:renewId and a.typeFlag=:typeFlag and a.notificationId=:announcementId  ")
	List<SaveSendNotificationModel> getAllNotificationsForReport(Integer renewId, String typeFlag, Integer announcementId);

	@Transactional
	@Modifying
	@Query(value="update SaveSendNotificationModel a set a.seenUnseenStatusFlag='1' where a.tabId IN (:tabId)")
	void updateNotificationStatus(List<Integer> tabId);

	@Query(value="select a from SaveSendNotificationModel a where a.renewStaffId=:renewId and a.typeFlag=:typeFlag and a.seenUnseenStatusFlag=0 and a.successFailureFlag='1' ")
	List<SaveSendNotificationModel> getAllNotificationsUnseenCount(Integer renewId, String typeFlag);

	@Transactional
	@Modifying
	@Query(value="update SaveSendNotificationModel a set a.seenUnseenStatusFlag='1' where a.renewStaffId=:renewId and a.typeFlag=:typeFlag and a.successFailureFlag='1'")
	void updateAllNotificationStatus(Integer renewId, String typeFlag);

	@Query(value="select a from SaveSendNotificationModel a where a.renewStaffId=:appUserId and a.typeFlag=:flag order by tabId desc ")
	Page<SaveSendNotificationModel> getAllNotifications(Integer appUserId, String flag, Pageable  offset);
}

