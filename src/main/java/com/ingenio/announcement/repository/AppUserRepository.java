package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.StaffDetailsBean;
import com.ingenio.announcement.bean.StaffInfoBean;
import com.ingenio.announcement.model.AppUserModel;

public interface AppUserRepository extends JpaRepository<AppUserModel, Integer> {

	@Query(value="select new com.ingenio.announcement.model.AppUserModel(a.appUserRoleId,coalesce(a.firstName,''),coalesce(a.lastName,''),coalesce(a.staffId,''),a.roleName) from AppUserModel a where a.staffId=:renewStudentId and a.schoolid=:schoolid and a.roleName='ROLE_PARENT1' and a.isDel='0'")
	AppUserModel getUserDetails(Integer renewStudentId, Integer schoolid);
	
	@Query(value="select new com.ingenio.announcement.model.AppUserModel(a.appUserRoleId,coalesce(a.firstName,''),coalesce(a.lastName,''),coalesce(a.staffId,''),a.roleName) from AppUserModel a where a.appUserRoleId=:userId and a.schoolid=:schoolid and a.isDel='0'")
	AppUserModel getUserDetailsWithUserId(Integer userId, Integer schoolid);

	@Query("select new com.ingenio.announcement.bean.StaffInfoBean(b.contactno,b.femailId) from AppUserModel a "
			+ "left join StaffBasicDetailsModel b on a.staffId=b.staffId "
			+ "where a.roleName='ROLE_PRINCIPAL'  and a.schoolid=:schoolid ")
	List<StaffInfoBean> getRolePrincipalDetails(Integer schoolid);

	
//	@Query(value="select a from AppUserModel a where a.staffId=:renewStudentId and a.schoolMasterModel.schoolid=:schoolid and a.roleName='ROLE_PARENT1' and a.isDel='0'")
//	AppUserModel getUserDetails(Integer renewStudentId, Integer schoolid);
	
}
