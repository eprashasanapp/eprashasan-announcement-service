package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.CountBean;
import com.ingenio.announcement.model.CategoryMasterModel;
import com.ingenio.announcement.model.StandardMasterModel;

public interface StandardMasterRepository extends JpaRepository<StandardMasterModel, Integer>{

	@Query(value="select a.standardName from StandardMasterModel a where a.standardId=:standardId ")
	String getStandard(Integer standardId);

	@Query(value="select new com.ingenio.announcement.bean.CountBean(a.standardId,a.standardName) from StandardMasterModel a where a.schoolMasterModel.schoolid=:schoolId ")
	List<CountBean> getAllStandards(Integer schoolId);

	
	
	
}
