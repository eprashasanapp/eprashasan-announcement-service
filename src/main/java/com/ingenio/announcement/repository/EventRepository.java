package com.ingenio.announcement.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.EventResponseBean;
import com.ingenio.announcement.bean.PersonalNoteRequestBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.AppUserRoleModel;
import com.ingenio.announcement.model.EventModel;
import com.ingenio.announcement.model.SchoolMasterModel;

public interface EventRepository extends JpaRepository<EventModel, Integer>{
	
	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.standardId,0),COALESCE(a.standardName,''),COALESCE(b.divisionId,0),COALESCE(b.divisionName,'')) from StandardMasterModel a "
			+ "left join DivisionMasterModel b on (a.standardId=b.standardMasterModel.standardId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid ) "
			+ "where a.schoolMasterModel.schoolid=:schoolId order by a.standardPriority , b.divisionId ")
	List<StandardDivisionBean> getStandardDivisionList(Integer schoolId);
	
	
	
	@Query(value="SELECT COALESCE(MAX(a.eventId),0)+1 from EventModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.eventAssignToId),0)+1 from EventAssignToModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAssignToId(Integer schoolId);


	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0),COALESCE(a.eventTitle,''),"
	+ "COALESCE(a.eventStartDate,''),COALESCE(a.eventEndDate,''),COALESCE(a.eventStartTime,''),COALESCE(a.eventEndtime,''),"
	+ "COALESCE(a.eventVenue,''), COALESCE(a.eventDescription,''),COALESCE(a.eventReminderMessage,'') , a.staffId ,"
	+ "COALESCE(b.eventStatus,'') ,COALESCE(b.eventAssignToId,0), COALESCE(c.eventTypeId,0),COALESCE(c.eventType,''),COALESCE(c.eventTypeColor,'') ) "
	+ "from EventModel a "
	+ "left join EventAssignToModel b on (b.eventModel.eventId=a.eventId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid ) "	
	+ "left join EventTypeMasterModel c on (c.eventTypeId=a.eventTypeMasterModel.eventTypeId  ) "
	+ "where (b.staffStudentId=:studentStaffId and b.role=:profileRole) and b.yearMasterModel.yearId=:yearId "
	+ "and MONTH(DATE_FORMAT(eventStartDate,'%Y-%m-%d'))=:month and YEAR(DATE_FORMAT(eventStartDate,'%Y-%m-%d'))=:year and DAY(DATE_FORMAT(eventStartDate,'%Y-%m-%d'))=:day group by a.eventId  ")
	List<EventResponseBean> getStudentEvent(Integer day , Integer yearId, String profileRole, Integer studentStaffId, Integer month, Integer year);
	
	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0),COALESCE(a.eventTitle,''),"
	+ "COALESCE(a.eventStartDate,''),COALESCE(a.eventEndDate,''),COALESCE(a.eventStartTime,''),COALESCE(a.eventEndtime,''),"
	+ "COALESCE(a.eventVenue,''), COALESCE(a.eventDescription,''),COALESCE(a.eventReminderMessage,'') , a.staffId ,"
	+ "COALESCE(b.eventStatus,'') ,COALESCE(b.eventAssignToId,0),COALESCE(c.eventTypeId,0),COALESCE(c.eventType,''),COALESCE(c.eventTypeColor,'') ) "
	+ "from EventModel a "
	+ "left join EventAssignToModel b on (b.eventModel.eventId=a.eventId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid ) "
	+ "left join EventTypeMasterModel c on (c.eventTypeId=a.eventTypeMasterModel.eventTypeId  ) "
	+ "where (b.staffStudentId=:studentStaffId and b.role=:profileRole) and b.yearMasterModel.yearId=:yearId "
	+ "and MONTH(DATE_FORMAT(eventStartDate,'%Y-%m-%d'))=:month and YEAR(DATE_FORMAT(eventStartDate,'%Y-%m-%d'))=:year group by a.eventId  ")
	List<EventResponseBean> getStudentEvent(Integer yearId, String profileRole, Integer studentStaffId, Integer month, Integer year);

	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0),COALESCE(a.eventTitle,''),"
			+ "COALESCE(a.eventStartDate,''),COALESCE(a.eventEndDate,''),COALESCE(a.eventStartTime,''),COALESCE(a.eventEndtime,''),"
			+ "COALESCE(a.eventVenue,''), COALESCE(a.eventDescription,''),COALESCE(a.eventReminderMessage,'') , a.staffId ,"
			+ "COALESCE(b.eventStatus,'') ,COALESCE(b.eventAssignToId,0),COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as postedBy, "
			+ "COALESCE(f.roleName,'') as postedByRole , coalesce(e.sregNo,''), coalesce(a.schoolMasterModel.schoolid,0) , "
			+ "COALESCE(c.eventTypeId,0),COALESCE(c.eventType,''),COALESCE(c.eventTypeColor,'') )"
			+ "from EventModel a "
			+ "left join EventAssignToModel b on b.eventModel.eventId=a.eventId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "	
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and e.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid 	"
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join EventTypeMasterModel c on (c.eventTypeId=a.eventTypeMasterModel.eventTypeId  ) "
			+ "where b.staffStudentId=:studentStaffId and a.eventId=:eventId group by a.eventId  ")
	List<EventResponseBean> getStudentEventDetails(Integer eventId, Integer studentStaffId);
	
	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0),COALESCE(a.eventTitle,''),"
			+ "COALESCE(a.eventStartDate,''),COALESCE(a.eventEndDate,''),COALESCE(a.eventStartTime,''),COALESCE(a.eventEndtime,''),"
			+ "COALESCE(a.eventVenue,''), COALESCE(a.eventDescription,''),COALESCE(a.eventReminderMessage,'') , a.staffId ,"
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as postedBy, "
			+ "COALESCE(f.roleName,'') as postedByRole , coalesce(e.sregNo,''), coalesce(a.schoolMasterModel.schoolid,0) , "
			+ "COALESCE(c.eventTypeId,0),COALESCE(c.eventType,''),COALESCE(c.eventTypeColor,'') )"
			+ "from EventModel a "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and e.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid 	"
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join EventTypeMasterModel c on (c.eventTypeId=a.eventTypeMasterModel.eventTypeId  ) "
			+ "where  a.eventId=:eventId group by a.eventId  ")
	List<EventResponseBean> getStudentEventDetails(Integer eventId);


	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0) , "
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0),"
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus IN (1,2,3,5,6,7) and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0),"
			+ "(COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus IN (1,2,3,5,6,7) and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0) - "
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus IN (2,3,5,6,7) and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0)),"
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus IN (2,3,5,6,7) and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0),"
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus=3 and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0),"
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus=5 and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0),"
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus=6 and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0),"
			+ "COALESCE((select count(a1) from EventAssignToModel a1 where a1.eventStatus=7 and a1.eventModel.eventId=a.eventId group by a1.eventModel.eventId),0) )"
			+ "from EventModel a "
			+ "left join EventAssignToModel b on b.eventModel.eventId=a.eventId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "where a.staffId=?1 and b.yearMasterModel.yearId=?2 and a.eventId=?3 "
			+ "group by a.eventId  order by a.cDate desc ")
	List<EventResponseBean> findAssignedEventCount(Integer studentStaffId, Integer yearId, Integer eventId);


	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(b.standardId,0),COALESCE(b.standardName,''), " + 
			"COALESCE(c.divisionId,0),COALESCE(c.divisionName,''),COALESCE(e.eventId,0), COALESCE(e.eventTitle,''),COALESCE(e.eventVenue,''),COALESCE(e.eventStartDate,''),"
			+ "COALESCE(e.eventStartTime,''),"
			+ "f.staffStudentId,f.role)  " + 
			"from SchoolMasterModel a  " + 
			"left join StandardMasterModel b on a.schoolid=b.schoolMasterModel.schoolid  " + 
			"left join DivisionMasterModel c on c.standardMasterModel.standardId=b.standardId   " + 
			"left join YearMasterModel d on a.schoolid=d.schoolMasterModel.schoolid 	 " + 
			"left join EventModel e on a.schoolid=e.schoolMasterModel.schoolid " + 
			"left join EventAssignToModel f on f.eventModel.eventId=e.eventId  " + 
			"where a.schoolid=:schoolid and d.yearId=:yearId and (f.eventStatus = '5' OR f.eventStatus = '7') and DATE_FORMAT(e.eventStartDate,'%d-%m-%Y')>=:date " + 
			"and DATEDIFF(e.eventStartDate,DATE_FORMAT(STR_TO_DATE('=:date','%d-%m-%Y'),'%Y-%m-%d')) <3 "+
			"group by b.standardId,c.divisionId,e.eventId, e.eventId, f.staffStudentId,f.role " + 
			"order by a.schoolid,d.yearId,b.standardId,c.divisionId  ")
	List<EventResponseBean> findEvents(Integer schoolid, Integer yearId, String date);


	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.renewStudentId,0),COALESCE(d.studentId,0),"
			+ "COALESCE(CONCAT(COALESCE(d.studInitialName,''),' ',COALESCE(d.studFName,''),' ',COALESCE(d.studMName,''),' ',COALESCE(d.studLName,'')),''), "
			+ "COALESCE(c.rollNo,0),COALESCE(b.eventStatus,''),COALESCE(d.studentRegNo,''),d.schoolMasterModel.schoolid,COALESCE(d.grBookName.grBookId,0) ) from EventModel a "
			+ "left join EventAssignToModel b on a.eventId=b.eventModel.eventId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join StudentStandardRenewModel c on c.renewStudentId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StudentMasterModel d on d.studentId=c.studentMasterModel.studentId and a.schoolMasterModel.schoolid=d.schoolMasterModel.schoolid  "
			+ "where a.eventId=?1 and b.role='Parent' and  d.isApproval='0' and d.isAdmissionCancelled='1' order by b.eventStatus,c.rollNo  ")
	List<StudentDetailsBean> getStudentDetailsForEvent(Integer eventId);


	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.staffId,0),"
			+ "COALESCE(CONCAT(COALESCE(c.initialname,''),' ',COALESCE(c.firstname,''),' ',COALESCE(c.secondname,''),' ',COALESCE(c.lastname,'')),''), "
			+ "COALESCE(b.eventStatus,''),COALESCE(c.sregNo,''),c.schoolMasterModel.schoolid) from EventModel a "
			+ "left join EventAssignToModel b on a.eventId=b.eventModel.eventId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join StaffBasicDetailsModel c on c.staffId=b.staffStudentId and c.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid  "
			+ "where a.eventId=?1 and b.role='Teacher' order by b.eventStatus,c.staffId  ")
	List<StudentDetailsBean> getStaffDetailsForEvent(Integer eventId);


	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0),COALESCE(a.eventTitle,''),"
			+ "COALESCE(a.eventStartDate,''),COALESCE(a.eventEndDate,''),COALESCE(a.eventStartTime,''),COALESCE(a.eventEndtime,''),"
			+ "COALESCE(a.eventVenue,''), COALESCE(a.eventDescription,''),COALESCE(a.eventReminderMessage,'') , a.staffId ,"
			+ "COALESCE(c.eventTypeId,0),COALESCE(c.eventType,''),COALESCE(c.eventTypeColor,'') ) "
			+ "from EventModel a "
			+ "left join EventCalendarAssignToModel b on (b.eventModel.eventId=a.eventId) "
			+ "left join EventTypeMasterModel c on (c.eventTypeId=a.eventTypeMasterModel.eventTypeId  ) "
			+ "where a.schoolMasterModel.schoolid=:schoolId and b.standardMasterModel.standardId =:standardId and  "
			+ " b.divisionMasterModel.divisionId =:divisionId and MONTH(DATE_FORMAT(a.eventStartDate,'%Y-%m-%d'))=:month group by a.eventId  ")
	List<EventResponseBean> getStudentEvent(Integer schoolId, Integer standardId, Integer divisionId, Integer month);


	@Query(value="select new com.ingenio.announcement.bean.PersonalNoteRequestBean(COALESCE(c.personalNoteId,0),"
			+ "COALESCE(c.personal_note,''),COALESCE(c.reminderTime,''),COALESCE(c.reminderDate,'')  ,"
			+ "COALESCE(c.schoolMasterModel.schoolid,0),COALESCE(c.userId.appUserRoleId,0)) from PersonalNoteModel c "
			+ "where c.userId.appUserRoleId=:userId and MONTH(DATE_FORMAT(c.reminderDate,'%Y-%m-%d'))=:month  ")
	List<PersonalNoteRequestBean> getPersonalNote(Integer userId, Integer month);


	@Query(value="SELECT coalesce(MAX(a.eventAttachmentId),0)+1 from EventAttachmentModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAttachmentId(Integer schoolId);


	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(coalesce(a.serverFilePath,''),coalesce(a.eventAttachmentId,0),coalesce(a.fileName,'')) from EventAttachmentModel a where a.eventModel.eventId=:eventId ")
	List<AnnouncementResponseBean> getAttachments(Integer eventId);


	@Query(value="select new com.ingenio.announcement.bean.EventResponseBean(COALESCE(a.eventId,0),COALESCE(a.eventTitle,''),"
			+ "COALESCE(a.eventStartDate,''),COALESCE(a.eventEndDate,''),COALESCE(a.eventStartTime,''),COALESCE(a.eventEndtime,''),"
			+ "COALESCE(a.eventVenue,''), COALESCE(a.eventDescription,''),COALESCE(a.eventReminderMessage,'') , "
			+"COALESCE(b.standardMasterModel.standardId,0),COALESCE(b.divisionMasterModel.divisionId,0)) "
			+ "from EventModel a "
			+ "left join EventCalendarAssignToModel b on (b.eventModel.eventId=a.eventId) "
			+ "where DATE_FORMAT(a.eventStartDate,'%d-%m-%Y')=:date  ")
	List<EventResponseBean> findEvents(String date);


	@Query(value="select COALESCE(c.fcm_id,'') from StudentStandardRenewModel c "
			+ "where c.standardMasterModel.standardId=:standardId and c.divisionMasterModel.divisionId=:divisionId  ")
	List<String> getAndroidTokenList(Integer standardId, Integer divisionId);



	




}
