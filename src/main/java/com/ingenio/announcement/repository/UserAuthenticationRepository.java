package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.FeeSendSmsNumbersMstrModel;
import com.ingenio.announcement.model.SchoolMasterModel;



@Repository
public interface UserAuthenticationRepository extends JpaRepository<FeeSendSmsNumbersMstrModel, Integer>{
	
	List<FeeSendSmsNumbersMstrModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

}

