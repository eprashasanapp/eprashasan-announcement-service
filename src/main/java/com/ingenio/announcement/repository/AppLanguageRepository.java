package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.AppLanguageModel;

@Repository
public interface AppLanguageRepository extends JpaRepository<AppLanguageModel, Integer> {
	
}
