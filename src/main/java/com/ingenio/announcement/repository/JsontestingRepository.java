package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.AppLanguageModel;
import com.ingenio.announcement.model.JsonTestingModel;

@Repository
public interface JsontestingRepository extends JpaRepository<JsonTestingModel, Integer> {

	@Query(value="SELECT a from JsonTestingModel a where a.schoolId=:schoolId ")
	JsonTestingModel getdata(Integer schoolId);
	
}
