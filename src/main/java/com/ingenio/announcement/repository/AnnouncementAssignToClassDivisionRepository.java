package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StandardDivisionReportBean;
import com.ingenio.announcement.model.AnnouncementAssignToClassDivisionModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.SchoolMasterModel;

@Repository
public interface AnnouncementAssignToClassDivisionRepository extends JpaRepository<AnnouncementAssignToClassDivisionModel, Integer>{

	@Query(value="select a from AnnouncementAssignToClassDivisionModel a where a.announcementModel.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId ")
	List<AnnouncementAssignToClassDivisionModel> findByAnnouncementAndSchoolId(Integer announcementId,
			Integer schoolId);

	@Query(value="select a.announcementModel.announcementId from AnnouncementAssignToClassDivisionModel a"
			+ " where a.yearMasterModel.yearId=:yearId and a.standardMasterModel.standardId=:standardId and "
			+ " a.divisionMasterModel.divisionId=:divisionId and  a.schoolMasterModel.schoolid=:schoolId ")
	List<Integer> findAnnouncementIds(Integer yearId, Integer standardId, Integer divisionId, Integer schoolId);

	//for fee pending
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.studentId,'student',coalesce(b.mmobile,0),b.studFName,app.appUserRoleId,b.memail,y.yearId,y.year,st.standardId) from AnnouncementAssignToClassDivisionModel d  "
			+ "left join StudentStandardRenewModel a on  a.standardMasterModel.standardId=d.standardMasterModel.standardId "
			+ " and a.yearMasterModel.yearId=d.yearMasterModel.yearId and a.divisionMasterModel.divisionId=d.divisionMasterModel.divisionId  "
			+ "left join StudentMasterModel b on b.studentId=a.studentMasterModel.studentId "
			+ "left join AppUserModel app on app.staffId=b.studentId and app.roleName='ROLE_PARENT1' "
			+ "left join YearMasterModel y on y.yearId=a.yearMasterModel.yearId "
			+ "left join StandardMasterModel st on st.standardId=a.standardMasterModel.standardId "
			+ "where ( (a.isOnlineAdmission = 1 and a.isApproval = 1) or (a.isOnlineAdmission = 0) ) and b.isAdmissionCancelled=1 and  d.announcementModel.announcementId= :announcementId and d.schoolMasterModel.schoolid=:schoolid group by app.staffId ")
	List<PhoneNumberBean> getPhoneNumbers(Integer schoolid, Integer announcementId);
	
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.studentId,'student',coalesce(b.mmobile,0),b.studFName) from AnnouncementAssignToClassDivisionModel d  "
			+ "left join StudentStandardRenewModel a on a.yearMasterModel.yearId=d.yearMasterModel.yearId AND a.divisionMasterModel.divisionId=d.divisionMasterModel.divisionId and a.standardMasterModel.standardId=d.standardMasterModel.standardId "
			+ " and a.yearMasterModel.yearId=d.yearMasterModel.yearId  "
			+ "left join StudentMasterModel b on b.studentId=a.studentMasterModel.studentId "
			//+ "left join AppUserModel app on app.staffId=b.studentId and app.roleName='ROLE_PARENT1' "
			+ "where ( (a.isOnlineAdmission = 1 and a.isApproval = 1) or (a.isOnlineAdmission = 0) ) and b.isAdmissionCancelled=1  and d.announcementModel.announcementId= :announcementId and d.schoolMasterModel.schoolid=:schoolid  ")
	List<PhoneNumberBean> getPhoneNumbersForBal(Integer schoolid, Integer announcementId);

	List<AnnouncementAssignToClassDivisionModel> findBySchoolMasterModelAndAnnouncementModel(SchoolMasterModel schoolModel,
			AnnouncementModel announcement);

	@Query("select new com.ingenio.announcement.bean.StandardDivisionBean(a.standardMasterModel.standardId,a.standardMasterModel.standardName,a.divisionMasterModel.divisionId,a.divisionMasterModel.divisionName,a.yearMasterModel.yearId,a.announcementModel.publishDate ) from AnnouncementAssignToClassDivisionModel a where "
			+ "a.schoolMasterModel.schoolid=:schoolid and a.announcementModel.announcementId=:announcementId order by  a.standardMasterModel.standardId,a.divisionMasterModel.divisionId ")
	List<StandardDivisionBean> getStandardDivisinYearAndPublishDate(Integer schoolid,
			Integer announcementId);

	
	
	

}
