package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementCommentModel;



public interface AnnouncementCommentRepository extends JpaRepository<AnnouncementCommentModel, Integer>{

	@Query(value="SELECT coalesce(MAX(a.commentId),0)+1 from AnnouncementCommentModel a where a.schoolMasterModel.schoolid=:schoolid ")
	String getMaxId(Integer schoolid);

	@Query(value="SELECT a from AnnouncementCommentModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId =:announcementId"
			+ " order by a.commentId desc ")
	List<AnnouncementCommentModel> findComments(Integer schoolId, Integer announcementId);

	@Query(value="SELECT a from AnnouncementCommentModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId =:announcementId and a.appUserModel.appUserRoleId=:appUserRoleId "
			+ " order by a.commentId desc ")
	List<AnnouncementCommentModel> getComments(Integer schoolId, Integer announcementId, Integer appUserRoleId);

}
