package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.SchoolWiseFcmModel;

public interface SchoolWiseFcmRepositry extends JpaRepository<SchoolWiseFcmModel, Integer>{

	@Query(value="select a from SchoolWiseFcmModel a where a.schoolMasterModel.schoolid=:schoolId")
	List<SchoolWiseFcmModel> findBySchoolId(Integer schoolId);

	
}
