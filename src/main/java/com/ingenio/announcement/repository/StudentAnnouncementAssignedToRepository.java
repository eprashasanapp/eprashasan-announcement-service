package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.UserDetailsBean;
import com.ingenio.announcement.model.AndroidFCMTokenMaster;
import com.ingenio.announcement.model.AnnouncementAssignToModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;

@Repository
public interface StudentAnnouncementAssignedToRepository extends JpaRepository<AnnouncementAssignToModel, Integer>{

	AnnouncementAssignToModel findByAnnouncementAssignToId(Integer announcementAssignToId);

	void deleteByAnnouncementModel(AnnouncementModel announcementModel);

	@Query(value="select a from AnnouncementAssignToModel a where a.announcementModel.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId ")
	List<AnnouncementAssignToModel> getAssignToList(Integer announcementId, Integer schoolId);

	@Query("select st.studentMasterModel.studentId from AnnouncementAssignToModel a "
			+ "left join StudentStandardRenewModel st on a.staffStudentId=st.renewStudentId and st.alreadyRenew='1'  where a.announcementModel.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId  and a.role='Parent' ")
	List<Integer> getStudents(Integer announcementId,Integer schoolId);

	@Query("select a.staffStudentId from AnnouncementAssignToModel a where a.announcementModel.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId and a.role='Teacher'  ")
	List<Integer> getStaffs(Integer announcementId, Integer schoolId);

//for fee pending
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,a.role,c.mmobile,c.studFName,b.firstname,staff.appUserRoleId as staffUserId,stud.appUserRoleId as studUserId,case when c.studentId>0 then c.memail when b.staffId>0 then b.femailId end ,y.yearId,y.year,st.standardId) from AnnouncementAssignToModel a  "
			+ "left join StaffBasicDetailsModel b on b.staffId=a.staffStudentId and a.role='Teacher' "
			+ "left join StudentStandardRenewModel r on r.renewStudentId=a.staffStudentId and (a.role='Student' OR a.role='Parent') "
			+ "left join StudentMasterModel c on c.studentId=r.studentMasterModel.studentId and (a.role='Student' OR a.role='Parent')  "
			+ "left join AppUserModel staff on staff.staffId=b.staffId and (staff.roleName='ROLE_TEACHINGSTAFF' or staff.roleName='ROLE_NONTEACHINGSTAFF' or staff.roleName='ROLE_PRINCIPAL' ) "
			+ "left join AppUserModel stud on stud.staffId=c.studentId and stud.roleName='ROLE_PARENT1' "
			+ "left join YearMasterModel y on y.yearId=r.yearMasterModel.yearId "
			+ "left join StandardMasterModel st on st.standardId=r.standardMasterModel.standardId "
			+ "where a.schoolMasterModel.schoolid= :schoolid and a.announcementModel.announcementId=:announcementId and a.isDel=0 group by c.studentId order by a.role   ")
	List<PhoneNumberBean> getPhoneNumbers(Integer schoolid, Integer announcementId);
	
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,a.role,c.mmobile,c.studFName,b.firstname) from AnnouncementAssignToModel a  "
			+ "left join StaffBasicDetailsModel b on b.staffId=a.staffStudentId and a.role='Teacher' "
			+ "left join StudentStandardRenewModel r on r.renewStudentId=a.staffStudentId and (a.role='Student' OR a.role='Parent') "
			+ "left join StudentMasterModel c on c.studentId=r.studentMasterModel.studentId and (a.role='Student' OR a.role='Parent')  "
			+ "where a.schoolMasterModel.schoolid= :schoolid and a.announcementModel.announcementId=:announcementId and a.isDel=0 order by a.role ")
	List<PhoneNumberBean> getPhoneNumbersForBal(Integer schoolid, Integer announcementId);

	
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,a.role,b.firstname,sch.schoolid,app.appUserRoleId) from SchoolMasterModel sch "
			+ "left join AnnouncementAssignToModel a on a.schoolMasterModel.schoolid=sch.schoolid "
			+ "left join StaffBasicDetailsModel b on b.staffId=a.staffStudentId and (a.role='Teacher' or a.role='Principal') "
			+ "left join AppUserModel app on app.staffId=b.staffId  "
			+ "where a.schoolMasterModel.sansthaKey= :sansthaKey and a.announcementModel.announcementId=:announcementId and a.isDel=0 order by a.role ")
	List<PhoneNumberBean> getPhoneNumbersForBalSanstha(String sansthaKey, Integer announcementId);

	
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,a.role,c.mmobile,concat(COALESCE(c.studInitialName,''),' ',COALESCE(c.studFName,''),' ',COALESCE(c.studMName,''),' ',COALESCE(c.studLName,'')),concat(COALESCE(b.initialname,''),' ',COALESCE(b.firstname,''),' ',COALESCE(b.secondname,''),' ',COALESCE(b.lastname,'')),staff.appUserRoleId as staffUserId,stud.appUserRoleId as studUserId"
			+ ",b.femailId,c.memail,stffcm.token,studfcm.token,c.studentRegNo,std.standardId,std.standardName,div.divisionId,div.divisionName, "
			+ "coalesce((select CASE WHEN s.viewsId IS NOT NULL THEN 1 ELSE 0 END from AnnouncementViewsModel s where s.announcementModel.announcementId=:announcementId and (s.appUserModel.appUserRoleId=staff.appUserRoleId or s.appUserModel.appUserRoleId=stud.appUserRoleId)),'') "
			+ ",COALESCE(t.likeDislike,''),( select CASE WHEN count(u.comment) > 0 THEN 1 ELSE 0 END from AnnouncementCommentModel u where u.announcementModel.announcementId=:announcementId and (u.appUserModel.appUserRoleId=staff.appUserRoleId or u.appUserModel.appUserRoleId=stud.appUserRoleId) ) , "
			+ " ( select CASE WHEN COUNT(v.reactionId) > 0 THEN 1 ELSE 0 END from AnnouncementReactionModel v where v.announcementModel.announcementId=:announcementId and (v.appUserModel.appUserRoleId=staff.appUserRoleId or v.appUserModel.appUserRoleId=stud.appUserRoleId))) from AnnouncementAssignToModel a  "
			+ "left join StaffBasicDetailsModel b on b.staffId=a.staffStudentId and a.role='Teacher' "
			+ "left join StudentStandardRenewModel r on r.renewStudentId=a.staffStudentId and (a.role='Student' OR a.role='Parent') "
			+ "left join StudentMasterModel c on c.studentId=r.studentMasterModel.studentId and (a.role='Student' OR a.role='Parent')  "
			+ "left join AppUserModel staff on staff.staffId=b.staffId and (staff.roleName='ROLE_TEACHINGSTAFF' or staff.roleName='ROLE_NONTEACHINGSTAFF' or staff.roleName='ROLE_PRINCIPAL' ) "
			+ "left join AppUserModel stud on stud.staffId=c.studentId and stud.roleName='ROLE_PARENT1' "
			+ "left join AndroidFCMTokenMaster stffcm on stffcm.notificationUserId=staff.appUserRoleId "
			+ "left join AndroidFCMTokenMaster studfcm on studfcm.notificationUserId=stud.appUserRoleId "
			+ "left join StandardMasterModel std on std.standardId=r.standardMasterModel.standardId "
			+ "left join DivisionMasterModel div on div.divisionId=r.divisionMasterModel.divisionId  "
			+ "left join AnnouncementLikeorDislikeModel t on t.announcementModel.announcementId=:announcementId and (t.appUserModel.appUserRoleId=staff.appUserRoleId or t.appUserModel.appUserRoleId=stud.appUserRoleId) "
			+ "where a.schoolMasterModel.schoolid= :schoolid and a.announcementModel.announcementId=:announcementId and a.isDel=0 order by a.role ")
	List<PhoneNumberBean> getStaffStudentReport(Integer schoolid, Integer announcementId);

	//void getStaffInfo(Integer announcementId);
	
	
	@Query("select new com.ingenio.announcement.bean.UserDetailsBean(b.sregNo,b.staffId,COALESCE(b.initialname,''),COALESCE(b.firstname,''),COALESCE(b.secondname,''),"
			+ "COALESCE(b.lastname,''),COALESCE(b.gender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.contactnosms,''),COALESCE(b.birthdate,''),COALESCE(b.birthplace,''),"
			+ "COALESCE(b.nationality,''),COALESCE(b.joiningDate,''),COALESCE(g.designationName,''),COALESCE(f.departmentName,''),b.femailId, case when (select av.viewsId from AnnouncementViewsModel av where av.appUserModel.appUserRoleId=app.appUserRoleId and av.announcementModel.announcementId=:announcementId group by av.announcementModel.announcementId )>0 then 1 else 0 end ,"
			+ "app.appUserRoleId)  "
			+ "from StaffBasicDetailsModel b  "
			+ "left join AnnouncementAssignToModel h on b.staffId=h.staffStudentId "
			+ "left join CasteMasterModel c on c.casteId=b.casteMasterModel.casteId and c.isDel='0'   "
			+ "left join CategoryMasterModel d on d.categoryId=b.categoryMasterModel.categoryId and d.isDel='0'  "
			+ "left join ReligionMasterModel e on e.religionId=b.religionMasterModel.religionId and e.isDel='0' "
			+ "left join DepartmentModel f on f.departmentId=b.departmentModel.departmentId and f.isDel='0'    "
			+ "left join DesignationModel g on g.designationId=b.designationModel.designationId and g.isDel='0'  "
			+ "left join AppUserModel app on app.staffId=b.staffId "
			//+ "left join AnnouncementViewsModel h on h.appUserModel.appUserRoleId=app.appUserRoleId  "
			+ "where h.announcementModel.announcementId=:announcementId " )
	List<UserDetailsBean> findStaffDetails(Integer announcementId);

}
