package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.SmsMessageMasterModel;

@Repository
public interface StudentSmsMasterRepository extends JpaRepository<SmsMessageMasterModel, Integer>{
	
	@Query(value="SELECT COALESCE(MAX(a.sms_msg_master_id),0)+1 from SmsMessageMasterModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);
}
