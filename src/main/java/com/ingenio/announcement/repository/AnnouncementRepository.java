package com.ingenio.announcement.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.bean.AnnouncementBean;
import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.AnnouncementModel;

public interface AnnouncementRepository extends PagingAndSortingRepository<AnnouncementModel, Integer>{

	@Query(value="SELECT COALESCE(MAX(a.announcementId),0)+1 from AnnouncementModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.announcementAssignToId),0)+1 from AnnouncementAssignToModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAssignToId(Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),"
			+ "COALESCE(a.announcementTitle,''),COALESCE(a.startDate,''), "
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (1,2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
			+ "(COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (1,2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0) - "
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0)),"
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus=3 and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
			+ "COALESCE((select count(c1.announcementAttachmentId) from AnnouncementAttachmentModel c1 where c1.announcementModel.announcementId=a.announcementId group by c1.announcementModel.announcementId),0),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent)  "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join AnnouncementAttachmentModel c on c.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid "
			+ "where a.staffId=?1 and b.yearMasterModel.yearId=?2 and a.announcementId=?3 and a.schoolMasterModel.schoolid=?4 "
			+ "group by a.announcementId  order by a.CDate desc ")
	List<AnnouncementResponseBean> findAssignedAnnouncementCount(Integer staffId, Integer yearId,Integer announcementId, Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.standardId,0),COALESCE(a.standardName,''),COALESCE(b.divisionId,0),COALESCE(b.divisionName,'')) from StandardMasterModel a "
//			+ "left join DivisionMasterModel b on ( a.standardId=b.standardMasterModel.standardId and b.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid ) where a.schoolMasterModel.schoolid=:schoolId  and a.isDel='0' order by a.standardId , b.divisionId ")
			+ "left join DivisionMasterModel b on ( a.standardId=b.standardMasterModel.standardId and b.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid ) "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' AND b.isDel = '0' "
			+ "order by a.standardId , b.divisionId ")
	List<StandardDivisionBean> getStandardDivisionList(Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,'') ,COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is null or i.renewId=0) "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole) OR a.staffId=:staffStudentId) "
			+ " and a.sendForApproval='1' and a.announcementTypeModel.announcementTypeId =:typeId and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncement(Integer staffStudentId, String profileRole,
			Integer typeId,Integer schoolid);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,'') ,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is null or i.renewId=0) "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole) OR a.staffId=:staffStudentId) "
			+ "and  a.sendForApproval='1'  and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncement(Integer staffStudentId, String profileRole,
			Integer schoolid);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),"
			+ "COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),"
			+ "COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),"
			+ "COALESCE(a.noticeSendingFlag,0), COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , "
			+ "COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is null or i.renewId=0) "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole) OR a.staffId=:staffStudentId) "
			+ "and  a.sendForApproval='1' and a.announcementTypeModel.announcementTypeId =:typeId and a.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementWithAnnouncementId(Integer staffStudentId, String profileRole,
			Integer typeId ,Integer announcementId,Integer schoolid );
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),a.schoolMasterModel.schoolid,"
			+ "COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) ,"
			+ " COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,'') ,COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is null or i.renewId=0) "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole)"
			+ " OR a.staffId=:staffStudentId) and a.startDate=:date  and  "
			+ " a.announcementTypeModel.announcementTypeId =:typeId and a.sendForApproval='1' and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncement(Integer staffStudentId, String profileRole,Date date,
			Integer typeId,Integer schoolid );

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),a.schoolMasterModel.schoolid,"
			+ "COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) ,"
			+ " COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is null or i.renewId=0) "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole)"
			+ " OR a.staffId=:staffStudentId) and a.startDate=:date    "
			+ "  and a.sendForApproval='1' and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncement(Integer staffStudentId, String profileRole,Date date,
			Integer schoolid );

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),"
			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) ,"
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is null or i.renewId=0) "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole)"
			+ " OR a.staffId=:staffStudentId) and a.startDate=:date and  "
			+ " a.announcementTypeModel.announcementTypeId =:typeId and a.sendForApproval='1' and a.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementWithAnnouncementId(Integer staffStudentId,  String profileRole,Date date,
			Integer typeId ,Integer announcementId,Integer schoolid );

	@Query(value="SELECT coalesce(MAX(a.announcementAttachmentId),0)+1 from AnnouncementAttachmentModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAttachmentId(Integer schoolId);
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(coalesce(a.serverFilePath,''),coalesce(a.announcementAttachmentId,0),coalesce(a.fileName,'')) from AnnouncementAttachmentModel a where a.announcementModel.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId ")
	List<AnnouncementResponseBean> getAttachments(Integer announcementId, Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.renewStudentId,0),COALESCE(d.studentId,0), "
			+ "COALESCE(CONCAT(COALESCE(d.studInitialName,''),' ',COALESCE(d.studFName,''),' ',COALESCE(d.studMName,''),' ',COALESCE(d.studLName,'')),''), "
			+ "COALESCE(c.rollNo,0),COALESCE(b.announcementStatus,''),COALESCE(d.studentRegNo,''),d.schoolMasterModel.schoolid,COALESCE(d.grBookName.grBookId,0) ) from AnnouncementModel a "
			+ "left join AnnouncementAssignToModel b on a.announcementId=b.announcementModel.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StudentStandardRenewModel c on c.renewStudentId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StudentMasterModel d on d.studentId=c.studentMasterModel.studentId and d.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "where a.announcementId=?1 and b.role='Parent' order by b.announcementStatus,c.rollNo  ")
	List<StudentDetailsBean> getStudentDetailsForAnnouncement(Integer announcementId);

	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.staffId,0),"
			+ "COALESCE(CONCAT(COALESCE(c.initialname,''),' ',COALESCE(c.firstname,''),' ',COALESCE(c.secondname,''),' ',COALESCE(c.lastname,'')),''), "
			+ "COALESCE(b.announcementStatus,''),COALESCE(c.sregNo,''),c.schoolMasterModel.schoolid) from AnnouncementModel a "
			+ "left join AnnouncementAssignToModel b on a.announcementId=b.announcementModel.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel c on c.staffId=b.staffStudentId and c.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "where a.announcementId=?1 and b.role='Teacher' order by b.announcementStatus,c.staffId  ")
	List<StudentDetailsBean> getStaffDetailsForAnnouncement(Integer announcementId);

	
	@Transactional
	@Modifying
	@Query("update AnnouncementModel a Set a.announcementSendFlag =:i  where  a.announcementId=:announcementId  ")
	void updateAnnouncementSentFlag(int i, Integer announcementId);

	@Transactional
	@Modifying
	@Query("update AnnouncementModel a Set a.whatAppSendingFlag =1  where  a.announcementId=:saveId  ")
	void updateWhatsappFlagStatus(Integer saveId);

	@Transactional
	@Modifying
	@Query("update AnnouncementModel a Set a.noticeSendingFlag =1  where  a.announcementId=:announcementId  ")
	void updateNoticeFlagStatus(Integer announcementId);
	
	@Transactional
	@Modifying
	@Query("update AnnouncementModel a Set a.sendForApproval =1  where  a.announcementId=:announcementId  ")
	void updateSendForApproval(Integer announcementId);
	
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),"
			+ "COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) , a.isFeedback , a.userCanComment , a.publishFlag ,COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) OR a.staffId=:staffStudentId) "
			+ "and a.startDate=:date and a.sendForApproval='1' and a.announcementTypeModel.announcementTypeId =:typeId and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementParent(Integer staffStudentId, String profileRole,Date date, Integer typeId,Integer schoolid);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),"
			+ "COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) , a.isFeedback , a.userCanComment , a.publishFlag,COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) OR a.staffId=:staffStudentId) "
			+ "and a.startDate=:date and a.sendForApproval='1'   and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementParent(Integer staffStudentId, String profileRole,Date date,Integer schoolid);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ " COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),"
			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0), "
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "//announcementTypeId announcementTypeName
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) OR a.staffId=:staffStudentId) "
			+ "and a.startDate=:date and a.sendForApproval='1'  and a.announcementTypeModel.announcementTypeId =:typeId and a.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId  group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementParentWithAnnouncementId(Integer staffStudentId,  String profileRole,Date date, Integer typeId, Integer announcementId,Integer schoolId, Pageable pageableo);

//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
//			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
//			+ "COALESCE(f.roleName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
//			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
//			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0) ) "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
//			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
//			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) "
//			+ "OR a.staffId=:staffStudentId) and b.yearMasterModel.yearId=:yearId and  "
//			+ "a.announcementTypeModel.announcementTypeId =:typeId and a.sendForApproval='1' and a.schoolMasterModel.schoolid=:schoolId group by a.announcementId  order by a.CDate desc ")
//	List<AnnouncementResponseBean> getStudentAnnouncementParent(Integer staffStudentId, Integer yearId, String profileRole,
//			Integer typeId,Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,'') ,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "left join AnnouncementApprovalStatusModel ab on ab.announcementModel.announcementId=a.announcementId  "
			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) "
			+ "OR a.staffId=:staffStudentId) and   "
			+ "a.announcementTypeModel.announcementTypeId =:typeId and a.sendForApproval='1' and ab.status='approved' and a.schoolMasterModel.schoolid=:schoolId group by a.announcementId  order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementParent(Integer staffStudentId, String profileRole,
			Integer typeId,Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,'') ,COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) "
			+ "OR a.staffId=:staffStudentId) and  "
			+ " a.sendForApproval='1' and a.schoolMasterModel.schoolid=:schoolId group by a.announcementId  order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementParent(Integer staffStudentId, String profileRole,
			Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),"
			+ "COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,"
			+ "COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),"
			+ "COALESCE(a.noticeSendingFlag,0), COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , "
			+ "COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where ((b.staffStudentId=:staffStudentId and (b.role=:profileRole OR b.role='Student')) "
			+ "OR a.staffId=:staffStudentId) and  "
			+ "a.announcementTypeModel.announcementTypeId =:typeId and a.sendForApproval='1' and a.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncementParentWithAnnouncementId(Integer staffStudentId, String profileRole,
			Integer typeId,Integer announcementId,Integer schoolid);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),"
			+ "COALESCE(a.announcementTitle,''),COALESCE(a.startDate,''), "
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementModel.announcementId=:announcementId and a1.standardMasterModel.standardId=:standardId and a1.divisionMasterModel.divisionId=:divisionId and a1.yearMasterModel.yearId=:yearId and a1.schoolMasterModel.schoolid=:schoolId  group by a1.announcementModel.announcementId,a1.standardMasterModel.standardId,a1.divisionMasterModel.divisionId),0),"
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (1,2,3) and a1.announcementModel.announcementId=:announcementId and a1.standardMasterModel.standardId=:standardId and a1.divisionMasterModel.divisionId=:divisionId and a1.yearMasterModel.yearId=:yearId and a1.schoolMasterModel.schoolid=:schoolId  group by a1.announcementModel.announcementId,a1.standardMasterModel.standardId,a1.divisionMasterModel.divisionId),0),"
			+ "(COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (1,2,3) and a1.announcementModel.announcementId=:announcementId and a1.standardMasterModel.standardId=:standardId and a1.divisionMasterModel.divisionId=:divisionId and a1.yearMasterModel.yearId=:yearId and a1.schoolMasterModel.schoolid=:schoolId  group by a1.announcementModel.announcementId,a1.standardMasterModel.standardId,a1.divisionMasterModel.divisionId),0) - "
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (2,3) and a1.announcementModel.announcementId=:announcementId and a1.standardMasterModel.standardId=:standardId and a1.divisionMasterModel.divisionId=:divisionId and a1.yearMasterModel.yearId=:yearId and a1.schoolMasterModel.schoolid=:schoolId  group by a1.announcementModel.announcementId,a1.standardMasterModel.standardId,a1.divisionMasterModel.divisionId),0)),"
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (2,3) and a1.announcementModel.announcementId=:announcementId and a1.standardMasterModel.standardId=:standardId and a1.divisionMasterModel.divisionId=:divisionId and a1.yearMasterModel.yearId=:yearId and a1.schoolMasterModel.schoolid=:schoolId  group by a1.announcementModel.announcementId,a1.standardMasterModel.standardId,a1.divisionMasterModel.divisionId),0),"
			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus=3 and a1.announcementModel.announcementId=:announcementId and a1.standardMasterModel.standardId=:standardId and a1.divisionMasterModel.divisionId=:divisionId and a1.yearMasterModel.yearId=:yearId and a1.schoolMasterModel.schoolid=:schoolId  group by a1.announcementModel.announcementId,a1.standardMasterModel.standardId,a1.divisionMasterModel.divisionId),0),"
			+ "COALESCE((select count(c1.announcementAttachmentId) from AnnouncementAttachmentModel c1 where c1.announcementModel.announcementId=:announcementId group by c1.announcementModel.announcementId),0),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent)  "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join AnnouncementAttachmentModel c on c.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid "
			+ "where b.standardMasterModel.standardId=:standardId and b.divisionMasterModel.divisionId=:divisionId and b.yearMasterModel.yearId=:yearId and a.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId "
			+ "group by a.announcementId,b.standardMasterModel.standardId,b.divisionMasterModel.divisionId  order by a.CDate desc ")
	List<AnnouncementResponseBean> getAnnouncementStudentcount(Integer standardId, Integer divisionId, Integer yearId,
			Integer schoolId, Integer announcementId);

	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.renewStudentId,0),COALESCE(d.studentId,0), "
			+ "COALESCE(CONCAT(COALESCE(d.studInitialName,''),' ',COALESCE(d.studFName,''),' ',COALESCE(d.studMName,''),' ',COALESCE(d.studLName,'')),''), "
			+ "COALESCE(c.rollNo,0),COALESCE(b.announcementStatus,''),COALESCE(d.studentRegNo,''),d.schoolMasterModel.schoolid,COALESCE(d.grBookName.grBookId,0) ) from AnnouncementModel a "
			+ "left join AnnouncementAssignToModel b on a.announcementId=b.announcementModel.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StudentStandardRenewModel c on c.renewStudentId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StudentMasterModel d on d.studentId=c.studentMasterModel.studentId and d.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "where a.announcementId=:announcementId and b.standardMasterModel.standardId=:standardId "
			+ "and b.divisionMasterModel.divisionId=:divisionId and b.yearMasterModel.yearId=:yearId and"
			+ " b.schoolMasterModel.schoolid=:schoolId and (b.role='Parent' OR b.role='Student')"
			+ " order by b.announcementStatus,c.rollNo  ")
	List<StudentDetailsBean> studentListByStandardDivision(Integer announcementId, Integer standardId,
			Integer divisionId, Integer yearId, Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,"
			+ "COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),"
			+ "COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) ,"
			+ " COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),"
			+ " case when f.roleName='ROLE_PARENT2' then COALESCE(j.imagePath,'') else COALESCE(i.imagePath,'') end "
			+ ",COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,'') ,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid and b.staffStudentId=a.staffId "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId=0 or i.renewId is NULL) and f.roleName<>'ROLE_PARENT2' "
			+ "left join IcardMaster j on a.staffId=j.studId and j.renewId>0 and  f.roleName='ROLE_PARENT2' "
			+ "where a.announcementId=:postId order by a.CDate desc ")
	AnnouncementResponseBean getAnnouncements(Integer postId);
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),"
			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) ,"
			+ " COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),"
			+ " case when f.roleName='ROLE_PARENT2' then COALESCE(j.imagePath,'') else COALESCE(i.imagePath,'') end "
			+ ",COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,'') ,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid and b.staffStudentId=a.staffId "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId=0 or i.renewId is NULL) and f.roleName<>'ROLE_PARENT2' "
			+ "left join IcardMaster j on a.staffId=j.studId and j.renewId>0 and  f.roleName='ROLE_PARENT2' "
			+ "where a.announcementId=:postId and a.announcementTypeModel.announcementTypeId=:typeId group by a.announcementId order by a.CDate desc  ")
	AnnouncementResponseBean getAnnouncements(Integer postId, Integer typeId);

//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
//			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
//			+ "COALESCE(f.roleName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,0),"
//			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
//			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0) ,"
//			+ " COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0) ) "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementAssignToClassDivisionModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "and b.yearMasterModel.yearId=:yearId and b.standardMasterModel.standardId=:standardId and b.divisionMasterModel.divisionId=:divisionId "
//			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
//			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
//			+ "where a.announcementId=:postId and a.announcementTypeModel.announcementTypeId=:typeId order by a.CDate desc ")
//	AnnouncementResponseBean getAnnouncementsClassWise(Integer postId, Integer typeId, Integer standardId, Integer divisionId, Integer yearId);

//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
//			+ "COALESCE((select COALESCE(count(c1.announcementAttachmentId),0) from AnnouncementAttachmentModel c1 "
//			+ "where c1.announcementModel.announcementId=a.announcementId "
//			+ "group by c1.announcementModel.announcementId),0),COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
//			+ "COALESCE(f.roleName,''),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
//			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
//			//+ "COALESCE(a.announcementTypeModel.announcementTypeId,0),"
//			//+ "COALESCE(a.announcementTypeModel.announcementTypeName,''),"
//			+ "COALESCE(a.sendForApproval,0) , COALESCE(a.isFeedback,0) , "
//			+ "COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0) ) "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementAttachmentModel c on c.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid "
//			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
//			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
//			+ "where f.appUserRoleId=:userId order by a.CDate desc ")
//	List<AnnouncementResponseBean> getAdminAnnouncement(Integer userId);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
			+ " COALESCE(a.sendForApproval,0) , a.announcementTypeModel.announcementTypeId ,"
			+ " COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,'') "
			+ " ,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId "
			+ "where f.appUserRoleId=:userId and a.announcementTypeModel.announcementTypeId=:typeId order by a.CDate desc ")
	List<AnnouncementResponseBean> getAdminAnnouncement(Integer userId, Integer typeId);
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "				
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0),"
			+ "COALESCE(a.sendForApproval,0) , a.announcementTypeModel.announcementTypeId ,"
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ " from AnnouncementModel a "			
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId "
			+ "where f.appUserRoleId=:userId order by a.CDate desc ")
	List<AnnouncementResponseBean> getAdminAnnouncement(Integer userId);
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(coalesce(a.serverFilePath,''),coalesce(a.announcementAttachmentId,0),coalesce(a.fileName,'')) from AnnouncementAttachmentModel a where a.announcementModel.announcementId=:announcementId ")
	List<AnnouncementResponseBean> getAttachments(Integer announcementId);

	@Query("select a.announcement from AnnouncementModel a where a.announcementId=:announcementId ")
	String getMessage(Integer announcementId);

	@Query("select COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName from StaffBasicDetailsModel e "
			+ "left join AnnouncementModel a on a.staffId=e.staffId "
			+ "where a.announcementId=:announcementId ")
	String getNameOfSender(Integer announcementId);

	@Modifying
	@Transactional
	@Query("update AnnouncementModel a set a.publishFlag=1, a.publishDate=NOW()  where a.announcementId=:announcementId ")
	void updateFlah(Integer announcementId);

	
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ " COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),"
			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0), "
			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent,"
			+ "f.firstName,f.lastName,f.roleName ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "//announcementTypeId announcementTypeName
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId  and  (i.renewId is null or i.renewId=0) "
			+ "where  a.announcementId=:announcementId and a.schoolMasterModel.schoolid=:schoolId ")
	List<AnnouncementResponseBean> getAnnouncement(Integer announcementId, Integer schoolId);
	
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementBean(a.priority,a.otherSchoolId,a.whatAppSendingFlag,a.deadline,a.complaintBy) from AnnouncementModel a where a.schoolMasterModel.schoolid=:schoolid and a.announcementId=:announcementId ")
	AnnouncementBean getPriority(Integer announcementId, Integer schoolid);

	@Query(value="select new com.ingenio.announcement.bean.AnnouncementBean(a.priority,a.otherSchoolId,a.whatAppSendingFlag,a.deadline,a.complaintBy) from AnnouncementModel a where  a.announcementId=:announcementId ")
	AnnouncementBean getPriority(Integer announcementId);

//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
//			+ " COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
//			+ "COALESCE(e.designationModel.designationName,''),"
//			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
//			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0), "
//			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.assignDate,a.deadline,a.actuallyCompletionDate,a.complaintBy,a.completionStatus ) "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "//announcementTypeId announcementTypeName
//			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
//			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
//			+ "left join IcardMaster i on a.staffId=i.studId "
//			+ "where  a.schoolMasterModel.schoolid=:schoolId ")
//	List<AnnouncementResponseBean> getAnnouncementSchoolWise(Integer schoolId);


	
//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
//			+ " COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
//			+ "COALESCE(e.designationModel.designationName,''),"
//			+ "a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0),"
//			+ "COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),COALESCE(a.noticeSendingFlag,0), "
//			+ "COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.assignDate,a.deadline,a.actuallyCompletionDate,a.complaintBy,a.completionStatus ) "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "//announcementTypeId announcementTypeName
//			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
//			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
//			+ "left join IcardMaster i on a.staffId=i.studId "
//			+ "where  a.schoolMasterModel.schoolid=:schoolId and a.whatAppSendingFlag=:announcementCategory ")
//		
//	List<AnnouncementResponseBean> getAnnouncementSchoolWise(Integer schoolId, Integer announcementCategory);

	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),"
			+ "COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,"
			+ "COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),"
			+ "COALESCE(a.noticeSendingFlag,0), COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , "
			+ "COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.assignDate,a.deadline,a.actuallyCompletionDate,a.complaintBy,a.completionStatus,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where  a.schoolMasterModel.schoolid=:schoolid group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getAnnouncementSchoolWise(Integer schoolid);
	
	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''),COALESCE(a.announcementSendFlag,0) , "
			+ "COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "COALESCE(e.designationModel.designationName,''),COALESCE(b.announcementStatus,''),"
			+ "COALESCE(b.announcementAssignToId,0),a.schoolMasterModel.schoolid,"
			+ "COALESCE(e.sregNo,''),COALESCE(a.staffId,0),COALESCE(a.isWhatsappMsg,0),COALESCE(a.whatAppSendingFlag,0),"
			+ "COALESCE(a.noticeSendingFlag,0), COALESCE(a.isFeedback,0) , COALESCE(a.userCanComment,0) , "
			+ "COALESCE(a.publishFlag,0),COALESCE(i.imagePath,''),COALESCE(t.announcementTypeId,''),COALESCE(t.announcementTypeName,''),a.assignDate,a.deadline,a.actuallyCompletionDate,a.complaintBy,a.completionStatus,a.isSanstha,a.sansthakey,a.forPrincipal,a.forTeacher,a.forstudent) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementTypeModel t on t.announcementTypeId=a.announcementTypeModel.announcementTypeId "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "left join IcardMaster i on a.staffId=i.studId and (i.renewId is not null or i.renewId!=0) "
			+ "where  a.schoolMasterModel.schoolid=:schoolid and a.whatAppSendingFlag=:announcementCategory group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getAnnouncementSchoolWise(Integer schoolid, Integer announcementCategory);
	
	
	@Query("select a from AnnouncementModel a where a.whatAppSendingFlag=7 and  CURDATE()<=a.deadline and a.completionStatus=0 ")
	List<AnnouncementModel> getAllAnouncementForSendingRoutinMsg();
}
