package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.FeeSmsHistoryModel;

@Repository
public interface StudentSmsAssignedToRepository extends JpaRepository<FeeSmsHistoryModel, Integer>{
	
	@Query(value="SELECT COALESCE(MAX(a.sendSmsId),0)+1 from FeeSmsHistoryModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);
}
