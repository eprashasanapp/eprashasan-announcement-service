package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.FeedbackAnalyticsBean;
import com.ingenio.announcement.model.AnnouncementGroupModel;
import com.ingenio.announcement.model.AnnouncmenetQuestionsModel;
import com.ingenio.announcement.model.AnnouncmentOptionsOfQuestionsModel;

public interface AnnouncementQuestionRepository extends JpaRepository<AnnouncmenetQuestionsModel, Integer>{

	@Query(value="SELECT a from AnnouncmenetQuestionsModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId=:announcementId order by a.announcementQuestionsId  ")
	List<AnnouncmenetQuestionsModel> getAllQuestions(Integer schoolId, Integer announcementId);

	
	@Query("select new com.ingenio.announcement.bean.FeedbackAnalyticsBean(q.announcementQuestionsId,q.questions) "
			+ "from AnnouncmenetQuestionsModel q where q.announcementModel.announcementId=:announcementId and  q.schoolMasterModel.schoolid=:schoolId")
	List<FeedbackAnalyticsBean> FindByAnnouncementIdAndSchoolId(Integer announcementId, Integer schoolId);

	




	

	
}
