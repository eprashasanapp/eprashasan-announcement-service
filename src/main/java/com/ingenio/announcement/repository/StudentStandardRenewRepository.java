package com.ingenio.announcement.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.format.annotation.DateTimeFormat;

import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.StudentBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.bean.StudentStandardRenewBean;
import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.StandardMasterModel;
import com.ingenio.announcement.model.StudentStandardRenewModel;
import com.ingenio.announcement.model.YearMasterModel;

public interface StudentStandardRenewRepository extends JpaRepository<StudentStandardRenewModel, Integer>{

	@Query(value="select a from StudentStandardRenewModel a where a.alreadyRenew='1' and a.yearMasterModel.yearId=?1 and a.standardMasterModel.standardId=?2 "
			+ "and a.divisionMasterModel.divisionId=?3 and a.schoolMasterModel.schoolid=?4  AND ( a.collegeLeavingDate =''  OR a.collegeLeavingDate is null) order by a.rollNo ")
	List<StudentStandardRenewModel> findStudent(Integer yearId,Integer standardId, Integer divisionId,Integer schoolId);
	

	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean(COALESCE(a.renewStudentId,0),"
	+ "COALESCE(CONCAT(COALESCE(a.studentMasterModel.studFName,''),' ',COALESCE(a.studentMasterModel.studMName,''),' ',"
	+ "COALESCE(a.studentMasterModel.studLName,'')),'') as studentName ,"
	+ "COALESCE(a.rollNo,0),COALESCE(a.studentMasterModel.studentRegNo,''), "
	+ "COALESCE(a.studentMasterModel.studentId,0), COALESCE(a.studentMasterModel.grBookName.grBookId,0)) "
	+ "from StudentStandardRenewModel a "
	+ "where a.alreadyRenew='1' and a.yearMasterModel.yearId=?1 and a.standardMasterModel.standardId=?2  "  
	+ "and a.divisionMasterModel.divisionId=?3 and a.schoolMasterModel.schoolid=?4  AND ( a.collegeLeavingDate =''  OR a.collegeLeavingDate is null) AND (a.isOnlineAdmission='0' OR  (a.isOnlineAdmission='1' AND a.isApproval='1') OR (a.isRegistration='1' AND a.isRegistrationApprovedFlag='1')) order by a.rollNo  ")
	List<StudentDetailsBean> findStudent1(Integer yearId,Integer standardId, Integer divisionId,Integer schoolId);


	
	@Query(value="select new com.ingenio.announcement.bean.StudentStandardRenewBean(COALESCE(a.renewStudentId,0),"
	+ "COALESCE(a.standardMasterModel.standardId,0),COALESCE(a.divisionMasterModel.divisionId,0),COALESCE(a.studentMasterModel.mmobile,'0') ) "
	+ "from StudentStandardRenewModel a "	
	+ " where a.alreadyRenew='1' and a.yearMasterModel.yearId=:yearId "
	+"and a.divisionMasterModel.divisionId IN (:divisionNames) and a.schoolMasterModel.schoolid=:schoolId  AND ( a.collegeLeavingDate =''  OR a.collegeLeavingDate is null) and a.isCatlogSearch='0' order by a.rollNo ") 
	List<StudentStandardRenewBean> findStudentbydivision(Integer yearId, Integer schoolId, List<Integer> divisionNames);

	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean(COALESCE(a.renewStudentId,0),"
			+ "COALESCE(CONCAT(COALESCE(a.studentMasterModel.studFName,''),' ',COALESCE(a.studentMasterModel.studMName,''),' ',"
			+ "COALESCE(a.studentMasterModel.studLName,'')),'') as studentName ,"
			+ "COALESCE(a.rollNo,0),COALESCE(a.studentMasterModel.studentRegNo,''), "
			+ "COALESCE(a.studentMasterModel.studentId,0), COALESCE(a.studentMasterModel.birthDate,'NA'),COALESCE(a.studentMasterModel.mmobile,'0')) "
			+ "from StudentStandardRenewModel a "
			+ "where a.alreadyRenew='1' and a.schoolMasterModel.schoolid=:schoolid  AND (( a.collegeLeavingDate =''  OR a.collegeLeavingDate is null) OR (str_to_date(a.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:date,'%d-%m-%Y'))) "
			+ "AND (a.isOnlineAdmission='0' OR  (a.isOnlineAdmission='1' AND a.isApproval='1') OR (a.isRegistration='1' AND a.isRegistrationApprovedFlag='1')) order by a.rollNo  ")
	List<StudentDetailsBean> getstudentBirthdayList(Integer schoolid, String date);

	@Query(value="select new com.ingenio.announcement.bean.StudentStandardRenewBean(COALESCE(a.renewStudentId,0),"
			+ "COALESCE(a.standardMasterModel.standardId,0),COALESCE(a.divisionMasterModel.divisionId,0),COALESCE(a.studentMasterModel.mmobile,'0')) "
			+ "from StudentStandardRenewModel a "	
			+ " where a.alreadyRenew='1'  "
			+"and a.renewStudentId IN (:studentRenewId) and a.schoolMasterModel.schoolid=:schoolId  AND ( a.collegeLeavingDate =''  OR a.collegeLeavingDate is null) and a.isCatlogSearch='0' order by a.rollNo ")
	List<StudentStandardRenewBean> findStudentbyRenewId( Integer schoolId, List<Integer> studentRenewId);

	
	@Query("select new com.ingenio.announcement.bean.StudentBean(a.studentMasterModel.studentId,app.appUserRoleId) from StudentStandardRenewModel a "
			+ "left join StudentMasterModel st on st.studentId=a.studentMasterModel.studentId "
			+ "left join AppUserModel app on app.staffId=a.studentMasterModel.studentId AND app.roleName='ROLE_PARENT1' "
			+ "where a.standardMasterModel=:standard AND a.divisionMasterModel = :division AND a.yearMasterModel = :year AND "
			+ "   ( (a.isOnlineAdmission = 1 and a.isApproval = 1) or (a.isOnlineAdmission = 0) ) and (STR_TO_DATE(CONCAT(a.renewAdmissionDate, ' 00:00:00'), '%d-%m-%Y %H:%i:%s') <= :publishDate ) and "
			+ "(STR_TO_DATE(CONCAT(a.collegeLeavingDate, ' 00:00:00'), '%d-%m-%Y %H:%i:%s') >=:publishDate or "
			+ "a.collegeLeavingDate is null or a.collegeLeavingDate='' ) and st.isAdmissionCancelled=1  group by app.staffId ")
	List<StudentBean> getAllStudents(StandardMasterModel standard, DivisionMasterModel division,
			YearMasterModel year,@DateTimeFormat(pattern = "dd-MM-yy") Date publishDate);


	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(a.studentMasterModel.studentId,app.appUserRoleId"
			+ ",st.mmobile,concat(COALESCE(st.studInitialName,''),' ',COALESCE(st.studFName,''),' ',COALESCE(st.studMName,''),' ',COALESCE(st.studLName,'')),st.memail,"
			+ "studfcm.token,st.studentRegNo,std.standardId,std.standardName,div.divisionId,div.divisionName,"
			+ " coalesce((select CASE WHEN s.viewsId IS NOT NULL THEN 1 ELSE 0 END from AnnouncementViewsModel s where s.announcementModel.announcementId=:announcementId and  s.appUserModel.appUserRoleId=app.appUserRoleId),''),COALESCE(t.likeDislike,''),"
			+ "( select CASE WHEN count(u.comment) > 0 THEN 1 ELSE 0 END from AnnouncementCommentModel u where u.announcementModel.announcementId=:announcementId and  u.appUserModel.appUserRoleId=app.appUserRoleId ),"
			+ "( select CASE WHEN COUNT(v.reactionId) > 0 THEN 1 ELSE 0 END from AnnouncementReactionModel v where v.announcementModel.announcementId=:announcementId and v.appUserModel.appUserRoleId=app.appUserRoleId)) from StudentStandardRenewModel a "
			+ "left join StudentMasterModel st on st.studentId=a.studentMasterModel.studentId "
			+ "left join AppUserModel app on app.staffId=a.studentMasterModel.studentId AND app.roleName='ROLE_PARENT1' "
			+ "left join AndroidFCMTokenMaster studfcm on studfcm.notificationUserId=app.appUserRoleId "
		
			+ "left join AndroidFCMTokenMaster studfcm on studfcm.notificationUserId=app.appUserRoleId "
			+ "left join StandardMasterModel std on std.standardId=a.standardMasterModel.standardId "
			+ "left join DivisionMasterModel div on div.divisionId=a.divisionMasterModel.divisionId  "
			+ "left join AnnouncementLikeorDislikeModel t on t.announcementModel.announcementId=:announcementId and t.appUserModel.appUserRoleId=app.appUserRoleId "

			+ "where a.standardMasterModel.standardId=:standardId AND a.divisionMasterModel.divisionId = :divisionId AND a.yearMasterModel.yearId = :yearId AND "
			+ "   ( (a.isOnlineAdmission = 1 and a.isApproval = 1) or (a.isOnlineAdmission = 0) ) and (STR_TO_DATE(CONCAT(a.renewAdmissionDate, ' 00:00:00'), '%d-%m-%Y %H:%i:%s') <= :publishDate ) and "
			+ "(STR_TO_DATE(CONCAT(a.collegeLeavingDate, ' 00:00:00'), '%d-%m-%Y %H:%i:%s') >=:publishDate or "
			+ "a.collegeLeavingDate is null or a.collegeLeavingDate='' ) and a.schoolMasterModel.schoolid=:schoolid and st.isAdmissionCancelled=1 group by st.studentId ")
	List<PhoneNumberBean> getStaffStudentReport(Integer schoolid, Integer yearId, Integer standardId,
			Integer divisionId,String publishDate,Integer announcementId );
	

	
}
