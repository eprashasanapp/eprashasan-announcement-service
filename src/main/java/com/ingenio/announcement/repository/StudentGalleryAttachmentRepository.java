package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.GalleryAttachmentModel;
import com.ingenio.announcement.model.GalleryModel;

@Repository
public interface StudentGalleryAttachmentRepository extends JpaRepository<GalleryAttachmentModel, Integer> {

	List<GalleryAttachmentModel> findByGalleryModel(GalleryModel studentAssignGalleryModel);
	
	void deleteByGalleryModel(GalleryModel model);
}
