package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.FeeReceiptDetailsBean;
import com.ingenio.announcement.model.FeeReceiptEditLogModel;

public interface FeeReceiptEditLogRepository extends JpaRepository<FeeReceiptEditLogModel, Integer> {

//	@Query("select * from ")
//	void getAllReceiptData(Integer schoolId);

	
//	@Query("select new com.ingenio.fee.bean.FeeReceiptDetailsBean(COALESCE(b.studentRegNo,''),b.studentId,COALESCE(b.studInitialName,''),COALESCE(b.studFName,''),"
//			+ "COALESCE(b.studMName,''),COALESCE(b.studLName,''),COALESCE(b.studGender,''),COALESCE(c.castName,''),"
//			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.medium,''),COALESCE(b.mmobile,''),COALESCE(b.birthDate,''),"
//			+ "COALESCE(b.birthPlace,''),COALESCE(b.nationlity,''),COALESCE(b.admissionDate,''),f.renewStudentId,b.grBookName.grBookId,"
//			+ "COALESCE(b.fatherName,''),COALESCE(b.motherName,''),COALESCE(st.standardName,''),"
//			+ "COALESCE(h.divisionName,''),COALESCE(f.rollNo,0),COALESCE(f.standardMasterModel.standardId,0),"
//			+ "COALESCE(f.isOnlineAdmission,0),COALESCE(f.onlineAdmissionApprovalFlag,0),COALESCE(f.renewStudentId,0),COALESCE(h.divisionId,0), "
//			+ "0,0,'NA' "
//			+ ",j.yearMaster.yearId,j.yearMaster.year,0.0,0) "
//			+ "from FeeReceiptModel j  "
//			//+ " left join FeeSettingModel i on i.feeSettingId=j.feeSettingModel.feeSettingId and i.yearMasterModel.yearId=:yearId "
//			+ "left join StudentStandardRenewModel f on "
//			+ "f.studentMasterModel.studentId=j.studentId and f.yearMasterModel.yearId=j.yearMaster.yearId "
//			+ "and f.isDel='0' "
//			+ "left join StandardMasterModel st on st.standardId=f.standardMasterModel.standardId "
//
//			+ "left join DivisionMasterModel h on f.divisionMasterModel.divisionId=h.divisionId   "
//			+ "left join StudentMasterModel b on b.studentId=j.studentId "
//			+ "left join CasteMasterModel c on c.casteId=b.castemaster.casteId and c.isDel='0' "
//			+ "left join CategoryMasterModel d on d.categoryId=b.category.categoryId and d.isDel='0'  "
//			+ "left join ReligionMasterModel e on e.religionId=b.religion.religionId and e.isDel='0'  "
//			+ "where j.receiptId=:receiptId  ")
//	FeeReceiptDetailsBean getStudentInfoWithoutStudId(Integer receiptId);

}
