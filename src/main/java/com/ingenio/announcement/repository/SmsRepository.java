package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.model.FeeSendSmsNumbersMstrModel;
import com.ingenio.announcement.model.FeeSmsHistoryModel;

@Repository
public interface SmsRepository extends JpaRepository<FeeSmsHistoryModel, Integer> {

	@Query("select a from FeeSendSmsNumbersMstrModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' ")
	List<FeeSendSmsNumbersMstrModel> getSmsMasterList(Integer schoolId);

	@Transactional
	@Modifying
	@Query("update FeeSmsHistoryModel a Set a.deliveryStatus=:status where  a.isDel='0' and a.sendSmsId=:smssaveId2  ")
	void updateStatus(String status, Integer smssaveId2);



	@Transactional
	@Modifying
	@Query("update FeeSmsHistoryModel a Set a.smsId=:msgid where  a.isDel='0' and a.sendSmsId=:smssaveId2  ")
	void updateMsgId(String msgid, Integer smssaveId2);

  }
