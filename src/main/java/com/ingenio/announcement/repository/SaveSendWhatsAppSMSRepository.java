package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.SaveSendWhatsAppSMSModel;

public interface SaveSendWhatsAppSMSRepository extends JpaRepository<SaveSendWhatsAppSMSModel, Integer>{

	@Transactional
	@Modifying
	@Query(value="update SaveSendWhatsAppSMSModel a set a.compaignStatusFlag=:statusFlag where a.announcementModel.announcementId=:announcementId and a.renewStaffId=:renewId ")
	void updateFeeStatus(Integer announcementId, Integer renewId, int statusFlag);

}
