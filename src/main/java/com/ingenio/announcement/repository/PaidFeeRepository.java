package com.ingenio.announcement.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.FeePaidFeeModel;


public interface PaidFeeRepository extends JpaRepository<FeePaidFeeModel, Integer> {

	@Query(value="SELECT COALESCE(MAX(a.paidId),0)+1 from FeePaidFeeModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxPaidId(Integer schoolId);


	@Transactional
	@Modifying
	@Query("update FeePaidFeeModel a set a.paidfeeDate=:newDate where a.feeReceiptModel.receiptId=:receiptId  ")
	void updatePaidDateAgainstReceiptId(Integer receiptId, String newDate);


	@Transactional
	@Modifying
	@Query("update FeePaidFeeModel a set a.payFee=:paidFee where a.paidId=:paidId  ")
	void updatePaidFee(Integer paidId, Double paidFee);

}
