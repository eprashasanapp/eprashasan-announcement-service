package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.GalleryAssignToModel;
import com.ingenio.announcement.model.GalleryModel;

@Repository
public interface StudentGalleryAssignedToRepository extends JpaRepository<GalleryAssignToModel, Integer>{

	GalleryAssignToModel findByGalleryAssignToId(Integer announcementAssignToId);
	
	@Transactional
	void deleteByGalleryModel(GalleryModel model);

}
