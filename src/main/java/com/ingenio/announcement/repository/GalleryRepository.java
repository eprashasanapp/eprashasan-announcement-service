package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.bean.GalleryResponseBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.GalleryModel;

public interface GalleryRepository extends JpaRepository<GalleryModel, Integer>{
	
	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.standardId,0),COALESCE(a.standardName,''),COALESCE(b.divisionId,0),COALESCE(b.divisionName,'')) from StandardMasterModel a "
			+ "left join DivisionMasterModel b on ( a.standardId=b.standardMasterModel.standardId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid)  where a.schoolMasterModel.schoolid=:schoolId order by a.standardPriority , b.divisionId ")
	List<StandardDivisionBean> getStandardDivisionList(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.galleryId),0)+1 from GalleryModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.galleryAssignToId),0)+1 from GalleryAssignToModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAssignToId(Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.GalleryResponseBean(a.galleryId,COALESCE(a.galleryMessage,''),"
			+ "COALESCE(a.galleryTitle,''),a.startDate, "
			+ "COALESCE((select count(a1) from GalleryAssignToModel a1 where a1.galleryModel.galleryId=a.galleryId group by a1.galleryModel.galleryId),0),"
			+ "COALESCE((select count(a1) from GalleryAssignToModel a1 where a1.galleryStatus IN (1,2,3) and a1.galleryModel.galleryId=a.galleryId group by a1.galleryModel.galleryId),0),"
			+ "(COALESCE((select count(a1) from GalleryAssignToModel a1 where a1.galleryStatus IN (1,2,3) and a1.galleryModel.galleryId=a.galleryId group by a1.galleryModel.galleryId),0) - "
			+ "COALESCE((select count(a1) from GalleryAssignToModel a1 where a1.galleryStatus IN (2,3) and a1.galleryModel.galleryId=a.galleryId group by a1.galleryModel.galleryId),0)),"
			+ "COALESCE((select count(a1) from GalleryAssignToModel a1 where a1.galleryStatus IN (2,3) and a1.galleryModel.galleryId=a.galleryId group by a1.galleryModel.galleryId),0),"
			+ "COALESCE((select count(a1) from GalleryAssignToModel a1 where a1.galleryStatus=3 and a1.galleryModel.galleryId=a.galleryId group by a1.galleryModel.galleryId),0),"
			+ "COALESCE((select count(c1.galleryAttachmentId) from GalleryAttachmentModel c1 where c1.galleryModel.galleryId=a.galleryId group by c1.galleryModel.galleryId),0),COALESCE(a.galleryTime,'') )  "
			+ "from GalleryModel a "
			+ "left join GalleryAssignToModel b on b.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join GalleryAttachmentModel c on c.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "where a.staffId=?1 and b.yearMasterModel.yearId=?2 and a.galleryId=?3 "
			+ "group by a.galleryId  order by a.CDate desc ")
	List<GalleryResponseBean> findAssignedGalleryCount(Integer staffId, Integer yearId,Integer galleryId);



	@Query(value="select new com.ingenio.announcement.bean.GalleryResponseBean(a.galleryId,COALESCE(a.galleryMessage,''),COALESCE(a.galleryTitle,''),"
			+ "a.startDate, "
			+ "(select COALESCE(count(c1.galleryAttachmentId),0) from GalleryAttachmentModel c1 "
			+ "where c1.galleryModel.galleryId=a.galleryId "
			+ "group by c1.galleryModel.galleryId),COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "f.roleName,b.galleryStatus,b.galleryAssignToId,a.schoolMasterModel.schoolid,e.sregNo,a.staffId,COALESCE(a.galleryTime,'') ) "
			+ "from GalleryModel a "
			+ "left join GalleryAssignToModel b on b.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join GalleryAttachmentModel c on c.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and e.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid  "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole) OR a.staffId=:staffStudentId) and b.yearMasterModel.yearId=:yearId group by a.galleryId order by a.CDate desc ")
	List<GalleryResponseBean> getStudentGallery(Integer staffStudentId, Integer yearId, String profileRole,Pageable pageableo);

	@Query(value="SELECT coalesce(MAX(a.galleryAttachmentId),0)+1 from GalleryAttachmentModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAttachmentId(Integer schoolId);
	
	@Query(value="select new com.ingenio.announcement.bean.GalleryResponseBean(COALESCE(a.serverFilePath,''),COALESCE(a.galleryAttachmentId,0)) from GalleryAttachmentModel a where a.galleryModel.galleryId=:galleryId ")
	List<GalleryResponseBean> getAttachments(Integer galleryId);
	
	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.standardId,0),COALESCE(a.standardName,''),COALESCE(b.divisionId,0),COALESCE(b.divisionName,'')) from StandardMasterModel a "
			+ "left join DivisionMasterModel b on a.standardId=b.standardMasterModel.standardId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join GalleryAssignToModel c on a.standardId=c.standardMasterModel.standardId  and b.divisionId=c.divisionMasterModel.divisionId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid   "
			+ "left join GalleryModel d on c.galleryModel.galleryId=d.galleryId and a.schoolMasterModel.schoolid=d.schoolMasterModel.schoolid  "
			+ "where a.schoolMasterModel.schoolid=:schoolId  and d.staffId=:staffId group by a.standardId , b.divisionId ")
	List<StandardDivisionBean> getdivisionWiseFilterList(Integer schoolId, Integer staffId);
	
	
	@Query(value="select new com.ingenio.announcement.bean.GalleryResponseBean(a.galleryId,COALESCE(a.galleryMessage,''),COALESCE(a.galleryTitle,''),"
			+ "a.startDate, "
			+ "(select COALESCE(count(c1.galleryAttachmentId),0) from GalleryAttachmentModel c1 "
			+ "where c1.galleryModel.galleryId=a.galleryId "
			+ "group by c1.galleryModel.galleryId),COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "f.roleName,b.galleryStatus,b.galleryAssignToId,a.schoolMasterModel.schoolid,e.sregNo,a.staffId,COALESCE(a.galleryTime,'') ) "
			+ "from GalleryModel a "
			+ "left join GalleryAssignToModel b on b.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join GalleryAttachmentModel c on c.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and e.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid  "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "where ((b.staffStudentId=:staffId and b.role=:profileRole) OR a.staffId=:staffId) "
			+ "and b.yearMasterModel.yearId=:yearId and  b.standardMasterModel.standardId In (:standardList) and "
			+ "b.divisionMasterModel.divisionId In (:divisionList) "
			+ "group by a.galleryId order by a.CDate desc ")
	List<GalleryResponseBean> getStudentGallery(Integer staffId, Integer yearId, String profileRole, Pageable pageableo,
			List<Integer> standardList, List<Integer> divisionList);
	
	
	@Query(value="select new com.ingenio.announcement.bean.GalleryResponseBean(a.galleryId,COALESCE(a.galleryMessage,''),COALESCE(a.galleryTitle,''),"
			+ "a.startDate, "
			+ "(select COALESCE(count(c1.galleryAttachmentId),0) from GalleryAttachmentModel c1 "
			+ "where c1.galleryModel.galleryId=a.galleryId "
			+ "group by c1.galleryModel.galleryId),COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
			+ "f.roleName,b.galleryStatus,b.galleryAssignToId,a.schoolMasterModel.schoolid,e.sregNo,a.staffId,COALESCE(a.galleryTime,'') ) "
			+ "from GalleryModel a "
			+ "left join GalleryAssignToModel b on b.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join GalleryAttachmentModel c on c.galleryModel.galleryId=a.galleryId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and e.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid  "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "where ((b.staffStudentId=:studentStaffId and b.role=:profileRole) OR a.staffId=:studentStaffId) and b.yearMasterModel.yearId=:yearId and a.galleryId=:galleryId group by a.galleryId order by a.CDate desc ")
	List<GalleryResponseBean> getMyGalleryPostDetails(Integer galleryId, Integer yearId, Integer studentStaffId, String profileRole);

	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( c.renewStudentId,d.studentId,"
			+ "CONCAT(COALESCE(d.studInitialName,''),' ',COALESCE(d.studFName,''),' ',COALESCE(d.studMName,''),' ',COALESCE(d.studLName,'')), "
			+ "c.rollNo,b.galleryStatus,d.studentRegNo,d.schoolMasterModel.schoolid,d.grBookName.grBookId ) from GalleryModel a "
			+ "left join GalleryAssignToModel b on a.galleryId=b.galleryModel.galleryId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  "
			+ "left join StudentStandardRenewModel c on c.renewStudentId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StudentMasterModel d on d.studentId=c.studentMasterModel.studentId and a.schoolMasterModel.schoolid=d.schoolMasterModel.schoolid  "
			+ "where a.galleryId=?1 and b.role='Parent' and  d.isApproval='0' and d.isAdmissionCancelled='1' order by b.galleryStatus,c.rollNo  ")
	List<StudentDetailsBean> getStudentDetailsForGallery(Integer galleryId);
	
	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( c.staffId,"
			+ "CONCAT(COALESCE(c.initialname,''),' ',COALESCE(c.firstname,''),' ',COALESCE(c.secondname,''),' ',COALESCE(c.lastname,'')), "
			+ "b.galleryStatus,c.sregNo,c.schoolMasterModel.schoolid) from GalleryModel a "
			+ "left join GalleryAssignToModel b on a.galleryId=b.galleryModel.galleryId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid   "
			+ "left join StaffBasicDetailsModel c on c.staffId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "where a.galleryId=?1 and b.role='Teacher' order by b.galleryStatus,c.staffId  ")
	List<StudentDetailsBean> getStaffDetailsForGallery(Integer galleryId);

	
	@Transactional
	@Modifying
	@Query("update GalleryModel a Set a.gallerySendFlag =:i  where  a.galleryId=:galleryId  ")
	void updateGallerySentFlag(Integer i, Integer galleryId);
	
}
