package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementReactionModel;


public interface AnnouncementReactionRepository extends JpaRepository<AnnouncementReactionModel, Integer>{

	@Query(value="SELECT coalesce(MAX(a.reactionId),0)+1 from AnnouncementReactionModel a where a.schoolMasterModel.schoolid= :schoolid ")
	String getMaxId(Integer schoolid);

	@Query(value="SELECT a from AnnouncementReactionModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcmenetQuestionsModel.announcementQuestionsId=:announcementQuestionsId order by a.reactionId  ")
	List<AnnouncementReactionModel> getOptionId(Integer schoolId, Integer announcementQuestionsId);

	@Query(value="SELECT a from AnnouncementReactionModel a "
			+ " where a.schoolMasterModel.schoolid=:schoolId"
			+ " and a.announcmenetQuestionsModel.announcementQuestionsId=:announcementQuestionsId and a.appUserModel.appUserRoleId=:appUserId order by a.reactionId  ")
	List<AnnouncementReactionModel> getOptionId(Integer schoolId, Integer announcementQuestionsId, Integer appUserId);

	@Query("select count(r.reactionId) from AnnouncementReactionModel r "
			+ "where r.announcmenetQuestionsModel.announcementQuestionsId=:questionId and r.announcmentOptionsOfQuestionsModel.announcementOptionsofQuestionsId=:optionId")
	Integer getCount(Integer questionId, Integer optionId);

	@Query("select count(distinct r.appUserModel.appUserRoleId) from AnnouncementReactionModel r "
			+ "where r.announcementModel.announcementId=:announcementId and r.schoolMasterModel.schoolid=:schoolId")
	Integer getTotalUser(Integer announcementId, Integer schoolId);

}
