package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.GalleryAttachmentModel;
import com.ingenio.announcement.model.GalleryModel;

public interface GalleryAttachmentRepository extends JpaRepository<GalleryAttachmentModel, Integer> {

	List<GalleryAttachmentModel> findByGalleryModel(GalleryModel galleryModel);

}
