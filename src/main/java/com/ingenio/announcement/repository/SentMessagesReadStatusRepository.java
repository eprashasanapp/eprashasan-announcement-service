package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;

public interface SentMessagesReadStatusRepository extends JpaRepository<SentMessagesReadStatusModel, Integer>{

	//List<SentMessagesModel> findBySentMessageId(String messageSavedId);

	//List<SentMessagesReadStatusModel> findBySentMessageId(Integer sentMessageId);

	List<SentMessagesReadStatusModel> findBySentMessageIdOrderBySentMessageIdDesc(Integer sentMessageId);

	List<SentMessagesReadStatusModel> findBySentMessageId(Integer id);

}
