package com.ingenio.announcement.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.BannerImages;
import com.ingenio.announcement.bean.EventTypeBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.model.EventModel;
import com.ingenio.announcement.model.EventTypeMasterModel;

public interface EventTypeRepository extends JpaRepository<EventTypeMasterModel, Integer>{

	
	@Query(value="select new com.ingenio.announcement.bean.EventTypeBean(COALESCE(a.eventTypeId,0),COALESCE(a.eventType,''),"
			+ "COALESCE(a.eventTypeColor,'')) from EventTypeMasterModel a ")
	List<EventTypeBean> getAllEventTypes();

	
	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.standardId,0),COALESCE(a.standardName,'')) "
			+ " from StandardMasterModel a  where a.schoolMasterModel.schoolid=:schoolId order by a.standardPriority ")
	List<StandardDivisionBean> getAllStandardsList(Integer schoolId);

	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.divisionName,''),COALESCE(a.divisionId,0)) "
			+ " from DivisionMasterModel a  where a.standardMasterModel.standardId =:standardId  ")
	List<StandardDivisionBean> getAllDivisionList(Integer standardId);

	@Query(value="select new com.ingenio.announcement.bean.BannerImages(COALESCE(a.bannerImagesId,0),COALESCE(a.fileName,''),"
			+ " COALESCE(a.imagePath,''),COALESCE(a.yearName,''),COALESCE(a.monthId,0) ) "
			+ " from BannerImagesModel a  where a.schoolMasterModel.schoolid=:schoolId and a.yearMasterModel.yearId=:yearId "
			+ "and   a.yearName =:yearName and a.monthId=:monthId  ")
	List<BannerImages> getAllBannerList(Integer schoolId, Integer yearId, String yearName, Integer monthId);
	
	
}
