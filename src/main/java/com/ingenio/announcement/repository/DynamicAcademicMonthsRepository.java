package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.DynamicAcademicMonthsModel;
import com.ingenio.announcement.model.SchoolMasterModel;

@Repository
public interface DynamicAcademicMonthsRepository extends JpaRepository<DynamicAcademicMonthsModel, Integer>{
	
	List<DynamicAcademicMonthsModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

}
