package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.CountBean;
import com.ingenio.announcement.model.DivisionMasterModel;

public interface DivisionMasterRepository extends JpaRepository<DivisionMasterModel, Integer>{

	@Query(value="select a.divisionName from DivisionMasterModel a where a.divisionId=:divisionId ")
	String getDivision(Integer divisionId);

	@Query(value="select new com.ingenio.announcement.bean.CountBean(a.divisionId||'',a.divisionName,a.standardMasterModel.standardId) from DivisionMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.standardMasterModel.standardId=:standardId ")
	List<CountBean> getAllDivisions(Integer schoolId,Integer standardId);

}
