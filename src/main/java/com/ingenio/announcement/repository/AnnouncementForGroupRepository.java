package com.ingenio.announcement.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.bean.AnnouncementResponseBean;
import com.ingenio.announcement.bean.PhoneNumberBean;
import com.ingenio.announcement.bean.StandardDivisionBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.AnnouncementGroupModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AnnouncementUserToGroupMappingModel;
import com.ingenio.announcement.model.AnnouncmenetForGroupModel;
import com.ingenio.announcement.model.StaffBasicDetailsModel;

public interface AnnouncementForGroupRepository extends JpaRepository<AnnouncmenetForGroupModel, Integer>{

	@Query(value="SELECT a.announcementModel.announcementId from AnnouncmenetForGroupModel a where a.announcementForGroupId=:groupId ")
	List<Integer> getAllPosts(Integer groupId);

	@Query(value="SELECT a.announcementGroupModel.announcementGroupId from AnnouncmenetForGroupModel a where a.announcementModel.announcementId=:postId ")
	List<Integer> getGroupId(Integer postId);

	@Query(value="SELECT a.announcementModel.announcementId from AnnouncmenetForGroupModel a where a.announcementGroupModel.announcementGroupId=:groupId ")
	List<Integer> getPostIds(Integer groupId);

	void deleteByAnnouncementModel(AnnouncementModel announcementModel);

	@Query(value="SELECT a.appUserRoleId from AppUserModel a where a.schoolid=:schoolId and a.roleName='ROLE_PRINCIPAL' ")
	Integer getprincipalUserId(Integer schoolId);

//	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,case when staff.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') THEN 'Staff' WHEN stud.roleName = 'ROLE_PARENT1' THEN 'Student' END,c.mmobile,c.studFName,b.firstname,staff.appUserRoleId as staffUserId,stud.appUserRoleId as studUserId) from AnnouncmenetForGroupModel a "
//			+ "left join AnnouncementUserToGroupMappingModel z on a.announcementForGroupId=z.announcementGroupModel.announcementForGroupId "
//			+ "left join AppUserModel staff on staff.appUserRoleId=z.appUserModel.appUserRoleId AND (staff.roleName='ROLE_TEACHINGSTAFF' or staff.roleName='ROLE_PRINCIPAL' ) "
//			+ "left join AppUserModel stud on stud.appUserRoleId=z.appUserModel.appUserRoleId and stud.roleName='ROLE_PARENT1' "
//			+ "left join StudentMasterModel c on c.studentId=stud.staffId   "
//			+ "left join StaffBasicDetailsModel b on b.staffId=staff.staffId "
//			+ "where a.announcementModel.announcementId=:announcementId ")
//	List<PhoneNumberBean> getContactNumbersFromGroups(Integer announcementId);
//	
	@Query("select new com.ingenio.announcement.bean.PhoneNumberBean(b.contactnosms,b.staffId,c.studentId,case when st.roleName IN ('ROLE_TEACHINGSTAFF', 'ROLE_PRINCIPAL') THEN 'Staff' WHEN st.roleName = 'ROLE_PARENT1' THEN 'Student' END,c.mmobile,c.studFName,b.firstname,st.appUserRoleId as staffUserId,st.appUserRoleId as studUserIdcase,case when c.studentId>0 then c.memail when b.staffId>0 then b.femailId end) from AnnouncmenetForGroupModel a "
			+ "left join AnnouncementUserToGroupMappingModel z on a.announcementGroupModel.announcementGroupId=z.announcementGroupModel.announcementGroupId "
			+ "left join AppUserModel st on st.appUserRoleId=z.appUserModel.appUserRoleId  "
			//+ "left join AppUserModel stud on stud.appUserRoleId=z.appUserModel.appUserRoleId and stud.roleName='ROLE_PARENT1' "
			+ "left join StudentMasterModel c on c.studentId=st.staffId  and st.roleName='ROLE_PARENT1' "
			+ "left join StaffBasicDetailsModel b on b.staffId=st.staffId and (st.roleName='ROLE_TEACHINGSTAFF' or st.roleName='ROLE_PRINCIPAL') "
			+ "where a.announcementModel.announcementId=:announcementId group by c.studentId ")
	List<PhoneNumberBean> getContactNumbersFromGroups(Integer announcementId);
	

	
	
}
