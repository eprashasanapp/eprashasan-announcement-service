package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.OptionCountBean;
import com.ingenio.announcement.model.AnnouncmentOptionsOfQuestionsModel;

public interface AnnouncementOptionsForQuestionsRepository extends JpaRepository<AnnouncmentOptionsOfQuestionsModel, Integer>{

	@Query(value="SELECT a from AnnouncmentOptionsOfQuestionsModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and "
			+ " a.announcmenetQuestionsModel.announcementQuestionsId=:announcementQuestionsId order by a.announcementOptionsofQuestionsId  ")
	List<AnnouncmentOptionsOfQuestionsModel> getAllOptions(Integer schoolId, Integer announcementQuestionsId);

	@Query("select new com.ingenio.announcement.bean.OptionCountBean(o.announcementOptionsofQuestionsId,o.options) from  AnnouncmentOptionsOfQuestionsModel o  "
			+ "where o.announcementModel.announcementId=:announcementId and o.schoolMasterModel.schoolid=:schoolId and o.announcmenetQuestionsModel.announcementQuestionsId=:questionId")
	List<OptionCountBean> getOptins(Integer questionId, Integer announcementId, Integer schoolId);

	

	
}
