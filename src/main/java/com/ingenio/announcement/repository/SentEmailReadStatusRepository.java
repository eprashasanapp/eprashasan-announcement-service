package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.SentEmailReadStatusModel;

public interface SentEmailReadStatusRepository extends JpaRepository<SentEmailReadStatusModel, Integer>{

	List<SentEmailReadStatusModel> findBySentEmailId(Integer id);

}
