package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.EventCalendarAssignToModel;
import com.ingenio.announcement.model.EventModel;

public interface EventCalendarRepository extends JpaRepository<EventCalendarAssignToModel, Integer>{
	
	
	@Transactional
	void deleteByEventModel(EventModel model);
	




}
