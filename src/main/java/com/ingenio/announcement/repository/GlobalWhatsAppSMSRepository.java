package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.GlobalWhatsAppSMSModel;
import com.ingenio.announcement.model.SchoolWiseWhatsAppSMSModel;


public interface GlobalWhatsAppSMSRepository extends JpaRepository<GlobalWhatsAppSMSModel, Integer>{

	@Query("select a from SchoolWiseWhatsAppSMSModel a where a.schoolMasterModel.schoolid=:schoolId ")
	List<SchoolWiseWhatsAppSMSModel> getSchoolWiseSetting(Integer schoolId);

	@Query("select a from GlobalWhatsAppSMSModel a where a.language=:languageId ")
	List<GlobalWhatsAppSMSModel> getMessageSettingByLanguage(Integer languageId);

	@Transactional
	@Modifying
	@Query("update SchoolWiseWhatsAppSMSModel a Set a.assignMsgCount =:totalCount  where  a.schoolMasterModel.schoolid=:schoolid  ")
	void updateSchoolAssignMessage(Integer schoolid, String totalCount);

}
