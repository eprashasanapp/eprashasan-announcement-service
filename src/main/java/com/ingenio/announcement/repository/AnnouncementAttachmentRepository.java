package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementAttachmentModel;

public interface AnnouncementAttachmentRepository extends JpaRepository<AnnouncementAttachmentModel, Integer> {

	@Query(value="select a from AnnouncementAttachmentModel a where a.announcementModel.announcementId=:saveId")
	List<AnnouncementAttachmentModel> getAttachment(Integer saveId);

}
