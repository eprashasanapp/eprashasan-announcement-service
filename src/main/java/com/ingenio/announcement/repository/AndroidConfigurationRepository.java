package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.AndroidConfigurationModel;

public interface AndroidConfigurationRepository extends JpaRepository<AndroidConfigurationModel, Integer>{

	AndroidConfigurationModel findByAndroidId(Integer androidId);
}
