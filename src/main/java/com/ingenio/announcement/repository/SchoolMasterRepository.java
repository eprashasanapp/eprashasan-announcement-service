package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.SchoolNameBean;
import com.ingenio.announcement.model.SchoolMasterModel;

public interface SchoolMasterRepository extends JpaRepository<SchoolMasterModel, Integer> {

	SchoolMasterModel findBySchoolid(Integer schoolId);
	
	List<SchoolMasterModel> findAll();

	@Query(value="select a from SchoolMasterModel a")
	List<SchoolMasterModel> findAllSchool();

	@Query(value="select new com.ingenio.announcement.bean.SchoolNameBean(a.schoolAlias,a.schoolNameDecrypted) from SchoolMasterModel a where a.schoolid=:schoolId ")
	SchoolNameBean getShortSchoolName(Integer schoolId);
	
	
}
