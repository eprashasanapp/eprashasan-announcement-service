package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.bean.StaffDetailsBean;
import com.ingenio.announcement.bean.StudentDetailsBean;
import com.ingenio.announcement.model.EventAssignToModel;
import com.ingenio.announcement.model.EventModel;


@Repository
public interface StudentEventAssignedToRepository extends JpaRepository<EventAssignToModel, Integer>{


	EventAssignToModel findByEventAssignToId(Integer eventAssignToId);

	@Transactional
	void deleteByEventModel(EventModel model);
	
	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( c.renewStudentId,d.studentId,"
			+ "CONCAT(COALESCE(d.studInitialName,''),' ',COALESCE(d.studFName,''),' ',COALESCE(d.studMName,''),' ',COALESCE(d.studLName,'')), "
			+ "c.rollNo,b.eventStatus,d.studentRegNo,d.schoolMasterModel.schoolid,d.grBookName.grBookId ) from EventModel a "
			+ "left join  EventAssignToModel b on  ( a.eventId = b.eventModel.eventId and b.role='Parent' and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid   ) "
			+ "left join StudentStandardRenewModel c on c.renewStudentId=b.staffStudentId  and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
			+ "left join StudentMasterModel d on d.studentId=c.studentMasterModel.studentId and a.schoolMasterModel.schoolid=d.schoolMasterModel.schoolid   "
			+ "where b.eventModel.eventId=?1 and b.eventStatus=?2 and b.schoolMasterModel.schoolid=?3 and  d.isApproval='0' and d.isAdmissionCancelled='1' ")
	List<StudentDetailsBean> getStudentDetailsForEventStatus(Integer eventId, String eventStatus, Integer schoolId);
	
	@Query(value="select new com.ingenio.announcement.bean.StaffDetailsBean( c.staffId,"
			+ "CONCAT(COALESCE(c.initialname,''),' ',COALESCE(c.firstname,''),' ',COALESCE(c.secondname,''),' ',COALESCE(c.lastname,'')), "
			+ "c.sregNo ) from EventModel a "
			+ "left join  EventAssignToModel b on  ( a.eventId = b.eventModel.eventId and b.role='Teacher' and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid  ) "
			+ "left join StaffBasicDetailsModel c on c.staffId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid   "			
			+ "where b.eventModel.eventId=?1 and b.eventStatus=?2 and b.schoolMasterModel.schoolid=?3  ")
	List<StaffDetailsBean> getStaffDetailsForEventStatus(Integer eventId, String eventStatus, Integer schoolId);



}
