package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.DivisionMasterModel;
import com.ingenio.announcement.model.StudentMasterModel;

public interface StudentMasterRepository extends JpaRepository<StudentMasterModel, Integer>{

	@Query(value="select a from StudentMasterModel a "
			+ "left join StudentStandardRenewModel b on a.studentId=b.studentMasterModel.studentId "
			+ "where b.renewStudentId=:renewStudentId and a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' and b.isDel='0' ")
	StudentMasterModel getStudent(Integer renewStudentId, Integer schoolid);

	@Query(value="select a from DivisionMasterModel a where a.divisionId IN (:divisionNames) and a.schoolMasterModel.schoolid=:schoolId ")
	List<DivisionMasterModel> getStandardList(List<Integer> divisionNames, Integer schoolId);

	@Query("select a.memail from StudentMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.religion.religionId=:religionId and  a.memail is not null ")
	List<String> getAllStudents(Integer schoolId, Integer religionId);

	

}
