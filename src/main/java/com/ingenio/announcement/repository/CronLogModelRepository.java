package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.CronLogModel;

public interface CronLogModelRepository extends JpaRepository<CronLogModel, Integer> {

}
