package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.EventAttachmentModel;
import com.ingenio.announcement.model.EventModel;

public interface EventAttachmentRepository extends JpaRepository<EventAttachmentModel, Integer> {

	@Transactional
	void deleteByEventModel(EventModel model);
	
	
	

}
