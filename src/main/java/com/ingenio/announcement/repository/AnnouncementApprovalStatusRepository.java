package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.model.AnnouncementApprovalStatusModel;

public interface AnnouncementApprovalStatusRepository extends JpaRepository<AnnouncementApprovalStatusModel, Integer>{

	
	@Query(value="SELECT a from AnnouncementApprovalStatusModel a "
			+ "where a.schoolMasterModel.schoolid= :schoolId and a.announcementModel.announcementId =:postId ")
	List<AnnouncementApprovalStatusModel> approvalList(Integer schoolId,Integer postId);

	@Query("select a.status from  AnnouncementApprovalStatusModel a where a.appUserModel.appUserRoleId=:appUserId and a.schoolMasterModel.schoolid=:schoolid and a.status='approved' and a.announcementModel.announcementId=:postId ")
	List<String> getApprovalStatus(Integer appUserId, Integer schoolid, Integer postId);

	@Query("select a.announcementapprovalStatusId from AnnouncementApprovalStatusModel a where a.announcementModel.announcementId =:announcementId and a.schoolMasterModel.schoolid=:schoolid ")
	List<Integer> getApprovedStatus(Integer announcementId,Integer schoolid);

}
