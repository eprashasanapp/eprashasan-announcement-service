package com.ingenio.announcement.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.FeeReceiptModel;


public interface ReceiptRepository extends JpaRepository<FeeReceiptModel, Integer>{

	@Transactional
	@Modifying
	@Query("update FeeReceiptModel a set a.payTypeOrPrintDate=:newDate where a.receiptId=:receiptId ")
	void updateDate(Integer receiptId, Date newDate);

	
//	@Query("select new com.ingenio.fee.bean.DeletedFeeReceiptDeailBean(r.receiptId,s.schoolid,r.receiptNo,r.isconsolidation,r.headId,r.payTypeOrPrintFlag,r.payTypeOrPrintDate,	r.bankPaidDate, r.returnByUserDate	"
//			+ ",r.reconciliationFlag,r.reconciliationDate,r.bounceClearFlag,r.bounceClearDate,r.discardFlag,r.discardDate,r.bankId,	y.yearId,r.remark,u.appUserRoleId,r.isApproval,r.studentId,(select CONCAT(COALESCE(sm.studInitialName,''),' ',COALESCE(sm.studFName,''),' ',COALESCE(sm.studMName,''),' ',COALESCE(sm.studLName,'')) from StudentMasterModel sm "
//			+ "where sm.studentId=r.studentId),r.typeOfReceipt	"
//			+ ",r.totalAmount) from  FeeReceiptModel r "
//			+ "left join SchoolMasterModel s on s.schoolid=r.schoolMasterModel.schoolid "
//			+ "left join YearMasterModel y on y.yearId=r.yearMaster.yearId "
//			+ "left join AppUserRoleModel u on u.appUserRoleId=r.userId.appUserRoleId "
//			+ "where r.schoolMasterModel.schoolid=:schoolId and r.isDel=1  order by r.receiptId desc")
//	List<DeletedFeeReceiptDeailBean> getDeletedFeeReceiptDetails(Integer schoolId);
//	
	

}
