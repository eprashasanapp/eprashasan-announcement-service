package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.announcement.model.AnnouncementAttachmentModel;

public interface AttachmentRepository extends JpaRepository<AnnouncementAttachmentModel, Integer> {
	
	

}
