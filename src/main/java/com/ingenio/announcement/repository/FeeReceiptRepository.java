package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.announcement.model.FeeReceiptModel;

@Repository
public interface FeeReceiptRepository extends JpaRepository<FeeReceiptModel, Integer>{

	@Query(value="SELECT COALESCE(MAX(a.receiptId),0)+1 from FeeReceiptModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	

}
