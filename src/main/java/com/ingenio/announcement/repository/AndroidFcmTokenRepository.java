package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.Tokenbean;
import com.ingenio.announcement.model.AndroidFCMTokenMaster;

public interface AndroidFcmTokenRepository extends JpaRepository<AndroidFCMTokenMaster, Integer>{

	@Query("select new com.ingenio.announcement.bean.Tokenbean(a.token,a.mobileNo,a.notificationUserId.appUserRoleId) from AndroidFCMTokenMaster a where a.notificationUserId.appUserRoleId in :appUserId ")
	List<Tokenbean> getFcmToken(List<Integer> appUserId);

	@Query("select a.token from AndroidFCMTokenMaster a where a.mobileNo= :contactNumber order by a.fcmMasterTokenId desc  ")
	Page<String> getToken(String contactNumber, Pageable  pageable);

}
