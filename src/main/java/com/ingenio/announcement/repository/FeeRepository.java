package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.FeeSettingMultiModel;


@Repository
public interface FeeRepository extends JpaRepository<FeeSettingMultiModel, Integer> {
	
	@Query(value="SELECT coalesce(MAX(a.paidId),0)+1 from FeePaidFeeModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxPaidId(Integer schoolId);

	@Query(value="SELECT coalesce(MAX(a.receiptId),0)+1 from FeeReceiptModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxReceiptId(Integer schoolId);


	

	
	
}
