package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.FCMTokenBean;
import com.ingenio.announcement.model.AssignSubjectToStudentModel;

public interface AssignSubjectToStudentRepository extends JpaRepository<AssignSubjectToStudentModel, Integer>{
	
	List<AssignSubjectToStudentModel> findBySubjectId(String subjectId);

	@Query("Select new com.ingenio.announcement.bean.FCMTokenBean(COALESCE(d.token,''), COALESCE(d.deviceType,'2'),COALESCE(a.mmobile,0),COALESCE(c.appUserRoleId,0)) from StudentMasterModel a left join StudentStandardRenewModel b on a.studentId=b.studentMasterModel.studentId "
			+ "left join AppUserModel c on c.staffId=a.studentId "
			+ "left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
			+ "where b.renewStudentId=:renewStudentId and (c.roleName='ROLE_PARENT1') and  a.isApproval='0' and a.isAdmissionCancelled='1' ")
	List<FCMTokenBean> getToken(Integer renewStudentId);
	
	@Query("Select COALESCE(d.token,'') from StudentMasterModel a left join StudentStandardRenewModel b on a.studentId=b.studentMasterModel.studentId "
			+ "left join AppUserModel c on c.staffId=a.studentId "
			+ "left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
			+ "where b.renewStudentId=:renewStudentId and (c.roleName='ROLE_PARENT1') and  a.isApproval='0' and a.isAdmissionCancelled='1' "
			+ "and d.deviceType=:deviceType and d.token!='' and d.token is not null  ")
	String getToken(Integer renewStudentId,String deviceType);
	
	

	@Query("Select new com.ingenio.announcement.bean.FCMTokenBean(COALESCE(d.token,''), COALESCE(d.deviceType,'2'),COALESCE(a.contactnosms,''),COALESCE(c.appUserRoleId,0)) from StaffBasicDetailsModel a  "
			+ "left join AppUserModel c on c.staffId=a.staffId "
			+ "left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
			+ "where a.staffId=:staffId and (c.roleName='ROLE_TEACHINGSTAFF')  ")
	List<FCMTokenBean> getTokenForTeacher(Integer staffId);
	
	@Query("Select COALESCE(d.token,'') from StaffBasicDetailsModel a  "
			+ "left join AppUserModel c on c.staffId=a.staffId "
			+ "left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
			+ "where a.staffId=:staffId and (c.roleName='ROLE_TEACHINGSTAFF') "
			+ "and d.deviceType=:deviceType and d.token!='' and d.token is not null  ")
	String getTokenForTeacher(Integer staffId,String deviceType);

	@Query("Select new com.ingenio.announcement.bean.FCMTokenBean(COALESCE(d.token,''), COALESCE(d.deviceType,'2'),COALESCE(c.appUserRoleId,0)) "
			+ "from StudentMasterModel a "
			+ "left join StudentStandardRenewModel b on a.studentId=b.studentMasterModel.studentId "
			+ "left join AppUserModel c on c.staffId=a.studentId "
			+ "left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
			+ "where b.renewStudentId=:renewStudentId and (c.roleName='ROLE_PARENT1') and  a.isApproval='0' and a.isAdmissionCancelled='1' and d.deviceType=:deviceType and d.token!='' and d.token is not null ")
	List<FCMTokenBean> getTokenList(Integer renewStudentId, String deviceType);

	@Query("Select new com.ingenio.announcement.bean.FCMTokenBean(COALESCE(d.token,''), COALESCE(d.deviceType,'2'),COALESCE(c.appUserRoleId,0)) from StaffBasicDetailsModel a  "
			+ "left join AppUserModel c on c.staffId=a.staffId "
			+ "left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
			+ "where a.staffId=:staffId and (c.roleName='ROLE_TEACHINGSTAFF')  "
			+ "and d.deviceType=:deviceType and d.token!='' and d.token is not null ")
	List<FCMTokenBean> getTokenListForTeacher(Integer staffId, String deviceType);


}
