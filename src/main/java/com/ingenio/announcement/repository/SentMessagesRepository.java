package com.ingenio.announcement.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.StandardDivisionReportBean;
import com.ingenio.announcement.model.AndroidFCMTokenMaster;
import com.ingenio.announcement.model.AnnouncementCommentModel;
import com.ingenio.announcement.model.AnnouncementLikeorDislikeModel;
import com.ingenio.announcement.model.AnnouncementModel;
import com.ingenio.announcement.model.AnnouncementReactionModel;
import com.ingenio.announcement.model.SaveSendNotificationModel;
import com.ingenio.announcement.model.SentEmailModel;
import com.ingenio.announcement.model.SentEmailReadStatusModel;
import com.ingenio.announcement.model.SentMessagesModel;
import com.ingenio.announcement.model.SentMessagesReadStatusModel;
import com.ingenio.announcement.model.StaffBasicDetailsModel;
import com.ingenio.announcement.model.StudentMasterModel;

public interface SentMessagesRepository extends JpaRepository<SentMessagesModel, Integer> {

	Optional<SentMessagesModel> findByRcode(String rcode);

	
	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle,COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'')"
			+ ",COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'')) from SentMessagesModel a "
			+ "left join StaffBasicDetailsModel stf on stf.staffId=a.staffStudentId and a.role='Staff'  "
			+ "left join StudentMasterModel stud on stud.studentId=a.staffStudentId and a.role='Student'  "
			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.staffStudentId=:staffStudentId and a.role=:role order by id desc  ")
	List<SentMessagesModel> getMessages(Integer staffStudentId, String role);
	
	

	
	@Query("select a from SentMessagesModel a where a.staffStudentId=:staffStudentId and a.announcementId=:announcmentId and a.role=:role order by id desc  ")
	SentMessagesModel getMessagesForRepot(Integer staffStudentId, String role,Integer announcmentId);

	
	@Query("select a from SentMessagesModel a where a.staffStudentId in :staffStudentId and a.announcementId=:announcmentId and a.role=:role order by id desc  ")
	List<SentMessagesModel> getMessagesForRepot(List<Integer> staffStudentId, String role,Integer announcmentId);

//	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle) from SentMessagesModel a "
//			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.schoolId=:schoolid order by id desc  ")
//	List<SentMessagesModel> getAllMessagesReport(Integer schoolid, Pageable offset);

	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle,COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'')"
			+ ",COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'')) from SentMessagesModel a "
			+ "left join StaffBasicDetailsModel stf on stf.staffId=a.staffStudentId and a.role='Staff'  "
			+ "left join StudentMasterModel stud on stud.studentId=a.staffStudentId and a.role='Student'  "
			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.schoolId=:schoolid and a.otherSchoolId=:otherSchoolId order by id desc  ")
	List<SentMessagesModel> getAllMessagesReport(Integer schoolid,Integer otherSchoolId, Pageable offset);

	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle,COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'')"
			+ ",COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'')) from SentMessagesModel a "
			+ "left join StaffBasicDetailsModel stf on stf.staffId=a.staffStudentId and a.role='Staff'  "
			+ "left join StudentMasterModel stud on stud.studentId=a.staffStudentId and a.role='Student'  "
			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.schoolId=:schoolid order by id desc  ")
	List<SentMessagesModel> getAllMessagesReport(Integer schoolid, Pageable offset);

//	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle) from SentMessagesModel a "
//			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.schoolId=:schoolid  and (a.cDate >=:fromDate1 and a.cDate<=:toDate1 ) order by id desc  ")
//	List<SentMessagesModel> getAllMessagesReportDateWise(Integer schoolid, Date fromDate1, Date toDate1,
//			Pageable offset);

	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle,COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'') "
			+ " ,COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'') ) from SentMessagesModel a "
			+ "left join StaffBasicDetailsModel stf on stf.staffId=a.staffStudentId and a.role='Staff'  "
			+ "left join StudentMasterModel stud on stud.studentId=a.staffStudentId and a.role='Student'  "
			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.schoolId=:schoolid  and (a.cDate >=:fromDate1 and a.cDate<=:toDate1 ) and a.otherSchoolId=:otherSchoolId order by id desc  ")
	List<SentMessagesModel> getAllMessagesReportDateWise(Integer schoolid, Date fromDate1, Date toDate1,
			Integer otherSchoolId,Pageable offset);

	@Query("select new com.ingenio.announcement.model.SentMessagesModel(a.id,a.templateTypeId,a.message,a.role,a.staffStudentId,a.announcementId,a.schoolId,a.appUserId,a.mobileNumber,a.rcode,a.cDate,b.announcement,b.announcementTitle,	COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'') "
			+ " ,COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'') ) from SentMessagesModel a "
			+ "left join StaffBasicDetailsModel stf on stf.staffId=a.staffStudentId and a.role='Staff'  "
			+ "left join StudentMasterModel stud on stud.studentId=a.staffStudentId and a.role='Student'  "
			+ "left join AnnouncementModel b on b.announcementId=a.announcementId where a.schoolId=:schoolid  and (a.cDate >=:fromDate1 and a.cDate<=:toDate1 )  order by id desc  ")
	List<SentMessagesModel> getAllMessagesReportDateWise(Integer schoolid, Date fromDate1, Date toDate1,
			Pageable offset);

	
	@Query("select count(a.id) from SentMessagesModel a where a.mobileNumber is not null and a.mobileNumber!='0' and a.schoolId=:schoolId  ")
	Integer getSentMessagesToSchool(Integer schoolId);

	//COALESCE(CONCAT(COALESCE(stf.firstname,''),' ',COALESCE(stf.lastname,'')),'')
	//COALESCE(CONCAT(COALESCE(stud.studFName,''),' ',COALESCE(stud.studLName,'')),'')
	
	
//	@Query("select count(a) from SentMessagesModel a where a.announcementId=:announcementId and a.schoolId=:schoolId and a.mobileNumber is not null and a.mobileNumber!=0  ")
//	Integer getTotalMessageSent(Integer announcementId, Integer schoolId);

//	@Query("select new com.ingenio.announcement.bean.StandardDivisionReportBean(count(a) as totalSmsSent, "
//			+ "(select count(distinct b.sentMessageId) from SentMessagesReadStatusModel b where b.sentMessageId IN (select distinct  c.id  from SentMessagesModel c where c.announcementId=:announcementId and c.staffStudentId in :studentId and c.role='Student'  ) ) as totalSmsRead,	 "
//			+ "(select count(d.fcmMasterTokenId) from AndroidFCMTokenMaster d where d.notificationUserId.appUserRoleId in :appUserId ) as mobileAppAvailable,"
//			+ "(select count(e.tabId) from SaveSendNotificationModel e where e.notificationId=:announcementId and e.seenUnseenStatusFlag=1 and e.renewStaffId in :appUserId  ) as notificationRead,"
//			+ "(select count(f.viewsId) from AnnouncementViewsModel f where f.announcementModel.announcementId=:announcementId and f.appUserModel.appUserRoleId in :appUserId ) as noticeView,"
//			+ "(select count(g.likeId) from AnnouncementLikeorDislikeModel g where g.announcementModel.announcementId=:announcementId and g.likeDislike=1 and g.appUserModel.appUserRoleId in :appUserId ) as like,"
//			+ "(select count(g.likeId) from AnnouncementLikeorDislikeModel g where g.announcementModel.announcementId=:announcementId and g.likeDislike=0 and g.appUserModel.appUserRoleId in :appUserId ) as dislike, "
//			+ "(select count(h.commentId)  from AnnouncementCommentModel h where h.announcementModel.announcementId=:announcementId and h.appUserModel.appUserRoleId in :appUserId ) as comments, "
//			+ "(select count(i.reactionId) from AnnouncementReactionModel i where i.announcementModel.announcementId =:announcementId and i.appUserModel.appUserRoleId in :appUserId ) as feedback "
//			+ ") from SentMessagesModel a where a.announcementId=:announcementId and a.staffStudentId in :studentId and a.schoolId=:schoolId and a.mobileNumber is not null and a.mobileNumber!=0  ")
//	StandardDivisionReportBean getTotalMessageSent(Integer announcementId, Integer schoolId, List<Integer> appUserId, List<Integer> studentId);
//	
	
	@Query("select new com.ingenio.announcement.bean.StandardDivisionReportBean(count(a) as totalSmsSent, "
			+ "(select count(distinct b.sentMessageId) from SentMessagesReadStatusModel b where b.sentMessageId IN (select distinct  c.id  from SentMessagesModel c where c.announcementId=:announcementId and c.staffStudentId in :studentId and c.role='Student'  ) ) as totalSmsRead,	 "
			+ "(select count(d.fcmMasterTokenId) from AndroidFCMTokenMaster d where d.notificationUserId.appUserRoleId in :appUserId ) as mobileAppAvailable,"
			+ "(select count(e.tabId) from SaveSendNotificationModel e where e.notificationId=:announcementId and e.seenUnseenStatusFlag=1 and e.renewStaffId in :appUserId  ) as notificationRead,"
			+ "(select count(f.viewsId) from AnnouncementViewsModel f where f.announcementModel.announcementId=:announcementId and f.appUserModel.appUserRoleId in :appUserId ) as noticeView,"
			+ "(select count(g.likeId) from AnnouncementLikeorDislikeModel g where g.announcementModel.announcementId=:announcementId and g.likeDislike=1 and g.appUserModel.appUserRoleId in :appUserId ) as like,"
			+ "(select count(g.likeId) from AnnouncementLikeorDislikeModel g where g.announcementModel.announcementId=:announcementId and g.likeDislike=0 and g.appUserModel.appUserRoleId in :appUserId ) as dislike, "
			+ "(select count(h.commentId)  from AnnouncementCommentModel h where h.announcementModel.announcementId=:announcementId and h.appUserModel.appUserRoleId in :appUserId ) as comments, "
			+ "(select count(i.reactionId) from AnnouncementReactionModel i where i.announcementModel.announcementId =:announcementId and i.appUserModel.appUserRoleId in :appUserId ) as feedback,"
			+ "(select count(j.id) from SentEmailModel j where j.announcementId =:announcementId and j.appUserId in :appUserId and j.emailId!='0') as sentEmail  , "
			+ "(select count(j.id) from SentEmailModel j where j.announcementId =:announcementId and j.appUserId in :appUserId and j.emailId='0' ) as noEmailFound, "
			+ "(select count(distinct k.readStatusId) from SentEmailReadStatusModel k where k.sentEmailId in (select distinct l.id from SentEmailModel l where l.announcementId =:announcementId and l.appUserId in :appUserId and l.emailId!='0')  )as EmailreadStatus   "
			+ ") from SentMessagesModel a where a.announcementId=:announcementId and a.staffStudentId in :studentId and a.schoolId=:schoolId and a.mobileNumber is not null and a.mobileNumber!=0  ")
	StandardDivisionReportBean getTotalMessageSent(Integer announcementId, Integer schoolId, List<Integer> appUserId, List<Integer> studentId);

	@Query("select a.id from SentMessagesModel a where a.rcode=:rCode ")
	List<Integer> findByRCode(String rCode);
	
	
	
//	"select count(SELECT a.sentMessageId  FROM SentMessagesReadStatusModel a WHERE a.sentMessageId=4957 GROUP BY a.sentMessageId ) from SentMessagesReadStatusModel where "
//	SentMessagesReadStatusModel
	
}
