package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.YearMasterModel;

@Repository
public interface YearMasterRepository extends JpaRepository<YearMasterModel, Integer>{

	List<YearMasterModel> findBySchoolMasterModelOrderByYear(SchoolMasterModel schoolMasterModel);

	YearMasterModel findByYearId(Integer currentYear);

}
