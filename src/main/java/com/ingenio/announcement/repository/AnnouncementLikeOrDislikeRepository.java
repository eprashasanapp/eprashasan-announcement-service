package com.ingenio.announcement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.announcement.bean.AnnouncementCommentResponseBean;
import com.ingenio.announcement.model.AnnouncementLikeorDislikeModel;


public interface AnnouncementLikeOrDislikeRepository extends JpaRepository<AnnouncementLikeorDislikeModel, Integer> {

	@Query(value="SELECT coalesce(MAX(a.likeId),0)+1 from AnnouncementLikeorDislikeModel a where a.schoolMasterModel.schoolid= :schoolid ")
	String getMaxId(Integer schoolid);


	@Query(value="SELECT a from AnnouncementLikeorDislikeModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId=:announcementId order by a.likeId desc ")
	List<AnnouncementLikeorDislikeModel> findLikes(Integer schoolId, Integer announcementId);
	
	@Query(value="SELECT count(*) from AnnouncementLikeorDislikeModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.announcementModel.announcementId=:announcementId ")
	long countLikes(Integer schoolId, Integer announcementId);

}
