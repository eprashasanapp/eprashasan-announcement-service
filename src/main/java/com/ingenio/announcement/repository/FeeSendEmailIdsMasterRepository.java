package com.ingenio.announcement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ingenio.announcement.model.FeeSendEmailIdsMasterModel;


public interface FeeSendEmailIdsMasterRepository extends JpaRepository<FeeSendEmailIdsMasterModel, Integer>{

	@Query("select new com.ingenio.announcement.model.FeeSendEmailIdsMasterModel( a.userName, a.password ) from FeeSendEmailIdsMasterModel a where a.schoolMasterModel.schoolid=:schoolId")
	FeeSendEmailIdsMasterModel getCredentials(@Param("schoolId") Integer schoolId);
	
}
