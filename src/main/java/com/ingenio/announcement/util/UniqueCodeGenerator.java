package com.ingenio.announcement.util;

import java.util.UUID;

public class UniqueCodeGenerator {

	 public  String generateUniqueCode() {
	        UUID uuid = UUID.randomUUID();
	        long mostSigBits = uuid.getMostSignificantBits();
	        long leastSigBits = uuid.getLeastSignificantBits();
	        long combinedBits = mostSigBits ^ leastSigBits;
	        String uniqueCode = Long.toHexString(combinedBits).toUpperCase();

	        // Ensure the code is 9 digits long (padded with leading zeros if needed)
	        while (uniqueCode.length() < 9) {
	            uniqueCode = "0" + uniqueCode;
	        }

	        return uniqueCode.substring(0, 9); // Return the first 9 characters
	    }
}
