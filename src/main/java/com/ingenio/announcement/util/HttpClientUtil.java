package com.ingenio.announcement.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.annotation.PostConstruct;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.announcement.config.ConfigurationProperties;


public class HttpClientUtil {

	@PostConstruct
	public static String uploadImage(MultipartFile file, String filename, String folderName, String schoolKey,
			Integer language) {
		HttpResponse response;
		StringBuffer result = new StringBuffer();
		String result1 = "";
		String line = "";		
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(ConfigurationProperties.uploadImageUrl);
			// post.setHeader("Content-type", "application/json");

			File convFile = new File(file.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
			if (language == 1) {
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				StringBody fileNameString = new StringBody(filename, ContentType.MULTIPART_FORM_DATA);
				StringBody folderNameString = new StringBody(folderName, ContentType.MULTIPART_FORM_DATA);
				StringBody schoolkeyString = new StringBody(schoolKey, ContentType.MULTIPART_FORM_DATA);
				builder.addPart("file", new FileBody(convFile));
				builder.addPart("filename", fileNameString);
				builder.addPart("folderName", folderNameString);
				builder.addPart("schoolkey", schoolkeyString);
				post.setEntity(builder.build());			
			} else {
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				builder.setCharset(Charset.forName("UTF-8")).setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				StringBody fileNameString = new StringBody(filename,ContentType.create("text/plain", Charset.forName("UTF-8")));
				StringBody folderNameString = new StringBody(folderName,ContentType.create("text/plain", Charset.forName("UTF-8")));
				StringBody schoolkeyString = new StringBody(schoolKey,ContentType.create("text/plain", Charset.forName("UTF-8")));
				builder.addPart("file", new FileBody(convFile));
				builder.addPart("filename", fileNameString);
				builder.addPart("folderName", folderNameString);
				builder.addPart("schoolkey", schoolkeyString);
				post.setEntity(builder.build());
			}
			
			response = client.execute(post);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			final InputStreamReader streamReader = new InputStreamReader(response.getEntity().getContent(),
					Charset.forName("ISO-8859-1"));
			BufferedReader rd = new BufferedReader(streamReader);

			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			String[] arrOfStr = result.toString().split("cms");
			result1 = arrOfStr[0];
			convFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result1 + "/" + folderName + "/" + filename;
	}
}