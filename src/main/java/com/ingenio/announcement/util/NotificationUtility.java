package com.ingenio.announcement.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.internet.MimeMessage;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.ingenio.announcement.repository.SaveSendNotificationRepository;


@Component
public class NotificationUtility {

	UniqueCodeGenerator uniqueCode=new UniqueCodeGenerator();

	private final SaveSendNotificationRepository saveSendNotificationRepository;

	@Autowired
	public NotificationUtility(SaveSendNotificationRepository saveSendNotificationRepository) {
		this.saveSendNotificationRepository = saveSendNotificationRepository;
	}

	
	@Autowired
	SaveSendNotificationRepository notification;

	public void sendNotification(HttpClient httpClient, String serverKey,String mobileNumber,String fcmToken, HttpURLConnection conn, String body, String title)
	{
		// HttpClient httpClient = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost("https://fcm.googleapis.com/fcm/send");
		httpPost.setHeader("Authorization", "key=" + serverKey);
		httpPost.setHeader("Content-Type", "application/json");
		Integer success=0;
		if(fcmToken!=null) {
			try {

				JSONObject json = new JSONObject();
				json.put("to",fcmToken);

				JSONObject data = new JSONObject();
				data.put("mobileNo",mobileNumber);
				data.put("title", title);
				data.put("body", body);
				data.put("userId","1059000001");
				data.put("menuId",7);
				data.put("menuName","Notice");
				data.put("SeenStataus", "0");
				json.put("data", data);
				StringEntity stringEntity = new StringEntity(json.toString());
				httpPost.setEntity(stringEntity);

				HttpResponse response = httpClient.execute(httpPost);
				success = response.getStatusLine().getStatusCode();
				String statusMessage = response.getStatusLine().getReasonPhrase();
				Thread.sleep(1000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}




	public String hexaDecimalConverter(int decimalNumber) {
		if (decimalNumber == 0) {
			return "0";
		}

		StringBuilder hexBuilder = new StringBuilder();
		while (decimalNumber > 0) {
			int remainder = decimalNumber % 16;
			char hexDigit = (remainder < 10) ? (char) ('0' + remainder) : (char) ('A' + remainder - 10);
			hexBuilder.insert(0, hexDigit);
			decimalNumber /= 16;
		}
		return hexBuilder.toString();

	}




}



