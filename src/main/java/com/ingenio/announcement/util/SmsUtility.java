package com.ingenio.announcement.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class SmsUtility {

	
	public void sendTextMessage(String message, String apikey, String senderId, String type, String mainUrl,
			String statusUrl, String templateID, String user,String mobileNumber)
					throws UnsupportedEncodingException, MalformedURLException, IOException {
		String response;
		URLConnection myURLConnection = null;
		URL myURL = null;
		BufferedReader reader = null;

		String encoded_message = URLEncoder.encode(message, "UTF-8");
		StringBuilder sbPostData = new StringBuilder(mainUrl);
		StringBuilder sbPostData1 = new StringBuilder(mainUrl);
		sbPostData1.append("user=" + user);
		sbPostData1.append("&apikey=" + apikey);
		sbPostData1.append("&message=" + encoded_message);
		sbPostData1.append("&mobile=" + mobileNumber);
		sbPostData1.append("&senderid=" + senderId);
		sbPostData1.append("&type=" + "uni");
		sbPostData1.append("&tid=" + templateID);
		String msgid = "";
		mainUrl = sbPostData1.toString();
		myURL = new URL(mainUrl);
		myURLConnection = myURL.openConnection();
		myURLConnection.connect();
		
		reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		while ((response = reader.readLine()) != null) {
			msgid = response.trim();
		}
		reader.close();

		sbPostData = new StringBuilder(statusUrl);
		sbPostData.append("user=" + user);
		sbPostData.append("&apikey=" + apikey);
		//			check response here
		sbPostData.append("&msgid=" + msgid);
		mainUrl = sbPostData.toString();
		myURL = new URL(mainUrl);
		myURLConnection = myURL.openConnection();
		myURLConnection.connect();
		reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		while ((response = reader.readLine()) != null) {
			msgid = response.trim();
		}
		reader.close();
	}
}
