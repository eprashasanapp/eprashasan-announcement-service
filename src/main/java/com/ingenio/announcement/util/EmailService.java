package com.ingenio.announcement.util;

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
 public class EmailService {

    public void sendEmail(String fromEmail,String fromPassword ,String to, String subject, String text) {
        JavaMailSender mailSender = createMailSender(fromEmail,fromPassword);

        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper  = new MimeMessageHelper(message, "UTF-8");   
        try {
        helper .setFrom(fromEmail);
        helper .setTo(to);
        helper .setSubject(subject);
        helper .setText(text,true);
        mailSender.send(message);
        }catch(Exception e) {
        	e.printStackTrace();
        }
    }

    private JavaMailSender createMailSender(String fromEmail,String fromPassword) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com"); // Replace with your SMTP server
        mailSender.setPort(587); // Replace with your SMTP port
        mailSender.setUsername(fromEmail); // Use the dynamic sender's email
        mailSender.setPassword(fromPassword); // Replace with your password

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}
