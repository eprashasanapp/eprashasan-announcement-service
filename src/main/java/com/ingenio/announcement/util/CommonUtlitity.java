package com.ingenio.announcement.util;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.ingenio.announcement.model.AppLanguageModel;
import com.ingenio.announcement.model.DynamicAcademicMonthsModel;
import com.ingenio.announcement.model.SchoolMasterModel;
import com.ingenio.announcement.model.YearMasterModel;
import com.ingenio.announcement.repository.AppLanguageRepository;
import com.ingenio.announcement.repository.DynamicAcademicMonthsRepository;
import com.ingenio.announcement.repository.YearMasterRepository;

@Component
public class CommonUtlitity {
	
	@Autowired
	DynamicAcademicMonthsRepository dynamicAcademicMonthsRepository;
	
	@Autowired
	YearMasterRepository yearMasterRepository;
	
	@Autowired
	private AppLanguageRepository appLanguageRepository;
	
	@Autowired
	AmazonS3 amazonS3;
	
	public int getCurrentYear(Integer schoolId,String date) {
		int globleYearId = 0;
		try {
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DynamicAcademicMonthsModel> dynamicAcadYearBean = dynamicAcademicMonthsRepository.findBySchoolMasterModel(schoolMasterModel);
			if(CollectionUtils.isNotEmpty(dynamicAcadYearBean)) {
				String localDynamicAcadSdate=dynamicAcadYearBean.get(0).getFromMonth();
				String globalDynamicAcadSdate=localDynamicAcadSdate.split("-")[1];
				
				LocalDate now = LocalDate.now(); 
				if(StringUtils.isNotEmpty(date)) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
					now = LocalDate.parse(date,formatter);
				}
				
				int year = now.getYear();
				int month =(now.getMonthValue()); 
				String currentAcYr="";
				if (month >= Integer.parseInt(globalDynamicAcadSdate) && month <= 12) {
					year++;
					currentAcYr = now.getYear() + "-" + year;
				} else {
					year--;
					currentAcYr = year + "-" + now.getYear();
				}
				
				List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
				List<String> yearList = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(appLanguageList)) {
					for (int i = 0; i < appLanguageList.size(); i++) {
						yearList .add(Utility.convertEnglishToOther(currentAcYr, "" + appLanguageList.get(i).getLanguageId()));
					}
				}
				List<YearMasterModel> list = yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
				for(int i=0;i<list.size();i++) {
					if (yearList.contains(list.get(i).getYear())) {
						globleYearId = list.get(i).getYearId();
					}
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return globleYearId;
	}
	
	public String getCurrentYearName(Integer schoolId,String date) {
		try {
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DynamicAcademicMonthsModel> dynamicAcadYearBean = dynamicAcademicMonthsRepository.findBySchoolMasterModel(schoolMasterModel);
			if(CollectionUtils.isNotEmpty(dynamicAcadYearBean)) {
				String localDynamicAcadSdate=dynamicAcadYearBean.get(0).getFromMonth();
				String globalDynamicAcadSdate=localDynamicAcadSdate.split("-")[1];
				
				LocalDate now = LocalDate.now(); 
				if(StringUtils.isNotEmpty(date)) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
					now = LocalDate.parse(date,formatter);
				}
				
				int year = now.getYear();
				int month =(now.getMonthValue()); 
				String currentAcYr="";
				if (month >= Integer.parseInt(globalDynamicAcadSdate) && month <= 12) {
					year++;
					currentAcYr = now.getYear() + "-" + year;
				} else {
					year--;
					currentAcYr = year + "-" + now.getYear();
				}
				
				List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
				List<String> yearList = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(appLanguageList)) {
					for (int i = 0; i < appLanguageList.size(); i++) {
						yearList .add(Utility.convertEnglishToOther(currentAcYr, "" + appLanguageList.get(i).getLanguageId()));
					}
				}
				List<YearMasterModel> list = yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
				for(int i=0;i<list.size();i++) {
					if (yearList.contains(list.get(i).getYear())) {
						return list.get(i).getYear();
					}
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String getS3PreSignedUrl(String imagePath) {
		String[] arrayStr = imagePath.split(".com/");
		try {
			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * 10;
			expiration.setTime(expTimeMillis);
//			String bucketName = "eprashasan1";
			String bucketName = "eprashasan2";
			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName,
					arrayStr[1]).withMethod(HttpMethod.GET).withExpiration(expiration);
			URL url = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
			System.out.println(url);
			return url.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}


}
