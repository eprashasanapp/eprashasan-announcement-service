package com.ingenio.announcement.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.ingenio.announcement.bean.WhatsAppMessageBean;

public class Utility {
	
	public Utility() {
	}
	
	// Convert String parameter to UTF-8
	public static String convertInUTFFormat(String str) {
		if (StringUtils.isNotEmpty(str)&& !str.equals("null")) {
			try {
				str = new String(str.getBytes("iso-8859-1"), "UTF-8");
				return str;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public static String generatePrimaryKey(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
		
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				newGenetatedKey=globalDbSchoolKey+("0000000000".substring(0, ("0000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	public static String generatePrimaryKey1(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				return globalDbSchoolKey+("000000000000000".substring(0, ("000000000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	@SuppressWarnings("unchecked")
	public static String pushNotification(List<String> deviceTokenList,String title, String message1,String deviceType) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", "key=AAAAq0G-wj4:APA91bEufbsNcxfLABzlTAl_NjOsXcwV9l8-ipSfa6i4baVUzp17jRRSUmFIogTI5CmNDqmx5wGXDE4DdnfBVWQphts14AmS9cgA2nFAZXIQhGPGlpCTktwbyb-SArWpy-BWvIQUi60d");
		JSONObject message = new JSONObject();
		
		message.put("registration_ids",deviceTokenList);
		message.put("priority", "high");
		
		JSONObject data = new JSONObject();
		data.put("title", title);
		data.put("body", message1);
		
		if(deviceType.equals("2")) {
			message.put("notification",data);
		}
		else {
			message.put("notification",data);
		}
		
		post.setEntity(new StringEntity(message.toString(), "UTF-8"));
		try {
			client.execute(post);
			return "200";
		} catch (Exception e) { 
			e.printStackTrace();
			return "400";
		}
		
	}
	
	public static String convertNumberToWord(String number) {
		String number1="";
		if(number!=null && number.length()>=5) {
			Double numberDouble =Double.parseDouble(number);
			number = "" + numberDouble.intValue(); 
			if(number.length()==5) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("K");
			}
			if(number.length()==6) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("L");
			}
			if(number.length()==7) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("L");
			}
				if(number.length()==8) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("Cr");
			}
			if(number.length()==9) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("Cr");
			}
			
			if(number1.equals("")) {
				return number;
			}
			return number1;
		}else {
			return number;
		}
	}
	
	public static String convertEnglishToOther(String num,String language) {
		String newStr = new String();
		if (!Strings.isNullOrEmpty(num)) {
			if (language.equals("1")) {
				return num;
			}
			if (!language.equals("1")) {
				if (language.equals("2")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							case '-':
								newStr += "-";
								break;
								
							case '.':
								newStr += ".";
								break;
							// English to Marathi
							case '1':
								newStr += "१";
								break;
							
							case '2':
								newStr += "२";
								break;
							
							case '3':
								newStr += "३";
								break;
							
							case '4':
								newStr += "४";
								break;
							
							case '5':
								newStr += "५";
								break;
							
							case '6':
								newStr += "६";
								break;
							
							case '7':
								newStr += "७";
								break;
							
							case '8':
								newStr += "८";
								break;
							
							case '9':
								newStr += "९";
								break;
							
							case '0':
								newStr += "०";
								break;
							
							case '१':
								newStr += "१";
								break;
							
							case '२':
								newStr += "२";
								break;
							
							case '३':
								newStr += "३";
								break;
							
							case '४':
								newStr += "४";
								break;
							
							case '५':
								newStr += "५";
								break;
							
							case '६':
								newStr += "६";
								break;
							
							case '७':
								newStr += "७";
								break;
							
							case '८':
								newStr += "८";
								break;
							case '९':
								newStr += "९";
								break;
							
							case '०':
								newStr += "०";
								break;
						}
					}
				}
				
				if (language.equals("3")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Hindi
							case '1':
								newStr += "१";
								break;
							case '2':
								newStr += "२";
								break;
							case '3':
								newStr += "३";
								break;
							case '4':
								newStr += "४";
								break;
							case '5':
								newStr += "५";
								break;
							case '6':
								newStr += "६";
								break;
							case '7':
								newStr += "७";
								break;
							case '8':
								newStr += "८";
								break;
							case '9':
								newStr += "९";
								break;
							case '0':
								newStr += "०";
								break;
							case '१':
								newStr += "१";
								break;
							case '२':
								newStr += "२";
								break;
							case '३':
								newStr += "३";
								break;
							case '४':
								newStr += "४";
								break;
							case '५':
								newStr += "५";
								break;
							case '६':
								newStr += "६";
								break;
							case '७':
								newStr += "७";
								break;
							case '८':
								newStr += "८";
								break;
							case '९':
								newStr += "९";
								break;
							case '०':
								newStr += "०";
								break;
						}
					}
				}
				
				if (language.equals("4")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Gujrati
							case '1':
								newStr += "૧";
								break;
							
							case '2':
								newStr += "૨";
								break;
							
							case '3':
								newStr += "૩";
								break;
							
							case '4':
								newStr += "૪";
								break;
							
							case '5':
								newStr += "૫";
								break;
							
							case '6':
								newStr += "૬";
								break;
							
							case '7':
								newStr += "૭";
								break;
							
							case '8':
								newStr += "૮";
								break;
							
							case '9':
								newStr += "૯";
								break;
							
							case '0':
								newStr += "૦";
								break;
							
							case '૧':
								newStr += "૧";
								break;
							
							case '૨':
								newStr += "૨";
								break;
							
							case '૩':
								newStr += "૩";
								break;
							
							case '૪':
								newStr += "૪";
								break;
							
							case '૫':
								newStr += "૫";
								break;
							
							case '૬':
								newStr += "૬";
								break;
							
							case '૭':
								newStr += "૭";
								break;
							
							case '૮':
								newStr += "૮";
								break;
							case '૯':
								newStr += "૯";
								break;
							
							case '૦':
								newStr += "૦";
								break;
						}
					}
				}
			}
		}else {
			return num;
		}
		return newStr;
	}
	
	public static String getLanguage(Integer langId) {
		String language="en";
		switch (langId) {
		case 1:
			language="en";
			break;
		
		case 2:
			language="mr";
			break;
		
		case 3:
			language="gu";
			break;
		
		case 4:
			language="hi";
			break;
	}
		return language;
	}
	
	
	 public static String convertOtherToEnglish(String num) {
			String newStr = new String();
			if (StringUtils.isNotEmpty(num) && !num.equals("null")) {
				for (int i = 0; i < num.length(); i++ ) {
					char str = num.charAt(i);				
					switch (str) {
						case '-':
							newStr += "-";
							break;
							
						case '.':
							newStr += ".";
							break;
						
						// Start English
						case '1':
							newStr += "1";
							break;
						
						case '2':
							newStr += "2";
							break;
						
						case '3':
							newStr += "3";
							break;
						
						case '4':
							newStr += "4";
							break;
						
						case '5':
							newStr += "5";
							break;
						
						case '6':
							newStr += "6";
							break;
						
						case '7':
							newStr += "7";
							break;
						
						case '8':
							newStr += "8";
							break;
						
						case '9':
							newStr += "9";
							break;
						
						case '0':
							newStr += "0";
							break;
						
						// Start Marathi to English
						case '१':
							newStr += "1";
							break;
						
						case '२':
							newStr += "2";
							break;
						
						case '३':
							newStr += "3";
							break;
						
						case '४':
							newStr += "4";
							break;
						
						case '५':
							newStr += "5";
							break;
						
						case '६':
							newStr += "6";
							break;
						
						case '७':
							newStr += "7";
							break;
						
						case '८':
							newStr += "8";
							break;
						
						case '९':
							newStr += "9";
							break;
						
						case '०':
							newStr += "0";
							break;
						
						// Start Gujrati to English
						case '૧':
							newStr += "1";
							break;
						
						case '૨':
							newStr += "2";
							break;
						
						case '૩':
							newStr += "3";
							break;
						
						case '૪':
							newStr += "4";
							break;
						
						case '૫':
							newStr += "5";
							break;
						
						case '૬':
							newStr += "6";
							break;
						
						case '૭':
							newStr += "7";
							break;
						
						case '૮':
							newStr += "8";
							break;
						
						case '૯':
							newStr += "9";
							break;
						
						case '૦':
							newStr += "0";
							break;
					}
				}
			}else {
				return "";
			}
			return newStr;
		}
	 
	 public static String ordinal(int i) {
		    String[] suffixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
		    switch (i % 100) {
		    case 11:
		    case 12:
		    case 13:
		        return i + "th";
		    default:
		        return i + suffixes[i % 10];

		    }
		}
	 
		public static void sendNotificationNew(WhatsAppMessageBean tokenList) {
			try {
			ObjectMapper Obj = new ObjectMapper();
			String serverUrl = "13.233.210.164:8073";
//			String serverUrl="localhost:8073";
			String generateMastersUrl = "http://" + serverUrl + "/sendWhatsAppMessageNotification?isSendNotification=1";
			URL url = new URL(generateMastersUrl);
//			http://localhost:8083/sendWhatsAppMessageNotification
			HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
			postConnection.setRequestMethod("POST");
			postConnection.setRequestProperty("Content-Type", "application/json");
			postConnection.setDoOutput(true);
			postConnection.connect();
			OutputStream os = postConnection.getOutputStream();
			String jsonStr = StringUtils.EMPTY;
			try {
				jsonStr = Obj.writeValueAsString(tokenList);
//				System.out.println(jsonStr);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			os.write(jsonStr.getBytes());
			os.flush();
			os.close();
			int responseCode = postConnection.getResponseCode();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}

}
